package br.unicesumar.test;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import br.unicesumar.escoladeti.commons.CustomReportBuilder;
import br.unicesumar.escoladeti.commons.Session;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Telefone;
import br.unicesumar.escoladeti.sql.dinamico.SelectQuery;
import br.unicesumar.escoladeti.sql.dinamico.Tabela;
import org.junit.Assert;

public class TestSQLDinamico {

//	private static String WILDCARD = "%";
//	private static String SQLFinal;
//
//	public static String build(/*
//								 * List<String> parameterValues
//								 */) {
//		SelectQuery sql = new SelectQuery();
//
//		Tabela table1 = new Tabela("Aprendiz", "a1");
//		table1.addColunasAoSelect("nome");
//
//		Tabela table2 = new Tabela("Telefone", "t1");
//		table2.addColunasAoSelect("numero");
//
//		sql.addtabela(table1);
//		sql.addtabela(table2);
//
//		sql.addJoin(table1, "telefones", table2.getAlias());
//		sql.addGroupByColuna(table1, "nome");
//		SQLFinal = sql.toString();
//		return SQLFinal;
//	}
//
//	@Test
//	public void testeSql() {
//		// List<String> parameters = new ArrayList<String>();
//                
//		String sql = build();
//                Session.begin();
//
//		System.out.println("sql retornado: " + sql);
//		try {
//                    List test = Session.list(sql,100);
//                    System.out.println(test.get(0));
//                } catch (Exception e) {
//                    Session.rollback();
//                    e.printStackTrace();
//                  }
//                
//	}
//
///*	@Test
//	public void testeSessionJoinAprendizComTelefone() {
//          
//		Session.begin();
//		String sql = build();
//
//		try {
//			List joinList = Session.list(sql);
//			Iterator ite = joinList.iterator();
//			while (ite.hasNext()) {
//				Object[] objects = (Object[]) ite.next();
//				Aprendiz aprendiz = (Aprendiz) objects[0];
//				Telefone telefone = (Telefone) objects[1];
//				System.out.println("Nome aprendiz " + aprendiz.getNome());
//				System.out
//						.println("Numero do telefone " + telefone.getNumero());
//			}
//			Session.commit();
//		} catch (Exception e) {
//			Session.rollback();
//			e.printStackTrace();
//		}
//	}
//
//	*/
//	@Test
//	public void testeController() {
//		System.out
////		 .println(gerar("{\"nome\":\"Aprendiz\",\"alias\":\"a\",\"filtros\":[{\"nome\":\"Data de Nascimento\",\"checked\":true, \"value\":\"dataNascimento\"},{\"nome\":\"Nome\",\"checked\":true, \"value\":\"nome\"},{\"nome\":\"Telefone\",\"join\":true}],\"$promise\":{},\"$resolved\":true}"));
////				.println(gerar("{\"nome\":\"Aprendiz\",\"alias\":\"A\",\"filtros\":[{\"nome\":\"Nome\",\"value\":\"nome\",\"checked\":true},{\"nome\":\"Telefone\",\"join\":true,\"alias\":\"T\",\"value\":\"telefones\",\"coluna\":\"numero\",\"checked\":true}],\"titulo\":\"Aprendiz 6/11/2014 19:20\"}"));
////		.println(gerar("{\"nome\":\"Aprendiz\",\"alias\":\"A\",\"filtros\":[{\"nome\":\"Nome\",\"value\":\"nome\",\"checked\":true},{\"nome\":\"Endereco\",\"join\":true,\"alias\":\"E\",\"value\":\"enderecos\",\"coluna\":\"logradouro\",\"checked\":true}],\"titulo\":\"Aprendiz 6/11/2014 19:20\"}"));
////		.println(gerar("{\"titulo\":\"Teste\",\"nome\":\"Aprendiz\",\"alias\":\"a\",\"filtros\":[{\"nome\":\"Nome\",\"value\":\"nome\",\"checked\":true},{\"nome\":\"Data de nascimento\",\"value\":\"dataNascimento\",\"checked\":true}]}"));
//		.println(gerar("{\"nome\":\"Aprendiz\",\"alias\":\"A\",\"filtros\":[{\"nome\":\"Nome\",\"value\":\"nome\",\"checked\":true},{\"nome\":\"Data de nascimento\",\"value\":\"dataNascimento\"},{\"nome\":\"Nome do pai\",\"value\":\"nomePai\"},{\"nome\":\"Nome da mãe\",\"value\":\"nomeMae\"},{\"nome\":\"CPF\",\"value\":\"cpf\"},{\"nome\":\"RG\",\"value\":\"rg\"},{\"nome\":\"Estado Civil\",\"value\":\"estadoCivil\"},{\"nome\":\"Número NIS\",\"value\":\"nis\"},{\"nome\":\"Sexo\",\"value\":\"sexo\"},{\"nome\":\"Número PIS\",\"value\":\"pis\"},{\"nome\":\"Nacionalidade\",\"value\":\"nacionalidade\"},{\"nome\":\"Se alistou no serviço militar\",\"value\":\"alistouServicoMilitar\"},{\"nome\":\"Nome da escola\",\"value\":\"nomeEscola\"},{\"nome\":\"Nível de escolaridade\",\"value\":\"nivelEscolaridade\"},{\"nome\":\"Turno escolar\",\"value\":\"turnoEscolar\"},{\"nome\":\"Série escolar\",\"value\":\"serieEscolar\"},{\"nome\":\"Sabe usar programa básico de informática\",\"value\":\"conhecimentoBasicoInformatica\"},{\"nome\":\"Tem interesse em curso gratuito de informática\",\"value\":\"interesseCursoInformatica\"},{\"nome\":\"Trabalhou anteriormente\",\"value\":\"trabalhouAnteriormente\"},{\"nome\":\"Quantidade residentes em casa\",\"value\":\"quantidadeResidentesCasa\"},{\"nome\":\"Quantidade de pessoas que trabalham em casa\",\"value\":\"quantidadeTrabalhadoresCasa\"},{\"nome\":\"Familiar recebendo auxílio governo\",\"value\":\"familiarAuxilioGoverno\"},{\"nome\":\"Renda própria\",\"value\":\"rendaPropria\"},{\"nome\":\"Renda total\",\"value\":\"rendaTotal\"},{\"nome\":\"Possui filhos\",\"value\":\"possuiFilhos\"},{\"nome\":\"Participa do programa digitando o futuro\",\"value\":\"participaDigitandoFuturo\",\"checked\":false},{\"nome\":\"Telefone\",\"join\":true,\"alias\":\"T\",\"coluna\":\"numero\",\"value\":\"telefones\",\"checked\":false},{\"nome\":\"Endereço\",\"join\":true,\"alias\":\"E\",\"value\":\"enderecos\",\"coluna\":\"logradouro\",\"checked\":true},{\"nome\":\"E-mail\",\"value\":\"emails\",\"join\":true,\"coluna\":\"email\",\"alias\":\"M\",\"checked\":true}],\"titulo\":\"Aprendiz 7/11/2014 19:34\"}"));
//	.println(gerar("{\"nome\":\"Escola\",\"alias\":\"E\",\"filtros\":[{\"nome\":\"Razão Social\",\"value\":\"razaoSocial\",\"checked\":true},{\"nome\":\"Nome Fantasia\",\"value\":\"nome\"},{\"nome\":\"CNPJ\",\"value\":\"cnpj\"},{\"nome\":\"Telefone\",\"join\":true,\"alias\":\"T\",\"value\":\"telefones\",\"coluna\":\"numero\"},{\"nome\":\"Endereço\",\"join\":true,\"alias\":\"E\",\"value\":\"enderecos\",\"coluna\":\"logradouros\",\"checked\":true},{\"nome\":\"E-mail\",\"join\":true,\"alias\":\"M\",\"value\":\"emails\",\"coluna\":\"email\"}],\"titulo\":\"Relatório de Escola 26/11/2014 21:58\",\"tipo\":\"visualizar\"}"));	
	
//	}
//
//	public String gerar(String aJson) {
//		JSONObject json = new JSONObject(aJson);
//		String sql = getSql(json);
//		List<Object[]> registros = executarSql(sql);
//
//		JSONObject entradaParaRelatorio = parsearRetornoSelect(registros,json);
//		String caminho =  new CustomReportBuilder().build(json.getString("titulo"), entradaParaRelatorio);
//		return "{\"url\":\""+caminho+"\"}";
//	}
//
//	private JSONObject parsearRetornoSelect(List<Object[]> registros, JSONObject aJson) {
//		JSONArray colunas = new JSONArray();
//		for (int i = 0; i < aJson.getJSONArray("filtros").length(); i++) {
//			JSONObject filtro = aJson.getJSONArray("filtros").getJSONObject(i);
//			if (!filtro.isNull("checked") && filtro.getBoolean("checked")){
//				JSONObject coluna = new JSONObject();
//				coluna.put("name", filtro.getString("nome"));
//				JSONArray values = new JSONArray();
//				coluna.put("values", values);
//				colunas.put(coluna);
//			}
//		}
//
//		if (registros.size() > 0 && registros.get(0) instanceof Object[]) {
//			for (Object[] r : registros) {
//				if (r != null) {
//					for (int i = 0; i < r.length; i++) {
//						String valor = trataValor(r[i]);
//						JSONObject col = colunas.getJSONObject(i);						
//						
//						col.getJSONArray("values").put(valor);
//					}
//				}
//			}
//		} else {
//			// quando só tem um campo para retornar
//			for (Object valor : registros) {
//				String s = trataValor(valor);
//				colunas.getJSONObject(0).getJSONArray("values").put(s);
//				
//			}
//		}
//		
//		JSONObject entrada = new JSONObject();
//		entrada.put("columns", colunas);
//		return entrada;
//	}
//
//	private String trataValor(Object aValor) {
//		String resultado;
//		if (aValor instanceof java.sql.Date){
//			java.sql.Date data = (Date)aValor;
//			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
//			resultado = df.format(data);
//		}else{
//			resultado = String.valueOf(aValor);
//		}
//		resultado = (resultado == null || resultado.equals("null"))? "-":resultado;
//		return resultado;
//	}
//
//	@SuppressWarnings("unchecked")
//	private List<Object[]> executarSql(String aSql) {
//		Session.begin();
//		try {
//			List registros = Session.list(aSql, 200);
//			Session.commit();
//			return registros;
//		} catch (Exception e) {
//			Session.rollback();
//		}
//		return null;
//	}
//
//	private String getSql(JSONObject aJson) {
//		Tabela entidade = new Tabela(aJson.getString("nome"),
//				aJson.getString("alias"));
//		JSONArray filtros = aJson.getJSONArray("filtros");
//		SelectQuery sql = new SelectQuery();
//		sql.addtabela(entidade);
//		
//		for (int i = 0; i < filtros.length(); i++) {
//			JSONObject f = filtros.getJSONObject(i);
//			if (!f.isNull("checked") && f.getBoolean("checked")) {
//				if (!f.isNull("join") && f.getBoolean("join")) {
//					Tabela join = new Tabela(removeAcentos(f.getString("nome")), f.getString("alias"));
//					join.addColunasAoSelect(f.getString("coluna"));
//					sql.addtabela(join);
//					sql.addJoin(entidade, f.getString("value"), f.getString("alias"));
//				}else{
//					entidade.addColunasAoSelect(f.getString("value"));
//				}
//			}
//		}
//		return sql.toString();
//	}
//	
//	public String removeAcentos(String str) {
//	  str = Normalizer.normalize(str, Normalizer.Form.NFD);
//	  str = str.replaceAll("[^\\p{ASCII}]", "");
//	  return str;
//	 
//	}
}
