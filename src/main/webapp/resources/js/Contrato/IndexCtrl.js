(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, ContratoService, PaginationService) {

    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('contrato/' + id);
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(ContratoService.getPagination);
    };

    $scope.logic.remove = function(id) {
      ContratoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'ContratoService', 'PaginationService'];

  angular.module('app')
  .controller('ContratoIndexCtrl', IndexCtrl);

})();