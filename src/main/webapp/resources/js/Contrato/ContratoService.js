(function(){
  'use strict';

  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/contratos', {},
      {
        'pagination': {
          url:'rest/contratos/pageList',
          method:'GET',
          params: {pageIndex: '@pageIndex', valor: '@valor'}
        },
        'get': {
          url: 'rest/contratos/getDTO',
          method: 'GET',
          params: {id: '@id'}
        },
      });

    Service.getPagination = function(pageIndex, callback, filter) {
      resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
    };

    Service.get = function(id, callback){
      resource.get({ id: id }, callback);
    };

    return Service;
  };

  Factory.$inject = ['$resource'];

  angular.module('app')
    .factory('ContratoService', Factory);
})();