(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, ContratoService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if (angular.isDefined(id)) {
        ContratoService.get(id, function(data) {
          $scope.contrato = data;
        });
      }
    };

    $scope.editarAdvertencia = function(advertencia) {
      $modal.open({
        templateUrl: 'resources/partials/Contrato/advertencia-modal.html',
        controller: 'ContratoAdvertenciaModalCtrl',
        resolve: {
          contrato: function() { return $scope.contrato; },
          advertencia: function() { return advertencia; }
        }
      }).result.then(function() {
        $scope.logic.init();
      });
    };

    $scope.removerAdvertencia = function(vaga, index) {
      ContratoService.removerAdvertencia({
        idContrato: $scope.contrato.id,
        idAdvertencia: advertencia.id
      }, function() {
        $scope.logic.init();
      });
    };

  }

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'ContratoService'];

  angular.module('app')
  .controller('ContratoShowCtrl', FormCtrl);

})();