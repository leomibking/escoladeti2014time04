angular.module('app')

.controller('ContratoAdvertenciaModalCtrl', function($scope, $modalInstance, advertencia, contrato, TypeService, ContratoService, $routeParams) {
  $scope.advertencia = advertencia;

  TypeService.advertencia(function(data) {
    $scope.advertencias = data;
  });

  $scope.salvar = function(advertencia) {
    var obj = angular.copy(advertencia);
    obj.idAdvertencia = advertencia.id;
    obj.idContrato = contrato.id;
    obj.id = undefined;
    obj.status = undefined;

    ContratoService.salvarAdvertencia(obj, function() {
      $modalInstance.close();
    });
  }

  $scope.cancelar = function () {
    $modalInstance.close();
  }
});
