(function(){
  'use strict';
  
  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/relatorio/gerar', {}, 
      {
        'gerar': { method: 'POST' }
      });

    
    Service.gerarRelatorio = function(object, callback){
      resource.gerar(object, callback);
    };

    return Service;
  }; 

  Factory.$inject = ['$resource'];

  angular.module('app')
    .factory('RelatorioService', Factory);
})();