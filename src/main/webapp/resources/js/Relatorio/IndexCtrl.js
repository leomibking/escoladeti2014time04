(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, RelatorioService,RelatorioCrudService, PaginationService) {
    
    $scope.logic = {};

    $scope.entidades = [
      {
        nome : "Aprendiz",
        alias: "A",
        filtros: [
          {
            nome : "Nome",
            value: "nome"
          },
          {
            nome: "Data de nascimento",
            value: "dataNascimento"
          },
          {
            nome: "Nome do pai",
            value: "nomePai"
          },
          {
            nome: "Nome da mãe",
            value: "nomeMae"
          },
          {
            nome: "CPF",
            value: "cpf"
          },
          {
            nome: "RG",
            value: "rg"
          },
          {
            nome: "Estado Civil",
            value: "estadoCivil"
          },
          {
            nome: "Número NIS",
            value: "nisNumero"
          },
          {
            nome: "Sexo",
            value: "sexo"
          },
          {
            nome: "Número PIS",
            value: "pisNumero"
          },
          {
            nome: "Nacionalidade",
            value: "nacionalidade"
          },
          {
            nome: "Nome da escola",
            value: "nomeEscola"
          },
          {
            nome: "Nível de escolaridade",
            value: "curso"
          },
          {
            nome: "Turno escolar",
            value: "turnoEscola"
          },
          {
            nome: "Série escolar",
            value: "serie"
          },
          {
            nome: "Tem interesse em curso gratuito de informática",
            value: "interesseCursoInformatica"
          },
          {
            nome: "Trabalhou anteriormente",
            value: "empregadoAnteriormente"
          },
          {
            nome: "Quantidade residentes em casa",
            value: "residentesCasa"
          },
          {
            nome: "Quantidade de pessoas que trabalham em casa",
            value: "residentesEmpregados"
          },
          {
            nome: "Familiar recebendo auxílio governo",
            value: "familiarAuxilioGoverno"
          },
          {
            nome: "Renda própria",
            value: "rendaPropria"
          },
          {
            nome: "Renda total",
            value: "rendaTotal"
          },
          {
            nome: "Participa do programa digitando o futuro",
            value: "participaDigitandoFuturo"
          },
          {
            nome : "Telefone",
            join : true,
            alias: "T",
            value: "telefones",
            coluna: "numero",
          },
          {
            nome : "Endereço",
            join : true,
            alias: "E",
            value: "enderecos",
            coluna: "logradouro"
          },
          {
            nome: "Email",
            join: true,
            alias: "M",
            value: "emails",
            coluna: "email"
          }
        ]
      },
      {
        nome : "Escola",
        alias: "Z",
        filtros: [
          {
            nome : "Razão Social",
            value: "razaoSocial"
          },
          {
            nome: "Nome Fantasia",
            value: "nome"
          },
          {
            nome: "CNPJ",
            value: "cnpj"
          },
          {
            nome : "Telefone",
            join : true,
            alias: "T",
            value: "telefones",
            coluna: "numero"
          },
          {
            nome : "Endereço",
            join : true,
            alias: "E",
            value: "enderecos",
            coluna: "logradouro"
          },
          {
            nome: "E-mail",
            join: true,
            alias: "M",
            value: "emails",
            coluna: "email"
          }
        ]
      },
      {
        nome : "Empresa",
        alias: "S",
        filtros: [
          {
            nome : "Razão Social",
            value: "razaoSocial"
          },
          {
            nome: "Nome Fantasia",
            value: "nome"
          },
          {
            nome: "CNPJ",
            value: "cnpj"
          },
          {
            nome : "Telefone",
            join : true,
            alias: "T",
            value: "telefones",
            coluna: "numero"
          },
          {
            nome : "Endereço",
            join : true,
            alias: "E",
            value: "enderecos",
            coluna: "logradouro"
          },
          {
            nome: "E-mail",
            join: true,
            alias: "M",
            value: "emails",
            coluna: "email"
          }
        ]
      },
      {
        nome : "Professor",
        alias: "P",
        filtros: [
          {
            nome : "Nome",
            value: "nome"
          },
          {
            nome: "Sexo",
            value: "sexo"
          },
          {
            nome: "CPF",
            value: "cpf"
          },
          {
            nome: "RG",
            value: "rg"
          },
          {
            nome: "Data de nascimento",
            value: "dataNascimento"
          },
          {
            nome : "Telefone",
            join : true,
            alias: "T",
            value: "telefones",
            coluna: "numero"
          },
          {
            nome : "Endereço",
            join : true,
            alias: "E",
            value: "enderecos",
            coluna: "logradouro"
          },
          {
            nome: "E-mail",
            join: true,
            alias: "M",
            value: "emails",
            coluna: "email"
          }
        ]
      }
    ]

    $scope.incluirFiltro = function(filtro) {
    }

    // $scope.logic.show = function(id) {
    //   $location.path('/relatorio/' + id);
    // };

    // $scope.logic.edit = function(id) {
    //   $location.path('/relatorio/' + id + '/editar');
    // };

    // $scope.logic.new = function() {
    //   $location.path('/relatorio/novo');
    // };
    
    // $scope.logic.init = function() {
    //   $scope.logic.PaginationService = PaginationService.createInstance();
    //   $scope.logic.PaginationService.init(RelatorioService.getPagination);
    // };
    
    // $scope.logic.remove = function(id) {
    //   RelatorioService
    //   .remove(id, function() {
    //     $scope.logic.PaginationService
    //     .reload();
    //   });
    // };

    var currentdate = new Date();
    var datetime = currentdate.getDate() + "/"
      + (currentdate.getMonth()+1) + "/"
      + currentdate.getFullYear() + " "
      + currentdate.getHours() + ":"
      + currentdate.getMinutes();

    $scope.gerarRelatorio = function(tipo){
      $scope.entidadeSelecionada.titulo = "Relatório de " + $scope.entidadeSelecionada.nome +" "+ datetime;
      $scope.entidadeSelecionada.tipo = tipo;
      RelatorioService.gerarRelatorio($scope.entidadeSelecionada,function(data){
        if(tipo=='pdf'){
          var element = angular.element('<a/>');
          element.attr({
            href: '/EscolaDeTi2014Time04/'+data.url,
            target: '_self',
            download: 'Relatorio.pdf'
          })[0].click();
        }else if (tipo=='visualizar'){
          window.open('/EscolaDeTi2014Time04/'+data.url);
        }else if (tipo=='excel'){
          var element = angular.element('<a/>');
          element.attr({
            href: '/EscolaDeTi2014Time04/'+data.url,
            target: '_self',
            download: 'Relatorio.xlsx'
          })[0].click();
        }
      });
    };

    $scope.setarRelatorio = function(conteudo){
      $scope.entidadeSelecionada = JSON.parse(conteudo);
    }

    $scope.salvarRelatorio = function(){
      var relatorio = {
        descricao: $scope.nomeRelatorio,
        conteudo: JSON.stringify($scope.entidadeSelecionada)
      }
      RelatorioCrudService.save(relatorio,function(data){
        $('#modalSalvarRelatorio').modal('toggle');
        $scope.nomeRelatorio = '';
        $scope.listarRelatoriosSalvos();
      });
    }

    $scope.removerRelatorio = function(r){
      RelatorioCrudService.remove(r,function(data){
        $scope.listarRelatoriosSalvos();
      });
    }

    $scope.listarRelatoriosSalvos = function(){
      RelatorioCrudService.query(function(data){
        $scope.relatoriosSalvos = data;
      })
    }
    $scope.listarRelatoriosSalvos();
  };


  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'RelatorioService','RelatorioCrudService', 'PaginationService'];

  angular.module('app')
  .controller('RelatorioIndexCtrl', IndexCtrl);

})();