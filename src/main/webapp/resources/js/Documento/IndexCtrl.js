(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, DocumentoService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('documento/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('documento/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('documento/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(DocumentoService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      DocumentoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'DocumentoService', 'PaginationService'];

  angular.module('app')
  .controller('DocumentoIndexCtrl', IndexCtrl);

})();