(function() {
    "use strict";

    function FormCtrl($scope, $location, $routeParams, DocumentoService) {
        $scope.logic = {};

        $scope.logic.init = function() {
            var id = $routeParams.id;
            if (angular.isDefined(id)) {
                DocumentoService.get(id, function(data) {
                    $scope.documento = data;
                });
            }
        };

        $scope.logic.back = function() {
            $location.path('/documento');
        };

        $scope.logic.cancel = function() {
            $location.path('/documento');
        };

        $scope.logic.edit = function(id) {
            $location.path('/documento/' + id + '/editar');
        };

        $scope.logic.remove = function(id) {
            DocumentoService
                    .remove(id, function() {
                        $scope.logic.back();
                    });
        };
    }
    ;

    FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'DocumentoService'];

    angular.module('app')
            .controller('DocumentoShowCtrl', FormCtrl);

})();