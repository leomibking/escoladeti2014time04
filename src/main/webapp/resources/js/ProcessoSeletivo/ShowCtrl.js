angular.module('app')

.controller('ProcessoSeletivoShowCtrl', function($scope, $routeParams, ProcessoSeletivoService, TurmaService, PaginationService) {

  var todosStatus = [
    'pre-selecao',
    'selecao',
    'pos-selecao',
    'encerrado',
    'cancelado'
  ];

  $scope.selecionado = 'pre-selecao';
  $scope.novaTurma   = {};
  $scope.disciplinas = [];
  $scope.turmasLista = [];

  // MÉTODOS PRIVADOS

  // @private
  function _existeEtapaEmAbertoAndamento() {
    var out = false;

    angular.forEach($scope.processoSeletivo.etapas, function(item, key) {
      if (item.status == 'Aberta' || item.status == 'Andamento') {
        out = true;
      }
    });

    return out;
  }

  // @private
  function _naoFoiAprovadoNenhumAluno() {
    var out = true;

    angular.forEach($scope.processoSeletivo.selecionados, function(item,key) {
      if (item.aprovado) out = false;
    });

    return out;
  }

  // @private
  function _salvarSelecao() {
    if ($scope.processoSeletivo.status == 'Em Seleção') {

      if (_existeEtapaEmAbertoAndamento()) {
        alert('Exitem etapas em Aberto ou em Andamento.');
        return false;
      }

      if ($scope.processoSeletivo.aprovados.length > $scope.processoSeletivo.vaga.quantidade) {
        alert('Exitem mais candidatos aprovados que a quantidade de Vagas para o Processo.');
        return false;
      }

      if (_naoFoiAprovadoNenhumAluno()) {
        alert('Não foi aprovado nenhum Candidato nesse Processo');
        return false;
      }

      ProcessoSeletivoService.salvarSelecao($scope.processoSeletivo.id, {}, function(data) {
        $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        return true;
      });
    } else {
      return true;
    }
  }

  // @private
  function _existeAlgumAprovadoSemTurma() {
    var retorno = false;

    angular.forEach($scope.processoSeletivo.aprovados, function(item, key) {
      if ( ! item.selecionado) retorno = true;
    });

    return retorno;
  }

  // @private
  function _salvarPosSelecao() {
    if ($scope.processoSeletivo.status == 'Pós-Seleção') {
      if (_existeAlgumAprovadoSemTurma()) {
        alert('Exitem candidatos aprovados que não foram vinculados a nenhuma Turma');
        return false;
      }

      ProcessoSeletivoService.salvarPosSelecao($scope.processoSeletivo.id, $scope.processoSeletivo, function(data) {
        $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        return true;
      });
    } else {
      return true;
    }
  }

  // @private
  function _salvarPreSelecao() {
    if ($scope.processoSeletivo.status == 'Pré-Seleção') {
      if ($scope.aprendizesSelecionados.length == 0) {
        alert('Por favor, selecione pelo menos um candidado para poder prosseguir.');
        return false;
      }

      ProcessoSeletivoService.salvarPreSelecao($scope.processoSeletivo.id, {
        selecionados: $scope.aprendizesSelecionados
      }, function(data) {
        $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        return true;
      });
    } else {
      return true;
    }
  }

  // MÉTODOS PÚBLICOS

  $scope.setStatus = function(status) {
    $scope.selecionado = status;
  }

  $scope.atualizaStatusProcesso = function() {
    if ($scope.processoSeletivo.status == 'Pré-Seleção') {
      $scope.selecionado = 'pre-selecao';
      return;
    }

    if ($scope.processoSeletivo.status == 'Em Seleção') {
      $scope.selecionado = 'selecao';
      return;
    }

    if ($scope.processoSeletivo.status == 'Pós-Seleção') {
      $scope.selecionado = 'pos-selecao';
      return;
    }

    if ($scope.processoSeletivo.status == 'Encerrado') {
      $scope.selecionado = 'encerrado';
      return;
    }

    if ($scope.processoSeletivo.status == 'Cancelado') {
      $scope.selecionado = 'cancelado';
      return;
    }
  }

  $scope.setStatus = function(status) {
    $scope.selecionado = status;
  };

  $scope.undoStatus = function(statusAtual) {
    angular.forEach(todosStatus, function(status, key) {
      if(status === statusAtual) {
        $scope.selecionado = todosStatus[key - 1];
      }
    });
  }

  $scope.nextStatus = function(statusAtual) {
    if (statusAtual == 'pre-selecao') {
      if ( ! _salvarPreSelecao()) return;
    }

    if (statusAtual == 'selecao') {
      if ( ! _salvarSelecao()) return;
    }

    if (statusAtual == 'pos-selecao') {
      _salvarPosSelecao();
      return;
    }

    angular.forEach(todosStatus, function(status, key) {
      if (status === statusAtual) {
        $scope.selecionado = todosStatus[key + 1];
      }
    });
  }

  $scope.voltarAEtapaDePreSelecao = function(processoSeletivo) {
    if (processoSeletivo.status !== 'Em Seleção') return;

    var confirmacao = confirm('Esta operação irá desfazer toda a etapa de Seleção. Deseja prosseguir?');

    if (confirmacao) {
      ProcessoSeletivoService.voltarEtapa(processoSeletivo.id, function() {
        $scope.loadProcessoSeletivoShow(processoSeletivo.id);
      });
    }
  }

  $scope.voltarAEtapaDeSelecao = function(processoSeletivo) {
    ProcessoSeletivoService.voltarEtapa(processoSeletivo.id, function() {
      $scope.loadProcessoSeletivoShow(processoSeletivo.id);
    });
  }

  $scope.loadTurmasSemProcesso = function() {
    TurmaService.getTurmasSemProcesso(function(data) {
      $scope.turmasLista = data;
    });
  }

  $scope.loadProcessoSeletivoShow = function(id) {
    ProcessoSeletivoService.getShow(id, function(data) {
      $scope.processoSeletivo = data;
      var selecionados = [];
      for (item in data.selecionados){
          selecionados.push(data.selecionados[item].candidato);
      }
      $scope.aprendizesSelecionados = selecionados;
      $scope.loadTurmasSemProcesso();
      $scope.atualizaStatusProcesso();
    });
  }

  $scope.loadProcessoSeletivoShow($routeParams.id);
});
