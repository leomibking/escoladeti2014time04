angular.module('app')

.directive('processoSelecao', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/ProcessoSeletivo/directives/selecao/template.html',
    controller: function($scope, $http,ProcessoSeletivoService) {
      $scope.statusAberto = function(status) {
        return status == 'ABERTO';
      }

      $scope.listagemEtapas = true;

      $scope.toggleEtapa = function() {
        $scope.listagemEtapas = !$scope.listagemEtapas;
      }

      $scope.editarEtapa = function(etapa) {
        $scope.toggleEtapa();

        ProcessoSeletivoService.editarEtapa($scope.processoSeletivo.id, etapa.id, function(data) {
          $scope.etapaEdicao = data;
        });
      }

      $scope.salvaEdicaoEtapa = function(etapa) {
        ProcessoSeletivoService.salvarEtapaEdicao($scope.processoSeletivo.id, etapa.id, etapa, function(data) {
          $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        });

        $scope.toggleEtapa();
      }

      $scope.finalizarEtapa = function(etapa) {
        ProcessoSeletivoService.finalizarEtapa($scope.processoSeletivo.id, etapa.id, function(data) {
          $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        });
      }

      $scope.reabrirEtapa = function(etapa) {
        ProcessoSeletivoService.reabrirEtapa($scope.processoSeletivo.id, etapa.id, function(data) {
          $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        });
      }

      $scope.aprovarCandidato = function(candidato) {
        ProcessoSeletivoService.aprovarCandidato($scope.processoSeletivo.id, candidato.id, function(data) {
          $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        });
      }

      $scope.reprovarCandidato = function(candidato) {
        ProcessoSeletivoService.reprovarCandidato($scope.processoSeletivo.id, candidato.id, function(data) {
          $scope.loadProcessoSeletivoShow($scope.processoSeletivo.id);
        });
      }

      $scope.cancelarEdicaoEtapa = function() {
        $scope.etapaEdicao = null;
        $scope.toggleEtapa();
      }

      $scope.teste = function(candidato) {
        if ($scope.candidatoEdicao == undefined || $scope.candidatoEdicao == null) {
          $scope.candidatoEdicao = candidato;
          $scope.editando = !$scope.editando;
        } else {
          if ($scope.editando && candidato.nome == $scope.candidatoEdicao.nome) {
            $scope.editando = !$scope.editando;
            $scope.candidatoEdicao = null;
          } else {
            $scope.candidatoEdicao = candidato;
          }
        }
      }
    }
  }
});
