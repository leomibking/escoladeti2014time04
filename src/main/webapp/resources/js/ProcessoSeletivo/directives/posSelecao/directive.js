angular.module('app')

.directive('processoPosSelecao', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/ProcessoSeletivo/directives/posSelecao/template.html',
    controller: function($scope, ProcessoSeletivoService, TurmaService) {
      $scope.exibindoAlunos = false;

      $scope.exibirOcultarAlunos = function(turma) {
        if ($scope.turmaEdicao == undefined || $scope.turmaEdicao == null) {
          $scope.turmaEdicao = turma;
          $scope.exibindoAlunos = !$scope.exibindoAlunos;
        } else {
          if ($scope.exibindoAlunos && turma.nome == $scope.turmaEdicao.nome) {
            $scope.exibindoAlunos = !$scope.exibindoAlunos;
            $scope.turmaEdicao = null;
          } else {
            $scope.turmaEdicao = turma;
          }
        }
      }

      $scope.cancelarExibicaoDeAlunos = function() {
        $scope.exibindoAlunos = !$scope.exibindoAlunos;
        $scope.turmaEdicao = null;
      }

      $scope.dropSuccessHandlerAlunos = function($event, index, array) {
          array[index].selecionado = true;
      }

      $scope.onDropAlunos = function($event, $data, array) {
        if ($data.selecionado) return;
        var candidato = {};
        candidato.id   = $data.candidato.id;
        candidato.nome = $data.candidato.nome;
        array.push(candidato);
        $scope.turmaEdicao.qtAlunos = array.length;
      }

      $scope.removerAluno = function($index){
        for (item in $scope.processoSeletivo.aprovados) {
          if ($scope.processoSeletivo.aprovados[item].candidato.id == $scope.turmaEdicao.alunos[$index].id) {
            $scope.processoSeletivo.aprovados[item].selecionado = false;
            $scope.turmaEdicao.alunos.splice($index,1);
            $scope.turmaEdicao.qtAlunos = $scope.turmaEdicao.alunos.length;
          }
        }
      }

      $scope.turmaJaExiste = function(turma) {
        var retorno = false;
        angular.forEach($scope.processoSeletivo.turmas,function(item,key) {
          if (item.id == turma.id) {
            retorno = true;
            return;
         }
        });

        return retorno;
      }

      $scope.vincularTurma = function(turma) {
        if ( ! angular.isDefined(turma)) return;

        if ($scope.turmaJaExiste(turma)) {
          alert("Turma "+turma.nome+" ja foi inserida.");
          $scope.turma = null;
          return;
        }

        turma.alunos = [];
        turma.qtAlunos = turma.alunos.length;
        $scope.processoSeletivo.turmas.push(turma);
        $scope.turma = null;
      }

      $scope.excluirTurma = function(turma){
        var turmas = $scope.processoSeletivo.turmas;

        if (turma.alunos.length > 0) {
          alert("Não é possivel remover uma turma com alunos.");
          return;
        }

        for (item in turmas) {
          if (turmas[item].nome == turma.nome) {
              turmas.splice(item,1);

            if (($scope.turmaEdicao != null) && (turma.id == $scope.turmaEdicao.id)) {
              $scope.turmaEdicao = null;
              $scope.exibindoAlunos = false;
            }

            break;
          }
        }
      }

      $scope.adicionarNovaTurma = function(novaTurma) {
        $scope.salvarNovaTurma(novaTurma);
      }

      $scope.cancelarNovaTurma = function() {
          $scope.novaTurma = null;
      }

      $scope.salvarNovaTurma = function(novaTurma) {
        TurmaService.saveByDTO(novaTurma,function() {
          $scope.loadTurmasSemProcesso();
          novaTurma = null;
          $scope.novaTurma = null;
        });
      }
    }
  }
});
