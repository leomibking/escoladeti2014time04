angular.module('app')

.directive('processoPreSelecao', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/ProcessoSeletivo/directives/preSelecao/template.html',
    controller: function($scope, $http, PaginationService,ProcessoSeletivoService) {
      $scope.PaginationService = PaginationService.createInstance();

      $scope.listaFiltro = [];

      $scope.campos = [
        {
          'key': {'campo': 'nome', 'tipo': 'STRING'},
          'view': 'Nome',
          'condicoes': [
            {'key': 'Igual a','value': '='},
            {'key': 'Diferente de','value': '<>'},
            {'key': 'Contem','value': 'C'},
            {'key': 'NÃ£o Contem','value': 'NC'}
          ]
        },
        {
          'key': {'campo': 'dataNascimento', 'tipo': 'DATA'},
          'view': 'Idade',
          'condicoes': [
            {'key': 'Menor ou Igual a','value': '<='},
            {'key': 'Maior ou Igual a','value': '>='}
          ]
        },
        {
          'key': {'campo': 'rendaPerCapita', 'tipo': 'VALOR'},
          'view': 'Renda per Capita',
          'condicoes': [
            {'key': 'Menor que','value': '<'},
            {'key': 'Menor ou Igual a','value': '<='},
            {'key': 'Maior ou Igual a','value': '>='},
            {'key': 'Maior que','value': '>'},
            {'key': 'Igual a','value': '='},
            {'key': 'Diferente de','value': '<>'}
          ]
        },
        {
          'key': {'campo': 'id', 'tipo': 'VALOR'},
          'view': 'Id.',
          'condicoes': [
            {'key': 'Menor que','value': '<'},
            {'key': 'Menor ou Igual a','value': '<='},
            {'key': 'Maior ou Igual a','value': '>='},
            {'key': 'Maior que','value': '>'},
            {'key': 'Igual a','value': '='},
            {'key': 'Diferente de','value': '<>'}
          ]
        }
      ];

      $scope.PaginationService.init(function(pageIndex, callback) {
          var filtros = [];

          angular.forEach($scope.listaFiltro, function(item) {
            if ( ! angular.isDefined(item)) return;

            var filtro = {};

            if (angular.isDefined(item.chave)) {
              filtro.chave = item.chave.key.campo;
              filtro.tipo = item.chave.key.tipo;
            }

            if (angular.isDefined(item.condicao)) {
              filtro.condicao = item.condicao.value;
            }

            if (angular.isDefined(item.valor)) {
              filtro.valor = item.valor;
            }

            filtros.push(filtro);
          });

          ProcessoSeletivoService.paginationSelecao(pageIndex, {
            filtros: filtros
          }, callback);
      });

      $scope.aprendizesSelecionados = [];

      $scope.removerAprendizSelecionado = function(aprendiz) {
        $scope.aprendizesSelecionados.splice($scope.aprendizesSelecionados.indexOf(aprendiz), 1);
      }

      $scope.onDropPreSelecao = function(aprendiz, selecionados) {
        for (selecionado in selecionados) {
          if (aprendiz.id == selecionados[selecionado].id) return;
        }

        selecionados.push(aprendiz);
      }

      $scope.adicionarFiltro = function() {
        $scope.listaFiltro.push({});
      }

      $scope.adicionarFiltro();

      $scope.removerFiltro = function(index) {
        $scope.listaFiltro.splice(index, 1);
      }

      $scope.buscarAprendizFiltrando = function() {
        $scope.PaginationService.get(1);
      }
    }
  }
});
