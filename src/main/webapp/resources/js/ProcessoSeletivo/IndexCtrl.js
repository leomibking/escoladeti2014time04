(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, ProcessoSeletivoService, PaginationService) {
  
    $scope.logic = {};

    // $scope.processoseletivoshow = {};
    // $scope.logic.show = function(item) {
    //   $scope.processoseletivoshow = item;
    // };

    $scope.logic.show = function(id) {
      $location.path('processoseletivo/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('processoseletivo/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('processoseletivo/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(ProcessoSeletivoService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      ProcessoSeletivoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'ProcessoSeletivoService', 'PaginationService'];

  angular.module('app')
  .controller('ProcessoSeletivoIndexCtrl', IndexCtrl);

})();