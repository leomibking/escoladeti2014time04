angular.module('app')

.controller('ProcessoSeletivoFormCtrl', function($scope, $location, $routeParams, ProcessoSeletivoService, EmpresaService, $http, EtapaService, $modal) {
  $scope.logic = {};

  EmpresaService.listar(function(data) {
    $scope.empresas = data;
  });

  function _getEtapas() {
    EtapaService.query(function(data) {
      $scope.opcoesDeEtapa = data;
    });
  }

  $scope.logic.init = function () {
    _getEtapas();

    if ($routeParams.id) {
      ProcessoSeletivoService.get($routeParams.id, function(data) {
        $scope.processoseletivo = data;
        $scope.getVagas($scope.processoseletivo.empresa);
      });
    } else {
      $scope.processoseletivo = {
        status: 'Pré-Seleção',
        etapas: []
      };
    }
  }

  $scope.getVagas = function(empresa) {
    if (empresa) {
      EmpresaService.vagasDaEmpresa({
        idEmpresa: empresa.id
      }, function(data) {
        $scope.vagas = data;
      });
    } else {
      $scope.vagas = [];
    }
  }

  $scope.cadastrarNovaEtapa = function() {
    var modalInstance = $modal.open({
      templateUrl: 'resources/partials/Etapa/etapa-modal.html',
      controller: 'EtapaModalCtrl'
    });

    modalInstance.result.then(function() {
      _getEtapas();
    })
  }

  $scope.adicionarEtapa = function(etapa) {
    if ($scope.processoseletivo.etapas.indexOf(etapa) === -1) {
      etapa.idEtapa = etapa.id;
      delete etapa.observacao;
      delete etapa.id;
      $scope.processoseletivo.etapas.push(etapa);
    }

    $scope.etapaSel = null;
  }

  $scope.excluirEtapa = function(etapa, index) {
    $scope.processoseletivo.etapas.splice(index, 1);
  }

  $scope.logic.back = function () {
      $location.path('processoseletivo');
  }

  $scope.logic.cancel = function () {
    if ($scope.processoseletivo.id) {
      $location.path('processoseletivo/' + $scope.processoseletivo.id);
    } else {
      $location.path('processoseletivo');
    }
  }

  $scope.logic.canSave = function() {
    if ($scope.processoseletivo.etapas.length <= 0) {
      alert('É necessário informar ao menos uma Etapa.');
      return false;
    }

    if (new Date($scope.processoseletivo.dataInicio) > new Date($scope.processoseletivo.dataFim)) {
      alert('A Data de Inicio deve ser menor que a Data Final.');
      return false;
    }

    return true;
  }

  $scope.logic.save = function(obj) {
    if ( ! $scope.logic.canSave()) return;

    ProcessoSeletivoService.save(obj, function () {
      if ($scope.processoseletivo.id) {
        $location.path('processoseletivo/' + $scope.processoseletivo.id);
      } else {
        $location.path('processoseletivo');
      }
    });
  }

  $scope.remover = function (id) {
    ProcessoSeletivoService.remove(id, function() {
      $location.path('processoseletivo');
    });
  }
});
