(function(){
  'use strict';
  
  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/ProcessoSeletivo', {}, 
      {
        'pagination': { 
          url:'rest/ProcessoSeletivo/pageList',
          method:'GET',
          params: {pageIndex: '@pageIndex', valor: '@valor'} 
        },
        'paginationSelecao': { 
          url:'rest/aprendizes/listaDTO',
          method:'POST',
          params: {pageIndex: '@pageIndex'} 
        },
        'get': { 
          url:'rest/ProcessoSeletivo/:id/editar',
          method:'GET',
          params: {id: '@id'} 
        },
        'salvarNovaTurmaComAlunos':{
           url:'rest/ProcessoSeletivo/:id/turma',
           method:'POST',
           params:{ id:'@id'}
        },
        'getShow': { 
          url:'rest/ProcessoSeletivo/:id/show',
          method:'GET',
          params: {id: '@id'} 
        },
        'salvar':{
           url:'rest/ProcessoSeletivo/salvar',
           method:'POST' 
        },
        'save': { method: 'POST' },
        'query': { method: 'GET', isArray: true },
        'remove': { method: 'DELETE', params: {id: '@id'} },
        'editarEtapa':{
            method:'GET',
            params: {id: '@id', idEtapa:'@idEtapa'},
            url:'rest/ProcessoSeletivo/:id/etapas/:idEtapa'
        },
        'salvarEtapaEdicao':{
            url:'rest/ProcessoSeletivo/:id/etapas/:idEtapa',
            method:'POST',
            params:{ id:'@id', idEtapa:'@idEtapa'}
        },
        'finalizarEtapa':{
            url:'rest/ProcessoSeletivo/:id/etapas/:idEtapa/finalizar',
            method:'POST',
            params:{ id:'@id', idEtapa:'@idEtapa'}
        },
        'reabrirEtapa':{
            url:'rest/ProcessoSeletivo/:id/etapas/:idEtapa/reabrir',
            method:'POST',
            params:{ id:'@id', idEtapa:'@idEtapa'}
        },
        'salvarPreSelecao':{
            url:'rest/ProcessoSeletivo/:id/salvarpreselecao',
            method:'POST',
            params:{ id:'@id'}
        },
        'salvarSelecao':{
            url:'rest/ProcessoSeletivo/:id/salvarselecao',
            method:'POST',
            params:{ id:'@id'}
        },
        'salvarPosSelecao':{
            url:'rest/ProcessoSeletivo/:id/salvarposselecao',
            method:'POST',
            params:{ id:'@id'}
        },
        'aprovarCandidato':{
            url:'rest/ProcessoSeletivo/:id/selecionados/:idCandidato/aprovar',
            method:'POST',
            params:{ id:'@id',idCandidato:'@idCandidato'}
        },
        'reprovarCandidato':{
            url:'rest/ProcessoSeletivo/:id/selecionados/:idCandidato/reprovar',
            method:'POST',
            params:{ id:'@id',idCandidato:'@idCandidato'}
        },
        'voltarEtapa':{
            url:'rest/ProcessoSeletivo/:id/voltarStatus',
            method:'PUT',
            params:{ id:'@id'}
        }
      });
      
    Service.voltarEtapa = function(id,callback){
        resource.voltarEtapa({id: id},callback);
    };
      
    Service.aprovarCandidato = function(id, idCandidato, callback){
        resource.aprovarCandidato({ id: id, idCandidato:idCandidato }, callback);
    };
    
    Service.reprovarCandidato = function(id, idCandidato, callback){
        resource.reprovarCandidato({ id: id, idCandidato:idCandidato }, callback);
    };

    Service.getPagination = function(pageIndex, callback, filter) {
      resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
    };
    
    Service.paginationSelecao = function(pageIndex,filters,callback){
      resource.paginationSelecao({ pageIndex: pageIndex },filters,callback);  
    };

    Service.get = function(id, callback){
      resource.get({ id: id }, callback);
    };
    
    Service.getShow = function(id, callback){
      resource.getShow({ id: id }, callback);
    };
    
    Service.save = function(object, callback){
      resource.salvar(object, callback);
    };  
    
    Service.salvarPreSelecao = function(id,object, callback){
      resource.salvarPreSelecao({id:id},object, callback);
    };
    
    Service.salvarSelecao = function(id,object, callback){
      resource.salvarSelecao({id:id},object, callback);
    };
    
    Service.salvarPosSelecao = function(id,object, callback){
      resource.salvarPosSelecao({id:id},object, callback);
    };
    
    Service.query = function(callback) {
      resource.query(callback);
    };

    Service.remove = function(id, callback) {
      resource.remove({ id: id }, callback);
    };
    
    Service.editarEtapa = function(id, idEtapa,callback){
      resource.editarEtapa({id : id, idEtapa:idEtapa},callback);  
    };
    
    Service.salvarEtapaEdicao = function(id, idEtapa, etapa, callback){
      resource.salvarEtapaEdicao({id : id, idEtapa:idEtapa},etapa,callback);    
    };
    
    Service.finalizarEtapa= function(id, idEtapa, callback){
      resource.finalizarEtapa({id : id, idEtapa:idEtapa},callback);    
    };
    
    Service.reabrirEtapa= function(id, idEtapa, callback){
      resource.reabrirEtapa({id : id, idEtapa:idEtapa},callback);    
    };
    
    return Service;
  }; 

  Factory.$inject = ['$resource'];

  angular.module('app')
    .factory('ProcessoSeletivoService', Factory);
})();