angular.module('app')
.factory('ProfessorService', function($resource) {
  var resource = $resource('rest/professores', {}, 
    {
      'pagination': { 
        url:'rest/professores/pageList',
        method:'GET',
        params: {pageIndex: '@pageIndex', valor: '@valor'} 
      },
      'get': { method: 'GET', params: {id: '@id'} },
      'save': { method: 'POST' },
      'query': { method: 'GET', isArray: true },
      'remove': { method: 'DELETE', params: {id: '@id'} },
      'getByDTO':{ 
        method: 'GET',
        url : 'rest/professores/list',
        isArray: true 
      }
    }
  );

  resource.getPagination = function(pageIndex, callback, filter) {
    resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
  };

  return resource;
});

