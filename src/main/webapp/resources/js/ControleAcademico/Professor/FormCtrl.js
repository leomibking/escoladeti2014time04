angular.module('app')
.controller('ProfessorFormCtrl', function($scope, $routeParams, $location, ProfessorService) {

  if ($routeParams.id) {
    ProfessorService.get({id: $routeParams.id}, function(data) {
      $scope.professor = data;
    });
  } else {
    $scope.professor = new ProfessorService({
      telefones: [],
      emails: [],
      enderecos: []
    });
  }

  $scope.save = function(professor) {
    angular.forEach(professor.enderecos, function(endereco) {
      delete endereco.uf;
      delete endereco.cidades;
    });

    professor.$save(function() {
      $location.path('/professores');
    });
  }

  $scope.remover = function(professor) {
    ProfessorService.remove({id: professor.id}, function(data) {
      $location.path('/professores')
    });
  }
})
