angular.module('app')

.controller('ProfessorIndexCtrl', function($scope, $location, $routeParams, ProfessorService, PaginationService) {
  $scope.PaginationService = PaginationService.createInstance();
  $scope.PaginationService.init(ProfessorService.getPagination);
});
