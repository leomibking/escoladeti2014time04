angular.module('app')

.controller('TurmaShowCtrl', function($scope, $location, $routeParams, TurmaService, PaginationService, ProfessorService, DisciplinaService, AtividadeService, $http, $modal) {
  $scope.logic = {};

  // ATIVIDADES

  $scope.adicionarEditarAtividade = function(atividade) {
    var modalInstance = $modal.open({
      templateUrl: 'resources/partials/ControleAcademico/Turma/atividade-modal.html',
      controller: 'AtividadeModalCtrl',
      resolve: {
        turma: function() { return $scope.turma },
        atividade: function() { return atividade || {} }
      }
    });

    modalInstance.result.then(function() {
      $scope.logic.AtividadePaginacao.reload();
    })
  }

  $scope.realizarAtividade = function(atividade) {
    $scope.realizandoAtividade = !$scope.realizandoAtividade;

    TurmaService.getAtividadeEdicao($scope.turma.id, atividade.id, function(data) {
      $scope.atividadeEmRealizacao = data;
    });
  }

  $scope.salvarAtividadeEmRealizacao = function(atividade) {
    TurmaService.salvarEdicaoAtividade($scope.turma.id, atividade.id, atividade, function(data) {
      $scope.cancelarRealizacaoDeAtividade();
      $scope.logic.AtividadePaginacao.reload();
    });
  }

  $scope.cancelarRealizacaoDeAtividade = function() {
    $scope.realizandoAtividade = false;
    $scope.atividadeEmRealizacao = null;
  }

  $scope.excluirAtividade = function(atividade) {
    TurmaService.excluirAtividade($scope.turma.id, atividade.id, function(data) {
      $scope.cancelarRealizacaoDeAtividade();
      $scope.logic.AtividadePaginacao.reload();
    });
  }

  // CHAMADAS

  $scope.adicionarEditarChamada = function(chamada) {
    var modalInstance = $modal.open({
      templateUrl: 'resources/partials/ControleAcademico/Turma/chamada-modal.html',
      controller: 'ChamadaModalCtrl',
      resolve: {
        turma: function() { return $scope.turma },
        chamada: function() { return chamada || {} }
      }
    });

    modalInstance.result.then(function() {
      $scope.logic.ChamadaPaginacao.reload();
    })
  }

  $scope.realizarChamada = function(chamada) {
    $scope.realizandoChamada = !$scope.realizandoChamada;

    TurmaService.getChamadaEdicao($scope.turma.id, chamada.id, function(data) {
      $scope.chamadaEmRealizacao = data;
    });
  }

  $scope.salvarChamadaEmRealizacao = function(chamada) {
    TurmaService.salvarEdicaoChamada($scope.turma.id, chamada.id, chamada, function(data) {
      $scope.cancelarRelizacaoDeChamada();
      $scope.logic.ChamadaPaginacao.reload();
    });
  }

  $scope.cancelarRelizacaoDeChamada = function() {
    $scope.chamadaEmRealizacao = null;
    $scope.realizandoChamada = false;
  }

  $scope.excluirChamada = function(chamada) {
    TurmaService.excluirChamada($scope.turma.id, chamada.id,function(data) {
      $scope.cancelarRelizacaoDeChamada();
      $scope.logic.ChamadaPaginacao.reload();
    });
  }

  // ALUNOS

  $scope.adicionarAluno = function($item, $model, $label) {
    TurmaService.adicionarAlunoNaTurma($scope.turma.id, $item, function(data) {
      $scope.logic.AlunoPaginacao.reload();
      $('#selectAluno').val('');
    });
    $scope.alunoSelecionado = null;
    $('#selectAluno').val('');
  };

  $scope.excluirAluno = function(item) {
    TurmaService.excluirAluno($scope.turma.id, item.id, function(data) {
      $scope.logic.AlunoPaginacao.reload();
    });
  }

  $scope.getAlunosPesquisa = function(val) {
    return $http.get('rest/turma/aluno?nome=' + val).then(function(res) {
      return res.data;
    });
  }

  $scope.logic.init = function() {
    TurmaService.get($routeParams.id, function(data) {
      $scope.turma = data;
    });

    $scope.logic.AlunoPaginacao = PaginationService.createInstance();
    $scope.logic.AlunoPaginacao.init(function(index, callback) {
      TurmaService.getAlunoPaginado($routeParams.id, index, callback);
    });

    $scope.logic.ChamadaPaginacao = PaginationService.createInstance();
    $scope.logic.ChamadaPaginacao.init(function(index, callback) {
      TurmaService.getChamadaPaginado($routeParams.id, index, callback);
    });

    $scope.logic.AtividadePaginacao = PaginationService.createInstance();
    $scope.logic.AtividadePaginacao.init(function(index, callback) {
      TurmaService.getAtividadePaginado($routeParams.id, index, callback);
    });
  }

  $scope.logic.save = function(obj) {
    TurmaService.save(obj, function() {
      $scope.logic.back();
    });
  }

  $scope.logic.remove = function(id) {
    TurmaService.remove(id, function() {
      $scope.logic.PaginationService.reload();
    });
  }

  $scope.logic.remove = function(id) {
    TurmaService.remove(id, function() {
      $scope.logic.back();
    });
  }
});
