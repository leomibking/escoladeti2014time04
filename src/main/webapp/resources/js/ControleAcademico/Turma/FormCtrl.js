(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams,$http, TurmaService, DisciplinaService) {

    $scope.entity = {};
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      $scope.turma = {};
      if(angular.isDefined(id)) {
          TurmaService.getEdit(id, function(data){
             $scope.turma = data;
          });
      }else{
        $scope.turma = {};
      }
    };

    $scope.logic.back = function() {
      $location.path('/turma');
    };

    $scope.logic.cancel = function() {
      $location.path('/turma');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      TurmaService.saveByDTO(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      TurmaService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams','$http', 'TurmaService'];

  angular.module('app')
  .controller('TurmaFormCtrl', FormCtrl);

})();