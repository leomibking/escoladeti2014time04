angular.module('app')

.controller('AtividadeModalCtrl', function($scope, turma, atividade, $modalInstance, ProfessorService, DisciplinaService, TurmaService, AtividadeService) {
  $scope.atividade = atividade;

  AtividadeService.query(function(data) {
    $scope.tiposAtividade = data;
  });

  ProfessorService.getByDTO(function(data) {
    $scope.professores = data;
  });

  DisciplinaService.query(function(data) {
    $scope.disciplinas = data;
  });

  $scope.adicionarTipo = function() {
    var nome = prompt('Digite um nome para o tipo da Atividade:');

    if ( ! nome) return;

    AtividadeService.save({
      tipoAtividade: nome
    }, function() {
      AtividadeService.query(function(data) {
        $scope.tiposAtividade = data;
      });
    });
  }

  $scope.salvar = function(atividade) {
    if (atividade.id) {
      var alunos = atividade.alunos;
      delete atividade.alunos;
    }

    TurmaService.salvarNovaAtividade(turma.id, atividade, function(data) {
      atividade.alunos = alunos;
      $modalInstance.close();
    });
  }

  $scope.cancelar = function () {
    $modalInstance.dismiss('cancel');
  }
})
