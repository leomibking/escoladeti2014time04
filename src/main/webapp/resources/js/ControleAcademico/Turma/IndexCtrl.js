(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, TurmaService, PaginationService) {

    $scope.logic = {};


    $scope.logic.show = function(id) {
      $location.path('/turma/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('/turma/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/turma/novo');
    };
  
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(TurmaService.getPagination);
    };
   
    $scope.logic.remove = function(id) {
      TurmaService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
    
    $scope.logic.save = function(turma){
      TurmaService
        .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

 
  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'TurmaService', 'PaginationService'];

  angular.module('app')
  .controller('TurmaIndexCtrl', IndexCtrl);

})();