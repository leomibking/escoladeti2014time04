(function(){
  'use strict';

  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/turma', {},
    {
      'pagination': {
        url:'rest/turma/pageList',
        method:'GET',
        params: {pageIndex: '@pageIndex', valor: '@valor'}
      },
      'get': { method: 'GET', params: {id: '@id'} },
      'save': { method: 'POST' },
      'query': { method: 'GET', isArray: true },
      'remove': { method: 'DELETE', params: {id: '@id'} },
      'getEdit': {
        url:'rest/turma/:id/editar',
        method:'GET',
        params: {id: '@id'}
      },
      'saveByDTO':{
          url:'rest/turma/salvar',
          method:'POST'
       },
       //turma
       'turmaShow':{
           url:'rest/turma/:id/show',
           method:'GET',
           params:{ id:'@id'}
       },

       //alunos
       'alunoPaginado':{
           url:'rest/turma/:id/aluno',
           method:'GET',
           params:{ id:'@id' , pageIndex:'@pageIndex'}
       },
       'alunoEdicao':{
           url:'rest/turma/:id/aluno/:idAluno',
           method:'GET',
           params:{ id:'@id' , idAluno:'@idAluno'}
       },
       'adicionarAluno':{
           url:'rest/turma/:id/aluno',
           method:'POST',
           params:{ id:'@id'}
       },
       'excluirAluno':{
           url:'rest/turma/:id/aluno/:idAluno',
           method:'DELETE',
           params:{id:'@id', idAluno: '@idAluno'}
       },
       'pesquisarAlunoPeloNome':{
           url:'rest/turma/aluno',
           method:'GET',
           isArray : true,
           params:{ nome:'@nome'}
       },

       //atividades
       'atividadePaginado':{
           url:'rest/turma/:id/atividade',
           method:'GET',
           params:{ id:'@id' , pageIndex:'@pageIndex'}
       },
       'atividadeEdicao':{
           url:'rest/turma/:id/atividade/:idAtividade',
           method:'GET',
           params:{ id:'@id' , idAtividade:'@idAtividade'}
       },
       'salvarNovaAtividade':{
           url:'rest/turma/:id/atividade',
           method:'POST',
           params:{ id:'@id'}
       },
       'salvarEdicaoAtividade':{
           url:'rest/turma/:id/atividade/:idAtividade',
           method:'POST',
           params:{ id:'@id', idAtividade:'@idAtividade'}
       },
       'excluirAtividade':{
           url:'rest/turma/:id/atividade/:idAtividade',
           method:'DELETE',
           params:{ id:'@id', idAtividade:'@idAtividade'}
       },


       //chamadas
       'chamadaPaginado':{
           url:'rest/turma/:id/chamada',
           method:'GET',
           params:{ id:'@id' , pageIndex:'@pageIndex'}
       },
       'chamadaEdicao':{
           url:'rest/turma/:id/chamada/:idChamada',
           method:'GET',
           params:{ id:'@id' , idChamada:'@idChamada'}
       },
       'salvarNovaChamada':{
           url:'rest/turma/:id/chamada',
           method:'POST',
           params:{ id:'@id'}
       },
       'salvarEdicaoChamada':{
           url:'rest/turma/:id/chamada/:idChamada',
           method:'POST',
           params:{ id:'@id', idChamada:'@idChamada'}
       },
       'excluirChamada':{
           url:'rest/turma/:id/chamada/:idChamada',
           method:'DELETE',
           params:{ id:'@id', idChamada:'@idChamada'}
       },
       'getTurmasSemProcesso':{
            url:'rest/turma/semprocesso',
            method:'GET',
            isArray: true
       }
    });

    Service.getTurmasSemProcesso = function(callback){
        resource.getTurmasSemProcesso(callback);
    };

    //alunos
    Service.getAlunoPaginado = function(id, index,callback){
        resource.alunoPaginado({ id: id, pageIndex : index}, callback);
    };

    Service.getAlunoEdicao = function(id, idAluno,callback){
        resource.alunoEdicao({ id: id, idAluno : idAluno}, callback);
    };

    Service.salvarAluno = function(aluno,callback){
        callback();
    };

    Service.excluirAluno = function(id, idAluno, callback) {
      resource.excluirAluno({id: id, idAluno: idAluno}, callback);
    };

    Service.adicionarAlunoNaTurma = function(id, aluno, callback){
        resource.adicionarAluno({id:id},aluno,callback);
    };

    Service.pesquisarAlunoPeloNome = function(val,callback){
        resource.pesquisarAlunoPeloNome({nome:val},callback);
    };


    //atividade
    Service.getAtividadePaginado = function(id, index,callback){
        resource.atividadePaginado({ id: id, pageIndex : index}, callback);
    };

    Service.getAtividadeEdicao = function(id, idAtividade,callback){
        resource.atividadeEdicao({ id: id, idAtividade : idAtividade}, callback);
    };

    Service.salvarNovaAtividade = function(id,atividade,callback){
        resource.salvarNovaAtividade({id : id},atividade,callback);
    };

    Service.salvarEdicaoAtividade = function(id,idAtividade,dados,callback){
        resource.salvarEdicaoAtividade({id:id, idAtividade:idAtividade},dados,callback);
    };

    Service.excluirAtividade = function(id, idAtividade,callback){
        resource.excluirAtividade({id:id,idAtividade:idAtividade},callback);
    };

    //chamada
    Service.getChamadaPaginado = function(id, index,callback){
        resource.chamadaPaginado({ id: id, pageIndex : index}, callback);
    };

    Service.getChamadaEdicao = function(id, idChamada,callback){
        resource.chamadaEdicao({ id: id, idChamada : idChamada}, callback);
    };

    Service.salvarNovaChamada = function(id,chamada,callback){
        resource.salvarNovaChamada({id : id},chamada,callback);
    };

    Service.salvarEdicaoChamada = function(id,idChamada,dados,callback){
        resource.salvarEdicaoChamada({id:id, idChamada:idChamada},dados,callback);
    };

    Service.excluirChamada = function(id, idChamada,callback){
        resource.excluirChamada({id:id,idChamada:idChamada},callback);
    };




    Service.getPagination = function(pageIndex, callback, filter) {
      resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
    };

    Service.get = function(id, callback){
      resource.turmaShow({ id: id }, callback);
    };

    Service.getEdit = function(id, callback){
      resource.getEdit({id: id}, callback);
    };

    Service.save = function(object, callback){
      resource.save(object, callback);
    };

    Service.query = function(callback) {
      resource.query(callback);
    };

    Service.remove = function(id, callback) {
      resource.remove({ id: id }, callback);
    };

    Service.saveByDTO = function(obj, callback){
        resource.saveByDTO(obj, callback);
    };

    return Service;
  };

  Factory.$inject = ['$resource'];

  angular.module('app')
  .factory('TurmaService', Factory);
})();