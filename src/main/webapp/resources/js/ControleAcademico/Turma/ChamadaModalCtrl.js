angular.module('app')

.controller('ChamadaModalCtrl', function($scope, turma, chamada, $modalInstance, ProfessorService, DisciplinaService, TurmaService) {
  $scope.chamada = chamada;

  ProfessorService.getByDTO(function(data) {
    $scope.professores = data;
  });

  DisciplinaService.query(function(data) {
    $scope.disciplinas = data;
  });

  $scope.salvar = function(chamada) {
    if (chamada.id) {
      var alunos = chamada.alunos;
      delete chamada.alunos;
    }

    TurmaService.salvarNovaChamada(turma.id, chamada, function() {
      chamada.alunos = alunos;
      $modalInstance.close();
    });
  }

  $scope.cancelar = function () {
    $modalInstance.dismiss('cancel');
  }
})
