angular.module('app').controller('RelatorioPresencaIndexCtrl', function($scope, EmpresaService, PaginationService) {
  $scope.PaginationService = PaginationService.createInstance();
  $scope.PaginationService.init(EmpresaService.getPagination);
});
