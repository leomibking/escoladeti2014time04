(function(){
  'use strict';

  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/relatoriopresenca', {},
    {
      'pagination': {
        url:'rest/relatoriopresenca/pageList',
        method:'GET',
        params: {pageIndex: '@pageIndex', valor: '@valor'}
      },
      'get': { method: 'GET', params: {id: '@id'} },
      'save': { method: 'POST' },
      'query': { method: 'GET', isArray: true },
      'remove': { method: 'DELETE', params: {id: '@id'} },
      'getEdit': {
        url:'rest/relatoriopresenca/:id/editar',
        method:'GET',
        params: {id: '@id'}
      },
      'saveByDTO':{
          url:'rest/relatoriopresenca/salvar',
          method:'POST'
       },

       //empresa
        'vagasDaEmpresa': {
            url:'rest/empresas/vagasEmpresa',
            method:'GET',
            isArray: true,
          params:{
              idEmpresa: '@idEmpresa'
          }
        },
        'listar': {
            url:'rest/empresas/listar',
            method:'GET',
            isArray : true
        },

       //alunos
       'alunoPaginado':{
           url:'rest/relatoriopresenca/:id/aluno',
           method:'GET',
           params:{ id:'@id' , pageIndex:'@pageIndex'}
       },
       'pesquisarAlunoPeloNome':{
           url:'rest/relatoriopresenca/aluno',
           method:'GET',
           isArray : true,
           params:{ nome:'@nome'}
       },

       //chamadas
       'chamadaPaginado':{
           url:'rest/relatoriopresenca/:id/chamada',
           method:'GET',
           params:{ id:'@id' , pageIndex:'@pageIndex'}
       },
       'chamadaEdicao':{
           url:'rest/relatoriopresenca/:id/chamada/:idChamada',
           method:'GET',
           params:{ id:'@id' , idChamada:'@idChamada'}
       },
       'salvarNovaChamada':{
           url:'rest/relatoriopresenca/:id/chamada',
           method:'POST',
           params:{ id:'@id'}
       }
    });

    //empresa
    Service.vagasDaEmpresa = function(idEmpresa, callback){
       console.log('Chamando vagas do service');
       resource.vagasDaEmpresa({idEmpresa: idEmpresa}, callback);
    };

    //chamada
    Service.getChamadaPaginado = function(id, index,callback){
        resource.chamadaPaginado({ id: id, pageIndex : index}, callback);
    };

    Service.getChamadaEdicao = function(id, idChamada,callback){
        resource.chamadaEdicao({ id: id, idChamada : idChamada}, callback);
    };


    Service.getPagination = function(pageIndex, callback, filter) {
      resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
    };

    Service.get = function(id, callback){
      resource.relatoriopresencaShow({ id: id }, callback);
    };

    Service.getEdit = function(id, callback){
      resource.getEdit({id: id}, callback);
    };

    Service.save = function(object, callback){
      resource.save(object, callback);
    };

    Service.query = function(callback) {
      resource.query(callback);
    };

    Service.remove = function(id, callback) {
      resource.remove({ id: id }, callback);
    };

    Service.saveByDTO = function(obj, callback){
        console.log(JSON.stringify(obj));
        resource.saveByDTO(obj, callback);
    };

    return Service;
  };

  Factory.$inject = ['$resource'];

  angular.module('app')
  .factory('RelatorioPresencaService', Factory);
})();