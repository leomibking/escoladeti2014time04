angular.findInArrayById = function(array, value, callback) {
  if(angular.isDefined(value) && angular.isObject(value)){
    for(var key in array) {
      if(angular.equals(array[key].id, value.id)){
        callback(array[key]);
        break;
      }
    }
  }
};

angular.appArrayRemove = function(array, value) {
  var index = array.indexOf(value);
  if (index >=0)
    array.splice(index, 1);
}

angular.module('app', [
  'ngRoute',
  'ngResource',
  'ngToast',
  'ngAnimate',
  'ui.bootstrap',
  'angular-loading-bar',
  'ngCookies',
  'ngCpfCnpj',
  'ngDragDrop',
  'ui.utils'
  ])

.run(function($rootScope) {
  $rootScope.$on('$routeChangeSuccess', function(event, data) {
    $rootScope.controller = data.controller;
  });
})

// progress bar
.config(['cfpLoadingBarProvider', 'ngToastProvider', function(cfpLoadingBarProvider, ngToastProvider) {
  // tempo minimo de uma requisão para mostra a barra - default=100ms
  cfpLoadingBarProvider.latencyThreshold = 0;
  // spinner TRUE = mostrar
  cfpLoadingBarProvider.includeSpinner = true;
  // loading bar TRUE = mostrar
  cfpLoadingBarProvider.includeBar = true;

  ngToastProvider.configure({
    verticalPosition: 'bottom',
    horizontalPosition: 'right',
    maxNumber: 3
  });
}]);
