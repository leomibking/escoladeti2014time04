(function(){
  'use strict';

  angular.module('app')
  .config(['$routeProvider', 'TemplateUrlProvider', function($routeProvider, TemplateUrlProvider) {
      // Processos Seletivos

      $routeProvider.when('/selecao/novo', {
       templateUrl: TemplateUrlProvider.build('ProcessoSeletivo/novo.html'),
       controller: 'NovoProcessoSeletivoCtrl',
       perfil : 'escola'
     });

      $routeProvider.when('/turma', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Turma/index.html'),
        controller: 'TurmaIndexCtrl',
        perfil : 'escola'
      });

      $routeProvider.when('/turma/novo', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Turma/form.html'),
        controller: 'TurmaFormCtrl',
        perfil : 'escola'
      });

      $routeProvider.when('/turma/:id', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Turma/show.html'),
        controller: 'TurmaShowCtrl',
        perfil : 'escola'
      });

      $routeProvider.when('/turma/:id/editar', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Turma/form.html'),
        controller: 'TurmaFormCtrl',
        perfil : 'escola'
      });

      // Relatorio de Presença

      $routeProvider.when('/relatoriopresenca', {
        templateUrl: TemplateUrlProvider.build('/ControleAcademico/RelatorioPresenca/index.html'),
        controller: 'RelatorioPresencaIndexCtrl',
        perfil : 'admin'
      });

      // Processo Seletivo

      $routeProvider.when('/processoseletivo', {
        templateUrl: TemplateUrlProvider.build('ProcessoSeletivo/index.html'),
        controller: 'ProcessoSeletivoIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/processoseletivo/novo', {
        templateUrl: TemplateUrlProvider.build('ProcessoSeletivo/form.html'),
        controller: 'ProcessoSeletivoFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/processoseletivo/:id', {
        templateUrl: TemplateUrlProvider.build('ProcessoSeletivo/show.html'),
        controller: 'ProcessoSeletivoShowCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/processoseletivo/:id/editar', {
        templateUrl: TemplateUrlProvider.build('ProcessoSeletivo/form.html'),
        controller: 'ProcessoSeletivoFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/contrato', {
        templateUrl: TemplateUrlProvider.build('Contrato/index.html'),
        controller: 'ContratoIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/contrato/:id', {
        templateUrl: TemplateUrlProvider.build('Contrato/show.html'),
        controller: 'ContratoShowCtrl',
        perfil: 'admin'
      });
      
     //Histórico 
      $routeProvider.when('/log', {
        templateUrl: TemplateUrlProvider.build('Log/index.html'),
        controller: 'AprendizLogIndexCtrl',
        perfil : 'admin'
      });
      
      $routeProvider.when('/log/:id', {
        templateUrl: TemplateUrlProvider.build('Log/show.html'),
        controller: 'LogShowCtrl',
        perfil : 'admin'
      });

    }]);
})();