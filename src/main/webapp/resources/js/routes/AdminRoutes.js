(function(){
  'use strict';

  angular.module('app')
  .config(['$routeProvider', 'TemplateUrlProvider', function($routeProvider, TemplateUrlProvider) {

      /* Painel Administrativo
      ======================================================================== */

      $routeProvider.when('/admin', {
        templateUrl: TemplateUrlProvider.build('Admin/admin.html'),
        perfil: 'admin'
      });

      // Controle de Acesso
      $routeProvider.when('/admin/usuarios', {
        templateUrl: TemplateUrlProvider.build('/Admin/ControleAcesso/index.html'),
        controller: 'UsuarioIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/usuarios/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/ControleAcesso/form.html'),
        controller: 'UsuarioFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/usuarios/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/ControleAcesso/form.html'),
        controller: 'UsuarioFormCtrl',
        perfil: 'admin'
      });

      // Profissões

      $routeProvider.when('/admin/profissoes', {
        templateUrl: TemplateUrlProvider.build('/Admin/Profissao/index.html'),
        controller: 'ProfissaoIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/profissoes/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/Profissao/form.html'),
        controller: 'ProfissaoFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/profissoes/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/Profissao/form.html'),
        controller: 'ProfissaoFormCtrl',
        perfil : 'admin'
      });

      // Relatórios

      $routeProvider.when('/relatorios', {
        templateUrl: TemplateUrlProvider.build('/Relatorio/index.html'),
        controller: 'RelatorioIndexCtrl',
        perfil : 'admin'
      });

      // Disciplina
      $routeProvider.when('/admin/disciplina', {
        templateUrl: TemplateUrlProvider.build('/Admin/Disciplina/index.html'),
        controller: 'DisciplinaIndexCtrl',
        perfil: 'escola'
      });

      $routeProvider.when('/admin/disciplina/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/Disciplina/form.html'),
        controller: 'DisciplinaFormCrud',
        perfil: 'escola'
      });

      $routeProvider.when('/admin/disciplina/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/Disciplina/form.html'),
        controller: 'DisciplinaFormCrud',
        perfil: 'escola'
      });

      // Cidades

      $routeProvider.when('/admin/cidades', {
        templateUrl: TemplateUrlProvider.build('/Admin/Cidade/index.html'),
        controller: 'CidadeIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/cidades/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/Cidade/form.html'),
        controller: 'CidadeFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/cidades/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/Cidade/form.html'),
        controller: 'CidadeFormCtrl',
        perfil : 'admin'
      });

      // UFs

      $routeProvider.when('/admin/ufs', {
        templateUrl: TemplateUrlProvider.build('Admin/UF/index.html'),
        controller: 'UFIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/ufs/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/UF/form.html'),
        controller: 'UFFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/ufs/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/UF/form.html'),
        controller: 'UFFormCtrl',
        perfil : 'admin'
      });

      // Paises

      $routeProvider.when('/admin/paises', {
        templateUrl: TemplateUrlProvider.build('Admin/Pais/index.html'),
        controller: 'PaisIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/paises/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/Pais/form.html'),
        controller: 'PaisFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/paises/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/Pais/form.html'),
        controller: 'PaisFormCtrl',
        perfil : 'admin'
      });

      // UFs

      $routeProvider.when('/admin/ufs', {
        templateUrl: TemplateUrlProvider.build('Admin/UF/index.html'),
        controller: 'UFIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/ufs/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/UF/form.html'),
        controller: 'UFFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/ufs/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/UF/form.html'),
        controller: 'UFFormCtrl',
        perfil : 'admin'
      });

      //Nacionalidade
      $routeProvider.when('/admin/nacionalidade', {
        templateUrl: TemplateUrlProvider.build('/Admin/Nacionalidade/index.html'),
        controller: 'NacionalidadeIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/nacionalidade/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/Nacionalidade/form.html'),
        controller: 'NacionalidadeFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/nacionalidade/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/Nacionalidade/form.html'),
        controller: 'NacionalidadeFormCtrl',
        perfil : 'admin'
      });


      //StatusProcessoSeletivo
      $routeProvider.when('/admin/statusProcesso', {
        templateUrl: TemplateUrlProvider.build('/Admin/StatusProcessoSeletivo/index.html'),
        controller: 'StatusProcessoIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/statusProcesso/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/StatusProcessoSeletivo/form.html'),
        controller: 'StatusProcessoFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/statusProcesso/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/StatusProcessoSeletivo/form.html'),
        controller: 'StatusProcessoFormCtrl',
        perfil : 'admin'
      });


      // Area Endereco

      $routeProvider.when('/admin/areaEndereco', {
        templateUrl: TemplateUrlProvider.build('Admin/AreaEndereco/index.html'),
        controller: 'AreaEnderecoIndexCtrl'
      });

      $routeProvider.when('/admin/areaEndereco/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/AreaEndereco/form.html'),
        controller: 'AreaEnderecoFormCtrl'
      });

      $routeProvider.when('/admin/areaEndereco/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/AreaEndereco/form.html'),
        controller: 'AreaEnderecoFormCtrl'
      });

      // Escolaridade

      $routeProvider.when('/admin/escolaridade', {
        templateUrl: TemplateUrlProvider.build('Admin/Escolaridade/index.html'),
        controller: 'EscolaridadeIndexCtrl'
      });

      $routeProvider.when('/admin/escolaridade/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/Escolaridade/form.html'),
        controller: 'EscolaridadeFormCtrl'
      });

      $routeProvider.when('/admin/escolaridade/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/Escolaridade/form.html'),
        controller: 'EscolaridadeFormCtrl'
      });

      // // Curso

      // $routeProvider.when('/admin/curso', {
      //   templateUrl: TemplateUrlProvider.build('Admin/Curso/index.html'),
      //   controller: 'CursoIndexCtrl'
      // });

      // $routeProvider.when('/admin/curso/novo', {
      //   templateUrl: TemplateUrlProvider.build('Admin/Curso/form.html'),
      //   controller: 'CursoFormCtrl'
      // });

      // $routeProvider.when('/admin/curso/:id/editar', {
      //   templateUrl: TemplateUrlProvider.build('Admin/Curso/form.html'),
      //   controller: 'CursoFormCtrl'
      // });


      // Contrato

      //TipoAdvertencia
      $routeProvider.when('/admin/tipoAdvertencia', {
        templateUrl: TemplateUrlProvider.build('/Admin/TipoAdvertencia/index.html'),
        controller: 'TipoAdvertenciaIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/tipoAdvertencia/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/TipoAdvertencia/form.html'),
        controller: 'TipoAdvertenciaFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/admin/tipoAdvertencia/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/TipoAdvertencia/form.html'),
        controller: 'TipoAdvertenciaFormCtrl',
        perfil : 'admin'
      });

    }]);
})();
