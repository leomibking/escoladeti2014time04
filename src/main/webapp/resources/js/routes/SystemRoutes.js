(function(){
  'use strict';

  angular.module('app')
  .config(['$routeProvider', 'TemplateUrlProvider', function($routeProvider, TemplateUrlProvider) {
    $routeProvider.when('/', {
      templateUrl: TemplateUrlProvider.build('home.html')
    });

    $routeProvider.when('/login', {
      templateUrl: TemplateUrlProvider.build('login.html'),
      controller: 'LoginCtrl',
      publicRoute: true
    })

    $routeProvider.otherwise({redirectTo: '/'});
  }])

  .run(function($rootScope, $window, $location, SessaoService) {
    $rootScope.$on("$routeChangeStart",function(event, next, current){
      if (SessaoService.user.dataExpiracao &&
        (new Date(SessaoService.user.dataExpiracao).getTime() < new Date().getTime())){
        alert('Usuário expirado');
      SessaoService.logout();
    }else if(!SessaoService.user.isLoggedIn){
      if (next.$$route && (next.$$route.originalPath != '/login')){
            // toaster.pop('warning','Usuário não logado','Você deve realizar o login para executar esta ação');
            $location.url('/login');
          }
        }else if ($location.$$path == '/login'){
          $location.url('/');
        }else if (!SessaoService.hasAccess(next.$$route.perfil) || next.$$route.publicRoute === false){
          $location.url('/');
        }
      });
  })
})();
