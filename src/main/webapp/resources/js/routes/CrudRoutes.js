(function(){
  'use strict';

  angular.module('app')
  .config(['$routeProvider', 'TemplateUrlProvider', function($routeProvider, TemplateUrlProvider) {

      // Aprendizes

      $routeProvider.when('/aprendizes', {
        templateUrl: TemplateUrlProvider.build('Aprendiz/index.html'),
        controller: 'AprendizIndexCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/aprendizes/novo', {
        templateUrl: TemplateUrlProvider.build('Aprendiz/form.html'),
        controller: 'AprendizesFormCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/aprendizes/:id', {
        templateUrl: TemplateUrlProvider.build('Aprendiz/show.html'),
        controller: 'AprendizShowCtrl',
        perfil : 'admin'
      });

      $routeProvider.when('/aprendizes/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Aprendiz/form.html'),
        controller: 'AprendizesFormCtrl',
        perfil : 'admin'
      });

      // Empresas

      $routeProvider.when('/empresas', {
        templateUrl: TemplateUrlProvider.build('Empresa/index.html'),
        controller: 'EmpresaIndexCtrl',
        perfil: 'empresa'
      });

      $routeProvider.when('/empresas/novo', {
        templateUrl: TemplateUrlProvider.build('Empresa/form.html'),
        controller: 'EmpresasFormCtrl',
        perfil: 'empresa'
      });

      $routeProvider.when('/empresas/:id', {
        templateUrl: TemplateUrlProvider.build('Empresa/show.html'),
        controller: 'EmpresaShowCtrl',
        perfil: 'empresa'
      });

      $routeProvider.when('/empresas/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Empresa/form.html'),
        controller: 'EmpresasFormCtrl',
        perfil: 'empresa'
      });

      // Escolas

      //Atividade
      $routeProvider.when('/admin/atividade', {
        templateUrl: TemplateUrlProvider.build('/Admin/Atividade/index.html'),
        controller: 'AtividadeIndexCtrl',
        perfil : 'escola'
      });

      $routeProvider.when('/admin/atividade/novo', {
        templateUrl: TemplateUrlProvider.build('/Admin/Atividade/form.html'),
        controller: 'AtividadeFormCtrl',
        perfil : 'escola'
      });

      $routeProvider.when('/admin/atividade/:id/editar', {
        templateUrl: TemplateUrlProvider.build('/Admin/Atividade/form.html'),
        controller: 'AtividadeFormCtrl',
        perfil : 'escola'
      });

      $routeProvider.when('/escolas', {
        templateUrl: TemplateUrlProvider.build('Escola/index.html'),
        controller: 'EscolaIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/escolas/novo', {
        templateUrl: TemplateUrlProvider.build('Escola/form.html'),
        controller: 'EscolasFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/escolas/:id', {
        templateUrl: TemplateUrlProvider.build('Escola/show.html'),
        controller: 'EscolaShowCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/escolas/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Escola/form.html'),
        controller: 'EscolasFormCtrl',
        perfil: 'admin'
      });

      // Area Endereco

      $routeProvider.when('/admin/areaEndereco', {
        templateUrl: TemplateUrlProvider.build('Admin/AreaEndereco/index.html'),
        controller: 'AreaEnderecoIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/areaEndereco/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/AreaEndereco/form.html'),
        controller: 'AreaEnderecoFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/areaEndereco/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/AreaEndereco/form.html'),
        controller: 'AreaEnderecoFormCtrl',
        perfil: 'admin'
      });

      // Fluencia de Idioma

      $routeProvider.when('/admin/fluenciaIdioma', {
        templateUrl: TemplateUrlProvider.build('Admin/FluenciaIdioma/index.html'),
        controller: 'FluenciaIdiomaIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/fluenciaIdioma/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/FluenciaIdioma/form.html'),
        controller: 'FluenciaIdiomaFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/fluenciaIdioma/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/FluenciaIdioma/form.html'),
        controller: 'FluenciaIdiomaFormCtrl',
        perfil: 'admin'
      });

      // Estado Civil

      $routeProvider.when('/admin/estadoCivil', {
        templateUrl: TemplateUrlProvider.build('Admin/EstadoCivil/index.html'),
        controller: 'EstadoCivilIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/estadoCivil/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/EstadoCivil/form.html'),
        controller: 'EstadoCivilFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/estadoCivil/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/EstadoCivil/form.html'),
        controller: 'EstadoCivilFormCtrl',
        perfil: 'admin'
      });

      // Escolaridade

      $routeProvider.when('/admin/escolaridade', {
        templateUrl: TemplateUrlProvider.build('Admin/Escolaridade/index.html'),
        controller: 'EscolaridadeIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/escolaridade/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/Escolaridade/form.html'),
        controller: 'EscolaridadeFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/escolaridade/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/Escolaridade/form.html'),
        controller: 'EscolaridadeFormCtrl',
        perfil: 'admin'
      });

      // Curso

      $routeProvider.when('/admin/curso', {
        templateUrl: TemplateUrlProvider.build('Admin/Curso/index.html'),
        controller: 'CursoIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/curso/novo', {
        templateUrl: TemplateUrlProvider.build('Admin/Curso/form.html'),
        controller: 'CursoFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/admin/curso/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Admin/Curso/form.html'),
        controller: 'CursoFormCtrl',
        perfil: 'admin'
      });

      // Professor

      $routeProvider.when('/professores', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Professor/index.html'),
        controller: 'ProfessorIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/professores/novo', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Professor/form.html'),
        controller: 'ProfessorFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/professores/:id/editar', {
        templateUrl: TemplateUrlProvider.build('ControleAcademico/Professor/form.html'),
        controller: 'ProfessorFormCtrl',
        perfil: 'admin'
      });

      //Etapa

      $routeProvider.when('/etapa', {
        templateUrl: TemplateUrlProvider.build('Etapa/index.html'),
        controller: 'EtapaIndexCtrl',
        perfil: 'escola'
      });

      $routeProvider.when('/etapa/novo', {
        templateUrl: TemplateUrlProvider.build('Etapa/form.html'),
        controller: 'EtapaFormCtrl',
        perfil: 'escola'
      });

       // $routeProvider.when('/etapa/:id', {
       //   templateUrl: TemplateUrlProvider.build('Etapa/show.html'),
       //   controller: 'EtapaShowCtrl'
       // });

      $routeProvider.when('/etapa/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Etapa/form.html'),
        controller: 'EtapaFormCtrl',
        perfil: 'escola'
      });

      //Documento

      $routeProvider.when('/documento', {
        templateUrl: TemplateUrlProvider.build('Documento/index.html'),
        controller: 'DocumentoIndexCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/documento/novo', {
        templateUrl: TemplateUrlProvider.build('Documento/form.html'),
        controller: 'DocumentoFormCtrl',
        perfil: 'admin'
      });

      $routeProvider.when('/documento/:id', {
       templateUrl: TemplateUrlProvider.build('Documento/show.html'),
       controller: 'DocumentoShowCtrl',
       perfil: 'admin'
     });

      $routeProvider.when('/documento/:id/editar', {
        templateUrl: TemplateUrlProvider.build('Documento/form.html'),
        controller: 'DocumentoFormCtrl',
        perfil: 'admin'
      });
    }]);
})();
