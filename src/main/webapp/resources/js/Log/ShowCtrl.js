(function() {
  "use strict";

  function ShowCtrl($scope, $location, $routeParams, LogService) {
    $scope.logic = {};

    $scope.logs = [];

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if (angular.isDefined(id)) {
        LogService.getLog(id, function(data) {
          $scope.logs = data;
        });
      }
    };

    // $scope.logic.getLogs = function() {
    //     LogService.getLog(function(data) {
    //         $scope.logs = data;
    //     });
    // };

    $scope.logic.back = function() {
      $location.path('/log');
    };

    // $scope.logic.cancel = function() {
    //   $location.path('/log');
    // };

  };

  ShowCtrl.$inject = ['$scope', '$location', '$routeParams', 'LogService'];

  angular.module('app')
  .controller('LogShowCtrl', ShowCtrl);

})();