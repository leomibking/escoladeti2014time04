angular.module('app').controller('AprendizLogIndexCtrl', function($scope, AprendizService, PaginationService) {
  $scope.PaginationService = PaginationService.createInstance();
  $scope.PaginationService.init(AprendizService.getPagination);
});
