angular.module('app')

.controller('EscolaShowCtrl', function($scope, $location, $routeParams, EscolaService, TypeService) {
  EscolaService.get({id: $routeParams.id}, function(data) {
    $scope.escola = data;
  })
});
