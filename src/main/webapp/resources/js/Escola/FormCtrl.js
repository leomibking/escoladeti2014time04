angular.module('app')

.controller('EscolasFormCtrl', function($scope, EscolaService, $routeParams, $location) {
  if ($routeParams.id) {
    EscolaService.get({id: $routeParams.id}, function(data) {
      $scope.escola = data;
    });
  } else {
    $scope.escola = new EscolaService({
      telefones: [],
      emails: [],
      enderecos: []
    });
  }

  $scope.save = function(escola) {
    angular.forEach(escola.enderecos, function(endereco) {
      delete endereco.uf;
      delete endereco.cidades;
    });

    escola.nome = escola.razaoSocial;

    escola.$save(function() {
      $location.path('/escolas');
    });
  }

  $scope.remover = function(escola) {
    EscolaService.remove({id: escola.id}, function(data) {
      $location.path('/escolas')
    });
  }
})
