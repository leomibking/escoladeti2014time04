angular.module('app').factory('EscolaService', function($resource) {
  var Service = {};

  var resource = $resource('rest/escolas', {}, 
  {
    'pagination': { 
      url:'rest/escolas/pageList',
      method:'GET',
      params: {pageIndex: '@pageIndex', valor: '@valor'} 
    },
    'get': { method: 'GET', params: {id: '@id'} },
    'save': { method: 'POST' },
    'query': { method: 'GET', isArray: true },
    'remove': { method: 'DELETE', params: {id: '@id'} },
    'listar': {
        url:'rest/escolas/listar',
        method:'GET',
        isArray : true
    }
  })

  resource.getPagination = function(pageIndex, callback, filter) {
    resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
  };

  return resource;
});

