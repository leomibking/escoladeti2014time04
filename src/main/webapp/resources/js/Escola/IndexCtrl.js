angular.module('app')
  
.controller('EscolaIndexCtrl', function($scope, $location, $routeParams, EscolaService, PaginationService) {
  $scope.PaginationService = PaginationService.createInstance();
  $scope.PaginationService.init(EscolaService.getPagination);
});
