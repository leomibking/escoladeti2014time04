angular.module('app').factory('EmpresaService', function($resource) {
  var Service = {};

  var resource = $resource('rest/empresas', {}, 
  {
    'pagination': { 
      url:'rest/empresas/pageList',
      method:'GET',
      params: {pageIndex: '@pageIndex', valor: '@valor'} 
    },
    'get': { method: 'GET', params: {id: '@id'} },
    'save': { method: 'POST' },
    'query': { method: 'GET', isArray: true },
    'remove': { method: 'DELETE', params: {id: '@id'} },
    'salvarVaga': { 
      url:'rest/empresas/salvarVaga',
      method:'POST'
    },
    'removerVaga': { 
      url:'rest/empresas/removerVaga',
      method:'DELETE',
      params: {
        idEmpresa: '@idEmpresa',
        idVaga: '@idVaga'
      }
    },
    'vagasDaEmpresa': {
        url:'rest/empresas/vagasEmpresa',
        method:'GET',
        isArray: true,
        params:{
          idEmpresa: '@idEmpresa'
        }
    },
    'listar': {
        url:'rest/empresas/listar',
        method:'GET',
        isArray : true
    }
  })

  resource.getPagination = function(pageIndex, callback, filter) {
    resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
  };

  // Service.getPagination = function(pageIndex, callback, filter) {
  //   resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
  // };

  // Service.salvarVaga = function(object, callback){
  //   resource.salvarVaga(object, callback);
  // };

  // Service.removerVaga = function(idEmpresa, idVaga, callback) {
  //   resource.removerVaga({
  //     idEmpresa: idEmpresa,
  //     idVaga: idVaga
  //   }, callback);
  // };
  
   Service.vagasDaEmpresa = function(idEmpresa, callback){
       resource.vagasDaEmpresa({idEmpresa: idEmpresa}, callback);
   };

  // Service.get = function(id, callback){
  //   resource.get({ id: id }, callback);
  // };
  
  // Service.save = function(object, callback){
  //   resource.save(object, callback);
  // };
  
  // Service.query = function(callback) {
  //   resource.query(callback);
  // };
  
  // Service.listar = function(callbak){
  //   resource.listar(callbak)  ;
  // };

  // Service.remove = function(id, callback) {
  //   resource.remove({ id: id }, callback);
  // };

  return resource;
});

