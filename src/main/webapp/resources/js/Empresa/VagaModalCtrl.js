angular.module('app')

.controller('EmpresaVagaModalCtrl', function($scope, $modalInstance, vaga, empresa, ProfissaoService, TypeService, EmpresaService, $routeParams) {
  $scope.vaga = vaga;

  ProfissaoService.query(function(data) {
    $scope.profissoes = data;
  })

  TypeService.periodo(function(data) {
    $scope.periodos = data;
  });

  $scope.salvar = function(vaga) {
    var vagaCorreta = angular.copy(vaga);
    vagaCorreta.idVaga = vaga.id;
    vagaCorreta.idEmpresa = empresa.id;
    vagaCorreta.id = undefined;
    vagaCorreta.status = undefined;

    EmpresaService.salvarVaga(vagaCorreta, function() {
      $modalInstance.close();
    });
  }

  $scope.cancelar = function () {
    $modalInstance.close();
  }
});
