angular.module('app')

.controller('EmpresaShowCtrl', function($scope, $location, $routeParams, EmpresaService, ProfissaoService, TypeService, $modal) {
  EmpresaService.get({id: $routeParams.id}, function(data) {
    $scope.empresa = data;
  })

  $scope.adicionarVaga = function() {
    $modal.open({
      templateUrl: 'resources/partials/Empresa/vaga-modal.html',
      controller: 'EmpresaVagaModalCtrl',
      resolve: {
        empresa: function() { return $scope.empresa; },
        vaga: function() { return {}; }
      }
    }).result.then(function() {
      EmpresaService.get({id: $routeParams.id}, function(data) {
        $scope.empresa = data;
      });
    });
  };

  $scope.removerVaga = function(vaga, index) {
    EmpresaService.removerVaga({
      idEmpresa: $scope.empresa.id,
      idVaga: vaga.id
    }, function() {
      EmpresaService.get({id: $routeParams.id}, function(data) {
        $scope.empresa = data;
      });
    });
  };

  $scope.editarVaga = function(vaga) {
    $modal.open({
      templateUrl: 'resources/partials/Empresa/vaga-modal.html',
      controller: 'EmpresaVagaModalCtrl',
      resolve: {
        empresa: function() { return $scope.empresa; },
        vaga: function() { return vaga; }
      }
    }).result.then(function() {
      EmpresaService.get({id: $routeParams.id}, function(data) {
        $scope.empresa = data;
      });
    });
  };
});
