angular.module('app')
  
.controller('EmpresaIndexCtrl', function($scope, $location, $routeParams, EmpresaService, PaginationService) {
  $scope.PaginationService = PaginationService.createInstance();
  $scope.PaginationService.init(EmpresaService.getPagination);
});
