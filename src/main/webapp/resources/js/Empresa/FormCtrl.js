angular.module('app')

.controller('EmpresasFormCtrl', function($scope, EmpresaService, $routeParams, $location) {

  if ($routeParams.id) {
    EmpresaService.get({id: $routeParams.id}, function(data) {
      $scope.empresa = data;
    });
  } else {
    $scope.empresa = new EmpresaService({
      telefones: [],
      emails: [],
      enderecos: []
    });
  }

  $scope.save = function(empresa) {
    angular.forEach(empresa.enderecos, function(endereco) {
      delete endereco.uf;
      delete endereco.cidades;
    });

    empresa.$save(function() {
      $location.path('/empresas');
    });
  }

  $scope.remover = function(empresa) {
    EmpresaService.remove({id: empresa.id}, function(data) {
      $location.path('/empresas')
    });
  }
})
