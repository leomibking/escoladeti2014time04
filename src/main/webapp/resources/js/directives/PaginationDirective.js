(function(){
  "use strict";

  function Service() {
    var _service = {};
    _service.filter = "";

    var getPagination = null;
    var page = {};

    _service.setFilter = function(filter){
      _service.filter = filter;
      _service.get(1);
    };

    _service.getPage = function() {
      return page;
    };

    _service.getIndexs = function() {
      return page.indexList;
    };

    _service.getItens = function() {
      return page.itemList;
    };

    _service.get = function(pageIndex) {
      getPagination(pageIndex, function(data){
        page = data;
      },_service.filter);
    };

    _service.reload = function() {
      this.get(page.currentPage);
    };

    _service.init = function(ServicePaginationMethod){
      getPagination = ServicePaginationMethod;
      this.get(1);
    };

    _service.bof = function() {
      return page.currentPage == 1;
    };

    _service.eof = function() {
      return page.currentPage == Math.max(page.totalPage, 1);
    };

    _service.isActive = function(pageIndex) {
      return page.currentPage == pageIndex;
    };

    _service.first = function() {
      if(this.bof()){return;};
      this.get(1);
    };

    _service.back = function() {
      if(this.bof()){return;};
      this.get(--page.currentPage);
    };

    _service.next = function() {
      if(this.eof()){return;};
      this.get(++page.currentPage);
    };

    _service.last = function() {
      if(this.eof()){return;};
      this.get(page.totalPage);
    };

    return _service;

  }

  function Factory () {
    var _factory = {};

    _factory.createInstance = Service;
    return _factory;
  }

  function Directive () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
      '<div class="text-center">',
      '<ul class="pagination">',
      '<li data-ng-class="{disabled: service.bof()}">',
      '<a data-ng-click="service.first()">&laquo;</a>',
      '</li>',
      '<li data-ng-class="{disabled: service.bof()}">',
      '<a data-ng-click="service.back()">&lsaquo;</a>',
      '</li>',
      '<li data-ng-class="{active: service.isActive(pageIndex)}" ',
      'data-ng-repeat="pageIndex in service.getIndexs()">',
      '<a data-ng-click="service.get(pageIndex)">{{pageIndex}}</a>',
      '</li>',
      '<li data-ng-class="{disabled: service.eof()}">',
      '<a data-ng-click="service.next()">&rsaquo;</a>',
      '</li>',
      '<li data-ng-class="{disabled: service.eof()}">',
      '<a data-ng-click="service.last()">&raquo;</a>',
      '</li>',
      '</ul>',
      '</div>'
      ].join(''),
      scope: {
        service: "=paginationService"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveSearch () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<div class="input-group" style="margin-bottom: 5px;">',
          '<input type="text" class="form-control input-sm" placeholder="Buscar por..." ng-model="filtroDePesquisa" ng-enter="service.setFilter(filtroDePesquisa)" />',
          '<span class="input-group-btn">',
            '<button class="btn btn-sm btn-default" ng-click="service.setFilter(filtroDePesquisa)"><i class="fa fa-search"></i></button>',
          '</span>',
        '</div>'
      ].join(''),
      scope: {
        service: "=paginationService"
      },
      link: function(scope) {
      }
    };
  }

  /* https://gist.github.com/EpokK/5884263 */
  function ngEnter () {
    return function (scope, element, attrs) {
      element.bind("keydown keypress", function (event) {
        if(event.which === 13) {
          scope.$apply(function (){
            scope.$eval(attrs.ngEnter);
          });
          event.preventDefault();
        }
      });
    };
  }

  angular.module('app')
  .factory('PaginationService', Factory)
  .directive('paginationControls', Directive)
  .directive('paginationSearch', DirectiveSearch)
  .directive('ngEnter', ngEnter);

})();