angular.module('app')

.directive('emailIndex', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/email/index.html'
  }
});
