angular.module('app')

.directive('emailModal', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/email/modal.html',
    controller: function($scope) {
      $scope.logic.email = {};
      $scope.email = {};

      $scope.logic.email.save = function(email) {
        var emailEditavel = email;
        if(emailEditavel.edit == true) {
          angular.forEach($scope.emails, function(index) {
            if (emailEditavel.id == index.id) return index.email = emailEditavel.email
          });
        } else {
          $scope.emails.push(email);
        }

        $('#emailModal').modal('hide');
        $scope.email = {};
      };

      $scope.logic.email.edit = function(email) {
        $scope.email = {id: email.id, email: email.email, edit: true};
      };

      $scope.logic.email.remove = function(index) {
        if(confirm('Tem certeza?')) return $scope.emails.splice(index, 1);
      };

      $scope.clearInput = function() {
        $scope.email = {}
      }
    }
  }
});
