(function(){
  "use strict";

  function DirectivePanel () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
        '<div id="form-panel" class="panel" ng-transclude>',
        '</div>'
      ].join(''),
      scope: {},
      link: function(scope) {
      }
    };
  }

  function DirectiveHeading () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
        '<div id="form-panel-heading" class="panel-heading" ng-transclude>',
        '</div>'
      ].join(''),
      scope: {},
      link: function(scope) {
      }
    };
  }

  function DirectiveBody () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
        '<div id="form-panel-body" class="panel-body" ng-transclude>',
        '</div>'
      ].join(''),
      scope: {},
      link: function(scope) {
      }
    };
  }

  function DirectiveFooter () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
        '<div id="form-panel-footer" class="panel-footer" ng-transclude>',
        '</div>'
      ].join(''),
      scope: {},
      link: function(scope) {
      }
    };
  }

  angular.module('app')
    .directive('formPanel', DirectivePanel)
    .directive('formHeading', DirectiveHeading)
    .directive('formBody', DirectiveBody)
    .directive('formFooter', DirectiveFooter);
})();