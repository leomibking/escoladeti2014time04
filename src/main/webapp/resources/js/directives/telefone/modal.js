angular.module('app')

.directive('telefoneModal', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/telefone/modal.html',
    controller: function($scope) {
      $scope.logic.telefone = {};
      $scope.telefone = {};

      $scope.logic.telefone.save = function(telefone) {
        var telefoneEditavel = telefone;
        if (telefoneEditavel.edit == true) {
          angular.forEach($scope.telefones, function(index) {
            if (telefoneEditavel.id == index.id) {
              index.tipo = telefoneEditavel.tipo;
              index.ddd = telefoneEditavel.ddd;
              index.numero = telefoneEditavel.numero;
            }
          });
        } else {
          $scope.telefones.push(telefone);
        }

        $('#telefoneModal').modal('hide');
        $scope.telefone = {};
      };

      $scope.logic.telefone.edit = function(telefone) {
        var telefoneCopy = angular.copy(telefone);
        telefoneCopy.edit = true;
        $scope.telefone = telefoneCopy;
      };

      $scope.logic.telefone.remove = function(index) {
        if (confirm('Tem certeza?')) return $scope.telefones.splice(index, 1);
      };

      $scope.clearInput = function() {
        $scope.telefone = {}
      }
    }
  }
});
