angular.module('app')

.directive('telefoneIndex', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/telefone/index.html'
  }
});
