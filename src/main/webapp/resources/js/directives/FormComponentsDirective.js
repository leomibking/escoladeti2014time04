(function(){
  "use strict";

  function DirectiveHeaderContent () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
          '<span class="h3">',
            '<span>',
              '<span class="{{_Icon}}"></span>',
              '{{_Title}}',
            '</span>',
            '<span ng-transclude></span>',
          '</span>'
        ].join(''),
      scope: {
        _Title: "@title",
        _Icon: "@icon"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveNewButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_New()" class="btn btn-primary btn-sm pull-right">',
          'Novo',
        '</a>'
        ].join(''),
      scope: {
        _New: "&onNew"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveSaveButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_Save()" class="btn btn-success btn-sm">',
          'Salvar',
        '</a>'
        ].join(''),
      scope: {
        _Save: "&onSave"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveCancelButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_Cancel()" class="btn btn-warning btn-sm">',
          'Cancelar',
        '</a>'
        ].join(''),
      scope: {
        _Cancel: "&onCancel"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveEditButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_Edit()" class="btn btn-link">',
          '<span class="glyphicon glyphicon-pencil">',
          '</span>',
        '</a>'
        ].join(''),
      scope: {
        _Edit: "&onEdit"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveShowButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_Show()" class="btn btn-link">',
          '<span class="glyphicon glyphicon-eye-open">',
          '</span>',
        '</a>'
        ].join(''),
      scope: {
        _Show: "&onShow"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveRemoveButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_Remove()" class="btn btn-link">',
          '<span class="glyphicon glyphicon-remove">',
          '</span>',
        '</a>'
        ].join(''),
      scope: {
        _Remove: "&onRemove"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveReturnButton () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      template: [
        '<a data-ng-click="_Return()" class="btn btn-primary btn-sm">',
          'Voltar',
        '</a>'
        ].join(''),
      scope: {
        _Return: "&onReturn"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveFormCrudControls () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
        '<div class="row text-center">',
          '<div class="col-sm-12">',
            '<span ng-transclude></span>',
            '<form-save-button data-on-save="_Save()"></form-save-button>',
            '<form-cancel-button data-on-cancel="_Cancel()"></form-cancel-button>',
          '</div>',
        '</div>'
        ].join(''),
      scope: {
        _Save: "&onSave",
        _Cancel: "&onCancel"
      },
      link: function(scope) {
      }
    };
  }

  function DirectiveShowCrudControls () {
    return {
      restrict: 'E',
      replace: true,
      transclude: true,
      template: [
        '<div class="row text-center">',
          '<div class="col-sm-12">',
            '<span ng-transclude></span>',
            '<form-edit-button data-on-edit="_Edit()"></form-edit-button>',
            '<form-remove-button data-on-remove="_Remove()"></form-remove-button>',
            '<form-return-button data-on-return="_Return()"></form-return-button>',
          '</div>',
        '</div>'
        ].join(''),
      scope: {
        _Edit: "&onEdit",
        _Remove: "&onRemove",
        _Return: "&onReturn"
      },
      link: function(scope) {
      }
    };
  }

  angular.module('app')
    .directive('formHeaderContent', DirectiveHeaderContent)
    .directive('formNewButton', DirectiveNewButton)
    .directive('formSaveButton', DirectiveSaveButton)
    .directive('formCancelButton', DirectiveCancelButton)
    .directive('formEditButton', DirectiveEditButton)
    .directive('formShowButton', DirectiveShowButton)
    .directive('formRemoveButton', DirectiveRemoveButton)
    .directive('formReturnButton', DirectiveReturnButton)
    .directive('formShowCrudControls', DirectiveShowCrudControls)
    .directive('formFormCrudControls', DirectiveFormCrudControls);
})();