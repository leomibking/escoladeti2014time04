(function(){
  "use strict";

  // function ControllerOneToMany($scope) {
  //   // if(!angular.isArray($scope._EntityVariable)){
  //   //   lançar exception!
  //   // }
  //   var _DescriptionField;
  //   //fica fora para não chamar em toda interação
  //   var selectitems = [];

  //   var _init = function() {
  //     _DescriptionField = $scope._DescriptionField;
  //     //extrair para Provider
  //     if(_DescriptionField == undefined) {_DescriptionField = 'descricao';};
  //     $scope._Service.query(function(data) {
  //       selectitems = data;
  //     });
  //   }

  //   var _getItemList = function() {
  //     return $scope._EntityVariable;
  //   };

  //   var _existsItem = function(item) {
  //     for(var key in _getItemList()){
  //       if(item.id == _getItemList()[key].id){
  //         return true;
  //       }
  //     }
  //     return false;
  //   }

  //   //filtra os item já presentes na listagem para não readicionar
  //   var _getSelectList = function () {
  //     if(_getItemList() === undefined){return [];};
  //     return selectitems.filter(function(item) {
  //       // return $scope._EntityVariable.indexOf(item) === -1;
  //       return !_existsItem(item);
  //     });
  //   }

  //   var _add = function(item) {
  //     if((item == undefined)||(item == null)||(_existsItem(item))){return;};
  //     _getItemList().push(item);
  //   }

  //   var _remove = function(item) {
  //     var index = _getItemList().indexOf(item);
  //     if(index > -1){
  //       _getItemList().splice(index, 1);
  //     }
  //   }

  //   var _getDescriptionOfItem = function(item){
  //     return item[_DescriptionField];
  //   }

  //   this.init = _init;
  //   this.getItemList = _getItemList;
  //   this.getSelectList = _getSelectList;
  //   this.add = _add;
  //   this.remove = _remove;
  //   this.getDescriptionOfItem = _getDescriptionOfItem;
  // }

  // function DirectiveOneToMany () {
  //   return {
  //     restrict: 'E',
  //     replace: true,
  //     transclude: false,
  //     templateUrl: 'resources/framework/partials/onetomany.html',
  //     scope: {
  //       _Service: '=service',
  //       _EntityVariable: '=entityVariable',
  //       _DescriptionField: '@descriptionField'
  //     },
  //     link: function(scope) {},
  //     controller: ControllerOneToMany,
  //     controllerAs: 'directiveCtrl'
  //   };
  // }

  // //---------------------------------------------------------------------------

  // function ControllerOneToOne($scope) {
  //   var _DescriptionField;
  //   //fica fora para não chamar em toda interação
  //   var selectitems = [];

  //   var _init = function() {
  //     _DescriptionField = $scope._DescriptionField;
  //     //extrair para Provider
  //     if(_DescriptionField == undefined) {_DescriptionField = 'descricao';};
  //     $scope._Service.query(function(data) {
  //       selectitems = data;
  //     });
  //   }

  //   var _getItem = function() {
  //     return $scope._EntityVariable;
  //   };

  //   var _existsItem = function(item) {
  //     for(var key in _getItem()){
  //       if(item.id == _getItem().id){
  //         return true;
  //       }
  //     }
  //     return false;
  //   }

  //   //filtra os item já presentes na listagem para não readicionar
  //   var _getSelectList = function () {
  //     if(_getItem() === undefined){return [];};
  //     return selectitems.filter(function(item) {
  //       return !_existsItem(item);
  //     });
  //   }

  //   var _getDescriptionOfItem = function(item){
  //     return item[_DescriptionField];
  //   }

  //   this.init = _init;
  //   this.getItem = _getItem;
  //   this.getSelectList = _getSelectList;
  //   this.getDescriptionOfItem = _getDescriptionOfItem;
  // }

  // function DirectiveOneToOne () {
  //   return {
  //     restrict: 'E',
  //     replace: true,
  //     transclude: false,
  //     templateUrl: 'resources/framework/partials/onetoone.html',
  //     scope: {
  //       _Service: '=service',
  //       _EntityVariable: '=entityVariable',
  //       _DescriptionField: '@descriptionField'
  //     },
  //     link: function(scope) {},
  //     controller: ControllerOneToOne,
  //     controllerAs: 'directiveCtrl'
  //   };
  // }

  //---------------------------------------------------------------------------

  function ControllerManyToOne($scope) {
    var _this = this;

    _this.selectItems = [];

    // angular.findObjectInArray = function(array, value, callback) {
    //   if(angular.isDefined(value) && angular.isObject(value)){
    //     for(var key in array) {
    //       if(angular.equals(array[key].id, value.id)){
    //         callback(array[key]);
    //         break;
    //       }
    //     }
    //   }
    // }

    // angular.existsInArray = function(array, value) {
    //   var exists = false;
    //   angular.findObjectInArray(array, value, function(){
    //     exists = true;
    //   });
    //   return exists;
    //   // if(angular.isDefined(value) && angular.isObject(value)){
    //   //   for(var key in array) {
    //   //     if(angular.equals(array[key].id, value.id)){
    //   //       return true;
    //   //       break;
    //   //     }
    //   //   }
    //   //   return false;
    //   // }
    // }

    _this.init = function() {
      // $scope._Service[$scope._ServiceMethod](function(data) {
      //   _this.selectItems = data;
      //   angular.findObjectInArray(_this.selectItems, $scope._EntityVariable, function(item){
      //     _this.itemSelect = item;
      //   });
      // });

      _this.selectItems = $scope._GetList();
      angular.findObjectInArray(_this.selectItems, $scope._EntityVariable, function(item){
        _this.itemSelect = item;
      });

      _this.descriptionField = $scope._DescriptionField;
      if(angular.isUndefined(_this.descriptionField)) {
        _this.descriptionField = 'descricao';
      }

      //para sempre chamar, sem fazer if toda vez.
      _this._ChangeHandle = $scope._ChangeHandle;
      if(angular.isUndefined(_this._ChangeHandle)){
        _this._ChangeHandle = function(){};
      }
    }

    _this.getDescriptionOfItem = function(item){
      return item[_this.descriptionField];
    }

    _this.updateEntity = function() {
      $scope._EntityVariable = _this.itemSelect;
      _this._ChangeHandle();
    }

  }

  function DirectiveManyToOne () {
    return {
      restrict: 'E',
      replace: true,
      transclude: false,
      templateUrl: 'resources/framework/partials/manytoone.html',
      scope: {
        _Service: '=service',
        _ServiceMethod: '@serviceMethod',
        _GetList: '&onGetList',
        _EntityVariable: '=entityVariable',
        _DescriptionField: '@descriptionField',
        _ChangeHandle: '&onUpdate',
      },
      controller: ControllerManyToOne,
      controllerAs: 'directiveCtrl'
    };
  }

  angular.module('app')
    // .directive('oneToOne', DirectiveOneToOne)
    .directive('manyToOne', DirectiveManyToOne)
    // .directive('oneToMany', DirectiveOneToMany);
  })();