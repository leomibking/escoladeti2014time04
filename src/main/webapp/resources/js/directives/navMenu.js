angular.module('app')

.directive('navMenu', function($location) {
  return function(scope, element, attrs) {
    var url = element.attr('href') || element.find('a').attr('href');
    var exactMatch = scope.$eval(attrs.navMenuExactMatch);

    // remove hash in case of non html5 mode
    url = url.replace(/^#/, '');

    var regex = new RegExp('^' + url);

    // Esse if ta muito feio... tô com vergonha =( mas sem tempo de refatorar (GO HORSE FTW)
    var updateElement = function() {
      if (exactMatch) {
        if (url === $location.path()) {
          element.addClass('active');
        } else {
          element.removeClass('active');
        }
      } else {
        if ( regex.test($location.path()) ) {
          element.addClass('active');
        } else {
          element.removeClass('active');
        }
      }
    }

    scope.$on('$routeChangeStart', updateElement);

    updateElement();
  }
});
