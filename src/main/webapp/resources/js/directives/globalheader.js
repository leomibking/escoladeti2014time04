angular.module('app')

.directive('globalheader', function() {
  return {
    restrict: 'E',
    scope: {},
    templateUrl: "resources/partials/navbar.html",
    controller: function($scope, SessaoService) {
      $scope.sessao = SessaoService
    }
  }
})
