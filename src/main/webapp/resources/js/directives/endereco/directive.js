angular.module('app')

.directive('enderecoForm', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/enderecoForm/template.html',
    controller: function($scope, UFService, CidadeService) {
      var BRASIL = {id: 1};

      var cidadeSelecionada = $scope.endereco.cidade;
      var ufSelecionado = cidadeSelecionada ? cidadeSelecionada.uf : null;
      var pais = ufSelecionado ? ufSelecionado.pais : BRASIL;

      if ( ! $scope.endereco.area) {
        $scope.endereco.area = 'URBANA';
      }

      $scope.getUFs = function(pais) {
        UFService.porPais({paisId: pais.id}, function(data) {
          $scope.ufs = data;

          if (ufSelecionado) {
            angular.forEach($scope.ufs, function(uf) {
              if (uf.id === ufSelecionado.id) {
                $scope.uf = uf;
                $scope.getCidades(uf);
              }
            });
          }
        });
      }

      $scope.getCidades = function(uf) {
        CidadeService.porUF({UFId: uf.id}, function(data) {
          $scope.cidades = data;

          if (cidadeSelecionada) {
            angular.forEach($scope.cidades, function(cidade) {
              if (cidade.id === cidadeSelecionada.id) {
                $scope.endereco.cidade = cidade;
              }
            });
          }
        });
      }

      $scope.getUFs(pais);
    }
  }
});