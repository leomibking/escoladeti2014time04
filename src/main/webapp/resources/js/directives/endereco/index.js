angular.module('app')

.directive('enderecoIndex', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/endereco/index.html'
  }
});
