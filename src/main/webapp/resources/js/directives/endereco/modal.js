angular.module('app')

.directive('enderecoModal', function() {
  return {
    restrict: 'E',
    templateUrl: 'resources/js/directives/endereco/modal.html',
    controller: function($scope, CidadeService, UFService) {
      $scope.logic.endereco.atualizaCidade = function(uf) {
        if(angular.isUndefined(uf)){
          $scope.logic.cidades = [];
          return;
        }
        
        CidadeService.porUF(uf.id, function(data) {
          $scope.logic.cidades = data;
          angular.findInArrayById(data, $scope.endereco.cidade, function(item){
            $scope.endereco.cidade = item;
          });
        });
      };

      $scope.logic.endereco.atualizaUF = function() {
        UFService.query(function(data) {
          $scope.logic.ufs = data;
          angular.findInArrayById(data, $scope.logic.endereco.uf, function(item){
            $scope.logic.endereco.uf = item;
          });
          $scope.logic.endereco.atualizaCidade($scope.logic.endereco.uf);
        });
      };

      $scope.logic.endereco.edit = function(endereco){
        var enderecoCopy = angular.copy(endereco);
        enderecoCopy.edit = true;
        $scope.endereco = enderecoCopy;
      };

      $scope.logic.endereco.save = function(endereco){
        var enderecoEditavel = endereco;
        if (enderecoEditavel.edit == true) {
          angular.forEach($scope.enderecos, function(index) {
            if (enderecoEditavel.id == index.id) {
              index.cep = enderecoEditavel.cep;
              index.area = enderecoEditavel.area;
              index.uf = enderecoEditavel.uf;
              index.cidade = enderecoEditavel.cidade;
              index.logradouro = enderecoEditavel.logradouro;
              index.bairro = enderecoEditavel.bairro;
              index.complemento = enderecoEditavel.complemento;
              index.numero = enderecoEditavel.numero;
            }
          });
        } else {
          $scope.enderecos.push(endereco);
        }

        $('#enderecoModal').modal('hide');
        $scope.logic.endereco.edit({});
      };

      $scope.logic.endereco.remove = function(index) {
        if (confirm('Tem certeza?')) return $scope.enderecos.splice(index, 1);
      };
    }
  }
});
