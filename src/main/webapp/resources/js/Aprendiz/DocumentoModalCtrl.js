angular.module('app')

.controller('AprendizDocumentoModalCtrl', function($scope, $modalInstance, documento, aprendiz, AprendizService, $routeParams) {
  $scope.documento = documento;

  $scope.salvar = function(documento) {
    documento.idAprendiz = aprendiz.id;

    AprendizService.salvarDocumento(documento, function() {
      $modalInstance.close();
    });
  }

  $scope.cancelar = function () {
    $modalInstance.dismiss('cancel');
  }
});
