angular.module('app')

.directive('enderecoFoda', function(UFService, CidadeService, CEPService) {
  return {
    restrict: 'E',
    replace: true,
    scope: {para: '=para'},
    templateUrl: 'resources/js/directives/endereco-foda.html',
    link: function(scope, element, attrs) {
      scope.$watch('para', function(newValue) {
        if (newValue) {
          angular.forEach(scope.para.enderecos, function(endereco) {
            endereco.uf = endereco.cidade.uf;
            scope.getCidadesByUF(endereco.uf, endereco);
          });
        }
      });

      scope.consultaCEP = function(endereco) {
        if (!endereco.cep || endereco.cep.length < 8) return;

        CEPService.get(endereco.cep, function(data) {
          var a = scope.ufs;
          var u = data.uf;
          var l = a.length;

          for (var i = 0; i < l; i++) {
            if (a[i].sigla == u) {
              endereco.uf = a[i];
              break;
            }
          }

          scope.getCidadesByUF(endereco.uf, endereco, function() {
            var a = endereco.cidades;
            var c = data.cidade;
            var l = a.length;

            for (var i = 0; i < l; i++) {
              if(a[i].nome == c) {
                endereco.cidade = a[i];
                break;
              }
            }
          })

          endereco.bairro = data.bairro;
          endereco.logradouro = data.logradouro;
        });
      }

      scope.areaEnderecoTipos = [
        {value: 'URBANA', descricao: 'Área urbana'},
        {value: 'RURAL', descricao: 'Área rural'}
      ];

      UFService.query(function(data) {
        scope.ufs = data;
      });

      scope.getCidadesByUF = function(uf, endereco, cb) {
        CidadeService.porUF(uf.id, function(data) {
          endereco.cidades = data;
          if (angular.isDefined(cb) && angular.isFunction(cb)) {
            cb();
          }
        });
      }

      scope.adicionarEndereco = function(enderecos) {
        enderecos.push({});
      }

      scope.removerEndereco = function(endereco, from) {
        from.splice(from.indexOf(endereco), 1);
      }
    }
  }
})

.directive('telefoneFoda', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {entidade: '=para'},
    templateUrl: 'resources/js/directives/telefone-foda.html',
    link: function(scope, element, attrs) {
      scope.telefoneTipos = ['CELULAR','COMERCIAL', 'FIXO', 'RECADO', 'RESIDENCIAL'];

      scope.adicionarTelefone = function(entidade) {
        entidade.telefones.push({tipo: scope.telefoneTipos[0]});
      }

      scope.removerTelefone = function(telefone, entidade) {
        entidade.telefones.splice(entidade.telefones.indexOf(telefone), 1);
      }
    }
  }
})

.directive('emailFoda', function() {
  return {
    restrict: 'E',
    replace: true,
    scope: {entidade: '=para'},
    templateUrl: 'resources/js/directives/email-foda.html',
    link: function(scope, element, attrs) {
      scope.adicionarEmail = function(entidade) {
        entidade.emails.push({});
      }

      scope.removerEmail = function(email, entidade) {
        entidade.emails.splice(entidade.emails.indexOf(email), 1);
      }
    }
  }
})

.controller('AprendizesFormCtrl', function($scope, $location, $routeParams, AprendizService, UFService, CidadeService, TypeService, ProfissaoService, EscolaService) {

  EscolaService.query(function(data) {
    angular.forEach(data, function(escola) {
      delete escola.emails;
      delete escola.enderecos;
      delete escola.telefones;
    });

    $scope.escolas = data;
  });

  ProfissaoService.query(function(data) {
    $scope.profissoes = data;
  });

  UFService.query(function(data) {
    $scope.ufs = data;
  });

  $scope.types = {
    nacionalidade: TypeService.nacionalidade(),
    cor: TypeService.cor(),
    civil: TypeService.civil(),
    habilidadeManual: TypeService.habilidadeManual(),
    descobertaPrograma: TypeService.descobertaPrograma(),
    motivoParticiparPrograma: TypeService.motivoParticiparPrograma(),
    cnh: TypeService.cnh(),
    fluenciaIdioma: TypeService.fluenciaIdioma(),
    turnoEscola: TypeService.turnoEscola(),
    grausDeEscolaridade: TypeService.curso()
  }

  // MODEL BOOSTRAP

  if ($routeParams.id) {
    AprendizService.get({id: $routeParams.id}, function(data) {
      $scope.aprendiz = data;

      if ($scope.aprendiz.cidadeNaturalidade) {
        $scope.ufNaturalidade = $scope.aprendiz.cidadeNaturalidade.uf;
        $scope.getCidadesByUF($scope.ufNaturalidade, $scope.aprendiz);
      }
    });
  } else {
    $scope.aprendiz = new AprendizService({
      telefones: [],
      emails: [],
      enderecos: []
    });
  }

  $scope.getCidadesByUF = function(uf, endereco, cb) {
    CidadeService.porUF(uf.id, function(data) {
      endereco.cidades = data;
      if (angular.isDefined(cb) && angular.isFunction(cb)) {
        cb();
      }
    });
  }

  $scope.calcularRendaPerCapita = function(aprendiz) {
    aprendiz.rendaPerCapita = aprendiz.rendaTotal / aprendiz.residentesCasa;
  }

  $scope.save = function(aprendiz) {
      
    var id = aprendiz.id;
    angular.forEach(aprendiz.enderecos, function(endereco) {
      delete endereco.uf;
      delete endereco.cidades;
    });

    // Cidades para a escolha da naturalidade
    delete aprendiz.cidades;

    aprendiz.$save(function() {
      if (id != undefined || id != null){
        $location.path('/aprendizes/'+id);  
      }else{
        $location.path('/aprendizes');
      }
    });
  }

  $scope.remover = function(id) {
    AprendizService.remove({id: id}, function() {
      $location.path('/aprendizes');
    });
  }
});
