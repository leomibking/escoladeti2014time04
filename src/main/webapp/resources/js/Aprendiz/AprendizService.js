angular.module('app').factory('AprendizService', function($resource) {
  var Service = {};

  var resource = $resource('rest/aprendizes', {},
  {
    'pagination': {
      url:'rest/aprendizes/pageList',
      method:'GET',
      params: {pageIndex: '@pageIndex', valor: '@valor'}
    },
    'get': { method: 'GET', params: {id: '@id'} },
    'save': { method: 'POST' },
    'query': { method: 'GET', isArray: true },
    'remove': { method: 'DELETE', params: {id: '@id'} },
    'salvarDocumento': {
      url: 'rest/aprendizes/salvarDocumento',
      method: 'POST'
    },
    'removerDocumento': {
      url: 'rest/aprendizes/removerDocumento',
      method: 'DELETE',
      params: {
        idAprendiz: '@idAprendiz',
        idDocumento: '@idDocumento'
      }
    }
  });

  resource.getPagination = function(pageIndex, callback, filter) {
    resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
  };

  // Service.salvarDocumento = function(object, callback){
  //   resource.salvarDocumento(object, callback);
  // };

  // Service.removerDocumento = function(idAprendiz, idDocumento, callback) {
  //   resource.removerDocumento({
  //     idAprendiz: idAprendiz,
  //     idDocumento: idDocumento
  //   }, callback);
  // };

  // Service.get = function(id, callback){
  //   resource.get({ id: id }, callback);
  // };

  // Service.save = function(object, callback){
  //   resource.save(object, callback);
  // };

  // Service.query = function(callback) {
  //   resource.query(callback);
  // }

  // Service.remove = function(id, callback) {
  //   resource.remove({ id: id }, callback);
  // };

  return resource;
});
