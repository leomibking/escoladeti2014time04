angular.module('app')

.controller('AprendizShowCtrl', function($scope, $routeParams, AprendizService, $modal, $http) {
  AprendizService.get({id: $routeParams.id}, function(data) {
    $scope.aprendiz = data;
  });

  $scope.adicionarDocuemnto = function() {
    $modal.open({
      templateUrl: 'resources/partials/Aprendiz/documento-modal.html',
      controller: 'AprendizDocumentoModalCtrl',
      resolve: {
        aprendiz: function() { return $scope.aprendiz; },
        documento: function() { return {}; }
      }
    }).result.then(function() {
      AprendizService.get({id: $routeParams.id}, function(data) {
        $scope.aprendiz = data;
      });
    });
  }

  $scope.ducumentoEngragar = function(documento) {
    documento.idAprendiz = $scope.aprendiz.id;
    documento.idDocumento = documento.id;
    documento.dtEntrega = new Date();
    delete documento.id;

    AprendizService.salvarDocumento(documento);
    AprendizService.get({id: $routeParams.id}, function(data) {
      $scope.aprendiz = data;
    });
  }

  $scope.documentoNaoEntregue = function(documento) {
    documento.idAprendiz = $scope.aprendiz.id;
    documento.idDocumento = documento.id;
    documento.dtEntrega = null;
    delete documento.id;

    AprendizService.salvarDocumento(documento);
    AprendizService.get({id: $routeParams.id}, function(data) {
      $scope.aprendiz = data;
    });
  }

  $scope.removerDocumento = function(documento, index) {
    AprendizService.removerDocumento({
      idAprendiz: $scope.aprendiz.id,
      idDocumento: documento.id
    }, function() {
      $scope.aprendiz.documentos.splice(index, 1);
    });
  };

  // TODO: fix
  $scope.editarDocumento = function(documento, index) {
    $modal.open({
      templateUrl: 'resources/partials/Aprendiz/documento-modal.html',
      controller: 'AprendizDocumentoModalCtrl',
      resolve: {
        aprendiz: function() { return $scope.aprendiz; },
        documento: function() { return documento; }
      }
    }).result.then(function(documentoSalvo) {
      $scope.aprendiz.documentos[index] = documentoSalvo;
    });
  };

  $http.get('rest/aprendizes/' + $routeParams.id + '/dadosAprendizagem').success(function (data) {
    $scope.dadosAprendizagem = data;
  });
});
