angular.module('app').controller('AprendizIndexCtrl', function($scope, AprendizService, PaginationService) {
  $scope.PaginationService = PaginationService.createInstance();
  $scope.PaginationService.init(AprendizService.getPagination);
});
