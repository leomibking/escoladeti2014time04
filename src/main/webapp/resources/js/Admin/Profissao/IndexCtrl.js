(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, ProfissaoService, PaginationService) {

    $scope.logic = {};

    $scope.logic.edit = function(id) {
      $location.path('/admin/profissoes/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/admin/profissoes/novo');
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(ProfissaoService.getPagination);
    };

    $scope.logic.remove = function(id) {
      ProfissaoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'ProfissaoService', 'PaginationService'];

  angular.module('app')
  .controller('ProfissaoIndexCtrl', IndexCtrl);
})();