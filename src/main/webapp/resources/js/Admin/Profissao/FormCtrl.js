(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, ProfissaoService) {
    
    $scope.logic = {};

    $scope.logic.init = function() {
      $scope.profissao = {};
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        ProfissaoService.get(id, function(data) {
          $scope.profissao = data;
        });
      }
    };

    $scope.logic.back = function() {
      $location.path('/admin/profissoes');
    };

    $scope.logic.cancel = function() {
      $location.path('/admin/profissoes');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      ProfissaoService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      ProfissaoService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'ProfissaoService'];
  
  angular.module('app')
  .controller('ProfissaoFormCtrl', FormCtrl);
})();