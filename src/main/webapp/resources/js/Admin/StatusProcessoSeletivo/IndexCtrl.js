(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, StatusProcessoService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/statusProcesso/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/statusProcesso/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/statusProcesso/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(StatusProcessoService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      StatusProcessoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'StatusProcessoService', 'PaginationService'];

  angular.module('app')
  .controller('StatusProcessoIndexCtrl', IndexCtrl);

})();