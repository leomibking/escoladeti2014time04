(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, StatusProcessoService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        StatusProcessoService.get(id, function(data) {
          $scope.statusP = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/statusProcesso');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/statusProcesso');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      StatusProcessoService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      StatusProcessoService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'StatusProcessoService'];
  
  angular.module('app')
  .controller('StatusProcessoFormCtrl', FormCtrl);

})();