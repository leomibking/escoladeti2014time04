(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, PaisService) {
    
    $scope.logic = {};

    $scope.logic.init = function() {
      $scope.pais = {};
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        PaisService.get(id, function(data) {
          $scope.pais = data;
        });
      }
    };

    $scope.logic.back = function() {
      $location.path('/admin/paises');
    };

    $scope.logic.cancel = function() {
      $location.path('/admin/paises');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      PaisService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      PaisService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'PaisService'];
  
  angular.module('app')
  .controller('PaisFormCtrl', FormCtrl);
})();