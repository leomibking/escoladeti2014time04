(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, PaisService, PaginationService) {

    $scope.logic = {};

    $scope.logic.edit = function(id) {
      $location.path('/admin/paises/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/admin/paises/novo');
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(PaisService.getPagination);
    };

    $scope.logic.remove = function(id) {
      PaisService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'PaisService', 'PaginationService'];

  angular.module('app')
  .controller('PaisIndexCtrl', IndexCtrl);
})();