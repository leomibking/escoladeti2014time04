(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, AtividadeService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/atividade/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/atividade/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/atividade/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(AtividadeService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      AtividadeService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'AtividadeService', 'PaginationService'];

  angular.module('app')
  .controller('AtividadeIndexCtrl', IndexCtrl);

})();