(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, AtividadeService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        AtividadeService.get(id, function(data) {
          $scope.atividade = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/atividade');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/atividade');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      AtividadeService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      AtividadeService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'AtividadeService'];
  
  angular.module('app')
  .controller('AtividadeFormCtrl', FormCtrl);

})();