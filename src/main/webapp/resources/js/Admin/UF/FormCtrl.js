(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, UFService, PaisService) {
    
    $scope.logic = {};

    var _loadSelects = function() {
      PaisService.query(function(data) {
        $scope.logic.paises = data;
        angular.findInArrayById(data, $scope.estado.pais, function(item){
          $scope.estado.pais = item;
        });
      });
    };

    $scope.logic.init = function() {
      $scope.estado = {};
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        UFService.get(id, function(data) {
          $scope.estado = data;
          _loadSelects();
        });
      } else {
        _loadSelects();
      }
    };

    $scope.logic.back = function() {
      $location.path('/admin/ufs');
    };

    $scope.logic.cancel = function() {
      $location.path('/admin/ufs');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      UFService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      UFService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'UFService', 'PaisService'];
  
  angular.module('app')
  .controller('UFFormCtrl', FormCtrl);
})();