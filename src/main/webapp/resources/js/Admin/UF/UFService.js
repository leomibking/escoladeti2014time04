(function(){
  'use strict';
  
  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/ufs', {}, 
    {
      'pagination': { 
        url:'rest/ufs/pageList',
        method:'GET',
        params: {pageIndex: '@pageIndex', valor: '@valor'} 
      },
      'get': { method: 'GET', params: {id: '@id'} },
      'save': { method: 'POST' },
      'query': { method: 'GET', isArray: true },
      'remove': { method: 'DELETE', params: {id: '@id'} },
      'porPais': { 
        url:'rest/ufs/porPais',
        method:'GET',
        isArray: true,
        params: {id: '@id'}
      }
    });

    
    Service.getPagination = function(pageIndex, callback, filter) {
      resource.pagination({ pageIndex: pageIndex, valor: filter }, callback);
    };

    Service.get = function(id, callback){
      resource.get({ id: id }, callback);
    };
    
    Service.save = function(object, callback){
      resource.save(object, callback);
    };
    
    Service.query = function(callback) {
      resource.query(callback);
    }

    Service.remove = function(id, callback) {
      resource.remove({ id: id }, callback);
    };

    Service.porPais = function(id, callback) {
      resource.porPais({ id: id }, callback);
    };

    return Service;
  }; 

  Factory.$inject = ['$resource'];

  angular.module('app')
  .factory('UFService', Factory);
})();
