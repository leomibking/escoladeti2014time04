(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, UFService, PaginationService) {

    $scope.logic = {};

    $scope.logic.edit = function(id) {
      $location.path('/admin/ufs/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/admin/ufs/novo');
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(UFService.getPagination);
    };

    $scope.logic.remove = function(id) {
      UFService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'UFService', 'PaginationService'];

  angular.module('app')
  .controller('UFIndexCtrl', IndexCtrl);
})();