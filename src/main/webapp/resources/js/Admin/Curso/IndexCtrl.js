(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, CursoService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/curso/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/curso/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/curso/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(CursoService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      CursoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'CursoService', 'PaginationService'];

  angular.module('app')
  .controller('CursoIndexCtrl', IndexCtrl);

})();