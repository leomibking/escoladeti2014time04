(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, CursoService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        CursoService.get(id, function(data) {
          $scope.curso = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/curso');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/curso');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      CursoService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      CursoService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'CursoService'];
  
  angular.module('app')
  .controller('CursoFormCtrl', FormCtrl);

})();