(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, PaisService, UFService, CidadeService) {
    
    $scope.logic = {};

    $scope.logic.atualizaUf = function(pais) {
      UFService.porPais(pais.id, function(data) {
        $scope.logic.ufs = data;
        angular.findInArrayById(data, $scope.cidade.uf, function(item){
          $scope.cidade.uf = item;
        });
      });
    }

    $scope.logic.atualizaPais = function() {
      PaisService.query(function(data) {
        $scope.logic.paises = data;
        angular.findInArrayById(data, $scope.logic.pais, function(item){
          $scope.logic.pais = item;
          $scope.logic.atualizaUf(item)
        });
      });
    };

    var _loadSelects = function() {
      $scope.logic.atualizaPais();
    };

    $scope.logic.init = function() {
      $scope.cidade = {};
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        CidadeService.get(id, function(data) {
          $scope.cidade = data;
          $scope.logic.pais = $scope.cidade.uf.pais;
          _loadSelects();
        });
      } else {
        _loadSelects();
      }
    };

    $scope.logic.back = function() {
      $location.path('/admin/cidades');
    };

    $scope.logic.cancel = function() {
      $location.path('/admin/cidades');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      CidadeService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      CidadeService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'PaisService', 'UFService', 'CidadeService'];
  
  angular.module('app')
  .controller('CidadeFormCtrl', FormCtrl);
})();
