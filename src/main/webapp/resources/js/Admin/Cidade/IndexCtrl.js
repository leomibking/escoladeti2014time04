(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, CidadeService, PaginationService) {

    $scope.logic = {};

    $scope.logic.edit = function(id) {
      $location.path('/admin/cidades/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/admin/cidades/novo');
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(CidadeService.getPagination);
    };

    $scope.logic.remove = function(id) {
      CidadeService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'CidadeService', 'PaginationService'];

  angular.module('app')
  .controller('CidadeIndexCtrl', IndexCtrl);
})();