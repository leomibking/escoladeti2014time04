angular.module('app').factory('SessaoService', ['$http','$rootScope','$location','$cookieStore',
  function($http, $rootScope,$location,$cookieStore) {
    var Sessao = {

      user : {
        login : $cookieStore.get(md5('login')),
        senha: $cookieStore.get(md5('senha')),
        perfil: $cookieStore.get(md5('perfil')),
        isLoggedIn: $cookieStore.get(md5('isLoggedIn')),
        dataExpiracao: $cookieStore.get(md5('dataExpiracao'))
      },
      cookieLogin : md5('login'),
      cookieSenha : md5('senha'),
      cookieLoggedIn : md5('isLoggedIn'),
      cookiePerfil : md5('perfil'),
      cookiedataExpiracao : md5('dataExpiracao'),

      getSenha : function(){
        return md5(this.user.senha);
      },

      getPerfil : function(){
        return md5(this.user.perfil);
      },

      setCookies: function(){
        $cookieStore.put(this.cookieLogin,this.user.login);
        $cookieStore.put(this.cookieSenha,this.getSenha());
        $cookieStore.put(this.cookieLoggedIn,this.user.isLoggedIn);
        $cookieStore.put(this.cookiePerfil,this.getPerfil());
        $cookieStore.put(this.cookiedataExpiracao,this.user.dataExpiracao);
      },

      removeCookies: function(){
        $cookieStore.remove(this.cookieLogin);
        $cookieStore.remove(this.cookieSenha);
        $cookieStore.remove(this.cookieLoggedIn);
        $cookieStore.remove(this.cookiePerfil);
        $cookieStore.remove(this.cookiedataExpiracao);
        this.user.login = undefined;
        this.user.senha = undefined;
        this.user.isLoggedIn = undefined;
        this.user.perfil = undefined;
        this.user.dataExpiracao = undefined;
      },

      login: function() {
        self = this;
        delete this.user.isLoggedIn;
        delete this.user.perfil;
        $http.post('./login',this.user).success(function (data){
          if(data.message){
            alert(data.message);
          }else{
            self.user = data;
            self.user.isLoggedIn = true;
            self.user.perfil= data.perfilDeAcesso.nome;
            self.setCookies();
            // foi usado window.location por não atualizar o escopo e não aparecer os botoes
            window.location.assign("/EscolaDeTi2014Time04/")
          }
        });
      },

      logout: function() {
        this.removeCookies();
        $location.url('/');
      },

      isAdmin: function(){
        return this.user.perfil == md5('admin');
      },

      isEmpresa: function(){
        return this.isAdmin() || this.user.perfil == md5('empresa');
      },

      isEscola: function(){
        return this.isAdmin() || this.user.perfil == md5('escola');
      },

      hasAccess: function(perfil){
        self = this;
        switch(perfil){
          case 'admin': return self.isAdmin();break;
          case 'escola': return self.isEscola();break;
          case 'empresa': return self.isEmpresa();break;
        }
      }
    };

    return Sessao;
  }
]);