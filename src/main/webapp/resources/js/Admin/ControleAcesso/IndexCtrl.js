angular.module('app')

.controller('UsuarioIndexCtrl', function($scope, UsuarioService, PaginationService) {
  $scope.logic = {};

  $scope.logic.init = function() {
    $scope.logic.PaginationService = PaginationService.createInstance();
    $scope.logic.PaginationService.init(UsuarioService.getPagination);
  };
})

.controller('UsuarioFormCtrl', function($scope, $location, $routeParams, UsuarioService, PerfisdeAcessoService) {
  if ($routeParams.id) {
    UsuarioService.get($routeParams.id, function(data) {
      $scope.usuario = data;
    })
  }

  PerfisdeAcessoService.query(function(data) {
    $scope.perfisDeAcesso = data;
  });

  $scope.salvar = function(usuario) {
    UsuarioService.save(usuario, function() {
      $scope.cancelar();
    });
  }

  $scope.cancelar = function() {
    $location.path('/admin/usuarios');
  }

  $scope.remover = function(id) {
    UsuarioService.remove(id, function() {
      $scope.cancelar();
    });
  }
})
