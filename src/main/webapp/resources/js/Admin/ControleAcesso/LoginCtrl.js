angular.module('app')

.controller('LoginCtrl', function(SessaoService, $scope, $location) {
  $scope.logar = function() {
    SessaoService.user = $scope.usuario;
    SessaoService.login();
  }

  $scope.sessao = SessaoService;
});
