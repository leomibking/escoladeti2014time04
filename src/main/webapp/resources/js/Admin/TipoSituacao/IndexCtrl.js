(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, TipoSituacaoService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/tipoSituacao/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/tipoSituacao/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/tipoSituacao/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(TipoSituacaoService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      TipoSituacaoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'TipoSituacaoService', 'PaginationService'];

  angular.module('app')
  .controller('TipoSituacaoIndexCtrl', IndexCtrl);

})();