(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, TipoSituacaoService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        TipoSituacaoService.get(id, function(data) {
          $scope.situacao = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/tipoSituacao');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/tipoSituacao');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      TipoSituacaoService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      TipoSituacaoService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'TipoSituacaoService'];
  
  angular.module('app')
  .controller('TipoSituacaoFormCtrl', FormCtrl);

})();