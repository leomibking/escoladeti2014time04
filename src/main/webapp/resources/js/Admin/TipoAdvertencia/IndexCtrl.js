(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, TipoAdvertenciaService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/tipoAdvertencia/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/tipoAdvertencia/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/tipoAdvertencia/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(TipoAdvertenciaService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      TipoAdvertenciaService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'TipoAdvertenciaService', 'PaginationService'];

  angular.module('app')
  .controller('TipoAdvertenciaIndexCtrl', IndexCtrl);

})();