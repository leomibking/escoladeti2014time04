(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, TipoAdvertenciaService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        TipoAdvertenciaService.get(id, function(data) {
          $scope.advertencia = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/tipoAdvertencia');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/tipoAdvertencia');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      TipoAdvertenciaService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      TipoAdvertenciaService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'TipoAdvertenciaService'];
  
  angular.module('app')
  .controller('TipoAdvertenciaFormCtrl', FormCtrl);

})();