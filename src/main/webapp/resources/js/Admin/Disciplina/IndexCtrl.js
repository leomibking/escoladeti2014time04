(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, DisciplinaService, PaginationService) {
    
    $scope.logic = {};

   
    $scope.logic.show = function(id) {
      $location.path('admin/disciplina/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/disciplina/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/disciplina/novo');
    };
    
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(DisciplinaService.getPagination);
    };
  };

 
  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'DisciplinaService', 'PaginationService'];

  angular.module('app')
  .controller('DisciplinaIndexCtrl', IndexCtrl);

})();
