(function() {
  "use strict";

  function FormCrud($scope, $location, $routeParams, DisciplinaService) {

    $scope.logic = {};


    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        DisciplinaService.get(id, function(data) {
          $scope.disciplina = data;
        });
      }
    };


    $scope.logic.back = function() {
      $location.path('admin/disciplina');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/disciplina');
    };


    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      console.dir(obj);
      if(!$scope.logic.canSave()){ return };
      DisciplinaService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.remove = function(id) {

      DisciplinaService.remove(id, function() {
        $location.path('/admin/disciplina');
      });
    };
  };


  FormCrud.$inject = ['$scope', '$location', '$routeParams', 'DisciplinaService'];
  
  angular.module('app')
  .controller('DisciplinaFormCrud', FormCrud);

})();
