(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, EscolaridadeService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        EscolaridadeService.get(id, function(data) {
          $scope.escolaridade = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/escolaridade');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/escolaridade');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      EscolaridadeService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      EscolaridadeService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'EscolaridadeService'];
  
  angular.module('app')
  .controller('EscolaridadeFormCtrl', FormCtrl);

})();