(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, EscolaridadeService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/escolaridade/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/escolaridade/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/escolaridade/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(EscolaridadeService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      EscolaridadeService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'EscolaridadeService', 'PaginationService'];

  angular.module('app')
  .controller('EscolaridadeIndexCtrl', IndexCtrl);

})();