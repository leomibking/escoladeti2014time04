(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, NacionalidadeService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        NacionalidadeService.get(id, function(data) {
          $scope.nacionalidade = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/nacionalidade');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/nacionalidade');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      NacionalidadeService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      NacionalidadeService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'NacionalidadeService'];
  
  angular.module('app')
  .controller('NacionalidadeFormCtrl', FormCtrl);

})();