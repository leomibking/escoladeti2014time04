(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, NacionalidadeService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/nacionalidade/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/nacionalidade/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/nacionalidade/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(NacionalidadeService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      NacionalidadeService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'NacionalidadeService', 'PaginationService'];

  angular.module('app')
  .controller('NacionalidadeIndexCtrl', IndexCtrl);

})();