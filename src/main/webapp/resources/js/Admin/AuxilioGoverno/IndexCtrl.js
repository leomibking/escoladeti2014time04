(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, AuxilioGovernoService, PaginationService) {

    $scope.logic = {};

    $scope.logic.edit = function(id) {
      $location.path('/admin/auxiliosGoverno/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/admin/auxiliosGoverno/novo');
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(AuxilioGovernoService.getPagination);
    };

    $scope.logic.remove = function(id) {
      AuxilioGovernoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'AuxilioGovernoService', 'PaginationService'];

  angular.module('app')
  .controller('AuxilioGovernoIndexCtrl', IndexCtrl);
})();