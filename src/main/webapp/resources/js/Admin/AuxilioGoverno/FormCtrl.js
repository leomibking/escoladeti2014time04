(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, AuxilioGovernoService) {
    
    $scope.logic = {};

    $scope.logic.init = function() {
      $scope.auxilio = {};
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        AuxilioGovernoService.get(id, function(data) {
          $scope.auxilio = data;
        });
      }
    };

    $scope.logic.back = function() {
      $location.path('/admin/auxiliosGoverno');
    };

    $scope.logic.cancel = function() {
      $location.path('/admin/auxiliosGoverno');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      AuxilioGovernoService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      AuxilioGovernoService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'AuxilioGovernoService'];
  
  angular.module('app')
  .controller('AuxilioGovernoFormCtrl', FormCtrl);
})();