(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, AreaEnderecoService) {
    $scope.logic = {};

    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        AreaEnderecoService.get(id, function(data) {
          $scope.areaEndereco = data;
        });
      }
    };

      $scope.logic.back = function() {
      $location.path('admin/areaEndereco');
    };

    $scope.logic.cancel = function() {
      $location.path('admin/areaEndereco');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      AreaEnderecoService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      AreaEnderecoService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'AreaEnderecoService'];
  
  angular.module('app')
  .controller('AreaEnderecoFormCtrl', FormCtrl);

})();