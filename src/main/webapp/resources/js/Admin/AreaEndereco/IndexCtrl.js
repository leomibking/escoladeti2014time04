(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, AreaEnderecoService, PaginationService) {
  
    $scope.logic = {};

    $scope.logic.show = function(id) {
      $location.path('admin/areaEndereco/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('admin/areaEndereco/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('admin/areaEndereco/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(AreaEnderecoService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      AreaEnderecoService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'AreaEnderecoService', 'PaginationService'];

  angular.module('app')
  .controller('AreaEnderecoIndexCtrl', IndexCtrl);

})();