angular.module('app')

.filter('cnpj', function() {
  return function(cnpjStr) {
    var partes = /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/.exec(cnpjStr);
    var formatado = cnpjStr;

    if (partes && partes.length === 6) {
      formatado = partes[1] + '.' + partes[2] + '.' + partes[3] + '/' + partes[4] + '-' + partes[5];
    }

    return formatado;
  }
})
