angular.module('app')

.filter('cpf', function() {
  return function(cpfStr) {
    var partes = /^(\d{3})(\d{3})(\d{3})(\d{2})/.exec(cpfStr);
    var formatado = cpfStr;

    if (partes && partes.length === 5) {
      formatado = partes[1] + '.' + partes[2] + '.' + partes[3] + '-' + partes[4];
    }

    return formatado;
  }
})
