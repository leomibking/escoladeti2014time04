angular.module('app')

.filter('idadeFilter', function() {
  return function(nascimento) {
    var nascimento = new Date(nascimento),
        nascimentoDiff = Date.now() - nascimento.getTime(),
        idade = new Date(nascimentoDiff);

    return Math.abs(idade.getUTCFullYear() - 1970);
  };
});
