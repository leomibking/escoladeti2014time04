angular.module('app')

.filter('filterData', function() {
  return function(dateStr) {
    var pad = function (s) { return (s < 10) ? '0' + s : s; };
    var d = new Date(dateStr);

    return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
  };
});
