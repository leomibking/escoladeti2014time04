angular.module('app')

.filter('formataTelefone', function() {
  return function(numero) {
    if (numero == undefined){
        return numero;
    }
        
    var result = numero;
    var array = numero.match(/(\d{4})(\d{4})/);

    if (array) {
      result = array[1] + '-' + array[2];
    }

    return result;
  };
});
