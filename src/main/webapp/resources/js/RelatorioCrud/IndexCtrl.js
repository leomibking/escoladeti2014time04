(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, RelatorioCrudService, PaginationService) {

    $scope.logic = {};

    $scope.logic.edit = function(id) {
      $location.path('/rest/relatorio/crud' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/rest/relatorio/crud/novo');
    };

    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(RelatorioService.getPagination);
    };

    $scope.logic.remove = function(id) {
      RelatorioService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'RelatorioCrudService', 'PaginationService'];

  angular.module('app')
  .controller('RelatorioCrudIndexCtrl', IndexCtrl);
})();