(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, RelatorioCrudService) {
    
    $scope.logic = {};

    $scope.logic.init = function() {
      $scope.relatorio = {};
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        RelatorioService.get(id, function(data) {
          $scope.relatorio = data;
        });
      }
    };

    $scope.logic.back = function() {
      $location.path('/rest/relatorio/crud');
    };

    $scope.logic.cancel = function() {
      $location.path('/rest/relatorio/crud');
    };

    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      RelatorioService.save(obj, function(){
        $scope.logic.back();
      });
    };

    $scope.logic.remove = function(id) {
      RelatorioService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'RelatorioCrudService'];
  
  angular.module('app')
  .controller('RelatorioFormCtrl', FormCtrl);
})();