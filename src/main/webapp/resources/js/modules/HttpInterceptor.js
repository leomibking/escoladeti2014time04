(function(){
  'use strict';
  angular.module('app')
  .config(['$provide', '$httpProvider', function ($provide, $httpProvider) {

    $provide.factory('AppHttpInterceptor', ['$q', 'NotificationService', function ($q, NService) {
      return {
        request: function (config) {
          if (config.method == "DELETE" && (config.url.indexOf("rest/") != -1 ) ) {
            if (!confirm("Tem certeza que deseja excluir?")) {
              var cancel = $q.defer();
              config.timeout = cancel.promise;
              cancel.resolve();
            }
          }

          /*if(config.url.indexOf("rest/")  != -1){
          	config.url = "api/" + config.url;
          } */

          return config || $q.when(config);
        },

        requestError: function (rejection) {
          console.error('requestError');
          console.dir(rejection);
          return $q.reject(rejection);
        },

        response: function (response) {
          if (response.config.method == "POST" && (response.config.url.indexOf("rest/") != -1 ) ) {
            if(response.data.success){
              NService.add({
                content: response.data.message,
                class: 'success'
              });
            }
          }
          if (response.config.method == "DELETE" && (response.config.url.indexOf("rest/") != -1 ) ) {
            if(response.data.success){
              NService.add({
                content: response.data.message,
                class: 'warning'
              });
            }
          }
          return response || $q.when(response);
        },

        responseError: function (rejection) {
          console.error('responseError');
          console.dir(rejection);
          return $q.reject(rejection);
        }
      };
    }]);

    $httpProvider.interceptors.push('AppHttpInterceptor');
  }]);
})();