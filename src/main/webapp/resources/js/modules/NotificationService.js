(function(){
  "use strict";

  angular.module('app')
  .factory('NotificationService', ['ngToast', function(ngToast) {
    var Service = {};
    Service.add = function(notification){
      var config = {
        content: notification,
        class: 'info',
        dismissOnTimeout: true,
        timeout: 5000,
        // dismissButton: true,
        dismissOnClick: true
      };

      angular.extend(config, notification);

      ngToast.create(config);
    };
    return Service;
  }]);
})();