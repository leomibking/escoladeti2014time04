(function(){
  'use strict';

  angular.module('app').provider('TemplateUrl', function() {
    
    /*
    *
    * O objeto TemplateUrl é responsável por encapsular
    *   a configuração de url do servidor.
    * Chamando o método .build(), passando o caminho do arquivo HTML,
    *   irá construir a url para o arquivo com base na configuração, exemplo:
    *
    * TemplateUrl.build('MinhaTela.html');
    *
    *   retornará a STRING:
    *
    *     'resources/app/partials/MinhaTela.html'
    *
    * TemplateUrl.build('Pasta1/Pasta2/.../PastaN/MinhaTela.html');
    *
    *   retornará a STRING:
    *
    *     'resources/app/partials/Pasta1/Pasta2/.../PastaN/MinhaTela.html'
    *
    */

    var _this = this;
    var SERVER_FOLDER_PATH = 'resources/';
    var VIEWS_FOLDER_PATH = 'partials/';

    _this.build = function(fileUrl) { return SERVER_FOLDER_PATH + VIEWS_FOLDER_PATH + fileUrl;}
    
    return {
      build: _this.build,
      $get: function() {
        return {
          build: _this.build
        };
      }
    };

  });
})();