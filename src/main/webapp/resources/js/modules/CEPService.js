(function(){
  'use strict';

  function Factory($http) {
    var httpRequest = function(config) {
      $http({
        method: 'GET',
        url: config.url + config.param + config.url2,
        dataType: 'json'
      }).
      success(function(data, status) {
        config.parser(data);
      }).
      error(function(data, status) {
        config.backup();
      });
    };

    var callback;

    var API_BACKUP_1 = {
      url: 'http://appservidor.com.br/webservice/cep?cep=',
      url2: '',
      param: '',
      parser: function(data) {
        if(data.total = 0) {
          return;
        }
        var d = {};
        d.logradouro = data.logradouro_completo;
        d.bairro = data.bairro;
        d.cidade = data.cidade;
        d.uf = data.uf_sigla;
        callback(d);
      },
      backup: function() {}
    };

    var API_MASTER = {
      url: 'http://cep.republicavirtual.com.br/web_cep.php?cep=',
      url2: '&formato=json',
      param: '',
      parser: function(data) {
        if(data.resultado != 2 && data.resultado != 1) {
          httpRequest(API_BACKUP_1);
          return;
        }
        var d = {};
        d.logradouro = data.tipo_logradouro + ' ' + data.logradouro;
        d.bairro = data.bairro;
        d.cidade = data.cidade;
        d.uf = data.uf;
        callback(d);
      },
      backup: function() {
        httpRequest(API_BACKUP_1);
      }
    };
    var Service = {};

    Service.get = function (cep, cb) {
      API_MASTER.param = cep;
      API_BACKUP_1.param = cep;
      callback = cb;
      httpRequest(API_MASTER);
    }

    return Service;
  };

  Factory.$inject = ['$http'];

  angular.module('app')
  .factory('CEPService', Factory);
})();