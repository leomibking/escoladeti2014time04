(function(){
  'use strict';

  function Factory($resource) {
    var resource = $resource('rest/types', {},
    {
      'areaEndereco': {
        url:'rest/types/areaEndereco',
        method:'GET',
        isArray: true
      },
      'cor': {
        url:'rest/types/cor',
        method:'GET',
        isArray: true
      },
      'cnh': {
        url:'rest/types/cnh',
        method:'GET',
        isArray: true
      },
      'civil': {
        url:'rest/types/estadoCivil',
        method:'GET',
        isArray: true
      },
      'curso': {
        url:'rest/types/curso',
        method:'GET',
        isArray: true
      },
      'cursoEscola': {
        url:'rest/types/cursoEscola',
        method:'GET',
        isArray: true
      },
      'descobertaPrograma': {
        url:'rest/types/descobertaPrograma',
        method:'GET',
        isArray: true
      },
      'escolaridadeResponsavel': {
        url:'rest/types/escolaridadeResponsavel',
        method:'GET',
        isArray: true
      },
      'fluenciaIdioma': {
        url:'rest/types/fluenciaIdioma',
        method:'GET',
        isArray: true
      },
      'habilidadeManual': {
        url:'rest/types/habilidadeManual',
        method:'GET',
        isArray: true
      },
      'motivoParticiparPrograma': {
        url:'rest/types/motivoParticiparPrograma',
        method:'GET',
        isArray: true
      },
      'nacionalidade': {
        url:'rest/types/nacionalidade',
        method:'GET',
        isArray: true
      },
      'sexo': {
        url:'rest/types/sexo',
        method:'GET',
        isArray: true
      },
      'statusServicoMilitar': {
        url:'rest/types/statusServicoMilitar',
        method:'GET',
        isArray: true
      },
      'tipoDeficiencia': {
        url:'rest/types/tipoDeficiencia',
        method:'GET',
        isArray: true
      },
      'tipoTelefone': {
        url:'rest/types/tipoTelefone',
        method:'GET',
        isArray: true
      },
      'turnoEscola': {
        url:'rest/types/turnoEscola',
        method:'GET',
        isArray: true
      },
      'periodo':{
        url:'rest/types/periodo',
        method:'GET',
        isArray: true
      },
      'tipoEtapa':{
        url:'rest/types/tipoEtapa',
        method:'GET',
        isArray: true
      },
      'advertencia':{
        url:'rest/types/advertencia',
        method:'GET',
        isArray: true
      }
    });

return resource;
};

Factory.$inject = ['$resource'];

angular.module('app')
.factory('TypeService', Factory);
})();