angular.module('app')

.controller('EtapaModalCtrl', function($scope, $modalInstance, EtapaService, TypeService) {
  TypeService.tipoEtapa(function(data) {
    $scope.tiposDeEtapa = data;
  });

  $scope.salvar = function(etapa) {
    EtapaService.save(etapa, function() {
      $modalInstance.close();
    });
  }

  $scope.cancelar = function () {
    $modalInstance.dismiss('cancel');
  }
});
