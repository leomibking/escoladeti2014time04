(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, EtapaService, PaginationService) {
  
    $scope.logic = {};

    $scope.etapashow = {};
    $scope.logic.show = function(item) {
      $scope.etapashow = item;
    };

    $scope.logic.edit = function(id) {
      $location.path('etapa/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('etapa/novo');
    };
   
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService.createInstance();
      $scope.logic.PaginationService.init(EtapaService.getPagination);
    };
    
    $scope.logic.remove = function(id) {
      EtapaService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'EtapaService', 'PaginationService'];

  angular.module('app')
  .controller('EtapaIndexCtrl', IndexCtrl);

})();