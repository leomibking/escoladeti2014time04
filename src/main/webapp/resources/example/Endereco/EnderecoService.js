(function(){
  'use strict';
  
  function Factory($resource) {
    var Service = {};

    var resource = $resource('rest/Endereco', {}, 
      {
        'pagination': { 
          url:'rest/Endereco/pageList',
          method:'GET',
          params: {pageIndex: '@pageIndex'} 
        },
        'get': { method: 'GET', params: {id: '@id'} },
        'save': { method: 'POST' },
        'query': { method: 'GET', isArray: true },
        'remove': { method: 'DELETE', params: {id: '@id'} }
      });

    Service.getPagination = function(pageIndex, callback) {
      resource.pagination({ pageIndex: pageIndex }, callback);
    };

    Service.get = function(id, callback){
      resource.get({ id: id }, callback);
    };
    
    Service.save = function(object, callback){
      resource.save(object, callback);
    };
    
    Service.query = function(callback) {
      resource.query(callback);
    }

    Service.remove = function(id, callback) {
      resource.remove({ id: id }, callback);
    };

    return Service;
  }; 

  Factory.$inject = ['$resource'];

  angular.module('App.Services')
    .factory('EnderecoService', Factory);
})();