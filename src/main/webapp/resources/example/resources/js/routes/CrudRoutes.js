(function(){
  'use strict';

  angular.module('app')
  .config(['$routeProvider', 'TemplateUrlProvider', function($routeProvider, TemplateUrlProvider){
      // objeto de configuração para simplificar o route de NOVO e EDITAR, pois são as mesmas config.
      var routeConfig;

      $routeProvider.when("/aprendizes", {
        templateUrl: TemplateUrlProvider.build("Aprendiz/index.html"),
        controller: "AprendizIndexCtrl"
      });

      routeConfig = {
        templateUrl: TemplateUrlProvider.build("Aprendiz/form.html"),
        controller: "AprendizFormCtrl"
      };

      $routeProvider.when("/aprendizes/novo", routeConfig);

      $routeProvider.when("/aprendizes/:id/editar", routeConfig);

    }]);  
})();