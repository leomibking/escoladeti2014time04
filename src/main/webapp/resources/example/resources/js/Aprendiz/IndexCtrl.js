(function() {
  "use strict";

  function IndexCtrl($scope, $location, $routeParams, AprendizService, PaginationService) {
    /* BEGIN - CODE AREA */

    /* END - CODE AREA */

    /*
    * A propriedade LOGIC tem o objeto de centralizar
    *   comportamentos comuns de um controller, 
    *     o que tambem viabiliza a futura abstração.
    */
    $scope.logic = {};

    /*
    * Métodos para navegação na pagina.
    */
    $scope.logic.show = function(id) {
      $location.path('/aprendizes/' + id);
    };

    $scope.logic.edit = function(id) {
      $location.path('/aprendizes/' + id + '/editar');
    };

    $scope.logic.new = function() {
      $location.path('/aprendizes/novo');
    };
    /*
    * Método para inicializar o controller,
    *   verifica se na URL atual possui o parametro :ID
    *     e caso tenha / esteja definido( angular.isDefined(id) )
    *       busca pelo serviço da tela o objeto.
    */
    $scope.logic.init = function() {
      $scope.logic.PaginationService = PaginationService;
      $scope.logic.PaginationService.init(AprendizService.getPagination);
    };
    /*
    * Método para remover um item
    *   chamando o serviço da tela.
    * Caso sucesso, chama o método .back(),
    *   para retornar a pagina anterior
    */
    $scope.logic.remove = function(id) {
      AprendizService
      .remove(id, function() {
        $scope.logic.PaginationService
        .reload();
      });
    };
  };

  /*
  * Informa para o $inject do Angular o que deve ser injetado no controller.
  *
  * function MeuController(param1, param2, param3, ... , paramN) {...}
  * MeuController.$inject = ['objeto1', 'objeto2', 'objeto3'];
  *
  * A ordem do itens no array do $inject é a ordem 
  *   que será injetada nos parâmetros do controller,
  *     iniciando do primeiro parâmetros do controller.
  */
  IndexCtrl.$inject = ['$scope', '$location', '$routeParams', 'AprendizService', 'PaginationService'];

  angular.module('app')
  .controller('AprendizIndexCtrl', IndexCtrl);

})();