(function() {
  "use strict";

  function FormCtrl($scope, $location, $routeParams, AprendizService) {
    /*
    * A propriedade LOGIC tem o objeto de centralizar
    *   comportamentos comuns de um controller, 
    *     o que tambem viabiliza a futura abstração.
    */
    $scope.logic = {};

    /*
    * Método para inicializar o controller,
    *   verifica se na URL atual possui o parametro :ID
    *     e caso tenha / esteja definido( angular.isDefined(id) )
    *       busca pelo serviço da tela o objeto.
    */
    $scope.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        AprendizService.get(id, function(data) {
          $scope.aprendiz = data;
        });
      }
    };
    /*
    * Métodos para navegação na pagina.
    */

    $scope.logic.back = function() {
      $location.path('/aprendizes');
    };

    $scope.logic.cancel = function() {
      $location.path('/aprendizes');
    };

    /*
    * Método que retorna um boleano,
    *   para verificar se pode salvar ou não.
    */
    $scope.logic.canSave = function() {
      return true;
    };

    $scope.logic.save = function(obj) {
      if(!$scope.logic.canSave()){ return };
      AprendizService.save(obj, function(){
        $scope.logic.back();
      });
    };

    /*
    * Método para remover um item
    *   chamando o serviço da tela.
    * Caso sucesso, chama o método .back(),
    *   para retornar a pagina anterior
    */
    $scope.logic.remove = function(id) {
      AprendizService
      .remove(id, function() {
        $scope.logic.back();
      });
    };
  };

  /*
  * Informa para o $inject do Angular o que deve ser injetado no controller.
  *
  * function MeuController(param1, param2, param3, ... , paramN) {...}
  * MeuController.$inject = ['objeto1', 'objeto2', 'objeto3'];
  *
  * A ordem do itens no array do $inject é a ordem 
  *   que será injetada nos parâmetros do controller,
  *     iniciando do primeiro parâmetros do controller.
  */
  FormCtrl.$inject = ['$scope', '$location', '$routeParams', 'AprendizService'];
  
  angular.module('app')
  .controller('AprendizFormCtrl', FormCtrl);

})();