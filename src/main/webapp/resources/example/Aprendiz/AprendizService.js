(function(){
  'use strict';
  
  function Factory($resource) {
    var Service = {};

    /*
    * O $resource é uma implementação com maior abstração para simplificar o $http,
    *   o construtor recebe a URL, os parêmetros da URL e os métodos que possui.
    *
    *   Exemplo:
    *
    *   $resource('URL', {PARAMS}, 
    *     {
    *       'NomeDoMetodo': {PARAMS}
    *     });
    *
    *
    *   $resource('UrlMapeadaNoControllerDoServidor', {}, 
    *     {
    *       'get': { method: 'GET', params: {id: '@id'} },
    *       'save': { method: 'POST' }
    *     });
    *
    *   SAVE é o nome do método e no objeto de parâmetro, a propriedade METHOD
    *     indica o tipo do método: GET, PUT, DELETE, REMOVE, POST.
    *
    *   GET possui no objeto PARAMS a propriedade ID, que irá adicionar o 
    *     parâmetro ID na URL para realizar um GET:
    *   Exemplo:
    *
    *   UrlDoResource -> 'rest/MeuController'
    *
    *   MeuServicoComResource.get(21, ~callback~);
    *   UrlDoResourceChamandoOGet -> 'rest/MeuController/21'
    *
    *   Objeto para configurações
    *     url = url personalizada para o método.
    *     method = tipo do método
    *     isArray = se TRUE indica que a URL retorna um array, ou seja, uma lista
    *     params = objeto que contem os parâmetros se serão injetados na URL
    */

    var resource = $resource('rest/PessoaFisica', {}, 
      {
        'pagination': { 
          url:'rest/PessoaFisica/pageList',
          method:'GET',
          params: {pageIndex: '@pageIndex'} 
        },
        'get': { method: 'GET', params: {id: '@id'} },
        'save': { method: 'POST' },
        'query': { method: 'GET', isArray: true },
        'remove': { method: 'DELETE', params: {id: '@id'} }
      });

    Service.getPagination = function(pageIndex, callback) {
      resource.pagination({ pageIndex: pageIndex }, callback);
    };

    Service.get = function(id, callback){
      resource.get({ id: id }, callback);
    };
    
    Service.save = function(object, callback){
      resource.save(object, callback);
    };
    
    Service.query = function(callback) {
      resource.query(callback);
    }

    Service.remove = function(id, callback) {
      resource.remove({ id: id }, callback);
    };

    return Service;
  }; 

  Factory.$inject = ['$resource'];

  angular.module('App.Services')
    .factory('AprendizService', Factory);
})();