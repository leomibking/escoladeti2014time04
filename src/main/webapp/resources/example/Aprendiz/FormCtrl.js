(function(){
  'use strict';

  function FormCtrl($location, $routeParams, AprendizService, EnderecoService, CategoriaService) {
/* 
    * Váriavel para guardar a referencia do THIS, o próprio controller
    *   evitando conflitos em funções por usar THIS e substituindo pela variável.
    */
    var _this = this;

    /*
    * Propriedade do controller que contem o objeto da tela atual,
    *   por exemplo uma Cidade ou Profissão ou Aprendiz.
    *
    * O uso da nomenclatura ENTITY serve para a padronização do mesmo e reconhecimento
    *   em métodos internos, para abstrações.
    *
    * _this.entity = { id: 1, nome: 'herp', idade: 18 };
    * _this.entity.id;
    * _this.entity.nome;
    * _this.entity.idade;
    */

    // BEGIN - CODE AREA //

    _this.list = [];


    // END - CODE AREA //
    
    /*
    * A propriedade LOGIC tem o objeto de centralizar
    *   comportamentos comuns de um controller, 
    *     o que tambem viabiliza a futura abstração.
    */
    _this.logic = {};

    /*
    * Método para inicializar o controller,
    *   verifica se na URL atual possui o parametro :ID
    *     e caso tenha / esteja definido( angular.isDefined(id) )
    *       busca pelo serviço da tela o objeto.
    */
    _this.logic.init = function() {
      var id = $routeParams.id;
      if(angular.isDefined(id)) {
        AprendizService.get(id, function(data) {
          _this.entity = data;
        });
      }
    };
    /*
    * Métodos para navegação na pagina.
    */
    _this.logic.edit = function(id) {
      $location.path('/aprendiz/' + id + '/editar');
    };

    _this.logic.back = function() {
      $location.path('/aprendiz');
    };

    _this.logic.cancel = function() {
      $location.path('/aprendiz');
    };

    /*
    * Método que retorna um boleano,
    *   para verificar se pode salvar ou não.
    */
    _this.logic.canSave = function() {
      return true;
    };

    _this.logic.save = function(obj) {
      if(!_this.logic.canSave()){ return };
      AprendizService.save(obj, function(){
        _this.logic.back();
      });
    };

    /*
    * Método para remover um item
    *   chamando o serviço da tela.
    * Caso sucesso, chama o método .back(),
    *   para retornar a pagina anterior
    */
    _this.logic.remove = function(id) {
      AprendizService
        .remove(id, function() {
          _this.logic.back();
        });
    };
  };
  /*
  * Informa para o $inject do Angular o que deve ser injetado no controller.
  *
  * function MeuController(param1, param2, param3, ... , paramN) {...}
  * MeuController.$inject = ['objeto1', 'objeto2', 'objeto3'];
  *
  * A ordem do itens no array do $inject é a ordem 
  *   que será injetada nos parâmetros do controller,
  *     iniciando do primeiro parâmetros do controller.
  */
  FormCtrl.$inject = ['$location', '$routeParams', 'AprendizService', 'EnderecoService', 'CategoriaService'];
  
  angular.module('App.Controllers')
    .controller('FormCtrl', FormCtrl);
})();