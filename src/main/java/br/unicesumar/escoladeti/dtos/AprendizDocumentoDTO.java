package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;

public class AprendizDocumentoDTO extends BaseDTO {
    private Long idAprendiz;
    private Long idDocumento;
    private String nome;
    
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dtEntrega;

    public Long getIdAprendiz() {
        return idAprendiz;
    }

    public void setIdAprendiz(Long idAprendiz) {
        this.idAprendiz = idAprendiz;
    }

    public Long getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(Long idDocumento) {
        this.idDocumento = idDocumento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDtEntrega() {
        return dtEntrega;
    }

    public void setDtEntrega(Date dtEntrega) {
        this.dtEntrega = dtEntrega;
    }
}
