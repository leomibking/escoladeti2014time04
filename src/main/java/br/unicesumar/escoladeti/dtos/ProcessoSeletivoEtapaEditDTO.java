package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;
import java.util.ArrayList;
import java.util.List;

public class ProcessoSeletivoEtapaEditDTO {

    private EtapaDTO etapa;
    private Long id;
    private String status;
    
    private List<ProcessoSeletivoEtapaAprendizDTO> selecionados = new ArrayList<>();

    public EtapaDTO getEtapa() {
        return etapa;
    }

    public void setEtapa(EtapaDTO etapa) {
        this.etapa = etapa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ProcessoSeletivoEtapaAprendizDTO> getSelecionados() {
        return selecionados;
    }

    public void setSelecionados(List<ProcessoSeletivoEtapaAprendizDTO> selecionados) {
        this.selecionados = selecionados;
    }

    public String getStatus() {
        return status;
    }
    
    public ProcessoSeletivoEtapaEditDTO(ProcessoSeletivoEtapa pse) {
        this.etapa = new EtapaDTO(pse.getEtapa());
        this.id = pse.getId();
        
        if (pse.getStatus() != null){
            this.status = pse.getStatus().getDescricao();
        }
        
        for (ProcessoSeletivoEtapaAprendiz e : pse.getSelecionados()){
            this.selecionados.add(new ProcessoSeletivoEtapaAprendizDTO(e));
        }
    }
    
    public ProcessoSeletivoEtapaEditDTO(){
        
    }

}
