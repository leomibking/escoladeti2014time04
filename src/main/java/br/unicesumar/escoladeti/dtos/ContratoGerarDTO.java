package br.unicesumar.escoladeti.dtos;

import java.util.Date;

public class ContratoGerarDTO extends BaseDTO {

    private Long idAprendiz;
    private Long idTurma;
    private Long idVaga;
    private Date dataInicio;
    private Date dataFim;

    public Long getIdAprendiz() {
        return idAprendiz;
    }

    public void setIdAprendiz(Long idAprendiz) {
        this.idAprendiz = idAprendiz;
    }

    public Long getIdTurma() {
        return idTurma;
    }

    public void setIdTurma(Long idTurma) {
        this.idTurma = idTurma;
    }

    public Long getIdVaga() {
        return idVaga;
    }

    public void setIdVaga(Long idVaga) {
        this.idVaga = idVaga;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

}
