package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.Filter;
import java.util.ArrayList;
import java.util.List;


public class FilterDTO {
    
    private List<Filter> filtros = new ArrayList<>();

    public FilterDTO() {
    }

    public FilterDTO(List<Filter> filters) {
        for (Filter f : filters){
            this.filtros.add(f);
        }
    }

    public List<Filter> getFiltros() {
        return filtros;
    }

    public void setFiltros(List<Filter> filtros) {
        this.filtros = filtros;
    }
    
}