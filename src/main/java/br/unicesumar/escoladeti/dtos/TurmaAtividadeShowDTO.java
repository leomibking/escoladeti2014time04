package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.entity.TurmaAtividade;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;


public class TurmaAtividadeShowDTO {
    
    private TurmaDisciplinaDTO disciplina;
    private TurmaProfessorDTO professor;
    private TurmaAtividadeDTO atividade;
    
    private Long id;
    
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataAtividade;
    
    private Double valorAtividade;
    private String descricaoDaAtividade;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public TurmaDisciplinaDTO getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(TurmaDisciplinaDTO disciplina) {
        this.disciplina = disciplina;
    }

    public TurmaProfessorDTO getProfessor() {
        return professor;
    }

    public void setProfessor(TurmaProfessorDTO professor) {
        this.professor = professor;
    }

    public TurmaAtividadeDTO getAtividade() {
        return atividade;
    }

    public void setAtividade(TurmaAtividadeDTO atividade) {
        this.atividade = atividade;
    }

    public Date getDataAtividade() {
        return dataAtividade;
    }

    public void setDataAtividade(Date dataAtividade) {
        this.dataAtividade = dataAtividade;
    }

    public Double getValorAtividade() {
        return valorAtividade;
    }

    public void setValorAtividade(Double valorAtividade) {
        this.valorAtividade = valorAtividade;
    }

    public String getDescricaoDaAtividade() {
        return descricaoDaAtividade;
    }

    public void setDescricaoDaAtividade(String descricaoDaAtividade) {
        this.descricaoDaAtividade = descricaoDaAtividade;
    }
    
    public TurmaAtividadeShowDTO(TurmaAtividade ta) {
        this.id = ta.getId();
        this.dataAtividade = ta.getDataAtividade();
        this.descricaoDaAtividade = ta.getDescricaoAtividade();
        this.valorAtividade = ta.getValor();
        this.disciplina = new TurmaDisciplinaDTO(ta.getDisciplina());
        this.professor = new TurmaProfessorDTO(ta.getProfessor());
        this.atividade =  new TurmaAtividadeDTO(ta.getAtividade());
    }

    public TurmaAtividadeShowDTO() {
    }
    
}