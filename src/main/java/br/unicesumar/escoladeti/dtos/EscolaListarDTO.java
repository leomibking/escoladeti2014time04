package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Escola;
import br.unicesumar.escoladeti.entity.Telefone;
import java.util.ArrayList;
import java.util.List;

public class EscolaListarDTO {

    private Long id;
    private String nome;
    private String razaoSocial;
    private String cnpj;
    
    private List<TelefoneDTO> telefones = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public EscolaListarDTO() {
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public List<TelefoneDTO> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<TelefoneDTO> telefones) {
        this.telefones = telefones;
    }

    public EscolaListarDTO(Escola e) {
        this.id = e.getId();
        this.nome = e.getNome();
        this.razaoSocial = e.getRazaoSocial();
        this.cnpj = e.getCnpj();
        
        if (e.getTelefones() != null){
            for (Telefone t : e.getTelefones()){
                this.telefones.add(new TelefoneDTO(t));
            }
        }
    }
    
}
