package br.unicesumar.escoladeti.dtos;

import java.util.ArrayList;
import java.util.List;

public class ProcessoPreSelecaoDTO {

    private List<CandidatoProcessoDTO> selecionados;

    public List<CandidatoProcessoDTO> getSelecionados() {
        return selecionados;
    }

    public void setSelecionados(List<CandidatoProcessoDTO> selecionados) {
        this.selecionados = selecionados;
    }
    
    public ProcessoPreSelecaoDTO() {
        this.selecionados = new ArrayList<>();
    }

}
