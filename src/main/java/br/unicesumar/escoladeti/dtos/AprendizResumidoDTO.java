package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Telefone;
import java.util.ArrayList;
import java.util.List;


public class AprendizResumidoDTO {
    
    private Long id;
    private String nome;
    private List<TelefoneDTO> telefones = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<TelefoneDTO> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<TelefoneDTO> telefones) {
        this.telefones = telefones;
    }
    
    public AprendizResumidoDTO(Aprendiz a){
        this.id = a.getId();
        this.nome = a.getNome();
        if (a.getTelefones() != null){
            for (Telefone t : a.getTelefones()){
                this.telefones.add(new TelefoneDTO(t));
            }
        }
    }
    
    public AprendizResumidoDTO(){
        
    }

}