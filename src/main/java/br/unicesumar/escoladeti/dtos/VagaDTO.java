package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Vaga;


public class VagaDTO {
    private Long id;
    private String profissao;
    private String periodo;
    private String atividade;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    
    public VagaDTO() {
    }

    public VagaDTO(Vaga v) {
        this.id = v.getId();
        this.atividade = v.getAtividade();
        this.periodo = v.getPeriodo().getDescricao();
        this.profissao = v.getProfissao().getNome();
    }
    
}