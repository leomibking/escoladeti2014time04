package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.entity.TurmaChamada;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;


public class TurmaChamadaShowDTO {
    
    private TurmaDisciplinaDTO disciplina;
    private TurmaProfessorDTO professor;
    
    private Long id;
    
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataChamada;
    
    private int qtAulas;
    private String descricao;

    public TurmaDisciplinaDTO getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(TurmaDisciplinaDTO disciplina) {
        this.disciplina = disciplina;
    }

    public TurmaProfessorDTO getProfessor() {
        return professor;
    }

    public void setProfessor(TurmaProfessorDTO professor) {
        this.professor = professor;
    }

    public Date getDataChamada() {
        return dataChamada;
    }

    public void setDataChamada(Date dataChamada) {
        this.dataChamada = dataChamada;
    }

    public int getQtAulas() {
        return qtAulas;
    }

    public void setQtAulas(int qtAulas) {
        this.qtAulas = qtAulas;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public TurmaChamadaShowDTO(TurmaChamada tc) {
        this.id = tc.getId();
        this.dataChamada = tc.getDataChamada();
        this.descricao = tc.getDescricao();
        this.qtAulas = tc.getQtAulas();
        this.disciplina = new TurmaDisciplinaDTO(tc.getDisciplina());
        this.professor = new TurmaProfessorDTO(tc.getProfessor());
    }

    public TurmaChamadaShowDTO() {
    }
    
}