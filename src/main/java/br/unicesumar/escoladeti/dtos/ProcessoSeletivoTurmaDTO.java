package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAluno;
import java.util.ArrayList;
import java.util.List;


public class ProcessoSeletivoTurmaDTO {
    private Long id;
    private String nome;
    private List<AprendizResumidoDTO> alunos = new ArrayList<>();
    private Long qtAlunos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<AprendizResumidoDTO> getAlunos() {
        return alunos;
    }
    
    public void setAlunos(List<AprendizResumidoDTO> alunos) {
        this.alunos = alunos;
    }

    public Long getQtAlunos() {
        return qtAlunos;
    }

    public void setQtAlunos(Long qtAluno) {
        this.qtAlunos = qtAluno;
    }
    
    public ProcessoSeletivoTurmaDTO(Turma t) {
        this.id = t.getId();
        this.nome = t.getNome();
    }

    public void addAlunos(List<TurmaAluno> alunosByTurma) {
        for (TurmaAluno t : alunosByTurma){
            this.alunos.add(new AprendizResumidoDTO(t.getAluno()));
        }
    }
    
    public ProcessoSeletivoTurmaDTO(){
        
    }
    
    public void atualizarQtAlunos(){
        this.qtAlunos = new Long(this.alunos.size());
    }
            

}