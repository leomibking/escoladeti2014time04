package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Professor;


public class TurmaProfessorDTO {
    
    private Long id;
    private String nome;

    public TurmaProfessorDTO(Professor professor) {
        this.id = professor.getId();
        this.nome = professor.getNome();
    }

    public TurmaProfessorDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}