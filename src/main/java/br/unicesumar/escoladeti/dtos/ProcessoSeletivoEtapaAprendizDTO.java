package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;


public class ProcessoSeletivoEtapaAprendizDTO {
    
    private Long id;
    private Long idAprendiz;
    private String nomeAprendiz;
    private String resultadoAberto;
    private Double resultadoValor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdAprendiz() {
        return idAprendiz;
    }

    public void setIdAprendiz(Long idAprendiz) {
        this.idAprendiz = idAprendiz;
    }

    public String getNomeAprendiz() {
        return nomeAprendiz;
    }

    public void setNomeAprendiz(String nomeAprendiz) {
        this.nomeAprendiz = nomeAprendiz;
    }

    public String getResultadoAberto() {
        return resultadoAberto;
    }

    public void setResultadoAberto(String resultadoAberto) {
        this.resultadoAberto = resultadoAberto;
    }

    public Double getResultadoValor() {
        return resultadoValor;
    }

    public void setResultadoValor(Double resultadoValor) {
        this.resultadoValor = resultadoValor;
    }
    
    public ProcessoSeletivoEtapaAprendizDTO(ProcessoSeletivoEtapaAprendiz e) {
        this.id = e.getId();
        this.idAprendiz = e.getCandidato().getId();
        this.nomeAprendiz = e.getCandidato().getNome();
        this.resultadoAberto = e.getResultadoAberto();
        this.resultadoValor  = e.getResultadoValor();
    }
    
    public ProcessoSeletivoEtapaAprendizDTO(){
        
    }
    

}