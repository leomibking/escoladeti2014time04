package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;


public class ProcessoSeletivoAprendizDTO {
    private CandidatoProcessoDTO candidato;
    private Long id;
    private boolean aprovado;
    private boolean selecionado;

    public CandidatoProcessoDTO getCandidato() {
        return candidato;
    }

    public void setCandidato(CandidatoProcessoDTO candidato) {
        this.candidato = candidato;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }

    public boolean isSelecionado() {
        return selecionado;
    }

    public void setSelecionado(boolean selecionado) {
        this.selecionado = selecionado;
    }
    
    public ProcessoSeletivoAprendizDTO(ProcessoSeletivoAprendiz pa) {
        this.candidato = new CandidatoProcessoDTO(pa.getCandidato());
        this.id = pa.getId();
        this.aprovado = pa.isAprovado();
    }
    
    public ProcessoSeletivoAprendizDTO(){
        
    }

}