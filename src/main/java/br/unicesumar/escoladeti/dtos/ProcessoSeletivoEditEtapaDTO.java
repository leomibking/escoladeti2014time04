package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;


public class ProcessoSeletivoEditEtapaDTO {
    private Long id;
    private Long idEtapa;
    private String descricao;
    private String tipoEtapa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdEtapa() {
        return idEtapa;
    }

    public void setIdEtapa(Long idEtapa) {
        this.idEtapa = idEtapa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipoEtapa() {
        return tipoEtapa;
    }

    public void setTipoEtapa(String tipoEtapa) {
        this.tipoEtapa = tipoEtapa;
    }
    
    public ProcessoSeletivoEditEtapaDTO() {
    }
    
    public ProcessoSeletivoEditEtapaDTO(ProcessoSeletivoEtapa pe){
        this.id = pe.getId();
        this.idEtapa = pe.getEtapa().getId();
        this.descricao = pe.getEtapa().getDescricao();
        this.tipoEtapa = pe.getEtapa().getTipoEtapa().getDescricao();
    }
}