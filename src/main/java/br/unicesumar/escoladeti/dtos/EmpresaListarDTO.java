package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Empresa;
import br.unicesumar.escoladeti.entity.Telefone;
import br.unicesumar.escoladeti.entity.Vaga;
import java.util.ArrayList;
import java.util.List;

public class EmpresaListarDTO extends BaseDTO {

    private Long id;
    private String razaoSocial;
    private String nome;
    private String cnpj;
    private int vagas;
    private List<TelefoneDTO> telefones = new ArrayList<>();

    public EmpresaListarDTO() {

    }

    public EmpresaListarDTO(Empresa e) {
        this.id = e.getId();
        this.razaoSocial = e.getRazaoSocial();
        this.nome = e.getNome();
        this.cnpj = e.getCnpj();
        if (e.getTelefones() != null) {
            for (Telefone t : e.getTelefones()) {
                this.telefones.add(new TelefoneDTO(t));
            }
        }
        int totalVagas = 0;
        for (Vaga v : e.getVagas()) {
            totalVagas += v.getQuantidade();
        }
        this.vagas = totalVagas;
    }

    public List<TelefoneDTO> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<TelefoneDTO> telefones) {
        this.telefones = telefones;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public int getVagas() {
        return vagas;
    }

    public void setVagas(int vagas) {
        this.vagas = vagas;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
