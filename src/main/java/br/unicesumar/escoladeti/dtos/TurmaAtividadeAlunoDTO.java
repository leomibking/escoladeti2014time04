package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.TurmaAtividadeAluno;


public class TurmaAtividadeAlunoDTO {
    
    private Long id;
    private Double nota;
    private String observacao;
    private TurmaAlunoDTO aluno;

    public TurmaAtividadeAlunoDTO() {
    }
    
    public TurmaAtividadeAlunoDTO(TurmaAtividadeAluno taa) {
        this.id = taa.getId();
        this.nota = taa.getNota();
        this.observacao = taa.getObservacao();
        this.aluno = new TurmaAlunoDTO(taa.getAluno());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getNota() {
        return nota;
    }

    public void setNota(Double nota) {
        this.nota = nota;
    }

    public TurmaAlunoDTO getAluno() {
        return aluno;
    }

    public void setAluno(TurmaAlunoDTO aluno) {
        this.aluno = aluno;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
}