package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.types.StatusProcessoSeletivo;
import java.util.Date;


public class ProcessoSeletivoIndexDTO {
    
    private Long id;
    private String nome;
    private Date dataInicio;
    private Date dataFim;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public ProcessoSeletivoIndexDTO(ProcessoSeletivo p){
        this.id = p.getId();
        this.nome = p.getNome();
        this.dataInicio = p.getDataInicio();
        this.dataFim = p.getDataFim();
        this.status = p.getStatus().getDescricao();
    }

}