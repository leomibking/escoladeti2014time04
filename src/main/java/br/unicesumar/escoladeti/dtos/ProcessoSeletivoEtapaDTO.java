package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;


public class ProcessoSeletivoEtapaDTO {
    private EtapaDTO etapa;
    private Long id;
    
    private String status;

    public EtapaDTO getEtapa() {
        return etapa;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setEtapa(EtapaDTO etapa) {
        this.etapa = etapa;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public ProcessoSeletivoEtapaDTO(ProcessoSeletivoEtapa pe) {
        this.etapa = new EtapaDTO(pe.getEtapa());
        this.id = pe.getId();
        if (pe.getStatus() != null){
            this.status = pe.getStatus().getDescricao();
        }
    }
    
    public ProcessoSeletivoEtapaDTO(){        
    }

}