package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Contrato;
import java.util.Date;

public class ContratoShowDTO {

    private Long id;
    private Date dataInicio;
    private Date dataFim;
    private String status;
    private Long idAprendiz;
    private Long idTurma;
    private Long idVaga;
    private Long idEmpresa;
    private String nomeAprendiz;
    private String nomeTurma;
    private String nomeVaga;
    private String nomeEmpresa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public Long getIdAprendiz() {
        return idAprendiz;
    }

    public void setIdAprendiz(Long idAprendiz) {
        this.idAprendiz = idAprendiz;
    }

    public Long getIdTurma() {
        return idTurma;
    }

    public void setIdTurma(Long idTurma) {
        this.idTurma = idTurma;
    }

    public Long getIdVaga() {
        return idVaga;
    }

    public void setIdVaga(Long idVaga) {
        this.idVaga = idVaga;
    }

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNomeAprendiz() {
        return nomeAprendiz;
    }

    public void setNomeAprendiz(String nomeAprendiz) {
        this.nomeAprendiz = nomeAprendiz;
    }

    public String getNomeTurma() {
        return nomeTurma;
    }

    public void setNomeTurma(String nomeTurma) {
        this.nomeTurma = nomeTurma;
    }

    public String getNomeVaga() {
        return nomeVaga;
    }

    public void setNomeVaga(String nomeVaga) {
        this.nomeVaga = nomeVaga;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public ContratoShowDTO(Contrato c) {
        this.id = c.getId();
        this.dataInicio = c.getDataInicio();
        this.dataFim = c.getDataFim();
        this.status = c.getStatus().getDescricao();
        this.idAprendiz = c.getAprendiz().getId();
        this.idTurma = c.getTurma().getId();
        this.idVaga = c.getVaga().getId();
        this.idEmpresa = c.getVaga().getEmpresa().getId();
        this.nomeAprendiz = c.getAprendiz().getNome();
        this.nomeTurma = c.getTurma().getNome();
        this.nomeVaga = c.getVaga().getAtividade();
        this.nomeEmpresa = c.getVaga().getEmpresa().getNome();
    }

}
