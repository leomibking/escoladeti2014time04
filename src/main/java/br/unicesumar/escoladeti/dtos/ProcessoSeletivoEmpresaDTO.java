package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Empresa;

public class ProcessoSeletivoEmpresaDTO {

    private Long id;
    private String razaoSocial;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public ProcessoSeletivoEmpresaDTO(Empresa e) {
        this.id = e.getId();
        this.razaoSocial = e.getRazaoSocial();
    }
    
    public ProcessoSeletivoEmpresaDTO(){
        
    }

}
