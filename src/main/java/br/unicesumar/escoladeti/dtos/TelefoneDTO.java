package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Telefone;

public class TelefoneDTO {

    private String ddd;
    private String numero;
    private String tipo;

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public TelefoneDTO() {

    }

    public TelefoneDTO(Telefone t) {
        this.ddd = t.getDdd();
        this.numero = t.getNumero();
        this.tipo = t.getTipo().getDescricao();
    }

}
