package br.unicesumar.escoladeti.dtos;

import java.util.Date;


public class AprendizLogDTO {
    private String nome;
    private Date data;
    private String observacao;
    private String acao;//O que foi feito, Inserção? Edição? Exclusao?

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getAcao() {
        return acao;
    }

    public void setAcao(String acao) {
        this.acao = acao;
    }
}
