package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;

public class ProcessoSeletivoAprendizEditDTO {

    private AprendizResumidoDTO candidato;

    public Long id;

    public AprendizResumidoDTO getCandidato() {
        return candidato;
    }

    public void setCandidato(AprendizResumidoDTO candidato) {
        this.candidato = candidato;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProcessoSeletivoAprendizEditDTO(ProcessoSeletivoAprendiz psa) {
        this.candidato = new AprendizResumidoDTO(psa.getCandidato());
        this.id = psa.getId();
    }

}
