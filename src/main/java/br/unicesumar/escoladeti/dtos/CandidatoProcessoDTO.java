package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Aprendiz;
import java.util.Date;

public class CandidatoProcessoDTO {

    private Long id;
    private String nome;
    private Date data;
    private String endereco;
    private String rendaPropria;
    private String rendaTotal;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getRendaPropria() {
        return rendaPropria;
    }

    public void setRendaPropria(String rendaPropria) {
        this.rendaPropria = rendaPropria;
    }

    public String getRendaTotal() {
        return rendaTotal;
    }

    public void setRendaTotal(String rendaTotal) {
        this.rendaTotal = rendaTotal;
    }

    
    
    public CandidatoProcessoDTO(Aprendiz a) {
        this.id = a.getId();
        this.nome = a.getNome();
        this.data = a.getDataNascimento();
        this.rendaPropria = a.getRendaPropria();
        this.rendaTotal = a.getRendaTotal();
        if (a.getEndereco(0) != null) {
            this.endereco = a.getEndereco(0).toString();
        }
    }
    
    public CandidatoProcessoDTO(){
        
    }
    
}
