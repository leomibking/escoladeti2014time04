package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.TurmaChamadaAluno;

public class TurmaChamadaAlunoDTO {
    
    private Long id;
    private boolean presente;
    private String observacao;
    private TurmaAlunoDTO aluno;

    public TurmaChamadaAlunoDTO() {
    }

    public TurmaChamadaAlunoDTO(TurmaChamadaAluno tca) {
        this.id = tca.getId();
        this.presente = tca.isPresente();
        this.observacao = tca.getObservacao();
        this.aluno = new TurmaAlunoDTO(tca.getAluno());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPresente() {
        return presente;
    }

    public void setPresente(boolean presente) {
        this.presente = presente;
    }

    public TurmaAlunoDTO getAluno() {
        return aluno;
    }

    public void setAluno(TurmaAlunoDTO aluno) {
        this.aluno = aluno;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
    
    
}