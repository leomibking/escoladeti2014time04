package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import br.unicesumar.escoladeti.entity.Turma;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class ProcessoSeletivoShowDTO {
    private Long id;
    private String nome;
    private Date dataInicio;
    private Date dataFim;
    private String status;
    private ProcessoSeletivoEmpresaDTO empresa;
    private ProcessoSeletivoEmpresaVagaDTO vaga;
    
    
    private List<ProcessoSeletivoAprendizDTO> selecionados = new ArrayList<>();
    private List<ProcessoSeletivoAprendizDTO> aprovados = new ArrayList<>();
    private List<ProcessoSeletivoEtapaDTO> etapas = new ArrayList<>();
    private List<ProcessoSeletivoTurmaDTO> turmas = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<ProcessoSeletivoAprendizDTO> getSelecionados() {
        return selecionados;
    }

    public void setSelecionados(List<ProcessoSeletivoAprendizDTO> selecionados) {
        this.selecionados = selecionados;
    }

    public List<ProcessoSeletivoEtapaDTO> getEtapas() {
        return etapas;
    }

    public void setEtapas(List<ProcessoSeletivoEtapaDTO> etapas) {
        this.etapas = etapas;
    }

    public ProcessoSeletivoEmpresaDTO getEmpresa() {
        return empresa;
    }

    public void setEmpresa(ProcessoSeletivoEmpresaDTO empresa) {
        this.empresa = empresa;
    }

    public List<ProcessoSeletivoAprendizDTO> getAprovados() {
        return aprovados;
    }

    public void setAprovados(List<ProcessoSeletivoAprendizDTO> aprovados) {
        this.aprovados = aprovados;
    }

    public List<ProcessoSeletivoTurmaDTO> getTurmas() {
        return turmas;
    }

    public void setTurmas(List<ProcessoSeletivoTurmaDTO> turmas) {
        this.turmas = turmas;
    }

    public ProcessoSeletivoEmpresaVagaDTO getVaga() {
        return vaga;
    }

    public void setVaga(ProcessoSeletivoEmpresaVagaDTO vaga) {
        this.vaga = vaga;
    }
    
    public ProcessoSeletivoShowDTO(ProcessoSeletivo p){
        this.id = p.getId();
        this.nome = p.getNome();
        this.dataInicio = p.getDataInicio();
        this.dataFim = p.getDataFim();
        this.status = p.getStatus().getDescricao();
        
        if (p.getEmpresa() != null) {
            this.empresa = new ProcessoSeletivoEmpresaDTO(p.getEmpresa());
        }
        
        if(p.getVaga() != null){
            this.vaga = new ProcessoSeletivoEmpresaVagaDTO(p.getVaga());
        }
        
        for (ProcessoSeletivoAprendiz pa : p.getCandidatos()){
            this.selecionados.add(new ProcessoSeletivoAprendizDTO(pa));
        }
        
        for (Turma t : p.getTurmas()){
            this.turmas.add(new ProcessoSeletivoTurmaDTO(t));
        }
        
        for (ProcessoSeletivoEtapa pe : p.getEtapas()){
            this.etapas.add(new ProcessoSeletivoEtapaDTO(pe));
        }
    }
    
    public ProcessoSeletivoShowDTO(){
        
    }
}