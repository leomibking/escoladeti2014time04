package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Contrato;
import java.util.Date;

public class ContratoIndexDTO {

    private Long id;
    private Date dataInicio;
    private Date dataFim;
    private String status;
    private String nomeAprendiz;
    private String nomeVaga;
    private String nomeEmpresa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNomeAprendiz() {
        return nomeAprendiz;
    }

    public void setNomeAprendiz(String nomeAprendiz) {
        this.nomeAprendiz = nomeAprendiz;
    }

    public String getNomeVaga() {
        return nomeVaga;
    }

    public void setNomeVaga(String nomeVaga) {
        this.nomeVaga = nomeVaga;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public ContratoIndexDTO(Contrato c) {
        this.id = c.getId();
        this.dataInicio = c.getDataInicio();
        this.dataFim = c.getDataFim();
        this.status = c.getStatus().getDescricao();
        this.nomeAprendiz = c.getAprendiz().getNome();
        this.nomeVaga = c.getVaga().getAtividade();
        this.nomeEmpresa = c.getVaga().getEmpresa().getNome();
    }

}
