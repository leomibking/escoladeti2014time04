package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Turma;


public class TurmaDTO {
    private Long id;
    
    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nomeTurma) {
        this.nome = nomeTurma;
    }
    
    public TurmaDTO(Turma t){
        this.id = t.getId();
        this.nome = t.getNome();
    }
    
    public TurmaDTO(){
        
    }
    
}