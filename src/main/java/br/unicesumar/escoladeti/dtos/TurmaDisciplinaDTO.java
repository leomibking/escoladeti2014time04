package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Disciplina;


public class TurmaDisciplinaDTO {
    
    private Long id;
    private String nome;

    public TurmaDisciplinaDTO(Disciplina disciplina) {
        this.id = disciplina.getId();
        this.nome = disciplina.getNome();
    }
    
    public TurmaDisciplinaDTO(){
        
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
    

}