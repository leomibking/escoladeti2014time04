package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProcessoSeletivoEditDTO {

    private Long id;
    //private String nome;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataInicio;

    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataFim;
    
    private ProcessoSeletivoEmpresaDTO empresa;
    private ProcessoSeletivoEmpresaVagaDTO vaga;

    private List<ProcessoSeletivoEditEtapaDTO> etapas = new ArrayList<>();
    
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }*/

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }
   
    public List<ProcessoSeletivoEditEtapaDTO> getEtapas() {
        return etapas;
    }

    public void setEtapas(List<ProcessoSeletivoEditEtapaDTO> etapas) {
        this.etapas = etapas;
    }

    public ProcessoSeletivoEmpresaDTO getEmpresa() {
        return empresa;
    }

    public void setEmpresa(ProcessoSeletivoEmpresaDTO empresa) {
        this.empresa = empresa;
    }

    public ProcessoSeletivoEmpresaVagaDTO getVaga() {
        return vaga;
    }

    public void setVaga(ProcessoSeletivoEmpresaVagaDTO vaga) {
        this.vaga = vaga;
    }

    public ProcessoSeletivoEditDTO() {

    }

    public ProcessoSeletivoEditDTO(ProcessoSeletivo p) {
        this.id = p.getId();
        //this.nome = p.getNome();
        this.dataInicio = p.getDataInicio();
        this.dataFim = p.getDataFim();
        this.status = p.getStatus().getDescricao();
        this.etapas = new ArrayList<>();
        for (ProcessoSeletivoEtapa e : p.getEtapas()) {
            this.etapas.add(new ProcessoSeletivoEditEtapaDTO(e));
        }
        if (p.getEmpresa() != null) {
            this.empresa = new ProcessoSeletivoEmpresaDTO(p.getEmpresa());
        }

        if (p.getVaga() != null) {
            this.vaga = new ProcessoSeletivoEmpresaVagaDTO(p.getVaga());
        }
    }

}
