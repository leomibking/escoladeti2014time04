package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.entity.TurmaChamada;
import br.unicesumar.escoladeti.entity.TurmaChamadaAluno;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TurmaChamadaEditDTO {
    
    private TurmaDisciplinaDTO disciplina;
    private TurmaProfessorDTO professor;
    private List<TurmaChamadaAlunoDTO> alunos;
    
    private Long id;
    
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataChamada;
    
    private int qtAulas;
    private String descricao;

    public TurmaChamadaEditDTO() {
    }

    public TurmaChamadaEditDTO(TurmaChamada tc) {
        this.id = tc.getId();
        this.dataChamada = tc.getDataChamada();
        this.descricao = tc.getDescricao();
        this.qtAulas = tc.getQtAulas();
        this.disciplina = new TurmaDisciplinaDTO(tc.getDisciplina());
        this.professor = new TurmaProfessorDTO(tc.getProfessor());
        this.alunos = new ArrayList<>();
        
        for (TurmaChamadaAluno tca : tc.getAlunos()){
            this.alunos.add(new TurmaChamadaAlunoDTO(tca));
        }
    }
    
    public TurmaDisciplinaDTO getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(TurmaDisciplinaDTO disciplina) {
        this.disciplina = disciplina;
    }

    public TurmaProfessorDTO getProfessor() {
        return professor;
    }

    public void setProfessor(TurmaProfessorDTO professor) {
        this.professor = professor;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataChamada() {
        return dataChamada;
    }

    public void setDataChamada(Date dataChamada) {
        this.dataChamada = dataChamada;
    }

    public int getQtAulas() {
        return qtAulas;
    }

    public void setQtAulas(int qtAulas) {
        this.qtAulas = qtAulas;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<TurmaChamadaAlunoDTO> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<TurmaChamadaAlunoDTO> alunos) {
        this.alunos = alunos;
    }

    public TurmaChamadaAlunoDTO find(TurmaChamadaAluno tal) {
        for (TurmaChamadaAlunoDTO ta : this.getAlunos()){
            if (ta.getId() == tal.getId()){
                return ta;
            }
        }
        return null;
    }
    
}