package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Aprendiz;

public class TurmaAlunoDTO {
    
    private Long id;
    private String nome;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TurmaAlunoDTO() {
    }

    public TurmaAlunoDTO(Aprendiz a) {
        this.id = a.getId();
        this.nome = a.getNome();
    }

}