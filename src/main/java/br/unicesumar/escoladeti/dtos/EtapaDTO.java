package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Etapa;


public class EtapaDTO {
    private Long idEtapa;
    private String descricao;
    private String tipoEtapa;

    public Long getIdEtapa() {
        return idEtapa;
    }

    public void setIdEtapa(Long id) {
        this.idEtapa = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipoEtapa() {
        return tipoEtapa;
    }

    public void setTipoEtapa(String tipoEtapa) {
        this.tipoEtapa = tipoEtapa;
    }
    
    public EtapaDTO(Etapa etapa) {
        this.idEtapa = etapa.getId();
        this.descricao = etapa.getDescricao();
        this.tipoEtapa = etapa.getTipoEtapa().getDescricao();
    }
    
    public EtapaDTO(){
        
    }

}