package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Vaga;


public class ProcessoSeletivoEmpresaVagaDTO {
    private Long id;    
    private String nome;
    private Long quantidade;
    private Long cargaHoraria;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Long quantidade) {
        this.quantidade = quantidade;
    }

    public Long getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(Long cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public ProcessoSeletivoEmpresaVagaDTO(Vaga v) {
        this.id = v.getId();
        this.nome = v.getProfissao().getNome();
        this.quantidade = new Long(v.getQuantidade());
        this.cargaHoraria = new Long(v.getCargaHorarioMes());
    }
    
    public ProcessoSeletivoEmpresaVagaDTO(){
        
    }
}