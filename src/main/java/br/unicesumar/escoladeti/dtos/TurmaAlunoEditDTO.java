package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.TurmaAluno;

public class TurmaAlunoEditDTO {
    
    private Long id;
    private TurmaAlunoDTO aluno;
    private String observacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TurmaAlunoDTO getAluno() {
        return aluno;
    }

    public void setAluno(TurmaAlunoDTO aluno) {
        this.aluno = aluno;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public TurmaAlunoEditDTO() {
    }

    public TurmaAlunoEditDTO(TurmaAluno ta) {
        this.id = ta.getId();
        this.aluno = new TurmaAlunoDTO(ta.getAluno());
        this.observacao = ta.getObservacaoAluno();
    }
}