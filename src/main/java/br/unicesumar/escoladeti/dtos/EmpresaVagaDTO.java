package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Profissao;
import br.unicesumar.escoladeti.types.Periodo;

public class EmpresaVagaDTO extends BaseDTO {

    private Long idEmpresa;
    private Long idVaga;
    private Profissao profissao;
    private int quantidade;
    private int cargaHorarioMes;
    private Periodo periodo;
    private String atividade;
    

    public Long getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Long getIdVaga() {
        return idVaga;
    }

    public void setIdVaga(Long idVaga) {
        this.idVaga = idVaga;
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getCargaHorarioMes() {
        return cargaHorarioMes;
    }

    public void setCargaHorarioMes(int cargaHorarioMes) {
        this.cargaHorarioMes = cargaHorarioMes;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }
}
