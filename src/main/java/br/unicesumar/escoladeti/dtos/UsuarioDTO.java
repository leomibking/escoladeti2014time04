package br.unicesumar.escoladeti.dtos;

public class UsuarioDTO{

    private String login;

    private String senha;

    public String getLogin() {
        return login;
    }

    public void setLogin(String usuario) {
        this.login = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
