package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.TurmaAluno;

public class TurmaAlunoShowDTO {
    private Long id;
    private TurmaAlunoDTO aluno;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TurmaAlunoDTO getAluno() {
        return aluno;
    }

    public void setAluno(TurmaAlunoDTO aluno) {
        this.aluno = aluno;
    }
    
    public TurmaAlunoShowDTO(){
        
    }
    public TurmaAlunoShowDTO(TurmaAluno t){
        this.id = t.getId();
        this.aluno = new TurmaAlunoDTO(t.getAluno());
    }    
}