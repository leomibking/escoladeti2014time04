package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Turma;
import java.util.ArrayList;
import java.util.List;

public class AprendizDadosAprendizagemDTO {

    private EmpresaListDTO empresa;
    private EscolaListarDTO escola;
    private VagaDTO vaga;
    private List<TurmaDTO> turmas = new ArrayList<>();

    public EmpresaListDTO getEmpresa() {
        return empresa;
    }

    public void setEmpresa(EmpresaListDTO empresa) {
        this.empresa = empresa;
    }

    public EscolaListarDTO getEscola() {
        return escola;
    }

    public void setEscola(EscolaListarDTO escola) {
        this.escola = escola;
    }

    public VagaDTO getVaga() {
        return vaga;
    }

    public void setVaga(VagaDTO vaga) {
        this.vaga = vaga;
    }

    public List<TurmaDTO> getTurmas() {
        return turmas;
    }

    public void setTurmas(List<TurmaDTO> turma) {
        this.turmas = turma;
    }

    public AprendizDadosAprendizagemDTO() {

    }

    public AprendizDadosAprendizagemDTO(Aprendiz a, List<Turma> t) {
        if (a.getEscola() != null) {
            this.escola = new EscolaListarDTO(a.getEscola());
        }

        if (a.getProcesso() != null) {
            this.empresa = new EmpresaListDTO(a.getProcesso().getEmpresa());
            this.vaga = new VagaDTO(a.getProcesso().getVaga());
        }
        
        if (t != null){
            for (Turma tu : t){
                this.turmas.add(new TurmaDTO(tu));
            }
        }
    }
}
