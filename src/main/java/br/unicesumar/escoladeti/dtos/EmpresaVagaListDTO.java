package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Vaga;


public class EmpresaVagaListDTO {
    private Long id;
    private String nome;
    private int quantidade;
    private int cargaHoraria;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public EmpresaVagaListDTO() {
    }

    public EmpresaVagaListDTO(Vaga v) {
        this.id = v.getId();
        this.nome = v.getProfissao().getNome();
        this.quantidade = v.getQuantidade();
        this.cargaHoraria = v.getCargaHorarioMes();
    }
    
}