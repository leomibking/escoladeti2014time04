package br.unicesumar.escoladeti.dtos;

public class TypeDTO extends BaseDTO {

    private String value;
    private String descricao;

    public TypeDTO(String value, String descricao) {
        this.value = value;
        this.descricao = descricao;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
