package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.entity.Turma;

public class TurmaShowDTO {

    private Long id;
    private String nome;
    
    //private List<TurmaAlunoShowDTO> alunos;
    //private List<TurmaAtividadeShowDTO> atividades;
    //private List<TurmaChamadaShowDTO> chamadas;
    
    private DataPage<TurmaAlunoShowDTO> alunos;
    private DataPage<TurmaAtividadeShowDTO> atividades;
    private DataPage<TurmaChamadaShowDTO> chamadas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    /*
    public List<TurmaAlunoShowDTO> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<TurmaAlunoShowDTO> alunos) {
        this.alunos = alunos;
    }

    public List<TurmaAtividadeShowDTO> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<TurmaAtividadeShowDTO> atividades) {
        this.atividades = atividades;
    }

    public List<TurmaChamadaShowDTO> getChamadas() {
        return chamadas;
    }

    public void setChamadas(List<TurmaChamadaShowDTO> chamadas) {
        this.chamadas = chamadas;
    } */
    
    public TurmaShowDTO() {
    }

    public TurmaShowDTO(Turma t) {        
        this.id = t.getId();
        this.nome = t.getNome();
        
        /*this.alunos = new ArrayList<>();
        this.atividades = new ArrayList<>();
        this.chamadas = new ArrayList<>();
        
        for (TurmaAluno a : t.getAlunos()) {
            this.alunos.add(new TurmaAlunoShowDTO(a));
        }

        for (TurmaAtividade ta : t.getAtividades()) {
            this.atividades.add(new TurmaAtividadeShowDTO(ta));
        }

        for (TurmaChamada tc : t.getListasDeChamada()) {
            this.chamadas.add(new TurmaChamadaShowDTO(tc));
        }*/
        
    }

    public DataPage<TurmaAlunoShowDTO> getAlunos() {
        return alunos;
    }

    public void setAlunos(DataPage<TurmaAlunoShowDTO> alunos) {
        this.alunos = alunos;
    }
    
    public DataPage<TurmaAtividadeShowDTO> getAtividades() {
        return atividades;
    }

    public void setAtividades(DataPage<TurmaAtividadeShowDTO> atividades) {
        this.atividades = atividades;
    }

    public DataPage<TurmaChamadaShowDTO> getChamadas() {
        return chamadas;
    }

    public void setChamadas(DataPage<TurmaChamadaShowDTO> chamadas) {
        this.chamadas = chamadas;
    }

}
