package br.unicesumar.escoladeti.dtos;

import br.unicesumar.escoladeti.entity.Atividade;

public class TurmaAtividadeDTO {

    private Long id;
    private String tipoAtividade;

    public TurmaAtividadeDTO(Atividade atividade) {
        this.id = atividade.getId();
        this.tipoAtividade = atividade.getTipoAtividade();
    }

    public TurmaAtividadeDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoAtividade() {
        return tipoAtividade;
    }

    public void setTipoAtividade(String tipoAtividade) {
        this.tipoAtividade = tipoAtividade;
    }
    
    

}
