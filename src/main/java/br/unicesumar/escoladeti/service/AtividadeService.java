package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Atividade;
import br.unicesumar.escoladeti.repository.AtividadeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AtividadeService extends BaseService<Atividade, AtividadeRepository> {
    @Override
    public String getCampoOrdenacao() {
        return "tipoAtividade";
    }
    
    @Override
    public Page<Atividade> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByTipoAtividadeContainingIgnoreCase(filtro, pr);
    }
}
