package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.TipoAdvertencia;
import br.unicesumar.escoladeti.repository.TipoAdvertenciaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TipoAdvertenciaService extends BaseService<TipoAdvertencia, TipoAdvertenciaRepository> {

    @Override
    public Page<TipoAdvertencia> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByDescricaoContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "descricao";
    }
}
