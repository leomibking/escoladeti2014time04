package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Email;
import br.unicesumar.escoladeti.entity.Empresa;
import br.unicesumar.escoladeti.entity.Endereco;
import br.unicesumar.escoladeti.entity.Professor;
import br.unicesumar.escoladeti.entity.Telefone;
import br.unicesumar.escoladeti.repository.ProfessorRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProfessorService extends BaseService<Professor, ProfessorRepository> {
    
    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private TelefoneService telefoneService;

    @Autowired
    private EmailService emailService;

    @Override
    public <S extends Professor> void save(S obj) {
        if (obj.getId() != null) {
            Professor professor = this.getById(obj.getId());

            List<Endereco> enderecos = professor.getEnderecos();
            enderecos.removeAll(obj.getEnderecos());
            for (Endereco e : enderecos) {
                enderecoService.delete(e.getId());
            }

            List<Telefone> telefones = professor.getTelefones();
            telefones.removeAll(obj.getTelefones());
            for (Telefone t : telefones) {
                telefoneService.delete(t.getId());
            }

            List<Email> emails = professor.getEmails();
            emails.removeAll(obj.getEmails());
            for (Email e : emails) {
                emailService.delete(e.getId());
            }
        }

        enderecoService.saveList(obj.getEnderecos());
        telefoneService.saveList(obj.getTelefones());
        emailService.saveList(obj.getEmails());
        super.save(obj); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }
    
    @Override
    public Page<Professor> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

}
