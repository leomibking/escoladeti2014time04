package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Midia;
import br.unicesumar.escoladeti.repository.MidiaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MidiaService extends BaseService<Midia, MidiaRepository>{
    
    @Override
    public Page<Midia> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByMidiaContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "midia";
    }
    
}
