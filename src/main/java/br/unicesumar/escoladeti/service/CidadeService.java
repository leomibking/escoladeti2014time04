package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.CidadeDTO;
import br.unicesumar.escoladeti.entity.Cidade;
import br.unicesumar.escoladeti.entity.UF;
import br.unicesumar.escoladeti.repository.CidadeRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CidadeService extends BaseService<Cidade, CidadeRepository> {

    @Autowired
    private UFService ufService;

    public List<CidadeDTO> findAllByUFId(Long ufId) {
        UF uf = ufService.getById(ufId);
        List<CidadeDTO> ret = new ArrayList<>();
        List<Cidade> data = getRepository().findAllByUfOrderByNomeAsc(uf);
        for (Cidade c : data){
            ret.add(new CidadeDTO(c));
        }
        return ret;
    }
    
    @Override
    public Page<Cidade> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }

}
