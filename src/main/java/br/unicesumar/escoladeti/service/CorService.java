package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Cor;
import br.unicesumar.escoladeti.repository.CorRepository;
import org.springframework.stereotype.Service;

@Service
public class CorService extends BaseService<Cor, CorRepository>{
    
}
