package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Endereco;
import br.unicesumar.escoladeti.repository.EnderecoRepository;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService extends BaseService<Endereco, EnderecoRepository> {

}
