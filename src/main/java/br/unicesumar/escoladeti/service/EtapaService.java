package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Etapa;
import br.unicesumar.escoladeti.repository.EtapaRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EtapaService extends BaseService<Etapa, EtapaRepository> {

    public List<Etapa> getByName(String nome) {
        return getRepository().findAllByDescricaoContainingIgnoreCaseOrderByDescricaoAsc(nome);
    }

    @Override
    public Page<Etapa> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByDescricaoContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "descricao";
    }
}
