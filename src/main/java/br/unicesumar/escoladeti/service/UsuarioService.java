package br.unicesumar.escoladeti.service;

import org.springframework.stereotype.Service;

import br.unicesumar.escoladeti.dtos.UsuarioDTO;
import br.unicesumar.escoladeti.entity.PerfilDeAcesso;
import br.unicesumar.escoladeti.entity.Usuario;
import br.unicesumar.escoladeti.repository.UsuarioRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Service
public class UsuarioService extends BaseService<Usuario, UsuarioRepository> {

    public void inicializarUsuarioAdmin() {
        Usuario root = repository.findByLogin("admin");
        if (root == null) {
            root = new Usuario();
            root.setNome("admin");
            root.setLogin("admin");
            root.setSenha("123mudar");
            PerfilDeAcesso perfil = new PerfilDeAcesso();
            perfil.setId(1L);
            root.setPerfilDeAcesso(perfil);
            repository.save(root);
        }
    }

    public Usuario getUsuario(UsuarioDTO usuario) {
        return repository.findByLoginAndSenha(usuario.getLogin(), usuario.getSenha());
    }

    @Override
    public Page<Usuario> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByLoginContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "login";
    }
}
