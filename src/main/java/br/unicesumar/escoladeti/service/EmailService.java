package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Email;
import br.unicesumar.escoladeti.repository.EmailRepository;
import org.springframework.stereotype.Service;

@Service
public class EmailService extends BaseService<Email, EmailRepository> {

}
