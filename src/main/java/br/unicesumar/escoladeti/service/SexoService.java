package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Sexo;
import br.unicesumar.escoladeti.repository.SexoRepository;
import org.springframework.stereotype.Service;

@Service
public class SexoService extends BaseService<Sexo, SexoRepository> {

}
