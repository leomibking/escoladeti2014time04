package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;
import br.unicesumar.escoladeti.repository.ProcessoSeletivoEtapaRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessoSeletivoEtapaService extends BaseService<ProcessoSeletivoEtapa, ProcessoSeletivoEtapaRepository> {

    @Autowired
    private ProcessoSeletivoEtapaAprendizService etapaAprendizService;

    public List<ProcessoSeletivoEtapa> getEtapas(ProcessoSeletivo ps) {
        return getRepository().findAllByProcessoOrderByEtapaDescricaoAsc(ps);
    }

    @Override
    public <S extends ProcessoSeletivoEtapa> void save(S obj) {
        ProcessoSeletivoEtapa pse = (ProcessoSeletivoEtapa) obj;
        super.save(obj);
        
        if (pse.getSelecionados() != null) {
            for (ProcessoSeletivoEtapaAprendiz a : pse.getSelecionados()) {
                etapaAprendizService.save(a);
            }
        }

        
    }

    public void removerSelecionados(ProcessoSeletivoEtapa etapa) {
        for (ProcessoSeletivoEtapaAprendiz aprendiz : etapa.getSelecionados()){
            etapaAprendizService.delete(aprendiz.getId());
        }
        etapa.getSelecionados().clear();
        this.save(etapa);
    }

}
