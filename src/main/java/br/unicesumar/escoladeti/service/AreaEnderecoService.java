package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.AreaEndereco;
import br.unicesumar.escoladeti.repository.AreaEnderecoRepository;
import org.springframework.stereotype.Service;

@Service
public class AreaEnderecoService extends BaseService<AreaEndereco, AreaEnderecoRepository>{
    
}
