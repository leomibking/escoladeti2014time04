package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAluno;
import br.unicesumar.escoladeti.repository.TurmaAlunoRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class TurmaAlunoService extends BaseService<TurmaAluno, TurmaAlunoRepository> {

    public DataPage<TurmaAluno> getAlunosDaTurmaPaginado(Turma t, Integer pageIndex) {
        return new DataPage<>(getRepository().findAllByTurmaOrderByAlunoNomeAsc(t,DataPage.pageRequestForAsc(pageIndex, "AlunoNome")));
    }
    
    public List<TurmaAluno> getAlunosDoProcessoSeletivo(Turma t, ProcessoSeletivo p){
        return getRepository().findAllByTurmaAndProcessoSeletivoOrderByAlunoNomeAsc(t,p);
    }

    public List<Turma> getTurmasDoAprendiz(Aprendiz a) {
        List<TurmaAluno> turmas = getRepository().findAllByAlunoOrderByAlunoNomeAsc(a);
        List<Turma> ret = new ArrayList<>();
        for (TurmaAluno aluno : turmas){
            ret.add(aluno.getTurma());
        }
        return ret;
    }

}
