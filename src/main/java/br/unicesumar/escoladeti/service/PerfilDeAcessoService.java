package br.unicesumar.escoladeti.service;

import org.springframework.stereotype.Service;

import br.unicesumar.escoladeti.entity.PerfilDeAcesso;
import br.unicesumar.escoladeti.entity.Usuario;
import br.unicesumar.escoladeti.repository.PerfilDeAcessoRepository;

@Service
public class PerfilDeAcessoService extends BaseService<PerfilDeAcesso, PerfilDeAcessoRepository> {

	public void inicializaPerfis() {
		inicializarUsuarioAdmin();
		inicializarUsuarioEmpresa();
		inicializarUsuarioEscola();
	}
	
	public void inicializarUsuarioAdmin() {
		PerfilDeAcesso perfil = repository.findByNome("admin");
		if (perfil == null) {
			perfil = new PerfilDeAcesso();
			perfil.setNome("admin");
			perfil.setDescricao("Administrador: tem acesso total ao sistema");			
			repository.save(perfil);
		}
	}
	
	public void inicializarUsuarioEscola() {
		PerfilDeAcesso perfil = repository.findByNome("escola");
		if (perfil == null) {
			perfil = new PerfilDeAcesso();
			perfil.setNome("escola");
			perfil.setDescricao("Escola: tem acesso às ações de escola");			
			repository.save(perfil);
		}
	}
	
	public void inicializarUsuarioEmpresa() {
		PerfilDeAcesso perfil = repository.findByNome("empresa");
		if (perfil == null) {
			perfil = new PerfilDeAcesso();
			perfil.setNome("empresa");
			perfil.setDescricao("Empresa: tem acesso às ações de empresa");			
			repository.save(perfil);
		}
	}

}
