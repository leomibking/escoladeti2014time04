package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.MotivoParticiparPrograma;
import br.unicesumar.escoladeti.repository.MotivoParticiparProgramaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class MotivoParticiparProgramaService extends BaseService<MotivoParticiparPrograma, MotivoParticiparProgramaRepository> {

    @Override
    public Page<MotivoParticiparPrograma> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByDescricaoContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "descricao";
    }
}
