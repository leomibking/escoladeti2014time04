package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.StatusServicoMilitar;
import br.unicesumar.escoladeti.repository.StatusServicoMilitarRepository;
import org.springframework.stereotype.Service;

@Service
public class StatusServicoMilitarService extends BaseService<StatusServicoMilitar, StatusServicoMilitarRepository> {

}
