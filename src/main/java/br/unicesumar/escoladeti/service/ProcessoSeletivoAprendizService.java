package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;
import br.unicesumar.escoladeti.repository.ProcessoSeletivoAprendizRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class ProcessoSeletivoAprendizService extends BaseService<ProcessoSeletivoAprendiz, ProcessoSeletivoAprendizRepository>{

    public DataPage<ProcessoSeletivoAprendiz> getSelecionadosPaginado(ProcessoSeletivo ps, Integer pageIndex) {
       return new DataPage<>(getRepository().findAllByProcessoOrderByCandidatoNomeAsc(ps,DataPage.pageRequestForAsc(pageIndex, "CandidatoNome")));
    }
    
    public DataPage<ProcessoSeletivoAprendiz> getAprovadosPaginados(ProcessoSeletivo ps, Integer pageIndex) {
        return new DataPage<>(getRepository().findAllByProcessoAndAprovadoTrueOrderByCandidatoNomeAsc(ps,DataPage.pageRequestForAsc(pageIndex, "CandidatoNome")));
    }

    public List<ProcessoSeletivoAprendiz> getAprovados(ProcessoSeletivo ps) {
        return getRepository().findAllByProcessoAndAprovadoTrueOrderByCandidatoNomeAsc(ps);
    }

}