package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.TipoSituacao;
import br.unicesumar.escoladeti.repository.TipoSituacaoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TipoSituacaoService extends BaseService<TipoSituacao, TipoSituacaoRepository> {

    @Override
    public Page<TipoSituacao> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByDescricaoContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "descricao";
    }
}
