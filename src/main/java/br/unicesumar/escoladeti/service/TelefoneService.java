package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Telefone;
import br.unicesumar.escoladeti.repository.TelefoneRepository;
import org.springframework.stereotype.Service;

@Service
public class TelefoneService extends BaseService<Telefone, TelefoneRepository> {

}
