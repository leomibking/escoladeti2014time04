package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.AprendizEspecificacao;
import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.dtos.AprendizDadosAprendizagemDTO;
import br.unicesumar.escoladeti.dtos.AprendizDocumentoDTO;
import br.unicesumar.escoladeti.dtos.CandidatoProcessoDTO;
import br.unicesumar.escoladeti.dtos.FilterDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Documento;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.repository.AprendizRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AprendizService extends BaseService<Aprendiz, AprendizRepository> {

    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private TelefoneService telefoneService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private DocumentoService documentoService;

    @Autowired
    private LogService logService;

    @Autowired
    private TurmaAlunoService tas;

    @Override
    public <S extends Aprendiz> void save(S obj) {
        enderecoService.saveList(obj.getEnderecos());
        telefoneService.saveList(obj.getTelefones());
        emailService.saveList(obj.getEmails());
        documentoService.saveList(obj.getDocumentos());
        super.save(obj);
    }

    public void salvarDocumento(AprendizDocumentoDTO dto) {
        Documento d;
        if (dto.getIdDocumento() != null) {
            d = documentoService.getById(dto.getIdDocumento());
        } else {
            d = new Documento();
        }

        d.setNome(dto.getNome());
        d.setDtEntrega(dto.getDtEntrega());

        this.documentoService.save(d);

        if (dto.getIdDocumento() == null) {
            Aprendiz a = this.getById(dto.getIdAprendiz());
            a.addDocumento(d);
            this.save(a);
        }
    }

    public void removerDocumento(Long idAprendiz, Long idDocumento) {
        Documento d = this.documentoService.getById(idDocumento);
        Aprendiz a = this.getById(idAprendiz);
        a.getDocumentos().remove(d);
        this.documentoService.delete(idDocumento);
        this.save(a);
    }

    public List<Aprendiz> getByName(String nome) {
        return getRepository().findAllByNomeContainingIgnoreCaseOrderByNomeAsc(nome);
    }

    public DataPage<CandidatoProcessoDTO> getAprendizesPreSelecao(Integer pageIndex, FilterDTO filtro) {
        DataPage<Aprendiz> pa = new DataPage(
                getRepository().findAll(
                        AprendizEspecificacao.getWhere(filtro.getFiltros()), DataPage.pageRequestForAsc(
                                pageIndex, this.getCampoOrdenacao()
                        )
                )
        );
        return ParserFactory.getCandidatoDTO().parse(pa);
    }

    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }

    @Override
    public Page<Aprendiz> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

    public AprendizDadosAprendizagemDTO getDadosAprendizagem(Long id) {
        Aprendiz a = this.getById(id);
        AprendizDadosAprendizagemDTO ret;
        if (a != null) {
            ret = new AprendizDadosAprendizagemDTO(a, this.getTurmasDoAprendiz(a));
        } else {
            ret = new AprendizDadosAprendizagemDTO();
        }

        return ret;
    }

    private List<Turma> getTurmasDoAprendiz(Aprendiz a) {
        return tas.getTurmasDoAprendiz(a);
    }

}
