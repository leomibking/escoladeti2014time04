package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Empresa;
import org.springframework.stereotype.Service;
import br.unicesumar.escoladeti.entity.Vaga;
import br.unicesumar.escoladeti.repository.VagaRepository;
import java.util.ArrayList;
import java.util.List;

@Service
public class VagaService extends BaseService<Vaga, VagaRepository> {

    @Override
    public <S extends Vaga> void save(S obj) {
        obj.setStatus("ABERTO");
        super.save(obj);
    }

    @Override
    public <S extends Vaga> void saveList(Iterable<S> objList) {
        for (Vaga v : objList) {
            v.setStatus("ABERTO");
        }
        super.saveList(objList);
    }


    public List<Vaga> getVagasByIdEmpresa(Empresa e) {
       return this.repository.findAllByEmpresaAndQuantidadeGreaterThanOrderByIdAsc(e,0);
    }
}
