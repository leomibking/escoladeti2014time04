package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.dtos.EscolaListarDTO;
import br.unicesumar.escoladeti.entity.Email;
import br.unicesumar.escoladeti.entity.Endereco;
import br.unicesumar.escoladeti.entity.Escola;
import br.unicesumar.escoladeti.entity.Telefone;
import br.unicesumar.escoladeti.repository.EscolaRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EscolaService extends BaseService<Escola, EscolaRepository> {

    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TelefoneService telefoneService;

    @Override
    public <S extends Escola> void save(S obj) {
        if (obj.getId() != null) {
            Escola escola = this.getById(obj.getId());

            List<Endereco> enderecos = escola.getEnderecos();
            enderecos.removeAll(obj.getEnderecos());
            for (Endereco e : enderecos) {
                enderecoService.delete(e.getId());
            }

            List<Telefone> telefones = escola.getTelefones();
            telefones.removeAll(obj.getTelefones());
            for (Telefone t : telefones) {
                telefoneService.delete(t.getId());
            }

            List<Email> emails = escola.getEmails();
            emails.removeAll(obj.getEmails());
            for (Email e : emails) {
                emailService.delete(e.getId());
            }
        }

        enderecoService.saveList(obj.getEnderecos());
        telefoneService.saveList(obj.getTelefones());
        emailService.saveList(obj.getEmails());
        super.save(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getCampoOrdenacao() {
        return "razaoSocial";
    }

    @Override
    public Page<Escola> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByRazaoSocialContainingIgnoreCase(filtro, pr);
    }

    public List<EscolaListarDTO> getEscolaByName(String nome) {
        List<Escola> lista;
        if (nome == null) {
            lista = getRepository().findAll();
        } else {
            lista = getRepository().findAllByRazaoSocialContainingIgnoreCase(nome);
        }

        List<EscolaListarDTO> ret = new ArrayList<>();
        for (Escola e : lista) {
            ret.add(new EscolaListarDTO(e));
        }
        return ret;
    }
}
