package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Nacionalidade;
import br.unicesumar.escoladeti.repository.NacionalidadeRepository;
import org.springframework.stereotype.Service;

@Service
public class NacionalidadeService extends BaseService<Nacionalidade, NacionalidadeRepository> {

}
