package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Disciplina;
import br.unicesumar.escoladeti.repository.DisciplinaRepository;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DisciplinaService extends BaseService<Disciplina, DisciplinaRepository> {

    public List<Disciplina> getByName(String nome) {
        return getRepository().findAllByNomeContainingOrderByNomeAsc(nome);
    }
    
    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }
    
    @Override
    public Page<Disciplina> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

}
