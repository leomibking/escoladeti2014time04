package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.TestDTO;
import br.unicesumar.escoladeti.dtos.TurmaAlunoDTO;
import br.unicesumar.escoladeti.dtos.TurmaAlunoShowDTO;
import br.unicesumar.escoladeti.dtos.TurmaAtividadeShowDTO;
import br.unicesumar.escoladeti.dtos.TurmaChamadaShowDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Atividade;
import br.unicesumar.escoladeti.entity.Disciplina;
import br.unicesumar.escoladeti.entity.Professor;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAluno;
import br.unicesumar.escoladeti.entity.TurmaAtividade;
import br.unicesumar.escoladeti.entity.TurmaAtividadeAluno;
import br.unicesumar.escoladeti.entity.TurmaChamada;
import br.unicesumar.escoladeti.entity.TurmaChamadaAluno;
import br.unicesumar.escoladeti.repository.TurmaRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class TurmaService extends BaseService<Turma, TurmaRepository> {

    @Autowired
    private TurmaChamadaService turmaChamadaService;

    @Autowired
    private TurmaAtividadeService turmaAtividadeService;

    @Autowired
    private TurmaAlunoService turmaAlunoService;

    @Autowired
    private ProfessorService professorService;

    @Autowired
    private DisciplinaService disciplinaService;

    @Autowired
    private AtividadeService atividadeService;

    @Autowired
    private TurmaAtividadeAlunoService turmaAtividadeAlunoService;

    @Autowired
    private TurmaChamadaAlunoSercice turmaChamadaAlunoService;

    @Autowired
    private AprendizService aprendizService;

    @Override
    public <S extends Turma> void save(S obj) {
        super.save(obj);
    }

    public TestDTO getTeste() {
        List<Object[]> resp = getRepository().teste();
        TestDTO t = new TestDTO();

        t.setNome((String) resp.get(0)[0]);
        t.setMedia((double) resp.get(0)[1]);
        t.setNometurma((String) resp.get(0)[2]);

        return t;
    }

    public TurmaChamada getChamadaById(Long idChamada) {
        return turmaChamadaService.getById(idChamada);
    }

    public TurmaAtividade getAtividadeById(Long idAtividade) {
        return turmaAtividadeService.getById(idAtividade);
    }

    public DataPage<TurmaAlunoShowDTO> getAlunosPaginados(Long idTurma, Integer pageIndex) {
        DataPage<TurmaAluno> dp = turmaAlunoService.getAlunosDaTurmaPaginado(this.getById(idTurma), pageIndex);
        List<TurmaAlunoShowDTO> lista = new ArrayList<>();
        for (TurmaAluno ta : dp.getItemList()) {
            lista.add(new TurmaAlunoShowDTO(ta));
        }
        return new DataPage<>(lista, dp.getCurrentPage(), dp.getTotalPage(), dp.getIndexList());
    }

    public DataPage<TurmaAtividadeShowDTO> getAtividadesPaginadas(Long idTurma, Integer pageIndex) {
        DataPage<TurmaAtividade> dp = turmaAtividadeService.getAtividadesDaTurmaPaginado(this.getById(idTurma), pageIndex);
        List<TurmaAtividadeShowDTO> lista = new ArrayList<>();
        for (TurmaAtividade ta : dp.getItemList()) {
            lista.add(new TurmaAtividadeShowDTO(ta));
        }
        return new DataPage<>(lista, dp.getCurrentPage(), dp.getTotalPage(), dp.getIndexList());
    }

    public DataPage<TurmaChamadaShowDTO> getChamadasPaginados(Long idTurma, Integer pageIndex) {
        DataPage<TurmaChamada> dp = turmaChamadaService.getChamadasDaTurmaPaginado(this.getById(idTurma), pageIndex);
        List<TurmaChamadaShowDTO> lista = new ArrayList<>();
        for (TurmaChamada ta : dp.getItemList()) {
            lista.add(new TurmaChamadaShowDTO(ta));
        }
        return new DataPage<>(lista, dp.getCurrentPage(), dp.getTotalPage(), dp.getIndexList());
    }

    public TurmaAluno getTurmaAlunoById(Long idAtividade) {
        return this.turmaAlunoService.getById(idAtividade);
    }

    public ServerResponse salvarAtividade(TurmaAtividade ta) {
        this.atividadeService.save(ta.getAtividade());
        this.disciplinaService.save(ta.getDisciplina());
        this.professorService.save(ta.getProfessor());
        this.save(ta.getTurma());

        for (TurmaAtividadeAluno taa : ta.getAlunos()) {
            this.turmaAtividadeAlunoService.save(taa);
        }

        turmaAtividadeService.save(ta);
        return new ServerResponse("Atividade salva com sucesso.", true);
    }

    public Professor getProfessorById(Long id) {
        return this.professorService.getById(id);
    }

    public Disciplina getDisciplinaById(Long id) {
        return this.disciplinaService.getById(id);
    }

    public Atividade getTipoAtividadeById(Long id) {
        return this.atividadeService.getById(id);
    }

    public ServerResponse salvarChamada(TurmaChamada tc) {
        this.disciplinaService.save(tc.getDisciplina());
        this.professorService.save(tc.getProfessor());
        this.save(tc.getTurma());

        for (TurmaChamadaAluno tca : tc.getAlunos()) {
            this.turmaChamadaAlunoService.save(tca);
        }

        turmaChamadaService.save(tc);
        return new ServerResponse("Chamada salva com sucesso.", true);
    }

    public ServerResponse atualizarNotasAtividade(TurmaAtividade tal) {
        for (TurmaAtividadeAluno ta : tal.getAlunos()) {
            this.turmaAtividadeAlunoService.save(ta);
        }
        return new ServerResponse("Dados atualizados com sucesso.", true);
    }

    public ServerResponse atualizarChamada(TurmaChamada tc) {
        for (TurmaChamadaAluno tca : tc.getAlunos()) {
            this.turmaChamadaAlunoService.save(tca);
        }
        return new ServerResponse("Dados atualizados com sucesso", true);
    }

    public ServerResponse removerChamada(TurmaChamada tc) {
        for (TurmaChamadaAluno ta : tc.getAlunos()) {
            turmaChamadaAlunoService.delete(ta.getId());
        }
        turmaChamadaService.delete(tc.getId());
        return new ServerResponse("Chamada removida com sucesso", true);
    }

    public ServerResponse removerAtividade(TurmaAtividade ta) {
        for (TurmaAtividadeAluno taa : ta.getAlunos()) {
            turmaAtividadeAlunoService.delete(taa.getId());
        }
        turmaAtividadeService.delete(ta.getId());
        return new ServerResponse("Atividade removida com sucesso", true);
    }

    public Aprendiz getAlunoById(Long id) {
        return this.aprendizService.getById(id);
    }

    public ServerResponse salvaNovoAluno(TurmaAluno ta) {
        this.save(ta.getTurma());
        this.turmaAlunoService.save(ta);
        return new ServerResponse("Aluno cadastrado com sucesso.", true);
    }

    public List<TurmaAlunoDTO> getAlunosPorNome(String nome) {
        List<TurmaAlunoDTO> resp = new ArrayList<>();
        for (Aprendiz a : this.aprendizService.getByName(nome)) {
            resp.add(new TurmaAlunoDTO(a));
        }
        return resp;
    }

    public List<Turma> getTurmasSemProcessoSeletivo() {
        return getRepository().findAll();
    }

    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }

    @Override
    public Page<Turma> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

    public ServerResponse removerAluno(Long id, Long idAluno) {
        try {
            turmaAlunoService.delete(idAluno);
            return new ServerResponse("Aluno removido com sucesso.");
        } catch (Exception e) {
            return new ServerResponse("Não foi possivel remover o aluno da Turma.");
        }
    }

}
