package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Curso;
import br.unicesumar.escoladeti.repository.CursoRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CursoService extends BaseService<Curso, CursoRepository>{
    
}
