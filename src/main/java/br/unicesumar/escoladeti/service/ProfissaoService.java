package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Profissao;
import br.unicesumar.escoladeti.repository.ProfissaoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProfissaoService extends BaseService<Profissao, ProfissaoRepository> {

    @Override
    public Page<Profissao> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }
}
