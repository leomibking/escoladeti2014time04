package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.dtos.AprendizLogDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Log;
import br.unicesumar.escoladeti.repository.LogRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class LogService extends BaseService<Log, LogRepository> {

    @Autowired
    private AprendizService aprendizService;

    public void gerarLog(Long idAprendiz, String msg) {
        //Usuario user = userService
        Log newLog = new Log();
        Aprendiz a = aprendizService.getById(idAprendiz);
        Date data = new Date();

        newLog.setAprendiz(a);
        newLog.setObservacao(msg);
        newLog.setData(data);

        this.save(newLog);
    }

    public List<AprendizLogDTO> getLogByAprendizId(Long idAprendiz) {
        List<AprendizLogDTO> aprendizLogs = new ArrayList<>();
        List<Log> logs = new ArrayList<>();
        logs = this.repository.findAllByAprendizIdOrderByIdAsc(idAprendiz);
        for (Log l : logs) {
            AprendizLogDTO dto = new AprendizLogDTO();
            dto.setNome(l.getAprendiz().getNome());
            dto.setData(l.getData());
            dto.setObservacao(l.getObservacao());
            aprendizLogs.add(dto);
        }

        return aprendizLogs;
    }
    
    @Override
    public Page<Log> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByAprendizNomeContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "AprendizNome";
    }
}
