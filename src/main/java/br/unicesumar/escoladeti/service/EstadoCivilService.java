package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.EstadoCivil;
import br.unicesumar.escoladeti.repository.EstadoCivilRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class EstadoCivilService extends BaseService<EstadoCivil, EstadoCivilRepository>{
    
}
