package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.TurmaAtividadeAluno;
import br.unicesumar.escoladeti.repository.TurmaAtividadeAlunoRepository;
import org.springframework.stereotype.Service;

@Service
public class TurmaAtividadeAlunoService extends BaseService<TurmaAtividadeAluno, TurmaAtividadeAlunoRepository>{

}