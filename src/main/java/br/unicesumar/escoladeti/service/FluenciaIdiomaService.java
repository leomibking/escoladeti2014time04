package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.FluenciaIdioma;
import br.unicesumar.escoladeti.repository.FluenciaIdiomaRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class FluenciaIdiomaService extends BaseService<FluenciaIdioma, FluenciaIdiomaRepository>{
    
}
