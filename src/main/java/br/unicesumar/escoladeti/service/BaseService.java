package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.Parser;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.entity.BaseEntity;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import static org.springframework.data.domain.Sort.Direction.ASC;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public abstract class BaseService<TEntity extends BaseEntity, TRepository extends JpaRepository> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Autowired
    protected TRepository repository;

    protected TRepository getRepository() {
        return this.repository;
    }

    // paginação
    public <T extends BaseDTO> DataPage<T> getPageList(Integer pageIndex, Parser parser) {
        DataPage<TEntity> data = new DataPage<>(getRepository().findAll(DataPage.pageRequestForAsc(pageIndex, this.getCampoOrdenacao())));
        return parser.parse(data);
    }
    
    public <T extends BaseDTO> DataPage<T> getPageList(Integer pageIndex, Parser parser, String filtro) {
        DataPage<TEntity> data = new DataPage<>(this.getPaginaFiltro(DataPage.pageRequestForAsc(pageIndex, this.getCampoOrdenacao()),filtro));
        return parser.parse(data);
    }
    
    public String getCampoOrdenacao(){
        return "id";
    }
    
    public Page<TEntity> getPaginaFiltro(Pageable pr,String filtro){
        return getRepository().findAll(pr);
    }

    // deletar 1
    public void delete(Long id) {
        getRepository().delete(id);
    }

    // deletar varios, lista de IDs
    public void deleteList(Iterable<Long> idList) {
        getRepository().delete(getRepository().findAll(idList));
    }

    // salvar 1
    @Transactional
    public <S extends TEntity> void save(S obj) {
        getRepository().save(obj);
    }

    // salvar lista
    @Transactional
    public <S extends TEntity> void saveList(Iterable<S> objList) {
        getRepository().save(objList);
    }

    // recuperar 1
    public TEntity getById(Long id) {
        TEntity obj = (TEntity) getRepository().findOne(id);
        return obj;
    }

    // recuperar varios
    public List<TEntity> getList() {
        return getListWithFilter("id");
    }

    // recuperar varios
    public List<TEntity> getList(Iterable<Long> idList) {
        return getRepository().findAll(idList);
    }

    // recuperar varios com ordenacao
    public List<TEntity> getListWithFilter(String filter) {
        List<TEntity> list = getRepository().findAll(new Sort(new Sort.Order(ASC, filter)));
        return list;
    }
}
