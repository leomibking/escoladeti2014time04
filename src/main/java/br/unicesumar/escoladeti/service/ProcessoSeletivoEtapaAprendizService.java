package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;
import br.unicesumar.escoladeti.repository.ProcessoSeletivoEtapaAprendizRepository;
import org.springframework.stereotype.Service;

@Service
public class ProcessoSeletivoEtapaAprendizService extends BaseService<ProcessoSeletivoEtapaAprendiz, ProcessoSeletivoEtapaAprendizRepository>{

}