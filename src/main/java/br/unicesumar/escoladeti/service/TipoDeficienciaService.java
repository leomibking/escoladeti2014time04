package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.TipoDeficiencia;
import br.unicesumar.escoladeti.repository.TipoDeficienciaRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoDeficienciaService extends BaseService<TipoDeficiencia, TipoDeficienciaRepository> {

}
