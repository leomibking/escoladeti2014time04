package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaChamada;
import br.unicesumar.escoladeti.repository.TurmaChamadaRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TurmaChamadaService extends BaseService<TurmaChamada, TurmaChamadaRepository> {

    public DataPage<TurmaChamada> getChamadasDaTurmaPaginado(Turma t, Integer pageIndex) {
        Sort s = new Sort(Sort.Direction.DESC, "dataChamada");
        return new DataPage<>(getRepository().findAllByTurmaOrderByDataChamadaDesc(t,DataPage.pageRequestFor(pageIndex, s)));
    }

}
