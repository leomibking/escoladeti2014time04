package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.CNH;
import br.unicesumar.escoladeti.repository.CNHRepository;
import org.springframework.stereotype.Service;

@Service
public class CNHService extends BaseService<CNH, CNHRepository>{
    
}
