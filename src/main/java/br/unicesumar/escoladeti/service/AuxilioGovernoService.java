package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.AuxilioGoverno;
import br.unicesumar.escoladeti.repository.AuxilioGovernoRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class AuxilioGovernoService extends BaseService<AuxilioGoverno, AuxilioGovernoRepository> {
    
    @Override
    public Page<AuxilioGoverno> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByDescricaoContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "descricao";
    }

}
