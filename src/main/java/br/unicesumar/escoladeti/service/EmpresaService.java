package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.dtos.EmpresaVagaDTO;
import br.unicesumar.escoladeti.dtos.EmpresaVagaListDTO;
import br.unicesumar.escoladeti.entity.Empresa;
import br.unicesumar.escoladeti.entity.Vaga;
import br.unicesumar.escoladeti.repository.EmpresaRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EmpresaService extends BaseService<Empresa, EmpresaRepository> {

    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TelefoneService telefoneService;

    @Autowired
    private VagaService vagaService;

    @Override
    public <S extends Empresa> void save(S obj) {
        enderecoService.saveList(obj.getEnderecos());
        telefoneService.saveList(obj.getTelefones());
        emailService.saveList(obj.getEmails());
        //vagaService.saveList(obj.getVagas());
        super.save(obj); //To change body of generated methods, choose Tools | Templates.
    }

    public void salvarVaga(EmpresaVagaDTO vaga) {
        Vaga v;
        
        if(vaga.getIdVaga() == null){
            v = new Vaga();
        }else{
            v = this.vagaService.getById(vaga.getIdVaga());
        }
        v.setProfissao(vaga.getProfissao());
        v.setQuantidade(vaga.getQuantidade());
        v.setPeriodo(vaga.getPeriodo());
        v.setCargaHorarioMes(vaga.getCargaHorarioMes());
        v.setAtividade(vaga.getAtividade());
        
        Empresa e = this.getById(vaga.getIdEmpresa());
        e.addVaga(v);
        v.setEmpresa(e);
        this.vagaService.save(v);
        this.save(e);
    }

    public void removerVaga(Long idEmpresa, Long idVaga) {
        Vaga v = this.vagaService.getById(idVaga);
        Empresa e = this.getById(idEmpresa);
        e.getVagas().remove(v);
        this.vagaService.delete(idVaga);
        this.repository.save(e);
    }

    public List<EmpresaVagaListDTO> getVagasEmpresa(Empresa e) {
        List<EmpresaVagaListDTO> retorno = new ArrayList<>();
        List<Vaga> vagas = vagaService.getVagasByIdEmpresa(e);
        for (Vaga v : vagas){
            retorno.add(new EmpresaVagaListDTO(v));
        }
        return retorno;
    }
    
    @Override
    public String getCampoOrdenacao() {
        return "razaoSocial";
    }
    
    @Override
    public Page<Empresa> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByRazaoSocialContainingIgnoreCase(filtro, pr);
    }
}
