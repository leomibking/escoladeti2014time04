package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAtividade;
import br.unicesumar.escoladeti.repository.TurmaAtividadeRepository;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class TurmaAtividadeService extends BaseService<TurmaAtividade, TurmaAtividadeRepository> {

    public DataPage<TurmaAtividade> getAtividadesDaTurmaPaginado(Turma t, Integer pageIndex) {
        Sort s = new Sort(Sort.Direction.DESC, "dataAtividade");
        return new DataPage<>(getRepository().findAllByTurmaOrderByDataAtividadeDesc(t,DataPage.pageRequestFor(pageIndex, s)));
    }

}
