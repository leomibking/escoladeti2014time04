package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.ContratoGerarDTO;
import br.unicesumar.escoladeti.dtos.EtapaDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoAprendizDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoEtapaDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Empresa;
import br.unicesumar.escoladeti.entity.Etapa;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAluno;
import br.unicesumar.escoladeti.entity.Vaga;
import br.unicesumar.escoladeti.repository.ProcessoSeletivoRepository;
import br.unicesumar.escoladeti.types.StatusEtapaProcessoSeletivo;
import br.unicesumar.escoladeti.types.StatusProcessoSeletivo;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProcessoSeletivoService extends BaseService<ProcessoSeletivo, ProcessoSeletivoRepository> {

    @Autowired
    private ProcessoSeletivoAprendizService processoAprendizService;

    @Autowired
    private ProcessoSeletivoEtapaAprendizService etapaAprendizService;

    @Autowired
    private ProcessoSeletivoEtapaService etapaService;

    @Autowired
    private TurmaAlunoService turmaAlunoSvr;

    @Autowired
    private TurmaService turmaSvr;

    @Autowired
    private EtapaService etapaSvr;

    @Autowired
    private VagaService vagaSvr;

    @Autowired
    private EmpresaService empresaSvr;

    @Autowired
    private AprendizService aprendizService;

    @Autowired
    private DataSource ds;

    @Autowired
    private ContratoService contratoService;

    public String teste() throws SQLException {
        Connection con = ds.getConnection();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select nome, status from processoseletivo");

        String sl = "";
        while (rs.next()) {
            System.out.println("Campo: " + rs.getString("nome") + " - " + rs.getString("status"));
            sl = sl + "Campo: " + rs.getString("nome") + " - " + rs.getString("status") + "\n";
        }
        con.close();
        return sl;
    }

    public List<ProcessoSeletivoAprendizDTO> getSelecionados(Long id) {
        List<ProcessoSeletivoAprendizDTO> retorno = new ArrayList<>();
        ProcessoSeletivo ps = this.getById(id);
        for (ProcessoSeletivoAprendiz pa : ps.getCandidatos()) {
            retorno.add(new ProcessoSeletivoAprendizDTO(pa));
        }
        return retorno;
    }

    public List<ProcessoSeletivoEtapaDTO> getEtapas(Long id) {
        List<ProcessoSeletivoEtapa> lista = etapaService.getEtapas(this.getById(id));
        List<ProcessoSeletivoEtapaDTO> retorno = new ArrayList<>();

        for (ProcessoSeletivoEtapa ta : lista) {
            retorno.add(new ProcessoSeletivoEtapaDTO(ta));
        }
        return retorno;
    }

    public ProcessoSeletivoEtapa getEtapaById(Long etapa) {
        return etapaService.getById(etapa);
    }

    public Etapa getByIdEtapa(Long id) {
        return etapaSvr.getById(id);
    }

    public ProcessoSeletivoAprendiz getSelecionadoById(Long selecionado) {
        return processoAprendizService.getById(selecionado);
    }

    public ProcessoSeletivoAprendiz getAprovadoById(Long aprovado) {
        ProcessoSeletivoAprendiz pa = processoAprendizService.getById(aprovado);
        if (pa != null && pa.isAprovado()) {
            return pa;
        } else {
            return null;
        }
    }

    public List<ProcessoSeletivoAprendizDTO> getAprovados(Long id) {
        List<ProcessoSeletivoAprendiz> lista = processoAprendizService.getAprovados(this.getById(id));
        List<ProcessoSeletivoAprendizDTO> retorno = new ArrayList<>();

        for (ProcessoSeletivoAprendiz ta : lista) {
            retorno.add(new ProcessoSeletivoAprendizDTO(ta));
        }
        return retorno;
    }

    //pesquisa de etapa
    public List<EtapaDTO> getEtapaPorNome(String nome) {
        List<EtapaDTO> resp = new ArrayList<>();
        for (Etapa a : this.etapaSvr.getByName(nome)) {
            resp.add(new EtapaDTO(a));
        }
        return resp;
    }

    // Add nova Etapa
    public void salvarEtapaProcessoSeletivo(ProcessoSeletivoEtapa pSeletivoEtapa) {
        this.etapaService.save(pSeletivoEtapa);
    }

    public List<TurmaAluno> getAlunosByTurma(Long idTurma, Long idProcesso) {
        return turmaAlunoSvr.getAlunosDoProcessoSeletivo(turmaSvr.getById(idTurma), this.getById(idProcesso));
    }

    public Vaga getVagaById(Long id) {
        return vagaSvr.getById(id);
    }

    public Empresa getEmpresaById(Long id) {
        return empresaSvr.getById(id);
    }

    @Override
    public <S extends ProcessoSeletivo> void save(S obj) {
        ProcessoSeletivo ps = (ProcessoSeletivo) obj;
        super.save(ps);

        for (ProcessoSeletivoEtapa e : ps.getEtapas()) {
            etapaService.save(e);
        }
    }

    public void removerEtapa(ProcessoSeletivoEtapa e) {
        for (ProcessoSeletivoEtapaAprendiz selecionado : e.getSelecionados()) {
            etapaAprendizService.delete(selecionado.getId());
        }
        etapaService.delete(e.getId());
    }

    public void salvarProcessoSeletivoAprendiz(ProcessoSeletivoAprendiz psa) {
        processoAprendizService.save(psa);
    }

    public Aprendiz getAprendiz(Long id) {
        return aprendizService.getById(id);
    }

    public void salvarAprendizEtapa(ProcessoSeletivoEtapaAprendiz psea) {
        etapaAprendizService.save(psea);
    }

    public ProcessoSeletivoEtapaAprendiz getAlunoDaEtapa(Long id) {
        return etapaAprendizService.getById(id);
    }

    public Turma getTurmaById(Long id) {
        return turmaSvr.getById(id);
    }

    public void salvarTurma(Turma t) {
        turmaSvr.save(t);
    }

    public void salvarNovoAlunoNaTurma(Aprendiz a, Turma t, ProcessoSeletivo p) {
        boolean achou = false;
        for (TurmaAluno al : t.getAlunos()) {
            if (al.getAluno().getId() == a.getId()) {
                achou = true;
                al.setProcessoSeletivo(p);
                turmaSvr.salvaNovoAluno(al);
                break;
            }
        }
        
        if (!achou) {
            TurmaAluno ta = new TurmaAluno(a);
            ta.setTurma(t);
            ta.setProcessoSeletivo(p);
            turmaSvr.salvaNovoAluno(ta);
        }
    }

    public void salvarAprendiz(Aprendiz a) {
        aprendizService.save(a);
    }

    public void salvarVaga(Vaga v) {
        vagaSvr.save(v);
    }

    public void setSelecionado(ProcessoSeletivoAprendizDTO psa, Long idProcesso) throws SQLException {
        Connection con = ds.getConnection();
        Statement st = con.createStatement();
        String sql = "select 1 from turmaaluno a where a.id_aluno = " + psa.getCandidato().getId() + " and a.id_processo_seletivo = " + idProcesso;

        ResultSet rs = st.executeQuery(sql);

        String sl = "";
        while (rs.next()) {
            psa.setSelecionado(true);
        }
        con.close();
    }

    public void gerarContrato(ContratoGerarDTO dto) {
        this.contratoService.gerarContrato(dto);
    }

    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }

    @Override
    public Page<ProcessoSeletivo> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

    public void removerCandidato(ProcessoSeletivoAprendiz candidato) {
        processoAprendizService.delete(candidato.getId());
    }

    public ServerResponse voltarStatus(Long id) {
        /*
         Status:
         PRESELECAO("Pré-Seleção"),
         SELECAO("Em Seleção"),
         POSSELECAO("Pós-Seleção"),
         ENCERRADO("Encerrado"),
         CANCELADO("Cancelado");
         - Pre seleção: nao faz nada
         */
        ProcessoSeletivo ps = this.getById(id);

        if (ps.getStatus() == StatusProcessoSeletivo.CANCELADO) {
            return new ServerResponse("Não é possivel voltar o Status de um Processo Cancelado.");
        } else if (ps.getStatus() == StatusProcessoSeletivo.ENCERRADO) {
            return new ServerResponse("Não é possivel voltar o Status de um Processo Encerrado.");
        } else if (ps.getStatus() == StatusProcessoSeletivo.POSSELECAO) {

            ps.setStatus(StatusProcessoSeletivo.SELECAO);
            this.save(ps);
            return new ServerResponse("Status do Processo retornado para Em Seleção.");

        } else if (ps.getStatus() == StatusProcessoSeletivo.SELECAO) {
            //remove os selecionados
            for (ProcessoSeletivoEtapa etapa : ps.getEtapas()) {
                etapa.setStatus(StatusEtapaProcessoSeletivo.ABERTO);
                etapaService.removerSelecionados(etapa);
            }
            //reprova todo mundo
            for (ProcessoSeletivoAprendiz aprendiz : ps.getCandidatos()) {
                aprendiz.setAprovado(false);
                this.salvarProcessoSeletivoAprendiz(aprendiz);
            }
            ps.setStatus(StatusProcessoSeletivo.PRESELECAO);
            this.save(ps);
            return new ServerResponse("Status do Processo retornado para Pré-Seleção.");
        } else {
            return new ServerResponse("Não é possivel voltar o Status de um Processo Cancelado.");
        }
    }

}
