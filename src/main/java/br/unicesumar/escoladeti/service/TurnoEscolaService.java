/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.TurnoEscola;
import br.unicesumar.escoladeti.repository.TurnoEscolaRepository;
import org.springframework.stereotype.Service;

@Service
public class TurnoEscolaService extends BaseService<TurnoEscola, TurnoEscolaRepository>{
    
}
