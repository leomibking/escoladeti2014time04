package br.unicesumar.escoladeti.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.unicesumar.escoladeti.entity.PessoaJuridica;
import br.unicesumar.escoladeti.repository.PessoaJuridicaRepository;

@Service
public class PessoaJuridicaService extends BaseService<PessoaJuridica, PessoaJuridicaRepository> {

    @Autowired
    private EnderecoService enderecoService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private TelefoneService telefoneService;

    @Autowired
    private VagaService vagaService;

    @Override
    public <S extends PessoaJuridica> void save(S obj) {
        PessoaJuridica pessoaJuridica = (PessoaJuridica) obj;
        enderecoService.saveList(pessoaJuridica.getEnderecos());

        telefoneService.saveList(pessoaJuridica.getTelefones());

        emailService.saveList(pessoaJuridica.getEmails());

//        vagaService.saveList(pessoaJuridica.getVagas());

        super.save(pessoaJuridica);
    }

    @Override
    public void delete(Long id) {
        enderecoService.delete(id);

        telefoneService.delete(id);

        emailService.delete(id);

        vagaService.delete(id);

        super.delete(id);
    }
}
