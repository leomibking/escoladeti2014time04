package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.TipoTelefone;
import br.unicesumar.escoladeti.repository.TipoTelefoneRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoTelefoneService extends BaseService<TipoTelefone, TipoTelefoneRepository> {

}
