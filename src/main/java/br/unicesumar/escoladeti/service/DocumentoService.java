package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Documento;
import br.unicesumar.escoladeti.repository.DocumentoRepository;
import org.springframework.stereotype.Service;

@Service
public class DocumentoService extends BaseService<Documento, DocumentoRepository> {
    
}
