package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Pais;
import br.unicesumar.escoladeti.entity.UF;
import br.unicesumar.escoladeti.repository.UFRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UFService extends BaseService<UF, UFRepository> {

    @Autowired
    private PaisService paisService;

    public List<UF> findAllByPaisId(Long paisId) {
        Pais pais = paisService.getById(paisId);
        return getRepository().findAllByPaisOrderByNomeAsc(pais);
    }
    
    @Override
    public Page<UF> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByNomeContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "nome";
    }

}
