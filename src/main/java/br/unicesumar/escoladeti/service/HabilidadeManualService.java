package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.HabilidadeManual;
import br.unicesumar.escoladeti.repository.HabilidadeManualRepository;
import org.springframework.stereotype.Service;

@Service
public class HabilidadeManualService extends BaseService<HabilidadeManual, HabilidadeManualRepository> {

}
