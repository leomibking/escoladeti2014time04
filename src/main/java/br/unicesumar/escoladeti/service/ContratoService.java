package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.dtos.ContratoGerarDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Contrato;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.Vaga;
import br.unicesumar.escoladeti.repository.ContratoRepository;
import br.unicesumar.escoladeti.types.StatusContrato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ContratoService extends BaseService<Contrato, ContratoRepository> {

    @Autowired
    private AprendizService aprendizService;
    @Autowired
    private TurmaService turmaService;
    @Autowired
    private VagaService vagaService;

    public void gerarContrato(ContratoGerarDTO dto) {
        Aprendiz a = aprendizService.getById(dto.getIdAprendiz());
        Turma t = turmaService.getById(dto.getIdTurma());
        Vaga v = vagaService.getById(dto.getIdVaga());

        Contrato con = new Contrato();
        con.setAprendiz(a);
        con.setTurma(t);
        con.setVaga(v);
        con.setDataInicio(dto.getDataInicio());
        con.setDataFim(dto.getDataFim());
        con.setStatus(StatusContrato.ABERTO);
        
        this.save(con);
    }
    
    @Override
    public Page<Contrato> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByAprendizNomeContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "AprendizNome";
    }

}
