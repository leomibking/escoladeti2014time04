package br.unicesumar.escoladeti.service;

import br.unicesumar.escoladeti.entity.Escolaridade;
import br.unicesumar.escoladeti.repository.EscolaridadeRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EscolaridadeService extends BaseService<Escolaridade, EscolaridadeRepository>{
    @Override
    public Page<Escolaridade> getPaginaFiltro(Pageable pr, String filtro) {
        return getRepository().findAllByEscolaridadeContainingIgnoreCase(filtro, pr);
    }

    @Override
    public String getCampoOrdenacao() {
        return "escolaridade";
    }
}
