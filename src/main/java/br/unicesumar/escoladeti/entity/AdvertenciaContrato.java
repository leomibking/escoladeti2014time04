package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class AdvertenciaContrato extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_contrato", referencedColumnName = "id")
    private Contrato contrato;

    @ManyToOne
    @JoinColumn(name = "id_tipoAdvertencia", referencedColumnName = "id")
    private TipoAdvertencia tipoAdvertencia;

    @Column(name = "descricao", nullable = false)
    private String descricao;

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public TipoAdvertencia getTipoAdvertencia() {
        return tipoAdvertencia;
    }

    public void setTipoAdvertencia(TipoAdvertencia tipoAdvertencia) {
        this.tipoAdvertencia = tipoAdvertencia;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
