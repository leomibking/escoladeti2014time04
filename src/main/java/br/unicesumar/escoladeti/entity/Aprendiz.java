package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.types.CNH;
import br.unicesumar.escoladeti.types.Curso;
import br.unicesumar.escoladeti.types.DescobertaPrograma;
import br.unicesumar.escoladeti.types.EscolaridadeResponsavel;
import br.unicesumar.escoladeti.types.FluenciaIdioma;
import br.unicesumar.escoladeti.types.HabilidadeManual;
import br.unicesumar.escoladeti.types.MotivoParticiparPrograma;
import br.unicesumar.escoladeti.types.Nacionalidade;
import br.unicesumar.escoladeti.types.TurnoEscola;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Aprendiz extends PessoaFisica {

    @JsonIgnore
    public Long getIdade() {
        if (this.getDataNascimento() != null) {
            long diffInMillies = new Date().getTime() - this.getDataNascimento().getTime();
            return new Long(TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS)/365);
        }else{
            return null;
        }
    }

    @Column
    private Boolean portadorDeficiencia;

    @Column(name = "tipoDeficiencia")
    private String tipoDeficiencia;

    @Column
    private Boolean dificuldadeLocomocao;

    @Column
    private Boolean medidaSocioEducativa;

    @Enumerated(EnumType.STRING)
    @Column(name = "habilidadeManual")
    private HabilidadeManual habilidadeManual;

    // ------ FILIACAO
    @Column
    private String nomePai;

    @Column(name = "rgPai")
    private String rgPai;

    @Column(name = "cpfPai")
    private String cpfPai;

    @ManyToOne
    @JoinColumn(name = "id_profissaopai", referencedColumnName = "id")
    private Profissao profissaoPai;

    @OneToMany
    @JoinColumn(name = "id_aprendiz", referencedColumnName = "id")
    private List<Documento> documentos;

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public Profissao getProfissaoPai() {
        return profissaoPai;
    }

    public void setProfissaoPai(Profissao profissaoPai) {
        this.profissaoPai = profissaoPai;
    }

    public Profissao getProfissaoMae() {
        return profissaoMae;
    }

    public void setProfissaoMae(Profissao profissaoMae) {
        this.profissaoMae = profissaoMae;
    }

    @Column
    private String nomeMae;

    @Column(name = "rgMae")
    private String rgMae;

    @Column(name = "cpfMae")
    private String cpfMae;

    @Column(name = "valeTransporte")
    private String valeTransporte;

    @ManyToOne
    @JoinColumn(name = "id_profissaomae", referencedColumnName = "id")
    private Profissao profissaoMae;

    @Column
    private String cartorio;

    @Column
    private String livro;

    @Column
    private String folha;

    @ManyToOne
    private Cidade cidadeNaturalidade;

    @Enumerated(EnumType.STRING)
    @Column(name = "nacionalidade")
    private Nacionalidade nacionalidade;

    public Boolean isConvocadoServicoMilitar() {
        return convocadoServicoMilitar;
    }

    public void setConvocadoServicoMilitar(Boolean convocadoServicoMilitar) {
        this.convocadoServicoMilitar = convocadoServicoMilitar;
    }

    @Column
    private String nre;

    @Column
    private Boolean tituloEleitor;

    @Column
    private String numeroEleitor;

    @Column
    private String zonaEleitor;

    @Column
    private String secaoEleitor;

    @Column
    private Boolean possuiCnh;

    @Column
    private String cnh;

    @Enumerated(EnumType.STRING)
    @Column(name = "tipoCnh")
    private CNH tipoCnh;

    @Column
    private Boolean alistamentoServicoMilitar;

    @Column
    private String numeroAlistamento;

    // ------ DADOS ESCOLARES
    @ManyToOne()
    private Escola escola;

    public Escola getEscola() {
        return escola;
    }

    public void setEscola(Escola escola) {
        this.escola = escola;
    }
    
    @Enumerated(EnumType.STRING)
    @Column(name = "curso")
    private Curso curso;

    @Column
    private String serie;

    @Column
    private Boolean basicoInformatica;

    @Column
    private Boolean interesseCursoInformatica;

    @Column
    private Boolean linguaEstrangeira;

    @Column
    private Boolean convocadoServicoMilitar;

    @Column
    private Boolean alistadoServicoMilitar;

    public Boolean isAlistadoServicoMilitar() {
        return alistadoServicoMilitar;
    }

    public void setAlistadoServicoMilitar(Boolean alistadoServicoMilitar) {
        this.alistadoServicoMilitar = alistadoServicoMilitar;
    }

    @Column
    private String linguasEstrangeiras1;

    @Column
    private String linguasEstrangeiras2;

    @Column
    private String linguasEstrangeiras3;

    @Enumerated(EnumType.STRING)
    @Column(name = "fluenciaIdioma1")
    private FluenciaIdioma fluenciaIdioma1;

    @Enumerated(EnumType.STRING)
    @Column(name = "fluenciaIdioma2")
    private FluenciaIdioma fluenciaIdioma2;

    @Enumerated(EnumType.STRING)
    @Column(name = "fluenciaIdioma3")
    private FluenciaIdioma fluenciaIdioma3;

    @Enumerated(EnumType.STRING)
    @Column(name = "turnoEscola")
    private TurnoEscola turnoEscola;

    @Column
    private String residentesEmpregados;

    @Column
    private String valorBeneficio;

    // ------ DADOS PROFISSIONAIS
    @Column
    private String pretensaoProfissional1;

    @Column
    private String pretensaoProfissional2;

    @Column
    private String pretensaoProfissional3;

    public Boolean isPortadorDeficiencia() {
        return portadorDeficiencia;
    }

    public void setPortadorDeficiencia(Boolean portadorDeficiencia) {
        this.portadorDeficiencia = portadorDeficiencia;
    }

    public String getTipoDeficiencia() {
        return tipoDeficiencia;
    }

    public void setTipoDeficiencia(String tipoDeficiencia) {
        this.tipoDeficiencia = tipoDeficiencia;
    }

    public Boolean isDificuldadeLocomocao() {
        return dificuldadeLocomocao;
    }

    public void setDificuldadeLocomocao(Boolean dificuldadeLocomocao) {
        this.dificuldadeLocomocao = dificuldadeLocomocao;
    }

    public Boolean isMedidaSocioEducativa() {
        return medidaSocioEducativa;
    }

    public void setMedidaSocioEducativa(Boolean medidaSocioEducativa) {
        this.medidaSocioEducativa = medidaSocioEducativa;
    }

    public HabilidadeManual getHabilidadeManual() {
        return habilidadeManual;
    }

    public void setHabilidadeManual(HabilidadeManual habilidadeManual) {
        this.habilidadeManual = habilidadeManual;
    }

    public String getNomePai() {
        return nomePai;
    }

    public void setNomePai(String nomePai) {
        this.nomePai = nomePai;
    }

    public String getRgPai() {
        return rgPai;
    }

    public void setRgPai(String rgPai) {
        this.rgPai = rgPai;
    }

    public String getCpfPai() {
        return cpfPai;
    }

    public void setCpfPai(String cpfPai) {
        this.cpfPai = cpfPai;
    }

    public String getNomeMae() {
        return nomeMae;
    }

    public void setNomeMae(String nomeMae) {
        this.nomeMae = nomeMae;
    }

    public String getRgMae() {
        return rgMae;
    }

    public void setRgMae(String rgMae) {
        this.rgMae = rgMae;
    }

    public String getCpfMae() {
        return cpfMae;
    }

    public void setCpfMae(String cpfMae) {
        this.cpfMae = cpfMae;
    }

    public String getValeTransporte() {
        return valeTransporte;
    }

    public void setValeTransporte(String valeTransporte) {
        this.valeTransporte = valeTransporte;
    }

    public String getCartorio() {
        return cartorio;
    }

    public void setCartorio(String cartorio) {
        this.cartorio = cartorio;
    }

    public String getLivro() {
        return livro;
    }

    public void setLivro(String livro) {
        this.livro = livro;
    }

    public String getFolha() {
        return folha;
    }

    public void setFolha(String folha) {
        this.folha = folha;
    }

    public Cidade getCidadeNaturalidade() {
        return cidadeNaturalidade;
    }

    public void setCidadeNaturalidade(Cidade cidadeNaturalidade) {
        this.cidadeNaturalidade = cidadeNaturalidade;
    }

    public Nacionalidade getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(Nacionalidade nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public String getNre() {
        return nre;
    }

    public void setNre(String nre) {
        this.nre = nre;
    }

    public Boolean isTituloEleitor() {
        return tituloEleitor;
    }

    public void setTituloEleitor(Boolean tituloEleitor) {
        this.tituloEleitor = tituloEleitor;
    }

    public String getNumeroEleitor() {
        return numeroEleitor;
    }

    public void setNumeroEleitor(String numeroEleitor) {
        this.numeroEleitor = numeroEleitor;
    }

    public String getZonaEleitor() {
        return zonaEleitor;
    }

    public void setZonaEleitor(String zonaEleitor) {
        this.zonaEleitor = zonaEleitor;
    }

    public String getSecaoEleitor() {
        return secaoEleitor;
    }

    public void setSecaoEleitor(String secaoEleitor) {
        this.secaoEleitor = secaoEleitor;
    }

    public Boolean isPossuiCnh() {
        return possuiCnh;
    }

    public void setPossuiCnh(Boolean possuiCnh) {
        this.possuiCnh = possuiCnh;
    }

    public String getCnh() {
        return cnh;
    }

    public void setCnh(String cnh) {
        this.cnh = cnh;
    }

    public CNH getTipoCnh() {
        return tipoCnh;
    }

    public void setTipoCnh(CNH tipoCnh) {
        this.tipoCnh = tipoCnh;
    }

    public Boolean isAlistamentoServicoMilitar() {
        return alistamentoServicoMilitar;
    }

    public void setAlistamentoServicoMilitar(Boolean alistamentoServicoMilitar) {
        this.alistamentoServicoMilitar = alistamentoServicoMilitar;
    }

    public String getNumeroAlistamento() {
        return numeroAlistamento;
    }

    public void setNumeroAlistamento(String numeroAlistamento) {
        this.numeroAlistamento = numeroAlistamento;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Boolean isBasicoInformatica() {
        return basicoInformatica;
    }

    public void setBasicoInformatica(Boolean basicoInformatica) {
        this.basicoInformatica = basicoInformatica;
    }

    public Boolean isInteresseCursoInformatica() {
        return interesseCursoInformatica;
    }

    public void setInteresseCursoInformatica(Boolean interesseCursoInformatica) {
        this.interesseCursoInformatica = interesseCursoInformatica;
    }

    public Boolean isLinguaEstrangeira() {
        return linguaEstrangeira;
    }

    public void setLinguaEstrangeira(Boolean linguaEstrangeira) {
        this.linguaEstrangeira = linguaEstrangeira;
    }

    public String getLinguasEstrangeiras1() {
        return linguasEstrangeiras1;
    }

    public void setLinguasEstrangeiras1(String linguasEstrangeiras1) {
        this.linguasEstrangeiras1 = linguasEstrangeiras1;
    }

    public String getLinguasEstrangeiras2() {
        return linguasEstrangeiras2;
    }

    public void setLinguasEstrangeiras2(String linguasEstrangeiras2) {
        this.linguasEstrangeiras2 = linguasEstrangeiras2;
    }

    public String getLinguasEstrangeiras3() {
        return linguasEstrangeiras3;
    }

    public void setLinguasEstrangeiras3(String linguasEstrangeiras3) {
        this.linguasEstrangeiras3 = linguasEstrangeiras3;
    }

    public FluenciaIdioma getFluenciaIdioma1() {
        return fluenciaIdioma1;
    }

    public void setFluenciaIdioma1(FluenciaIdioma fluenciaIdioma1) {
        this.fluenciaIdioma1 = fluenciaIdioma1;
    }

    public FluenciaIdioma getFluenciaIdioma2() {
        return fluenciaIdioma2;
    }

    public void setFluenciaIdioma2(FluenciaIdioma fluenciaIdioma2) {
        this.fluenciaIdioma2 = fluenciaIdioma2;
    }

    public FluenciaIdioma getFluenciaIdioma3() {
        return fluenciaIdioma3;
    }

    public void setFluenciaIdioma3(FluenciaIdioma fluenciaIdioma3) {
        this.fluenciaIdioma3 = fluenciaIdioma3;
    }

    public String getPretensaoProfissional1() {
        return pretensaoProfissional1;
    }

    public void setPretensaoProfissional1(String pretensaoProfissional1) {
        this.pretensaoProfissional1 = pretensaoProfissional1;
    }

    public String getPretensaoProfissional2() {
        return pretensaoProfissional2;
    }

    public void setPretensaoProfissional2(String pretensaoProfissional2) {
        this.pretensaoProfissional2 = pretensaoProfissional2;
    }

    public String getPretensaoProfissional3() {
        return pretensaoProfissional3;
    }

    public void setPretensaoProfissional3(String pretensaoProfissional3) {
        this.pretensaoProfissional3 = pretensaoProfissional3;
    }

    public Boolean isEmpregadoAnteriormente() {
        return empregadoAnteriormente;
    }

    public void setEmpregadoAnteriormente(Boolean empregadoAnteriormente) {
        this.empregadoAnteriormente = empregadoAnteriormente;
    }

    public Boolean isRemuneradoAnteriormente() {
        return remuneradoAnteriormente;
    }

    public void setRemuneradoAnteriormente(Boolean remuneradoAnteriormente) {
        this.remuneradoAnteriormente = remuneradoAnteriormente;
    }

    public Boolean isRegistradoAnteriormente() {
        return registradoAnteriormente;
    }

    public void setRegistradoAnteriormente(Boolean registradoAnteriormente) {
        this.registradoAnteriormente = registradoAnteriormente;
    }

    public Boolean isVoluntariadoAnteriormente() {
        return voluntariadoAnteriormente;
    }

    public void setVoluntariadoAnteriormente(Boolean voluntariadoAnteriormente) {
        this.voluntariadoAnteriormente = voluntariadoAnteriormente;
    }

    public Boolean isMoraComResponsavel() {
        return moraComResponsavel;
    }

    public void setMoraComResponsavel(Boolean moraComResponsavel) {
        this.moraComResponsavel = moraComResponsavel;
    }

    public EscolaridadeResponsavel getEscolaridadeResponsavel() {
        return escolaridadeResponsavel;
    }

    public void setEscolaridadeResponsavel(EscolaridadeResponsavel escolaridadeResponsavel) {
        this.escolaridadeResponsavel = escolaridadeResponsavel;
    }

    public String getSerieResponsavel() {
        return serieResponsavel;
    }

    public void setSerieResponsavel(String serieResponsavel) {
        this.serieResponsavel = serieResponsavel;
    }

    public String getResidentesCasa() {
        return residentesCasa;
    }

    public void setResidentesCasa(String residentesCasa) {
        this.residentesCasa = residentesCasa;
    }

    public Boolean isFamiliarAuxilioGoverno() {
        return familiarAuxilioGoverno;
    }

    public void setFamiliarAuxilioGoverno(Boolean familiarAuxilioGoverno) {
        this.familiarAuxilioGoverno = familiarAuxilioGoverno;
    }

    public Boolean isResponsavelPeloCartaobeneficio() {
        return responsavelPeloCartaobeneficio;
    }

    public void setResponsavelPeloCartaobeneficio(Boolean responsavelPeloCartaobeneficio) {
        this.responsavelPeloCartaobeneficio = responsavelPeloCartaobeneficio;
    }

    public String getRendaPropria() {
        return rendaPropria;
    }

    public void setRendaPropria(String rendaPropria) {
        this.rendaPropria = rendaPropria;
    }

    public String getRendaTotal() {
        return rendaTotal;
    }

    public void setRendaTotal(String rendaTotal) {
        this.rendaTotal = rendaTotal;
    }

    public Boolean isPossuiFilhos() {
        return possuiFilhos;
    }

    public void setPossuiFilhos(Boolean possuiFilhos) {
        this.possuiFilhos = possuiFilhos;
    }

    public Boolean isFilhoMesmoDomicilio() {
        return filhoMesmoDomicilio;
    }

    public void setFilhoMesmoDomicilio(Boolean filhoMesmoDomicilio) {
        this.filhoMesmoDomicilio = filhoMesmoDomicilio;
    }

    public String getNomeFilho() {
        return nomeFilho;
    }

    public void setNomeFilho(String nomeFilho) {
        this.nomeFilho = nomeFilho;
    }

    public Date getDataNascimentoFilho() {
        return dataNascimentoFilho;
    }

    public void setDataNascimentoFilho(Date dataNascimentoFilho) {
        this.dataNascimentoFilho = dataNascimentoFilho;
    }

    public Boolean isPrincipalResponsavelSustento() {
        return principalResponsavelSustento;
    }

    public void setPrincipalResponsavelSustento(Boolean principalResponsavelSustento) {
        this.principalResponsavelSustento = principalResponsavelSustento;
    }

    public DescobertaPrograma getDescobertaPrograma() {
        return descobertaPrograma;
    }

    public void setDescobertaPrograma(DescobertaPrograma descobertaPrograma) {
        this.descobertaPrograma = descobertaPrograma;
    }

    public String getOutraDescobertaPrograma() {
        return outraDescobertaPrograma;
    }

    public void setOutraDescobertaPrograma(String outraDescobertaPrograma) {
        this.outraDescobertaPrograma = outraDescobertaPrograma;
    }

    public MotivoParticiparPrograma getMotivoParticiparPrograma() {
        return motivoParticiparPrograma;
    }

    public void setMotivoParticiparPrograma(MotivoParticiparPrograma motivoParticiparPrograma) {
        this.motivoParticiparPrograma = motivoParticiparPrograma;
    }

    public Boolean isParticipaDigitandoFuturo() {
        return participaDigitandoFuturo;
    }

    public void setParticipaDigitandoFuturo(Boolean participaDigitandoFuturo) {
        this.participaDigitandoFuturo = participaDigitandoFuturo;
    }

    @Column
    private Boolean empregadoAnteriormente;

    @Column
    private Boolean remuneradoAnteriormente;

    @Column
    private Boolean registradoAnteriormente;

    @Column
    private Boolean voluntariadoAnteriormente;

    // ------ DADOS SOCIO-ECONOMICOS
    @Column
    private Boolean moraComResponsavel;

    @Enumerated(EnumType.STRING)
    @Column(name = "escolaridadeResponsavel")
    private EscolaridadeResponsavel escolaridadeResponsavel;

    @Column
    private String serieResponsavel;

    @Column
    private Boolean situacaoEscolaridadeResponsavel;

    public Boolean isSituacaoEscolaridadeResponsavel() {
        return situacaoEscolaridadeResponsavel;
    }

    public void setSituacaoEscolaridadeResponsavel(Boolean situacaoEscolaridadeResponsavel) {
        this.situacaoEscolaridadeResponsavel = situacaoEscolaridadeResponsavel;
    }

    @Column
    private String residentesCasa;

    @Column
    private Boolean familiarAuxilioGoverno;

    @Column
    private Boolean responsavelPeloCartaobeneficio;

    @Column
    private String rendaPropria;

    @Column
    private String rendaTotal;

    @Column
    private Boolean possuiFilhos;

    @Column
    private Boolean filhoMesmoDomicilio;

    @Column
    private String nomeFilho;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataNascimentoFilho;

    @Column
    private Boolean principalResponsavelSustento;

    @Enumerated(EnumType.STRING)
    @Column(name = "descobertaPrograma")
    private DescobertaPrograma descobertaPrograma;

    @Column
    private String outraDescobertaPrograma;

    @Enumerated(EnumType.STRING)
    @Column(name = "motivoParticiparPrograma")
    private MotivoParticiparPrograma motivoParticiparPrograma;

    @Column
    private String outroMotivoParticiparPrograma;

    public TurnoEscola getTurnoEscola() {
        return turnoEscola;
    }

    public void setTurnoEscola(TurnoEscola turnoEscola) {
        this.turnoEscola = turnoEscola;
    }

    public String getResidentesEmpregados() {
        return residentesEmpregados;
    }

    public void setResidentesEmpregados(String residentesEmpregados) {
        this.residentesEmpregados = residentesEmpregados;
    }

    public String getValorBeneficio() {
        return valorBeneficio;
    }

    public void setValorBeneficio(String valorBeneficio) {
        this.valorBeneficio = valorBeneficio;
    }

    public String getOutroMotivoParticiparPrograma() {
        return outroMotivoParticiparPrograma;
    }

    public void setOutroMotivoParticiparPrograma(String outroMotivoParticiparPrograma) {
        this.outroMotivoParticiparPrograma = outroMotivoParticiparPrograma;
    }

    @Column
    private Boolean participaDigitandoFuturo;

    public void addDocumento(Documento d) {
        if (this.documentos == null) {
            this.documentos = new ArrayList<>();
        }
        this.documentos.add(d);
    }

    @ManyToOne
    @JoinColumn(name = "id_processo", referencedColumnName = "id")
    @JsonIgnore
    private ProcessoSeletivo processo;

    public ProcessoSeletivo getProcesso() {
        return processo;
    }

    public void setProcesso(ProcessoSeletivo processo) {
        this.processo = processo;
    }
    
    @Column
    private Double rendaPerCapita;

    public Double getRendaPerCapita() {
        return rendaPerCapita;
    }

    public void setRendaPerCapita(Double rendaPerCapita) {
        this.rendaPerCapita = rendaPerCapita;
    }

    public Aprendiz() {
        this.rendaPerCapita = Double.valueOf(0);
    }

}
