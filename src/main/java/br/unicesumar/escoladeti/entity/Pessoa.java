package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa extends BaseEntity {

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataCadastro;

    @Column(nullable = false, length = 80)
    private String nome;

    @OneToMany
    @JoinColumn(name = "id_pessoa", referencedColumnName = "id")
    private List<Endereco> enderecos;

    @OneToMany
    @JoinColumn(name = "id_pessoa", referencedColumnName = "id")
    private List<Email> emails;

    @OneToMany
    @JoinColumn(name = "id_pessoa", referencedColumnName = "id")
    private List<Telefone> telefones;

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Endereco> getEnderecos() {
        return enderecos;
    }
    
    public Endereco getEndereco(int Index){
        if (this.enderecos.size() > 0){
            return this.enderecos.get(Index);    
        }else{
            return null;
        }
    }

    public void setEnderecos(List<Endereco> enderecos) {
        this.enderecos = enderecos;
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }

    public List<Telefone> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
        this.telefones = telefones;
    }
}
