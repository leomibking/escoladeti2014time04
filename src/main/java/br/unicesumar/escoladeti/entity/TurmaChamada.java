package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class TurmaChamada extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_turma")
    private Turma turma;

    @OneToMany
    @JoinColumn(name = "id_lista_chamada", referencedColumnName = "id")
    private List<TurmaChamadaAluno> alunos;

    @ManyToOne
    @JoinColumn(name = "id_disciplina", referencedColumnName = "id")
    private Disciplina disciplina;

    @ManyToOne
    @JoinColumn(name = "id_professor", referencedColumnName = "id")
    private Professor professor;

    @Column(name = "dt_chamada")
    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataChamada;

    @Column(name = "ds_chamada")
    private String descricao;
    
    @Column(name = "qt_aulas")
    private int qtAulas;

    public TurmaChamada() {
        this.alunos = new ArrayList<>();
    }

    public List<TurmaChamadaAluno> getAlunos() {
        return alunos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setAlunos(List<TurmaChamadaAluno> registros) {
        this.alunos = registros;
    }

    public Date getDataChamada() {
        return dataChamada;
    }

    public void setDataChamada(Date dataChamada) {
        this.dataChamada = dataChamada;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public int getQtAulas() {
        return qtAulas;
    }

    public void setQtAulas(int qtAulas) {
        this.qtAulas = qtAulas;
    }
    
}
