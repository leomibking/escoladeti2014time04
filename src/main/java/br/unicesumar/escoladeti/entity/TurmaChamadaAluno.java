package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TurmaChamadaAluno extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_aluno", referencedColumnName = "id")
    private Aprendiz aluno;
    
    @Column(name = "bo_presente")
    private boolean presente;
    
    @Column(name = "observacao_aluno")
    private String observacao;

    public Aprendiz getAluno() {
        return aluno;
    }

    public void setAluno(Aprendiz aluno) {
        this.aluno = aluno;
    }

    public boolean isPresente() {
        return presente;
    }

    public void setPresente(boolean presente) {
        this.presente = presente;
    }  

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
}
