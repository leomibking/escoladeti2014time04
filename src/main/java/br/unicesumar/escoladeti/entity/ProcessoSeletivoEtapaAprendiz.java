package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ProcessoSeletivoEtapaAprendiz extends BaseEntity {
    
    @ManyToOne
    @JoinColumn(name = "id_etapa", referencedColumnName = "id")
    private ProcessoSeletivoEtapa etapa;
    
    @ManyToOne
    @JoinColumn(name = "id_candidato", referencedColumnName = "id")
    private Aprendiz candidato;
    
    @Column
    private String resultadoAberto;
    
    @Column
    private double resultadoValor;

    public ProcessoSeletivoEtapa getEtapa() {
        return etapa;
    }

    public void setEtapa(ProcessoSeletivoEtapa etapa) {
        this.etapa = etapa;
    }

    public String getResultadoAberto() {
        return resultadoAberto;
    }

    public void setResultadoAberto(String resultadoAberto) {
        this.resultadoAberto = resultadoAberto;
    }

    public double getResultadoValor() {
        return resultadoValor;
    }

    public void setResultadoValor(double resultadoValor) {
        this.resultadoValor = resultadoValor;
    }

    public Aprendiz getCandidato() {
        return candidato;
    }

    public void setCandidato(Aprendiz candidato) {
        this.candidato = candidato;
    }
    
}