package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Escola extends PessoaJuridica {
    @Column
    private String responsavel;

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
    
}
