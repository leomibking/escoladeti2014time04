package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.types.TipoEtapa;
import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Etapa extends BaseEntity {
    
    @Column
    private String descricao;
    
    @Column
    private String observacao;
    
    @Column
    private TipoEtapa tipoEtapa;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public TipoEtapa getTipoEtapa() {
        return tipoEtapa;
    }

    public void setTipoEtapa(TipoEtapa tipoEtapa) {
        this.tipoEtapa = tipoEtapa;
    }
    
}
