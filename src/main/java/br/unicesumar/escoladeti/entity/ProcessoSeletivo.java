package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.types.StatusProcessoSeletivo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

@Entity
public class ProcessoSeletivo extends BaseEntity {

    @Column(name = "nome")
    private String nome;
    
    @ManyToOne
    @JoinColumn(name = "id_empresa", referencedColumnName = "id")
    private Empresa empresa;
    
    @OneToOne
    @JoinColumn(name = "id_vaga", referencedColumnName = "id")
    private Vaga vaga;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataInicio;
    
    @Column
    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataFim;
    
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private StatusProcessoSeletivo status;
    
    @OneToMany(mappedBy = "processo")
    private List<ProcessoSeletivoEtapa> etapas;
    
    @OneToMany(mappedBy = "processo")
    private List<ProcessoSeletivoAprendiz> candidatos;
    
    @ManyToMany(mappedBy = "processos")
    private List<Turma> turmas;

    public List<Turma> getTurmas() {
        return turmas;
    }

    public void setTurmas(List<Turma> turmas) {
        this.turmas = turmas;
    }
    
    public StatusProcessoSeletivo getStatus() {
        return status;
    }

    public void setStatus(StatusProcessoSeletivo status) {
        this.status = status;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public List<ProcessoSeletivoEtapa> getEtapas() {
        return etapas;
    }

    public void setEtapas(List<ProcessoSeletivoEtapa> etapas) {
        this.etapas = etapas;
    }

    public List<ProcessoSeletivoAprendiz> getCandidatos() {
        return candidatos;
    }

    public void setCandidatos(List<ProcessoSeletivoAprendiz> candidatos) {
        this.candidatos = candidatos;
    }

    public ProcessoSeletivo() {
        this.etapas = new ArrayList<>();
        this.candidatos = new ArrayList<>();
        this.status = StatusProcessoSeletivo.PRESELECAO;
    }

    public Vaga getVaga() {
        return vaga;
    }

    public void setVaga(Vaga vaga) {
        this.vaga = vaga;
    }

    public boolean existeAprendiz(Aprendiz a) {
        for (ProcessoSeletivoAprendiz pa : this.getCandidatos()){
            if (pa.getCandidato().getId() == a.getId()){
                return true;
            }
        }
        return false;
    }
    
    public int getQtAprovados(){
        int qt = 0;
        for (ProcessoSeletivoAprendiz pa : this.getCandidatos()){
            if (pa.isAprovado()){
                qt += 1;
            }
        }
        return qt;
    }

    public void gerarNomeEtapa() {
        this.nome = this.empresa.getNome()+" #"+this.getId();
    }

}
