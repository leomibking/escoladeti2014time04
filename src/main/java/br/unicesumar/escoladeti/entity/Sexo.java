package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Sexo extends BaseEntity {

    @Column
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
