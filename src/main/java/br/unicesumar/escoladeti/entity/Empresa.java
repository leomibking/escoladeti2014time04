package br.unicesumar.escoladeti.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

@Entity
public class Empresa extends PessoaJuridica {

    @OneToMany(mappedBy = "empresa")
    private List<Vaga> vagas;
    
    @Column
    private String responsavel;

    public Empresa() {
        this.vagas =  new ArrayList<>();
    }
    
    public List<Vaga> getVagas() {
        return vagas;
    }

    public void setVagas(List<Vaga> vagas) {
        this.vagas = vagas;
    }

    public void addVaga(Vaga v) {
        this.vagas.add(v);
    }

    public String getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }
    
}
