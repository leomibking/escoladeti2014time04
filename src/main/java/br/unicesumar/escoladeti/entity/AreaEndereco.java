package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class AreaEndereco extends BaseEntity{
    
    @Column(name = "areaEndereco")
    private String areaEndereco;

    public String getAreaEndereco() {
        return areaEndereco;
    }

    public void setAreaEndereco(String areaEndereco) {
        this.areaEndereco = areaEndereco;
    }
}
