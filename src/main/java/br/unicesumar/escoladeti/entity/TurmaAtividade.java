package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class TurmaAtividade extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_turma")
    private Turma turma;
    
    @OneToMany
    @JoinColumn(name = "id_lista_atividade", referencedColumnName = "id")
    private List<TurmaAtividadeAluno> alunos;
    
    @ManyToOne
    @JoinColumn(name = "id_atividade", referencedColumnName = "id")
    private Atividade atividade;
    
    @ManyToOne
    @JoinColumn(name = "id_disciplina", referencedColumnName = "id")
    private Disciplina disciplina;
    
    @ManyToOne
    @JoinColumn(name = "id_professor", referencedColumnName = "id")
    private Professor professor;
    
    @Column(name = "dt_atividade")
    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)    
    private Date dataAtividade;
    
    @Column(name = "ds_atividade")
    private String descricaoAtividade;
    
    @Column(name = "vl_atividade")
    private Double valor;
    
    public TurmaAtividade() {
        this.alunos = new ArrayList<>();
    }

    public String getDescricaoAtividade() {
        return descricaoAtividade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
    
    public void setDescricaoAtividade(String descricaoAtividade) {
        this.descricaoAtividade = descricaoAtividade;
    }
    
    public List<TurmaAtividadeAluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<TurmaAtividadeAluno> alunos) {
        this.alunos = alunos;
    }

    public Atividade getAtividade() {
        return atividade;
    }

    public void setAtividade(Atividade atividade) {
        this.atividade = atividade;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }

    public Date getDataAtividade() {
        return dataAtividade;
    }

    public void setDataAtividade(Date dataAtividade) {
        this.dataAtividade = dataAtividade;
    }

    public Professor getProfessor() {
        return professor;
    }

    public void setProfessor(Professor professor) {
        this.professor = professor;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }
    
    
    
}