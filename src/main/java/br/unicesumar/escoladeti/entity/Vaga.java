package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.types.Periodo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Vaga extends BaseEntity {

    @OneToOne
    private Profissao profissao;
    
    @ManyToOne
    @JoinColumn(name = "id_empresa", referencedColumnName = "id")
    @JsonIgnore
    private Empresa empresa;
    
    @Column(nullable = false)
    private int quantidade;

    @Enumerated(EnumType.STRING)
    @Column(name = "periodo")
    private Periodo periodo;
    
    @Column(name = "carga_horario_mes", nullable = false)
    private int cargaHorarioMes;
    
    @Column()
    private String atividade;
    
    @Column()
    private String status;

    
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public Periodo getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Periodo periodo) {
        this.periodo = periodo;
    }

    public int getCargaHorarioMes() {
        return cargaHorarioMes;
    }

    public void setCargaHorarioMes(int cargaHorarioMes) {
        this.cargaHorarioMes = cargaHorarioMes;
    }

    public String getAtividade() {
        return atividade;
    }

    public void setAtividade(String atividade) {
        this.atividade = atividade;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void atualizarQuantidadeDeVagas(int qtAprovados) {
        this.quantidade = this.quantidade - qtAprovados;
    }
}
