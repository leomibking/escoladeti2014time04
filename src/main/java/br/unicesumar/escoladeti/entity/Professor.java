package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Professor extends PessoaFisica {
    
    @Column(name = "atributo")
    private String atributoProfessor;

    public String getAtributoProfessor() {
        return atributoProfessor;
    }

    public void setAtributoProfessor(String atributoProfessor) {
        this.atributoProfessor = atributoProfessor;
    }
    
    

}
