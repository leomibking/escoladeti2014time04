package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TurmaAtividadeAluno extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_aluno", referencedColumnName = "id")
    private Aprendiz aluno;
    
    @Column(name = "nota_atividade")
    private double nota;
    
    @Column(name = "observacao_aluno")
    private String observacao;

    public TurmaAtividadeAluno() {
    }

    public Aprendiz getAluno() {
        return aluno;
    }

    public void setAluno(Aprendiz aluno) {
        this.aluno = aluno;
    }

    public double getNota() {
        return nota;
    }

    public void setNota(double nota) {
        this.nota = nota;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
    
}
