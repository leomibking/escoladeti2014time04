package br.unicesumar.escoladeti.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

@Entity
public class Turma extends BaseEntity {

    @Column(name = "nm_turma")
    private String nome;

    //anotação do Leo :) - leitura do mapeamento
    //Muitos alunos estão em uma turma <-
    //Uma turma tem muitos aluno ->
    @OneToMany(mappedBy = "turma")
    private List<TurmaAluno> alunos;

    @OneToMany(mappedBy = "turma")
    private List<TurmaAtividade> atividades;

    @OneToMany(mappedBy = "turma")
    private List<TurmaChamada> listasDeChamada;
    
    @ManyToMany
    //@JoinColumn(name = "id_processo", referencedColumnName = "id")
    private List<ProcessoSeletivo> processos;

    public Turma() {
        this.alunos = new ArrayList<>();
        this.atividades = new ArrayList<>();
        this.listasDeChamada = new ArrayList<>();
        this.processos = new ArrayList();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<TurmaAluno> getAlunos() {
        return alunos;
    }

    public void setAlunos(List<TurmaAluno> alunos) {
        this.alunos = alunos;
    }

    public List<TurmaAtividade> getAtividades() {
        return atividades;
    }

    public void setAtividades(List<TurmaAtividade> atividades) {
        this.atividades = atividades;
    }

    public List<TurmaChamada> getListasDeChamada() {
        return listasDeChamada;
    }

    public void setListasDeChamada(List<TurmaChamada> listasDeChamada) {
        this.listasDeChamada = listasDeChamada;
    }

    public List<ProcessoSeletivo> getProcesso() {
        return processos;
    }

    public void setProcesso(List<ProcessoSeletivo> processos) {
        this.processos = processos;
    }
    
    
}
