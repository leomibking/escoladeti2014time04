package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ProcessoSeletivoAprendiz extends BaseEntity{
    
    @ManyToOne
    @JoinColumn(name = "id_aluno", referencedColumnName = "id")
    private Aprendiz candidato;
    
    @ManyToOne
    @JoinColumn(name = "id_processo", referencedColumnName = "id")
    private ProcessoSeletivo processo;
    
    @Column(nullable = false)
    private boolean aprovado = false;

    public boolean isAprovado() {
        return aprovado;
    }

    public void setAprovado(boolean aprovado) {
        this.aprovado = aprovado;
    }    
    
    public Aprendiz getCandidato() {
        return candidato;
    }

    public void setCandidato(Aprendiz candidato) {
        this.candidato = candidato;
    }

    public ProcessoSeletivo getProcesso() {
        return processo;
    }

    public void setProcesso(ProcessoSeletivo processo) {
        this.processo = processo;
    }
    
}
