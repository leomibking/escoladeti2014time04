package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.types.TipoTelefone;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Telefone extends BaseEntity {

    @Column(nullable = false, length = 3)
    private String ddd;

    @Column(nullable = false, length = 15)
    private String numero;

    @Enumerated(EnumType.STRING)
    private TipoTelefone tipo;

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        this.ddd = ddd;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public TipoTelefone getTipo() {
        return tipo;
    }

    public void setTipo(TipoTelefone tipo) {
        this.tipo = tipo;
    }
}
