package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class FluenciaIdioma extends BaseEntity{
    
    @Column(name = "FluenciaIdioma")
    private String fluenciaIdioma;

    public String getFluenciaIdioma() {
        return fluenciaIdioma;
    }

    public void setFluenciaIdioma(String fluenciaIdioma) {
        this.fluenciaIdioma = fluenciaIdioma;
    }
    
}
