package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.types.StatusEtapaProcessoSeletivo;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class ProcessoSeletivoEtapa extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "id_processo", referencedColumnName = "id")
    private ProcessoSeletivo processo;
    
    @ManyToOne
    @JoinColumn(name = "id_etapa", referencedColumnName = "id")
    private Etapa etapa;
    
    @OneToMany(mappedBy = "etapa")
    private List<ProcessoSeletivoEtapaAprendiz> selecionados;
    
    private StatusEtapaProcessoSeletivo status;

    public StatusEtapaProcessoSeletivo getStatus() {
        return status;
    }

    public void setStatus(StatusEtapaProcessoSeletivo status) {
        this.status = status;
    }
    
    public ProcessoSeletivo getProcesso() {
        return processo;
    }

    public void setProcesso(ProcessoSeletivo processo) {
        this.processo = processo;
    }

    public Etapa getEtapa() {
        return etapa;
    }

    public void setEtapa(Etapa etapa) {
        this.etapa = etapa;
    }

    public List<ProcessoSeletivoEtapaAprendiz> getSelecionados() {
        return selecionados;
    }

    public void setSelecionados(List<ProcessoSeletivoEtapaAprendiz> selecionados) {
        this.selecionados = selecionados;
    }

    public ProcessoSeletivoEtapa() {
        this.status = StatusEtapaProcessoSeletivo.ABERTO;
    }
    
    
    
}