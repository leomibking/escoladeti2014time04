package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import br.unicesumar.escoladeti.types.StatusContrato;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Contrato extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "id_aprendiz", referencedColumnName = "id")
    private Aprendiz aprendiz;

    @ManyToOne
    @JoinColumn(name = "id_vaga", referencedColumnName = "id")
    private Vaga vaga;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataInicio;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataFim;

    @ManyToOne
    @JoinColumn(name = "id_turma", referencedColumnName = "id")
    private Turma turma;

    @Column(name = "status")
    private StatusContrato status;

    @OneToMany
    @JoinColumn(name = "id_contrato", referencedColumnName = "id")
    private List<SituacaoContrato> situacoes;

    @OneToMany
    @JoinColumn(name = "id_contrato", referencedColumnName = "id")
    private List<AdvertenciaContrato> advertencias;

    public Aprendiz getAprendiz() {
        return aprendiz;
    }

    public void setAprendiz(Aprendiz aprendiz) {
        this.aprendiz = aprendiz;
    }

    public Vaga getVaga() {
        return vaga;
    }

    public void setVaga(Vaga vaga) {
        this.vaga = vaga;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }

    public StatusContrato getStatus() {
        return status;
    }

    public void setStatus(StatusContrato status) {
        this.status = status;
    }

}
