package br.unicesumar.escoladeti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class TurmaAluno extends BaseEntity {

    //anotação do Leo :) - leitura do mapeamento
    //um aluno pode estar em muitas turmas <-
    //Muitos TurmaAluno tem esse aluno ->
    @ManyToOne
    @JoinColumn(name = "id_aluno", referencedColumnName = "id")
    private Aprendiz aluno;
    
    @ManyToOne
    @JoinColumn(name = "id_turma")
    private Turma turma;

    @Column(name = "obs_aluno")
    private String observacaoAluno;
    
    @ManyToOne
    @JoinColumn(name = "id_processo_seletivo", referencedColumnName = "id")
    private ProcessoSeletivo processoSeletivo;

    public ProcessoSeletivo getProcessoSeletivo() {
        return processoSeletivo;
    }

    public void setProcessoSeletivo(ProcessoSeletivo processoSeletivo) {
        this.processoSeletivo = processoSeletivo;
    }
    
    public Aprendiz getAluno() {
        return aluno;
    }

    public void setAluno(Aprendiz aluno) {
        this.aluno = aluno;
    }

    public String getObservacaoAluno() {
        return observacaoAluno;
    }

    public void setObservacaoAluno(String observacaoAluno) {
        this.observacaoAluno = observacaoAluno;
    }

    public TurmaAluno() {
    }

    public TurmaAluno(Aprendiz aluno) {
        this.aluno = aluno;
    }

    public Turma getTurma() {
        return turma;
    }

    public void setTurma(Turma turma) {
        this.turma = turma;
    }
    

}
