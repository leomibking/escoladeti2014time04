package br.unicesumar.escoladeti.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
public class Usuario extends BaseEntity {

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true)
    private String login;

    @Column(nullable = false)
    private String senha;

    @ManyToOne(optional = false)
    @JoinColumn(name = "id_perfildeacesso", referencedColumnName = "id")
    private PerfilDeAcesso perfilDeAcesso;
    
    @Temporal(TemporalType.DATE)
    @Column
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date dataExpiracao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public String getLogin() {
        return login;
    }

    public void setLogin(String usuario) {
        this.login = usuario;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

	public PerfilDeAcesso getPerfilDeAcesso() {
		return perfilDeAcesso;
	}

	public void setPerfilDeAcesso(PerfilDeAcesso perfilDeAcesso) {
		this.perfilDeAcesso = perfilDeAcesso;
	}

	public Date getDataExpiracao() {
		return dataExpiracao;
	}

	public void setDataExpiracao(Date dataExpiracao) {
		this.dataExpiracao = dataExpiracao;
	}

}
