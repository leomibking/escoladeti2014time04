package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.commons.CustomJsonDateDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;


@Entity
public class Log extends BaseEntity {
    
    @ManyToOne
    @JoinColumn(name = "id_aprendiz", referencedColumnName = "id")
    private Aprendiz aprendiz;

    @Temporal(javax.persistence.TemporalType.DATE)
    @Column
    @JsonDeserialize(using = CustomJsonDateDeserializer.class)
    private Date data;
    
    @Column
    private String observacao;

    public Aprendiz getAprendiz() {
        return aprendiz;
    }

    public void setAprendiz(Aprendiz aprendiz) {
        this.aprendiz = aprendiz;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
