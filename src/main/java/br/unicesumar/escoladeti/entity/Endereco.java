package br.unicesumar.escoladeti.entity;

import br.unicesumar.escoladeti.types.AreaEndereco;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Endereco extends BaseEntity {

    @Column(nullable = false, length = 10)
    private String cep;

    @Column(nullable = false, length = 50)
    private String logradouro;

    @Column(nullable = false, length = 6)
    private String numero;

    @Column(length = 100)
    private String complemento;

    @Column(nullable = false, length = 60)
    private String bairro;

    @Enumerated(EnumType.STRING)
    @Column
    private AreaEndereco area;

    @ManyToOne
    @JoinColumn(name = "id_cidade", referencedColumnName = "id")
    private Cidade cidade;

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public AreaEndereco getArea() {
        return area;
    }

    public void setArea(AreaEndereco area) {
        this.area = area;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @Override
    public String toString() {
        String ret;
        ret =  this.logradouro +", "+this.numero+", "+this.bairro;
        if (this.cidade != null){
            ret = ret +", "+this.cidade.toString();
        }
        return ret;
    }
    
    
}
