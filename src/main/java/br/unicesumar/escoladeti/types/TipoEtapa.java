package br.unicesumar.escoladeti.types;

public enum TipoEtapa {
    
    ABERTA("Aberta"),
    VALOR("Valor");

    private TipoEtapa(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
}
