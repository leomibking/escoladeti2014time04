/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unicesumar.escoladeti.types;

/**
 *
 * @author Leo
 */
public enum CursoEscola {

    FUNDAMENTAL_INCOMPLETO("Fundamental Incompleto"),
    SUPLETIVO_FUNDAMENTAL_INCOMPLETO("Supletivo Fundamental Incompleto"),
    MEDIO_INCOMPLETO("Médio Incompleto"),
    SUPLETIVO_MEDIO_INCOMPLETO("Supletivo Médio Incompleto"),
    MEDIO_TECNICO_COMPLETO("Médio Técnico Completo"),
    SUPERIOR_INCOMPLETO("Superior Incompleto");

    private CursoEscola(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
