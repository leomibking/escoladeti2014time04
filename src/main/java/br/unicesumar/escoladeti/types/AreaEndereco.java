package br.unicesumar.escoladeti.types;

public enum AreaEndereco {

    RURAL("Rural"),
    URBANA("Urbana");

    private AreaEndereco(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
