package br.unicesumar.escoladeti.types;

public enum Nacionalidade {

    BRASILEIRO("Brasileiro(a)"),
    NATURALIZADO("Naturalizado(a)"),
    ESTRANGEIRO("Estrangeiro(a)");

    private Nacionalidade(String descricao) {
        this.descricao = descricao;
    }
    
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
