/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unicesumar.escoladeti.types;

/**
 *
 * @author femenolli
 */
public enum TipoDeficiencia {

    MOTORA("Motora"),
    VISUAL("Visual"),
    AUDITIVA("Auditiva"),
    MENTAL("Mental"),
    FISICA("Física");

    private TipoDeficiencia(String descricao) {
        this.descricao = descricao;
    }
    
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
