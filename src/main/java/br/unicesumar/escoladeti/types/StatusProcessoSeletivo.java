package br.unicesumar.escoladeti.types;


public enum StatusProcessoSeletivo {
    
    PRESELECAO("Pré-Seleção"),
    SELECAO("Em Seleção"),
    POSSELECAO("Pós-Seleção"),
    ENCERRADO("Encerrado"),
    CANCELADO("Cancelado");

    private StatusProcessoSeletivo(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
    
}
