package br.unicesumar.escoladeti.types;

public enum CNH {

    A("A"), B("B"), AB("AB"), C("C"), D("D"), E("E");

    private CNH(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
