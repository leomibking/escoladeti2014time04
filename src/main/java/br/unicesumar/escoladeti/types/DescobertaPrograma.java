package br.unicesumar.escoladeti.types;


public enum DescobertaPrograma {

    TV("Tv"),
    CARTAZ("Cartaz"),
    RADIO("Radio"),
    ESCOLA("Escola"),
    AMIGOS("Amigos"),
    OUTRO("Outro");

    private DescobertaPrograma(String descricao) {
        this.descricao = descricao;
    }
    
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
