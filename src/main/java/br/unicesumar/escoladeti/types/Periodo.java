package br.unicesumar.escoladeti.types;

public enum Periodo {
    
    MATUTINO("Matutino"),
    VESPERTINO("Vespertino");
    
    
    private Periodo(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
