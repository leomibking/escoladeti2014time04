package br.unicesumar.escoladeti.types;


public enum MotivoParticiparPrograma {
    APRENDER_UMA_PROFISSAO("Aprender uma profissão"),
    ABRIR_SEU_PROPRIO_NEGOCIO("Abrir seu próprio negócio"),
    CONSEGUIR_EMRPEGO("Conseguir emprego"),
    AUXILIO_FINANCEIRO("Auxílio financeiro"),
    AJUDAR_A_COMUNIDADE("Ajudar a comunidade"),
    OUTRO("Outro");

    private MotivoParticiparPrograma(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
