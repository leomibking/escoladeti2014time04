package br.unicesumar.escoladeti.types;

public enum StatusContrato {

    CANCELADO("Cancelado"),
    APROVADO("Aprovado"),
    FINALIZADO("Finalizado"),
    PENDENTE("Pendente"),
    ABERTO("Aberto");

    private StatusContrato(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
