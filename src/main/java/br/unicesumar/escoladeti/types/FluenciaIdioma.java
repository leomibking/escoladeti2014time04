package br.unicesumar.escoladeti.types;


public enum FluenciaIdioma {
    LE("Lê"),
    FALA("Fala"),
    LE_FALA("Lê e Fala"),
    LE_ESCREVE("Lê e escreve"),
    LE_ESCREVE_FALA("Lê, escreve e fala");

    private FluenciaIdioma(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
