package br.unicesumar.escoladeti.types;

public enum StatusEtapaProcessoSeletivo {

    ABERTO("Aberta"),
    ANDAMENTO("Andamento"),
    FINALIZADO("Finalizada");

    private StatusEtapaProcessoSeletivo(String descricao) {
        this.descricao = descricao;
    }

    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return this.descricao;
    }
}
