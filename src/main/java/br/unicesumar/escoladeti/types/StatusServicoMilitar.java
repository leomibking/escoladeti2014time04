package br.unicesumar.escoladeti.types;

public enum StatusServicoMilitar {

    CONVOCADO("Convocado"),
    SERVINDO("Servindo"),
    AGUARDANDO("Aguardando"),
    DISPENSADO("Dispensado"),
    JA_SERVIU("já serviu");

    private StatusServicoMilitar(String descricao) {
        this.descricao = descricao;
    }
    
    
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
