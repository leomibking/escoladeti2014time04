/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unicesumar.escoladeti.types;

/**
 *
 * @author Leo
 */
public enum TipoTelefone {

    RESPONSAVEL("Responsável"),
    COMERCIAL("Comercial"),
    RESIDENCIAL("Residencial"),
    CELULAR("Celular"),
    FIXO("Fixo"),
    RECADO("Recado");

    private TipoTelefone(String descricao) {
        this.descricao = descricao;
    }
    
    private String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
