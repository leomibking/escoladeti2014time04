package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.dtos.TurmaProfessorDTO;
import br.unicesumar.escoladeti.entity.Professor;
import br.unicesumar.escoladeti.service.ProfessorService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/professores")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ProfessorController extends BaseController<Professor, ProfessorService> {

    @RequestMapping(value = {"/list"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TurmaProfessorDTO> getListaDTO() {
        List<Professor> p = getService().getList();
        List<TurmaProfessorDTO> ret = new ArrayList<>();

        for (Professor pr : p) {
            TurmaProfessorDTO professor = new TurmaProfessorDTO(pr);
            ret.add(professor);
        }

        return ret;
    }

}
