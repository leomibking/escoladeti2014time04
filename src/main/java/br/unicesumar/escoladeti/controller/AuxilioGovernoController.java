package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.AuxilioGoverno;
import br.unicesumar.escoladeti.service.AuxilioGovernoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/auxiliosGoverno")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class AuxilioGovernoController extends BaseController<AuxilioGoverno, AuxilioGovernoService> {

}
