package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Pais;
import br.unicesumar.escoladeti.service.PaisService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/paises")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PaisController extends BaseController<Pais, PaisService> {

}
