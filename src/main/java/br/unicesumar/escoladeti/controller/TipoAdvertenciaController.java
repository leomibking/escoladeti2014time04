package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.TipoAdvertencia;
import br.unicesumar.escoladeti.service.TipoAdvertenciaService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/tipoAdvertencia")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TipoAdvertenciaController extends BaseController<TipoAdvertencia, TipoAdvertenciaService> {

}
