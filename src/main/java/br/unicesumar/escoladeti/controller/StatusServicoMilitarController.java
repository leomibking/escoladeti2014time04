package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.StatusServicoMilitar;
import br.unicesumar.escoladeti.service.StatusServicoMilitarService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/statusMilitar")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class StatusServicoMilitarController extends BaseController<StatusServicoMilitar, StatusServicoMilitarService> {

}
