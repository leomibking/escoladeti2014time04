package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Disciplina;
import br.unicesumar.escoladeti.service.DisciplinaService;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/disciplina")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class DisciplinaController extends BaseController<Disciplina, DisciplinaService> {

    @RequestMapping(value = {""}, params = {"nome"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Disciplina> getByName(@RequestParam String nome) {
        return getService().getByName(nome);
    }
;
}
