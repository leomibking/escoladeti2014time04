package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.AprendizResumidoDTO;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.dtos.CandidatoProcessoDTO;
import br.unicesumar.escoladeti.dtos.ContratoGerarDTO;
import br.unicesumar.escoladeti.dtos.EtapaDTO;
import br.unicesumar.escoladeti.dtos.ProcessoPreSelecaoDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoAprendizDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoAprendizEditDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoEditDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoEditEtapaDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoEtapaAprendizDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoEtapaDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoEtapaEditDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoShowDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoTurmaDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.Vaga;
import br.unicesumar.escoladeti.service.LogService;
import br.unicesumar.escoladeti.service.ProcessoSeletivoService;
import br.unicesumar.escoladeti.types.StatusEtapaProcessoSeletivo;
import br.unicesumar.escoladeti.types.StatusProcessoSeletivo;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/ProcessoSeletivo")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ProcessoSeletivoController extends BaseController<ProcessoSeletivo, ProcessoSeletivoService> {

    @Autowired
    private LogService logService;

    // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getProcessoParser());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getProcessoParser(), valor);
        }
    }

    @Override
    public List<ProcessoSeletivo> query() {
        return null;
    }

    @Override
    public ProcessoSeletivo get(Long id
    ) {
        return null;
    }

    @Override
    public ServerResponse save(ProcessoSeletivo item
    ) {
        return null;
    }

    @RequestMapping(value = {"/{id}/show"}, method = RequestMethod.GET)
    @ResponseBody
    public ProcessoSeletivoShowDTO getShow(@PathVariable("id") Long id) throws SQLException {
        ProcessoSeletivo p = getService().getById(id);
        ProcessoSeletivoShowDTO ps = new ProcessoSeletivoShowDTO(p);
        ps.setAprovados(this.aprovados(id));

        for (ProcessoSeletivoTurmaDTO t : ps.getTurmas()) {
            t.addAlunos(getService().getAlunosByTurma(t.getId(), id));
            t.atualizarQtAlunos();
        }

        for (ProcessoSeletivoAprendizDTO psa : ps.getAprovados()) {
            getService().setSelecionado(psa, ps.getId());
        }

        for (ProcessoSeletivoAprendizDTO psa : ps.getSelecionados()) {
            getService().setSelecionado(psa, ps.getId());
        }

        return ps;
    }

    @RequestMapping(value = {"/{id}/editar"}, method = RequestMethod.GET)
    @ResponseBody
    public ProcessoSeletivoEditDTO getEdit(@PathVariable("id") Long id) {
        ProcessoSeletivo p = getService().getById(id);
        ProcessoSeletivoEditDTO ps = new ProcessoSeletivoEditDTO(p);
        return ps;
    }

    @RequestMapping(value = {"/{id}/salvarpreselecao"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse salvarPreSelecao(@RequestBody ProcessoPreSelecaoDTO dto, @PathVariable("id") Long id) {
        ProcessoSeletivo ps = getService().getById(id);
        for (CandidatoProcessoDTO adto : dto.getSelecionados()) {
            Aprendiz a = getService().getAprendiz(adto.getId());
            if (!ps.existeAprendiz(a)) {
                ProcessoSeletivoAprendiz psa = new ProcessoSeletivoAprendiz();
                psa.setAprovado(false);
                psa.setCandidato(a);
                psa.setProcesso(ps);
                getService().salvarProcessoSeletivoAprendiz(psa);
                ps.getCandidatos().add(psa);
            }

            for (ProcessoSeletivoEtapa e : ps.getEtapas()) {
                ProcessoSeletivoEtapaAprendiz psea = new ProcessoSeletivoEtapaAprendiz();
                psea.setCandidato(a);
                psea.setEtapa(e);
                psea.setResultadoAberto(null);
                psea.setResultadoValor(0);

                getService().salvarAprendizEtapa(psea);
                e.getSelecionados().add(psea);
            }
            logService.gerarLog(a.getId(), "Aprendiz aprovado na Pré-Seleção do Processo Seletivo " + ps.getNome());
        }

        ps.setStatus(StatusProcessoSeletivo.SELECAO);
        getService().save(ps);
        return new ServerResponse("Etapa de Pré Seleção foi salva com Sucesso.");
    }

    @RequestMapping(value = {"/{id}/salvarselecao"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse salvarSelecao(@RequestBody ProcessoSeletivoEditDTO dto, @PathVariable("id") Long id) {
        ProcessoSeletivo ps = getService().getById(id);
        ps.setStatus(StatusProcessoSeletivo.POSSELECAO);
        getService().save(ps);

        return new ServerResponse("Salvou selecao");
    }

    @RequestMapping(value = {"/{id}/salvarposselecao"}, method = RequestMethod.POST)
    @ResponseBody
    @Transactional
    public ServerResponse salvarPosSelecao(@RequestBody ProcessoSeletivoShowDTO dto, @PathVariable("id") Long id) {
        //salvar os alunos, trocar o status da seleção para concluido
        ProcessoSeletivo ps = getService().getById(id);

        if (ps.getStatus() == StatusProcessoSeletivo.ENCERRADO) {
            return new ServerResponse("Processo Seletivo " + ps.getNome() + " já está concluido.");
        }
        ContratoGerarDTO c;
        for (ProcessoSeletivoTurmaDTO turma : dto.getTurmas()) {

            if (turma.getAlunos() != null && turma.getAlunos().size() > 0) {

                //vincular o processo na turma
                Turma t = getService().getTurmaById(turma.getId());
                ps.getTurmas().add(t);
                t.getProcesso().add(ps);

                getService().save(ps);
                getService().salvarTurma(t);

                for (AprendizResumidoDTO aluno : turma.getAlunos()) {
                    Aprendiz a = getService().getAprendiz(aluno.getId());
                    getService().salvarNovoAlunoNaTurma(a, t, ps);
                    a.setProcesso(ps);
                    getService().salvarAprendiz(a);
                    logService.gerarLog(a.getId(), "O Aprendiz finalizou o Processo Seletivo " + ps.getNome()
                            + " e foi adicionado na turma " + t.getNome());
                    ///gera contrato
                    c = new ContratoGerarDTO();
                    c.setIdAprendiz(a.getId());
                    c.setIdVaga(dto.getVaga().getId());
                    c.setIdTurma(t.getId());
                    c.setDataInicio(new Date(System.currentTimeMillis()));
                    c.setDataFim(new Date(System.currentTimeMillis()));
                    getService().gerarContrato(c);
                    logService.gerarLog(a.getId(), "Contrato Gerado");
                }
            }

        }
        ps.setStatus(StatusProcessoSeletivo.ENCERRADO);
        getService().save(ps);

        //atualizar a quantidade de vagas na empresa disponivel para a vaga
        Vaga v = getService().getVagaById(dto.getVaga().getId());
        v.atualizarQuantidadeDeVagas(ps.getQtAprovados());
        getService().salvarVaga(v);

        return new ServerResponse("Processo Seletivo " + ps.getNome() + " concluido com sucesso.");
    }

    @RequestMapping(value = {"/salvar"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse salvarEdit(@RequestBody ProcessoSeletivoEditDTO dto) {
        try {
            ProcessoSeletivo ps;
            boolean achou = false;
            //salvar os dados do processo seletivo
            if (dto.getId() == null || dto.getId() == 0) {
                ps = new ProcessoSeletivo();
                //ps.setNome(dto.getNome());
                ps.setDataFim(dto.getDataFim());
                ps.setDataInicio(dto.getDataInicio());
                ps.setEmpresa(getService().getEmpresaById(dto.getEmpresa().getId()));
                ps.setVaga(getService().getVagaById(dto.getVaga().getId()));

                for (ProcessoSeletivoEditEtapaDTO etapaDTO : dto.getEtapas()) {
                    if (etapaDTO.getId() == null || etapaDTO.getId() <= 0) {
                        ProcessoSeletivoEtapa pse = new ProcessoSeletivoEtapa();
                        pse.setProcesso(ps);
                        pse.setEtapa(getService().getByIdEtapa(etapaDTO.getIdEtapa()));
                        //getService().salvarEtapaProcessoSeletivo(pse);
                        ps.getEtapas().add(pse);
                    }
                }
            } else {
                ps = getService().getById(dto.getId());
                //ps.setNome(dto.getNome());
                ps.setDataFim(dto.getDataFim());
                ps.setDataInicio(dto.getDataInicio());
                ps.setEmpresa(getService().getEmpresaById(dto.getEmpresa().getId()));
                ps.setVaga(getService().getVagaById(dto.getVaga().getId()));

                for (ProcessoSeletivoEditEtapaDTO etapaDTO : dto.getEtapas()) {
                    if (etapaDTO.getId() == null || etapaDTO.getId() <= 0) {
                        ProcessoSeletivoEtapa pse = new ProcessoSeletivoEtapa();
                        pse.setProcesso(ps);
                        pse.setEtapa(getService().getByIdEtapa(etapaDTO.getIdEtapa()));
                        getService().salvarEtapaProcessoSeletivo(pse);
                        ps.getEtapas().add(pse);
                    }
                }

                List<ProcessoSeletivoEtapa> remover = new ArrayList<>();

                for (ProcessoSeletivoEtapa e : ps.getEtapas()) {
                    achou = false;
                    //procura na lista de etapas do dto se a etapa existe
                    for (ProcessoSeletivoEditEtapaDTO pse : dto.getEtapas()) {
                        if (pse.getId() == null || pse.getId() == e.getId()) {
                            //se acho nao faz nada e sai do laço
                            achou = true;
                            break;
                        }
                    }
                    //se nao achou, remove
                    if (!achou) {
                        remover.add(e);
                    }
                }

                for (ProcessoSeletivoEtapa e : remover) {
                    ps.getEtapas().remove(e);
                    getService().removerEtapa(e);
                }
            }

            getService().save(ps);
            
            if ((ps.getNome() == "" || ps.getNome() == null) && ps.getId() != null){
                ps.gerarNomeEtapa();
                getService().save(ps);
            }
            return new ServerResponse("Processo Seletivo salvo com sucesso.", true);
        } catch (Exception e) {
            return new ServerResponse("Não foi possivel salvar o Processo Seletivo.", false);
        }
    }

    @RequestMapping(value = {"/{id}/selecionados"}, method = RequestMethod.GET)
    @ResponseBody
    public List<ProcessoSeletivoAprendizDTO> selecionados(@PathVariable("id") Long id) {
        return getService().getSelecionados(id);
    }

    @RequestMapping(value = {"/{id}/selecionados/{selecionado}"}, method = RequestMethod.GET)
    @ResponseBody
    public ProcessoSeletivoAprendizEditDTO selecionadosUnico(@PathVariable("id") Long id, @PathVariable("selecionado") Long selecionado) {
        ProcessoSeletivoAprendiz psa = getService().getSelecionadoById(selecionado);
        ProcessoSeletivoAprendizEditDTO t = new ProcessoSeletivoAprendizEditDTO(psa);
        return t;
    }

    @RequestMapping(value = {"/{id}/selecionados/{selecionado}/aprovar"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse aprovarCandidato(@PathVariable("id") Long id, @PathVariable("selecionado") Long selecionado) {
        ProcessoSeletivoAprendiz psa = getService().getSelecionadoById(selecionado);
        psa.setAprovado(true);
        getService().salvarProcessoSeletivoAprendiz(psa);
        logService.gerarLog(psa.getCandidato().getId(), "Aprendiz foi aprovado no Processo Seletivo " + psa.getProcesso().getNome());
        return new ServerResponse("Candidato " + psa.getCandidato().getNome() + " aprovado.");
    }

    @RequestMapping(value = {"/{id}/selecionados/{selecionado}/reprovar"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse reprovarCandidato(@PathVariable("id") Long id, @PathVariable("selecionado") Long selecionado) {
        ProcessoSeletivoAprendiz psa = getService().getSelecionadoById(selecionado);
        psa.setAprovado(false);
        getService().salvarProcessoSeletivoAprendiz(psa);
        logService.gerarLog(psa.getCandidato().getId(), "Aprendiz foi Reprovado no Processo Seletivo " + psa.getProcesso().getNome());
        return new ServerResponse("Candidato " + psa.getCandidato().getNome() + " reprovado.");
    }

    @RequestMapping(value = {"/{id}/aprovados"}, method = RequestMethod.GET)
    @ResponseBody
    public List<ProcessoSeletivoAprendizDTO> aprovados(@PathVariable("id") Long id
    ) {
        return getService().getAprovados(id);
    }

    @RequestMapping(value = {"/{id}/aprovados/{aprovado}"}, method = RequestMethod.GET)
    @ResponseBody
    public ProcessoSeletivoAprendizEditDTO aprovadoUnico(@PathVariable("id") Long id, @PathVariable("aprovado") Long aprovado) {
        ProcessoSeletivoAprendiz psa = getService().getAprovadoById(aprovado);
        if (psa != null) {
            return new ProcessoSeletivoAprendizEditDTO(psa);
        } else {
            return null;
        }
    }

    @RequestMapping(value = {"/{id}/etapas"}, method = RequestMethod.GET)
    @ResponseBody
    public List<ProcessoSeletivoEtapaDTO> etapas(@PathVariable("id") Long id
    ) {
        return getService().getEtapas(id);
    }

    @RequestMapping(value = {"/{id}/etapas/{etapa}"}, method = RequestMethod.GET)
    @ResponseBody
    public ProcessoSeletivoEtapaEditDTO etapasUnico(@PathVariable("id") Long id, @PathVariable("etapa") Long etapa) {
        ProcessoSeletivoEtapa pse = getService().getEtapaById(etapa);
        if (pse != null && pse.getProcesso().getId().equals(id)) {
            return new ProcessoSeletivoEtapaEditDTO(pse);
        } else {
            return null;
        }
    }

    @RequestMapping(value = {"/{id}/etapas/{etapa}"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse salvarEtapa(@RequestBody ProcessoSeletivoEtapaEditDTO dto, @PathVariable("id") Long id, @PathVariable("etapa") Long etapa) {
        //salvar os alunos, trocar o status para andamento
        ProcessoSeletivoEtapaAprendiz aprendiz;

        for (ProcessoSeletivoEtapaAprendizDTO e : dto.getSelecionados()) {
            aprendiz = getService().getAlunoDaEtapa(e.getId());
            aprendiz.setResultadoAberto(e.getResultadoAberto());
            aprendiz.setResultadoValor(e.getResultadoValor());
            getService().salvarAprendizEtapa(aprendiz);
            if (e.getResultadoValor() != 0.0) {
                logService.gerarLog(aprendiz.getCandidato().getId(), aprendiz.getCandidato().getNome() + " obteve a nota "
                        + e.getResultadoValor()+ " na Etapa " + aprendiz.getEtapa().getEtapa().getDescricao()
                        + aprendiz.getEtapa().getProcesso().getNome());
            } else if (e.getResultadoAberto() != null) {
                logService.gerarLog(aprendiz.getCandidato().getId(), "Observações sobre o(a) " + aprendiz.getCandidato().getNome()
                        + ": "
                        + e.getResultadoAberto() + " na Etapa " + aprendiz.getEtapa().getEtapa().getDescricao()
                        + " no Processo Seletivo "
                        + aprendiz.getEtapa().getProcesso().getNome());
            }

        }

        ProcessoSeletivoEtapa pse = getService().getEtapaById(etapa);

        if (pse.getStatus() == StatusEtapaProcessoSeletivo.ABERTO) {
            pse.setStatus(StatusEtapaProcessoSeletivo.ANDAMENTO);
            getService().salvarEtapaProcessoSeletivo(pse);
        }

        return new ServerResponse("Etapa " + pse.getEtapa().getDescricao() + " salva com sucesso.");
    }

    @RequestMapping(value = {"/{id}/etapas/{etapa}/finalizar"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse concluirEtapa(@PathVariable("id") Long id, @PathVariable("etapa") Long etapa) {
        ProcessoSeletivoEtapa pse = getService().getEtapaById(etapa);

        if (pse.getStatus() == StatusEtapaProcessoSeletivo.ANDAMENTO) {
            pse.setStatus(StatusEtapaProcessoSeletivo.FINALIZADO);
            getService().salvarEtapaProcessoSeletivo(pse);
        }
        return new ServerResponse("Etapa " + pse.getEtapa().getDescricao() + " concluida com sucesso.");
    }

    @RequestMapping(value = {"/{id}/etapas/{etapa}/reabrir"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse reabrirEtapa(@PathVariable("id") Long id, @PathVariable("etapa") Long etapa) {
        ProcessoSeletivoEtapa pse = getService().getEtapaById(etapa);

        if (pse.getStatus() == StatusEtapaProcessoSeletivo.FINALIZADO) {
            pse.setStatus(StatusEtapaProcessoSeletivo.ANDAMENTO);
            getService().salvarEtapaProcessoSeletivo(pse);
        }
        return new ServerResponse("Etapa " + pse.getEtapa().getDescricao() + " reaberta com sucesso.");
    }

    //pesquisa de etapas
    @RequestMapping(value = {"/etapa"}, params = {"nome"}, method = RequestMethod.GET)
    @ResponseBody
    public List<EtapaDTO> getEtapaPorNome(String nome) {
        return getService().getEtapaPorNome(nome);
    }

    @Override
    public ServerResponse remove(Long id) {
        ProcessoSeletivo ps = getService().getById(id);
        if (ps.getStatus() == StatusProcessoSeletivo.ENCERRADO){
            return new ServerResponse("Não é possivel remover um Processo Seletivo ja Encerrado");
        }else{
            
            for (ProcessoSeletivoAprendiz candidato : ps.getCandidatos()){
                getService().removerCandidato(candidato);
            }
            
            ps.getCandidatos().clear();
            
            for (ProcessoSeletivoEtapa etapa : ps.getEtapas()){
                getService().removerEtapa(etapa);
            }
            ps.getEtapas().clear();
            
            return super.remove(id);
        }
    }
    
    @RequestMapping(value = {"{id}/voltarStatus"}, method = RequestMethod.PUT)
    @ResponseBody
    public ServerResponse voltarStatus(@PathVariable("id") Long id) {
        return getService().voltarStatus(id);
    }
    
    
}
