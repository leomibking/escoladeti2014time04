package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.dtos.ContratoGerarDTO;
import br.unicesumar.escoladeti.dtos.ContratoShowDTO;
import br.unicesumar.escoladeti.entity.Contrato;
import br.unicesumar.escoladeti.service.ContratoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/contratos")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ContratoController extends BaseController<Contrato, ContratoService> {

    // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getContratoParser());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getContratoParser(), valor);
        }
    }

    @RequestMapping(value = {"/gerar"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse gerar(@RequestBody ContratoGerarDTO c) {
        // faz uns paranauê com o dto pra os guerezinhos do contrato!
        getService().gerarContrato(c);
        return new ServerResponse("");
    }

    // GET
    @RequestMapping(value = {"/getDTO"}, params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public ContratoShowDTO getDTO(@RequestParam Long id) {
        return new ContratoShowDTO(getService().getById(id));
    }
}
