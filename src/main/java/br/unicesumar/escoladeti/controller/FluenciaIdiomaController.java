package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.FluenciaIdioma;
import br.unicesumar.escoladeti.service.FluenciaIdiomaService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/fluenciaIdioma")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class FluenciaIdiomaController extends BaseController<FluenciaIdioma, FluenciaIdiomaService> {

}
