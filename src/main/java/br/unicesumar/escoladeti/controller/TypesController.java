package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.dtos.TypeDTO;
import br.unicesumar.escoladeti.types.AreaEndereco;
import br.unicesumar.escoladeti.types.CNH;
import br.unicesumar.escoladeti.types.Cor;
import br.unicesumar.escoladeti.types.Curso;
import br.unicesumar.escoladeti.types.CursoEscola;
import br.unicesumar.escoladeti.types.DescobertaPrograma;
import br.unicesumar.escoladeti.types.EscolaridadeResponsavel;
import br.unicesumar.escoladeti.types.EstadoCivil;
import br.unicesumar.escoladeti.types.FluenciaIdioma;
import br.unicesumar.escoladeti.types.HabilidadeManual;
import br.unicesumar.escoladeti.types.MotivoParticiparPrograma;
import br.unicesumar.escoladeti.types.Nacionalidade;
import br.unicesumar.escoladeti.types.Periodo;
import br.unicesumar.escoladeti.types.Sexo;
import br.unicesumar.escoladeti.types.StatusProcessoSeletivo;
import br.unicesumar.escoladeti.types.StatusServicoMilitar;
import br.unicesumar.escoladeti.types.TipoDeficiencia;
import br.unicesumar.escoladeti.types.TipoEtapa;
import br.unicesumar.escoladeti.types.TipoTelefone;
import br.unicesumar.escoladeti.types.TurnoEscola;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/types")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TypesController implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

    @RequestMapping(value = {"/areaEndereco"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> areaEndereco() {
        List<TypeDTO> l = new ArrayList<>();
        for (AreaEndereco t : AreaEndereco.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/cnh"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> cnh() {
        List<TypeDTO> l = new ArrayList<>();
        for (CNH t : CNH.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/cor"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> cor() {
        List<TypeDTO> l = new ArrayList<>();
        for (Cor t : Cor.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/curso"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> curso() {
        List<TypeDTO> l = new ArrayList<>();
        for (Curso t : Curso.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/cursoEscola"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> cursoEscola() {
        List<TypeDTO> l = new ArrayList<>();
        for (CursoEscola t : CursoEscola.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/descobertaPrograma"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> descobertaPrograma() {
        List<TypeDTO> l = new ArrayList<>();
        for (DescobertaPrograma t : DescobertaPrograma.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/escolaridadeResponsavel"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> escolaridadeResponsavel() {
        List<TypeDTO> l = new ArrayList<>();
        for (EscolaridadeResponsavel t : EscolaridadeResponsavel.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/estadoCivil"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> estadoCivil() {
        List<TypeDTO> l = new ArrayList<>();
        for (EstadoCivil t : EstadoCivil.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/fluenciaIdioma"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> fluenciaIdioma() {
        List<TypeDTO> l = new ArrayList<>();
        for (FluenciaIdioma t : FluenciaIdioma.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/habilidadeManual"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> habilidadeManual() {
        List<TypeDTO> l = new ArrayList<>();
        for (HabilidadeManual t : HabilidadeManual.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/motivoParticiparPrograma"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> motivoParticiparPrograma() {
        List<TypeDTO> l = new ArrayList<>();
        for (MotivoParticiparPrograma t : MotivoParticiparPrograma.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/nacionalidade"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> nacionalidade() {
        List<TypeDTO> l = new ArrayList<>();
        for (Nacionalidade t : Nacionalidade.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/sexo"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> sexo() {
        List<TypeDTO> l = new ArrayList<>();
        for (Sexo t : Sexo.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/statusServicoMilitar"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> statusServicoMilitar() {
        List<TypeDTO> l = new ArrayList<>();
        for (StatusServicoMilitar t : StatusServicoMilitar.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/tipoDeficiencia"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> tipoDeficiencia() {
        List<TypeDTO> l = new ArrayList<>();
        for (TipoDeficiencia t : TipoDeficiencia.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/tipoTelefone"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> tipoTelefone() {
        List<TypeDTO> l = new ArrayList<>();
        for (TipoTelefone t : TipoTelefone.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/turnoEscola"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> turnoEscola() {
        List<TypeDTO> l = new ArrayList<>();
        for (TurnoEscola t : TurnoEscola.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/statusProcessoSeletivo"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> statusProcessoSeletivo() {
        List<TypeDTO> l = new ArrayList<>();
        for (StatusProcessoSeletivo t : StatusProcessoSeletivo.values()) {
            l.add(new TypeDTO(t.name(), t.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/periodo"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> periodo() {
        List<TypeDTO> l = new ArrayList<>();
        for (Periodo p : Periodo.values()) {
            l.add(new TypeDTO(p.name(), p.getDescricao()));
        }
        return l;
    }

    @RequestMapping(value = {"/tipoEtapa"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TypeDTO> tipoEtapa() {
        List<TypeDTO> l = new ArrayList<>();
        for (TipoEtapa te : TipoEtapa.values()) {
            l.add(new TypeDTO(te.name(), te.getDescricao()));
        }
        return l;
    }
}
