package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Documento;
import br.unicesumar.escoladeti.service.DocumentoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/documento")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class DocumentoController extends BaseController<Documento, DocumentoService> {

}
