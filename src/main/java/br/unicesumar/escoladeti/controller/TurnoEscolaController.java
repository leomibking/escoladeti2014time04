/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.TurnoEscola;
import br.unicesumar.escoladeti.service.TurnoEscolaService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/turnoEscola")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TurnoEscolaController extends BaseController<TurnoEscola, TurnoEscolaService> {

}
