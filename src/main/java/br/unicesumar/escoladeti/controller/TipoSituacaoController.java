package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.TipoSituacao;
import br.unicesumar.escoladeti.service.TipoSituacaoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/tipoSituacao")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TipoSituacaoController extends BaseController<TipoSituacao, TipoSituacaoService> {

}
