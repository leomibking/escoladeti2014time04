package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.CidadeDTO;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;
import br.unicesumar.escoladeti.entity.Cidade;
import br.unicesumar.escoladeti.service.CidadeService;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/rest/cidades")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class CidadeController extends BaseController<Cidade, CidadeService> {

    /*
     *   Url final:
     *   http://localhost:8084/rest/cidades/porUF?id=1
     */
    @RequestMapping(value = {"/porUF"}, params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public List<CidadeDTO> porUF(@RequestParam Long id) {
        return getService().findAllByUFId(id);
    }

}
