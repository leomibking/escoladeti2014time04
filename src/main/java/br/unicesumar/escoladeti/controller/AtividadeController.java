package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Atividade;
import br.unicesumar.escoladeti.service.AtividadeService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/atividade")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class AtividadeController extends BaseController<Atividade, AtividadeService> {
    
    

}
