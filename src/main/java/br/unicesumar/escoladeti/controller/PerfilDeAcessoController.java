package br.unicesumar.escoladeti.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import br.unicesumar.escoladeti.entity.PerfilDeAcesso;
import br.unicesumar.escoladeti.service.PerfilDeAcessoService;

@Controller
@RequestMapping("/rest/perfisDeAcesso")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PerfilDeAcessoController extends BaseController<PerfilDeAcesso, PerfilDeAcessoService> {

}
