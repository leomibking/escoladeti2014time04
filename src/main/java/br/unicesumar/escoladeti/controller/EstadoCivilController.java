package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.BaseEntity;
import br.unicesumar.escoladeti.entity.EstadoCivil;
import br.unicesumar.escoladeti.service.BaseService;
import br.unicesumar.escoladeti.service.EstadoCivilService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/estadoCivil")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class EstadoCivilController extends BaseController<EstadoCivil, EstadoCivilService> {

}
