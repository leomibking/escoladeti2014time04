package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.EmpresaVagaDTO;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.dtos.EmpresaListDTO;
import br.unicesumar.escoladeti.dtos.EmpresaVagaListDTO;
import br.unicesumar.escoladeti.entity.Empresa;
import br.unicesumar.escoladeti.service.EmpresaService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/empresas")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class EmpresaController extends BaseController<Empresa, EmpresaService> {

    // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getEmpresaListaDTO());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getEmpresaListaDTO(), valor);
        }
    }

    @RequestMapping(value = {"/salvarVaga"}, method = RequestMethod.POST)
    @ResponseBody
    public EmpresaVagaDTO salvarVaga(@RequestBody EmpresaVagaDTO vaga) {
        this.getService().salvarVaga(vaga);
        return vaga;
    }

    @RequestMapping(value = {"/removerVaga"}, params = {"idEmpresa", "idVaga"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ServerResponse removerVaga(@RequestParam Long idEmpresa, @RequestParam Long idVaga) {
        try {
            this.getService().removerVaga(idEmpresa, idVaga);
            return new ServerResponse("Vaga removida com sucesso.");
        } catch (Exception e) {
            return new ServerResponse("Não foi possivel remover a vaga pois a mesma possui vinculos.");
        }
    }

    @RequestMapping(value = {"/vagasEmpresa"}, params = {"idEmpresa"}, method = RequestMethod.GET)
    @ResponseBody
    public List<EmpresaVagaListDTO> recuperarVaga(@RequestParam Long idEmpresa) {
        Empresa em = this.getService().getById(idEmpresa);
        return this.getService().getVagasEmpresa(em);
    }

    @Override
    public List<Empresa> query() {
        return null;
    }

    @RequestMapping(value = {"/listar"}, method = RequestMethod.GET)
    @ResponseBody
    public List<EmpresaListDTO> listarEmpresas() {
        List<EmpresaListDTO> retorno = new ArrayList<>();
        List<Empresa> lista = getService().getList();
        for (Empresa e : lista) {
            retorno.add(new EmpresaListDTO(e));
        }
        return retorno;
    }

}
