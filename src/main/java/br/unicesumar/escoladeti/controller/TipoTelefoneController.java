package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.TipoTelefone;
import br.unicesumar.escoladeti.service.TipoTelefoneService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/tipoTelefone")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TipoTelefoneController extends BaseController<TipoTelefone, TipoTelefoneService> {

}
