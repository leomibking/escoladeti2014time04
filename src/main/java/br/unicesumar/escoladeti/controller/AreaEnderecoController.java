package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.AreaEndereco;
import br.unicesumar.escoladeti.service.AreaEnderecoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/areaEndereco")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class AreaEnderecoController extends BaseController<AreaEndereco, AreaEnderecoService> {

}
