package br.unicesumar.escoladeti.controller;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.UsuarioDTO;
import br.unicesumar.escoladeti.entity.Usuario;
import br.unicesumar.escoladeti.service.PerfilDeAcessoService;
import br.unicesumar.escoladeti.service.UsuarioService;

@Controller
public class LoginController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private PerfilDeAcessoService perfilDeAcessoService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Serializable login(@RequestBody UsuarioDTO usuario) {
        perfilDeAcessoService.inicializaPerfis();
        usuarioService.inicializarUsuarioAdmin();
        Usuario u = usuarioService.getUsuario(usuario);
        if (u != null) {
            return u;
        } else {
            return new ServerResponse("Login ou senha inválidos");
        }
    }

}
