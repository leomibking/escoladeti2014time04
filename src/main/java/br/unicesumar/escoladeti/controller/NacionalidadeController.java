package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Nacionalidade;
import br.unicesumar.escoladeti.service.NacionalidadeService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/nacionalidade")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class NacionalidadeController extends BaseController<Nacionalidade, NacionalidadeService> {

}
