package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Etapa;
import br.unicesumar.escoladeti.service.EtapaService;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/etapas")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class EtapaController extends BaseController<Etapa, EtapaService> {

}
