package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.entity.BaseEntity;
import br.unicesumar.escoladeti.service.BaseService;
import br.unicesumar.escoladeti.service.LogService;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.text.ParseException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

public abstract class BaseController<TEntity extends BaseEntity, TService extends BaseService> implements Serializable {

    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(BaseController.class);

    @Autowired
    private TService service;
    
    @Autowired
    private LogService logService;

    private final String entityName;

    public BaseController() {
        Class<TEntity> typeOfT = (Class<TEntity>) ((ParameterizedType) getClass()
                .getGenericSuperclass())
                .getActualTypeArguments()[0];
        this.entityName = typeOfT.getSimpleName();
    }

    protected String getEntityName() {
        return this.entityName;
    }

    protected TService getService() {
        return this.service;
    }

    // PAGINATION    
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getParserGenericDTO());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getParserGenericDTO(), valor);
        }
    }

    // QUERY
    @RequestMapping(value = {""}, method = RequestMethod.GET)
    @ResponseBody
    public List<TEntity> query() {
        return getService().getList();
    }

    // GET
    @RequestMapping(value = {""}, params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public Object get(@RequestParam Long id) {
        return (Object) getService().getById(id);
    }

    // SAVE
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse save(@RequestBody TEntity item) throws ParseException {
        String msg = null;
        
        if(item.getId() == null){
            msg = this.getEntityName() + " cadastrada com sucesso.";
        }else{
            msg = this.getEntityName() + " editado com sucesso.";
        }
        getService().save(item);
        return new ServerResponse(msg);
    }

    // REMOVE
    @RequestMapping(value = {""}, params = {"id"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ServerResponse remove(@RequestParam Long id) {
        try {
            getService().delete(id);
            return new ServerResponse(this.getEntityName() + " removida com sucesso.");
        } catch (Exception e) {
            return new ServerResponse("Não é possivel remover o cadastro pois o mesmo possui vinculos.");
        }
    }
};
