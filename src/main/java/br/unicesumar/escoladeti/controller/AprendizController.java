package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.dtos.AprendizDadosAprendizagemDTO;
import br.unicesumar.escoladeti.dtos.AprendizDocumentoDTO;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.dtos.CandidatoProcessoDTO;
import br.unicesumar.escoladeti.dtos.FilterDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.service.AprendizService;
import br.unicesumar.escoladeti.service.LogService;
import java.text.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/aprendizes")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class AprendizController extends BaseController<Aprendiz, AprendizService> {

    @Autowired
    private LogService logService;

    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getAprendizResumidoDTO());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getAprendizResumidoDTO(), valor);
        }
    }

    @Override
    public Object get(Long id) {
        Aprendiz a = getService().getById(id);
        return a;
    }

    @Override
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse save(@RequestBody Aprendiz item) throws ParseException {
        if (item.getId() != null) {
            Aprendiz banco = getService().getById(item.getId());

            if (banco != null) {
                item.setProcesso(banco.getProcesso());
            }
        }
        ServerResponse sr = super.save(item);
        logService.gerarLog(item.getId(), sr.getMessage());
        return sr;
    }

    @RequestMapping(value = {"/salvarDocumento"}, method = RequestMethod.POST)
    @ResponseBody
    public AprendizDocumentoDTO salvarDocumento(@RequestBody AprendizDocumentoDTO documento) {
        this.getService().salvarDocumento(documento);
        return documento;
    }

    @RequestMapping(value = {"/removerDocumento"}, params = {"idAprendiz", "idDocumento"}, method = RequestMethod.DELETE)
    @ResponseBody
    public void removerDocumento(@RequestParam Long idAprendiz, @RequestParam Long idDocumento) {
        this.getService().removerDocumento(idAprendiz, idDocumento);
    }

    @RequestMapping(value = {"/listaDTO"}, method = RequestMethod.POST)
    @ResponseBody
    public DataPage<CandidatoProcessoDTO> listaDTOAprendiz(Integer pageIndex, @RequestBody FilterDTO filtro) {
        return this.getService().getAprendizesPreSelecao(pageIndex, filtro);
    }

    @RequestMapping(value = {"{id}/dadosAprendizagem"}, method = RequestMethod.GET)
    @ResponseBody
    public AprendizDadosAprendizagemDTO dadosAprendizagem(@PathVariable("id") Long id) {
        return this.getService().getDadosAprendizagem(id);
    }

}
