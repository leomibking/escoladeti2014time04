package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Relatorio;
import br.unicesumar.escoladeti.service.BaseService;
import br.unicesumar.escoladeti.service.RelatorioService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/relatorio/crud")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RelatorioCrudController extends BaseController<Relatorio, RelatorioService>{
   
}
