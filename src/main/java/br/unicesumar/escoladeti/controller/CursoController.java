package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Curso;
import br.unicesumar.escoladeti.service.CursoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/curso")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class CursoController extends BaseController<Curso, CursoService> {

}
