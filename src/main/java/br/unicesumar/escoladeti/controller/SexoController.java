package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Sexo;
import br.unicesumar.escoladeti.service.SexoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/sexo")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class SexoController extends BaseController<Sexo, SexoService> {

}
