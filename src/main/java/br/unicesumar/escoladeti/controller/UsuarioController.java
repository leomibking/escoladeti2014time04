package br.unicesumar.escoladeti.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

import br.unicesumar.escoladeti.entity.Usuario;
import br.unicesumar.escoladeti.service.UsuarioService;

@Controller
@RequestMapping("/rest/usuarios")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class UsuarioController extends BaseController<Usuario, UsuarioService> {

}
