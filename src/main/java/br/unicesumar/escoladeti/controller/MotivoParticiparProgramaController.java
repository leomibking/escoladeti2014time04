package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.MotivoParticiparPrograma;
import br.unicesumar.escoladeti.service.MotivoParticiparProgramaService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/motivo")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class MotivoParticiparProgramaController extends BaseController<MotivoParticiparPrograma, MotivoParticiparProgramaService> {

}
