package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.entity.PessoaJuridica;
import br.unicesumar.escoladeti.service.PessoaJuridicaService;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/PessoaJuridica")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class PessoaJuridicaController extends BaseController<PessoaJuridica, PessoaJuridicaService> {

    // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getEmpresaListaDTO());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getEmpresaListaDTO(), valor);
        }

    }
}
