package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.dtos.EscolaListarDTO;
import br.unicesumar.escoladeti.entity.Escola;
import br.unicesumar.escoladeti.service.EscolaService;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/escolas")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class EscolaController extends BaseController<Escola, EscolaService> {

    // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getEscolaListaDTO());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getEscolaListaDTO(), valor);
        }
    }
    
    // PAGINATION
    @RequestMapping(value = {"/listar"}, method = RequestMethod.GET)
    @ResponseBody
    public List<EscolaListarDTO> ListaDTO(String nome) {
        return getService().getEscolaByName(nome);
    }

}
