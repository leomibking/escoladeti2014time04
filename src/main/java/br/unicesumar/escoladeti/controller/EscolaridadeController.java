package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Escolaridade;
import br.unicesumar.escoladeti.service.EscolaridadeService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/escolaridade")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class EscolaridadeController extends BaseController<Escolaridade, EscolaridadeService> {

}
