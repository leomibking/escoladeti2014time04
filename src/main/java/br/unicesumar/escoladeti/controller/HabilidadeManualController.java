package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.HabilidadeManual;
import br.unicesumar.escoladeti.service.HabilidadeManualService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/habilidade")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class HabilidadeManualController extends BaseController<HabilidadeManual, HabilidadeManualService> {

}
