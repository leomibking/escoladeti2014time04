package br.unicesumar.escoladeti.controller;

import java.io.Serializable;
import java.sql.Date;
import java.text.DateFormat;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

import br.unicesumar.escoladeti.commons.CustomReportBuilder;
import br.unicesumar.escoladeti.commons.Session;
import br.unicesumar.escoladeti.sql.dinamico.SelectQuery;
import br.unicesumar.escoladeti.sql.dinamico.Tabela;

@Controller
@RequestMapping("/rest/relatorio")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class RelatorioController implements Serializable {

	private static final long serialVersionUID = 1L;

	@RequestMapping(value = { "/gerar" }, method = RequestMethod.POST)
	@ResponseBody
	public String gerar(@RequestBody String aJson) {
		JSONObject json = new JSONObject(aJson);
		String sql = getSql(json);
		List<Object[]> registros = executarSql(sql);

		JSONObject entradaParaRelatorio = parsearRetornoSelect(registros,json);
		String caminho =  new CustomReportBuilder().build(
				json.getString("titulo"), entradaParaRelatorio, json.getString("tipo"));
		return "{\"url\":\""+caminho+"\"}";
	}

	private JSONObject parsearRetornoSelect(List<Object[]> registros, JSONObject aJson) {
		JSONArray colunas = new JSONArray();
		for (int i = 0; i < aJson.getJSONArray("filtros").length(); i++) {
			JSONObject filtro = aJson.getJSONArray("filtros").getJSONObject(i);
			if (!filtro.isNull("checked") && filtro.getBoolean("checked")){
				JSONObject coluna = new JSONObject();
				coluna.put("name", filtro.getString("nome"));
				JSONArray values = new JSONArray();
				coluna.put("values", values);
				colunas.put(coluna);
			}
		}

		if (registros.size() > 0 && registros.get(0) instanceof Object[]) {
			for (Object[] r : registros) {
				if (r != null) {
					for (int i = 0; i < r.length; i++) {
						String valor = trataValor(r[i]);
						JSONObject col = colunas.getJSONObject(i);						
						
						col.getJSONArray("values").put(valor);
					}
				}
			}
		} else {
			// quando só tem um campo para retornar
			for (Object valor : registros) {
				String s = trataValor(valor);
				colunas.getJSONObject(0).getJSONArray("values").put(s);
				
			}
		}
		
		JSONObject entrada = new JSONObject();
		entrada.put("columns", colunas);
		return entrada;
	}

	private String trataValor(Object aValor) {
		String resultado;
		if (aValor instanceof java.sql.Date){
			java.sql.Date data = (Date)aValor;
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			resultado = df.format(data);
		}else{
			resultado = String.valueOf(aValor);
		}
		resultado = (resultado == null || resultado.equals("null"))? "-":resultado;
		return resultado;
	}

	@SuppressWarnings("unchecked")
	private List<Object[]> executarSql(String aSql) {
		Session.begin();
		try {
			List registros = Session.list(aSql, 200);
			Session.commit();
			return registros;
		} catch (Exception e) {
			Session.rollback();
			throw new RuntimeException("Sql: "+aSql +" "+ e.getMessage());
		}
	}

	private String getSql(JSONObject aJson) {
		Tabela entidade = new Tabela(aJson.getString("nome"),
				aJson.getString("alias"));
		JSONArray filtros = aJson.getJSONArray("filtros");
		SelectQuery sql = new SelectQuery();
		sql.addtabela(entidade);
		
		for (int i = 0; i < filtros.length(); i++) {
			JSONObject f = filtros.getJSONObject(i);
			if (!f.isNull("checked") && f.getBoolean("checked")) {
				if (!f.isNull("join") && f.getBoolean("join")) {
					Tabela join = new Tabela(removeAcentos(f.getString("nome")), f.getString("alias"));
					join.addColunasAoSelect(f.getString("coluna"));
					sql.addtabela(join);
					sql.addJoin(entidade, f.getString("value"), f.getString("alias"));
				}else{
					entidade.addColunasAoSelect(f.getString("value"));
				}
			}
		}
		return sql.toString();
	}
	
	public String removeAcentos(String str) {
	  str = Normalizer.normalize(str, Normalizer.Form.NFD);
	  str = str.replaceAll("[^\\p{ASCII}]", "");
	  return str;
	 
	}
}
