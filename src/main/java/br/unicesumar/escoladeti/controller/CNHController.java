package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.CNH;
import br.unicesumar.escoladeti.service.BaseService;
import br.unicesumar.escoladeti.service.CNHService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/cnh")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class CNHController extends BaseController<CNH, CNHService> {

}
