package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Cor;
import br.unicesumar.escoladeti.service.CorService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/cor")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class CorController extends BaseController<Cor, CorService> {

}
