package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.Profissao;
import br.unicesumar.escoladeti.service.ProfissaoService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/profissoes")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class ProfissaoController extends BaseController<Profissao, ProfissaoService> {

}
