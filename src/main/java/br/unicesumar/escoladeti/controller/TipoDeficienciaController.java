package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.TipoDeficiencia;
import br.unicesumar.escoladeti.service.TipoDeficienciaService;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/tipoDeficiencia")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TipoDeficienciaController extends BaseController<TipoDeficiencia, TipoDeficienciaService> {

}
