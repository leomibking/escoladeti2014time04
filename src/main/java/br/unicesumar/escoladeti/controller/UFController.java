package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.entity.UF;
import br.unicesumar.escoladeti.service.UFService;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/ufs")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class UFController extends BaseController<UF, UFService> {

    /*
     *   Url final:
     *   http://localhost:8084/rest/ufs/porPais?id=1
     */
    @RequestMapping(value = {"/porPais"}, params = {"id"}, method = RequestMethod.GET)
    @ResponseBody
    public List<UF> porPais(@RequestParam Long id) {
        return getService().findAllByPaisId(id);
    }

}
