package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.AprendizLogDTO;
import br.unicesumar.escoladeti.entity.Log;
import br.unicesumar.escoladeti.service.LogService;
import java.text.ParseException;
import java.util.List;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;


@Controller
@RequestMapping("/rest/log")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class LogController extends BaseController<Log, LogService>{
    
     
  /*  // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex) {
        return getService().getPageList(pageIndex,
                ParserFactory.());
        //tem que ser um DTO proprio para LOG!
    }*/
    
   
    @RequestMapping(value = {"/gerarLog"}, params = {"idAprendiz"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse gerarLog(@RequestParam Long idAprendiz) throws ParseException{
        this.getService().gerarLog(idAprendiz, "");
        return new ServerResponse("");
    }
    

    @RequestMapping(value = {"/getLog"}, params = {"idAprendiz"}, method = RequestMethod.GET)
    @ResponseBody
    public List<AprendizLogDTO> getLog(@RequestParam Long idAprendiz){
       return this.getService().getLogByAprendizId(idAprendiz);
    } 
}
