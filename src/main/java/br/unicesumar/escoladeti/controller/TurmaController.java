package br.unicesumar.escoladeti.controller;

import br.unicesumar.escoladeti.commons.DataPage;
import br.unicesumar.escoladeti.commons.ParserFactory;
import br.unicesumar.escoladeti.commons.ServerResponse;
import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.dtos.TestDTO;
import br.unicesumar.escoladeti.dtos.TurmaAlunoDTO;
import br.unicesumar.escoladeti.dtos.TurmaAlunoEditDTO;
import br.unicesumar.escoladeti.dtos.TurmaAlunoShowDTO;
import br.unicesumar.escoladeti.dtos.TurmaAtividadeAlunoDTO;
import br.unicesumar.escoladeti.dtos.TurmaAtividadeEditDTO;
import br.unicesumar.escoladeti.dtos.TurmaAtividadeShowDTO;
import br.unicesumar.escoladeti.dtos.TurmaChamadaAlunoDTO;
import br.unicesumar.escoladeti.dtos.TurmaChamadaEditDTO;
import br.unicesumar.escoladeti.dtos.TurmaChamadaShowDTO;
import br.unicesumar.escoladeti.dtos.TurmaDTO;
import br.unicesumar.escoladeti.dtos.TurmaShowDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAluno;
import br.unicesumar.escoladeti.entity.TurmaAtividade;
import br.unicesumar.escoladeti.entity.TurmaAtividadeAluno;
import br.unicesumar.escoladeti.entity.TurmaChamada;
import br.unicesumar.escoladeti.entity.TurmaChamadaAluno;
import br.unicesumar.escoladeti.service.LogService;
import br.unicesumar.escoladeti.service.TurmaService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.WebApplicationContext;

@Controller
@RequestMapping("/rest/turma")
@Scope(WebApplicationContext.SCOPE_REQUEST)
public class TurmaController extends BaseController<Turma, TurmaService> {

    @Autowired
    private LogService logService;

    // PAGINATION
    @RequestMapping(value = {"/pageList"}, method = RequestMethod.GET)
    @ResponseBody
    @Override
    public <T extends BaseDTO> DataPage<T> pageList(Integer pageIndex, String valor) {
        if (valor == null || valor == "") {
            return getService().getPageList(pageIndex, ParserFactory.getTurmaParser());
        } else {
            return getService().getPageList(pageIndex, ParserFactory.getTurmaParser(), valor);
        }
    }

    //remove usa o que ja existe, via id
    @Override
    public ServerResponse remove(Long id) {
        return super.remove(id); //To change body of generated methods, choose Tools | Templates.
    }

    //nao vai usar esse salvar, por causa do dto
    @Override
    public ServerResponse save(Turma item) {
        return null;
        //return super.save(item); //To change body of generated methods, choose Tools | Templates.
    }

    //nao vai usar esse get por causa da quantidade de dados.
    @Override
    public Turma get(Long id) {
        return null;
        //return super.get(id); //To change body of generated methods, choose Tools | Templates.
    }

    //nao vai usar esse get por causa da quantidade de dados.
    @Override
    public List<Turma> query() {
        return null;
        //return super.query(); //To change body of generated methods, choose Tools | Templates.
    }

    @RequestMapping(value = {"/{id}/editar"}, method = RequestMethod.GET)
    @ResponseBody
    public TurmaDTO getDTOEdicaoTurma(@PathVariable("id") Long id) {
        Turma turmaBanco = getService().getById(id);
        TurmaDTO turmaRetorno = new TurmaDTO(turmaBanco);
        return turmaRetorno;
    }

    @RequestMapping(value = {"/{id}/show"}, method = RequestMethod.GET)
    @ResponseBody
    public TurmaShowDTO getDTOShowTurma(@PathVariable("id") Long id) {
        Turma turma = getService().getById(id);
        TurmaShowDTO t = new TurmaShowDTO(turma);
        t.setAlunos(this.getAlunosPaginados(id, 1));
        t.setAtividades(this.getAtividadesPaginados(id, 1));
        t.setChamadas(this.getChamadasPaginados(id, 1));
        return t;
    }

    @RequestMapping(value = {"/salvar"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse salvarDTOTurma(@RequestBody TurmaDTO dto) {
        Turma turma;
        if (dto.getId() == null || dto.getId() == 0) {
            turma = new Turma();
            turma.setNome(dto.getNome());
        } else {
            turma = getService().getById(dto.getId());
            turma.setNome(dto.getNome());
        }
        getService().save(turma);
        return new ServerResponse("Turma salva com sucesso", true);
    }

    /* 
    
     ALUNO
    
    
     */
    //get 1 aluno da paginação
    @RequestMapping(value = {"/{id}/aluno/{idAluno}"}, method = RequestMethod.GET)
    @ResponseBody
    public TurmaAlunoEditDTO getAlunoById(@PathVariable("id") Long id, @PathVariable("idAluno") Long idAluno) {
        TurmaAluno ta = getService().getTurmaAlunoById(idAluno);
        TurmaAlunoEditDTO t = new TurmaAlunoEditDTO(ta);
        return t;
    }

    //alunos paginados
    @RequestMapping(value = {"/{id}/aluno"}, method = RequestMethod.GET)
    @ResponseBody
    public DataPage<TurmaAlunoShowDTO> getAlunosPaginados(@PathVariable("id") Long id, Integer pageIndex) {
        return getService().getAlunosPaginados(id, pageIndex);
    }

    //salvar novo aluno
    @RequestMapping(value = {"/{id}/aluno"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse adicionarAlunos(@PathVariable("id") Long id, @RequestBody TurmaAlunoDTO aluno) {
        Aprendiz a = getService().getAlunoById(aluno.getId());
        Turma t = getService().getById(id);

        for (TurmaAluno al : t.getAlunos()) {
            if (al.getAluno().getId() == a.getId()) {
                return new ServerResponse("O Aluno " + a.getNome() + " ja foi inserido na Turma " + t.getNome());
            }
        }

        TurmaAluno ta = new TurmaAluno();

        ta.setAluno(a);
        ta.setTurma(t);
        t.getAlunos().add(ta);
        logService.gerarLog(a.getId(), "Aprendiz inserido na turma " + t.getNome());
        return getService().salvaNovoAluno(ta);
    }
    
    //salvar novo aluno
    @RequestMapping(value = {"/{id}/aluno/{idAluno}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ServerResponse removerAluno(@PathVariable("id") Long id, @PathVariable("idAluno") Long idAluno) {
        return getService().removerAluno(id, idAluno);
    }

    //pesquisa de alunos
    @RequestMapping(value = {"/aluno"}, params = {"nome"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TurmaAlunoDTO> getAlunosPorNome(@RequestParam String nome) {
        return getService().getAlunosPorNome(nome);
    }

    /*     
    
    
     CHAMADA 
        
    
     */
    //retorna as chamadas da turma paginadas
    @RequestMapping(value = {"/{id}/chamada"}, method = RequestMethod.GET)
    @ResponseBody
    public DataPage<TurmaChamadaShowDTO> getChamadasPaginados(@PathVariable("id") Long id, Integer pageIndex) {
        return getService().getChamadasPaginados(id, pageIndex);
    }

    //get em uma chamada só
    @RequestMapping(value = {"/{id}/chamada/{idChamada}"}, method = RequestMethod.GET)
    @ResponseBody
    public TurmaChamadaEditDTO getChamadaById(@PathVariable("id") Long id, @PathVariable("idChamada") Long idChamada) {
        TurmaChamada tc = getService().getChamadaById(idChamada);
        TurmaChamadaEditDTO t = new TurmaChamadaEditDTO(tc);
        return t;
    }

    //remover uma chamada
    @RequestMapping(value = {"/{id}/chamada/{idChamada}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ServerResponse removerChamada(@PathVariable("id") Long id, @PathVariable("idChamada") Long idChamada) {
        TurmaChamada tc = getService().getChamadaById(idChamada);
        return getService().removerChamada(tc);
    }

    //salva uma nova chamada para a turma
    @RequestMapping(value = {"/{id}/chamada"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse novaChamada(@PathVariable("id") Long id, @RequestBody TurmaChamadaShowDTO tcs) {
        TurmaChamada tc;

        if (tcs.getId() != null) {
            //atualizar os dados
            tc = getService().getChamadaById(tcs.getId());
            
            tc.setDataChamada(tcs.getDataChamada());
            tc.setDescricao(tcs.getDescricao());
            tc.setQtAulas(tcs.getQtAulas());

            if (tcs.getProfessor().getId() != 0) {
                tc.setProfessor(getService().getProfessorById(tcs.getProfessor().getId()));
            }

            if (tcs.getDisciplina().getId() != 0) {
                tc.setDisciplina(getService().getDisciplinaById(tcs.getDisciplina().getId()));
            }
            
            return getService().salvarChamada(tc);
        } else {
            tc = new TurmaChamada();
            Turma t = getService().getById(id);

            tc.setTurma(t);

            tc.setDataChamada(tcs.getDataChamada());
            tc.setDescricao(tcs.getDescricao());
            tc.setQtAulas(tcs.getQtAulas());

            if (tcs.getProfessor().getId() != 0) {
                tc.setProfessor(getService().getProfessorById(tcs.getProfessor().getId()));
            }

            if (tcs.getDisciplina().getId() != 0) {
                tc.setDisciplina(getService().getDisciplinaById(tcs.getDisciplina().getId()));
            }

            //insere os registros
            for (TurmaAluno tal : t.getAlunos()) {
                TurmaChamadaAluno taa = new TurmaChamadaAluno();
                taa.setPresente(true);
                taa.setAluno(tal.getAluno());
                tc.getAlunos().add(taa);
            }

            return getService().salvarChamada(tc);
        }
    }

    //salva uma edição de uma chamada
    @RequestMapping(value = {"/{id}/chamada/{idChamada}"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse editarChamada(@PathVariable("id") Long id, @PathVariable("idChamada") Long idChamada, @RequestBody TurmaChamadaEditDTO tce) {
        TurmaChamada tc;
        if (tce.getId() == null) {
            return new ServerResponse("Chamada não foi cadastrada ainda.", false);
        } else {
            tc = getService().getChamadaById(idChamada);

            //insere os registros
            for (TurmaChamadaAluno tal : tc.getAlunos()) {
                TurmaChamadaAlunoDTO aluno = tce.find(tal);

                if (aluno != null) {
                    tal.setPresente(aluno.isPresente());
                    tal.setObservacao(aluno.getObservacao());
                    if (aluno.isPresente()) {
                        logService.gerarLog(aluno.getAluno().getId(), "Aprendiz esteve presente na aula de " + tc.getDisciplina().getNome()
                                + "\n\n"
                                + "Obs: " + aluno.getObservacao());
                    } else {
                        logService.gerarLog(aluno.getAluno().getId(), "Aprendiz esteve ausente na aula de " + tc.getDisciplina().getNome()
                                + "\n"
                                + "Obs: " + aluno.getObservacao());
                    }
                }
            }
        }
        return getService().atualizarChamada(tc);
    }

    /* 
    
    
     ATIVIDADE 
    
    
     */
    //lista de atividades paginadas
    @RequestMapping(value = {"/{id}/atividade"}, method = RequestMethod.GET)
    @ResponseBody
    public DataPage<TurmaAtividadeShowDTO> getAtividadesPaginados(@PathVariable("id") Long id, Integer pageIndex) {
        return getService().getAtividadesPaginadas(id, pageIndex);
    }

    //get de uma atividade da lista
    @RequestMapping(value = {"/{id}/atividade/{idAtividade}"}, method = RequestMethod.GET)
    @ResponseBody
    public TurmaAtividadeEditDTO getAtividadeById(@PathVariable("id") Long id, @PathVariable("idAtividade") Long idAtividade) {
        TurmaAtividade ta = getService().getAtividadeById(idAtividade);
        TurmaAtividadeEditDTO t = new TurmaAtividadeEditDTO(ta);
        return t;
    }

    @RequestMapping(value = {"/{id}/atividade/{idAtividade}"}, method = RequestMethod.DELETE)
    @ResponseBody
    public ServerResponse excluirAtividade(@PathVariable("id") Long id, @PathVariable("idAtividade") Long idAtividade) {
        TurmaAtividade ta = getService().getAtividadeById(idAtividade);
        return getService().removerAtividade(ta);
    }

    //salva uma nova atividade
    @RequestMapping(value = {"/{id}/atividade"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse novaAtividade(@PathVariable("id") Long id, @RequestBody TurmaAtividadeShowDTO at) {
        TurmaAtividade ta;
        if (at.getId() != null) {
            ta = getService().getAtividadeById(at.getId());
            ta.setDataAtividade(at.getDataAtividade());
            ta.setValor(at.getValorAtividade());
            ta.setDescricaoAtividade(at.getDescricaoDaAtividade());

            if (at.getProfessor().getId() != 0) {
                ta.setProfessor(getService().getProfessorById(at.getProfessor().getId()));
            }

            if (at.getDisciplina().getId() != 0) {
                ta.setDisciplina(getService().getDisciplinaById(at.getDisciplina().getId()));
            }

            if (at.getAtividade().getId() != 0) {
                ta.setAtividade(getService().getTipoAtividadeById(at.getAtividade().getId()));
            }
            return getService().salvarAtividade(ta);
        } else {
            ta = new TurmaAtividade();
            Turma t = getService().getById(id);

            ta.setTurma(t);

            ta.setDataAtividade(at.getDataAtividade());
            ta.setValor(at.getValorAtividade());
            ta.setDescricaoAtividade(at.getDescricaoDaAtividade());

            if (at.getProfessor().getId() != 0) {
                ta.setProfessor(getService().getProfessorById(at.getProfessor().getId()));
            }

            if (at.getDisciplina().getId() != 0) {
                ta.setDisciplina(getService().getDisciplinaById(at.getDisciplina().getId()));
            }

            if (at.getAtividade().getId() != 0) {
                ta.setAtividade(getService().getTipoAtividadeById(at.getAtividade().getId()));
            }

            //insere os registros
            for (TurmaAluno tal : t.getAlunos()) {
                TurmaAtividadeAluno taa = new TurmaAtividadeAluno();
                taa.setAluno(tal.getAluno());
                ta.getAlunos().add(taa);
            }
            return getService().salvarAtividade(ta);
        }
    }

    //salva uma edição de uma atividade
    @RequestMapping(value = {"/{id}/atividade/{idAtividade}"}, method = RequestMethod.POST)
    @ResponseBody
    public ServerResponse editarAtividade(@PathVariable("id") Long id, @PathVariable("idAtividade") Long idAtividade, @RequestBody TurmaAtividadeEditDTO at) {
        TurmaAtividade ta;
        if (at.getId() == null) {
            return new ServerResponse("Atividade não foi cadastrada ainda.", false);
        } else {
            ta = getService().getAtividadeById(idAtividade);

            //insere os registros
            for (TurmaAtividadeAluno tal : ta.getAlunos()) {
                TurmaAtividadeAlunoDTO aluno = at.find(tal);

                if (aluno != null) {
                    tal.setNota(aluno.getNota());
                    tal.setObservacao(aluno.getObservacao());
                    logService.gerarLog(aluno.getId(), "Aprendiz obteve a nota " + aluno.getNota()
                            + " na atividade " + ta.getAtividade().getTipoAtividade()
                            + "\n"
                            + "Obs: " + aluno.getObservacao());
                }
            }
        }
        return getService().atualizarNotasAtividade(ta);
    }

    @RequestMapping(value = {"/semprocesso"}, method = RequestMethod.GET)
    @ResponseBody
    public List<TurmaDTO> getTurmasSemProcesso() {
        List<Turma> lista = getService().getTurmasSemProcessoSeletivo();
        List<TurmaDTO> retorno = new ArrayList<>();
        for (Turma t : lista) {
            retorno.add(new TurmaDTO(t));
        }
        return retorno;
    }

    //testes de mapeamentos
    @RequestMapping(value = {"/teste"}, method = RequestMethod.GET)
    @ResponseBody
    public TestDTO teste() {
        return getService().getTeste();
    }
    
    @RequestMapping(value = {"/faltas"}, method = RequestMethod.GET)
    @ResponseBody
    public TestDTO faltas() {
        return getService().getTeste();
    }
    
    

}
