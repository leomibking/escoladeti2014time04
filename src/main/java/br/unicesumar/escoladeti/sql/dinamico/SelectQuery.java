package br.unicesumar.escoladeti.sql.dinamico;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class SelectQuery {

    private final List<Tabela> tabelas = new ArrayList<Tabela>();
    private final List<Criteria> criterias = new ArrayList<Criteria>();
    private String orderBycolunas;
    private boolean garanteUmOrderBy = false;

    private boolean isDistinct = false;
    private String condicaoWhere;

    public void setDistinct(boolean isDistinct) {
        this.isDistinct = isDistinct;
    }

//    public void addRightOuterJoin(Tabela tabela1, String coluna1, Tabela tabela2, String coluna2) {
//        String left = getColunaWithAlias(tabela1, coluna1);
//        
//        String right = getColunaWithAlias(tabela2, coluna2) + Criteria.OUTER_JOIN;
//        
//        Criteria joinCriteria = new Criteria(left, Criteria.EQUALS, right);
//        criterias.add(joinCriteria);
//    }

    public void addJoin(Tabela tabela, String coluna, String alias) {
        String right = getColunaWithAlias(tabela, coluna);
        Criteria innerJoin = new Criteria(Criteria.LEFT_OUTER_JOIN, right + " AS " + alias);
        criterias.add(innerJoin);
    }

    public void addCriteria(Tabela tabela, String coluna, String operador, Object value) {
        String left = getColunaWithAlias(tabela, coluna);
        Criteria simpleCriteria = new Criteria(operador, value.toString());
        criterias.add(simpleCriteria);
    }

    public void addCriteria(Tabela tabela, String coluna, String operador,
            List values) {
        String left = getColunaWithAlias(tabela, coluna);
        StringBuilder right = new StringBuilder();
        ListIterator<Object> valueIterator = values.listIterator();
        right.append("(");
        while (valueIterator.hasNext()) {
            right.append(valueIterator.next().toString());
            if (valueIterator.hasNext()) {
                right.append(",");
            }
        }
        right.append(")");

        Criteria simpleCriteria
                = new Criteria(operador, right.toString());
        criterias.add(simpleCriteria);
    }

    public void addtabela(Tabela tabela) {
        tabelas.add(tabela);
    }

    public String toString() {
        StringBuilder sql = new StringBuilder();
        anexarColunaSelect(sql);
        anexarTabelas(sql);
        anexarCriterias(sql);
        anexarWhere(sql, condicaoWhere);
        anexarOrderBy(sql);
        return sql.toString();
    }

    private void anexarColunaSelect(StringBuilder sql) {
        sql.append("SELECT ");
        sql.append("DISTINCT ");
        
        ListIterator<Tabela> tabelaIterator = tabelas.listIterator();
        List<String> selectValues = new ArrayList<String>();
        while (tabelaIterator.hasNext()) {
            Tabela tabela = tabelaIterator.next();
            selectValues.addAll(tabela.getColunasComAlias());
            selectValues.addAll(tabela.getGroupFunctions());
        }
        anexarValoresSelect(sql, selectValues);
    }

    private void anexarValoresSelect(StringBuilder sql,
            List<String> selectValues) {
        ListIterator<String> selectValueIterator = selectValues.listIterator();
        while (selectValueIterator.hasNext()) {
            String selectValue = selectValueIterator.next();
            sql.append(selectValue);
            if (selectValueIterator.hasNext()) {
                sql.append(",");
            }
            sql.append(" ");
        }
    }

    private void anexarTabelas(StringBuilder sql) {
        sql.append("\n");
        sql.append("FROM ");
        ListIterator<Tabela> tabelaIterator = tabelas.listIterator();
            Tabela tabela = tabelaIterator.next();
            sql.append(tabela.getNome());
            sql.append(" ");
            sql.append("AS");
            sql.append(" ");
            sql.append(tabela.getAlias());
    }

    private void anexarCriterias(StringBuilder sql) {
        if (criterias.size() > 0) {
            sql.append(" ");
        }
        ListIterator<Criteria> criteriaIterator = criterias.listIterator();
        while (criteriaIterator.hasNext()) {
            Criteria criteria = criteriaIterator.next();
            sql.append(criteria);
            sql.append("\n");
            if (criteriaIterator.hasNext() && !criteria.toString().contains("JOIN") ) {
                sql.append(" AND ");
            }
        }
    }

    private String getColunaWithAlias(Tabela tabela1, String coluna1) {
        return tabela1.getAlias() + "." + coluna1;
    }
    
    private void anexarOrderBy(StringBuilder sql){
      sql.append(" ORDER BY 1");
    }
    
    private void anexarWhere(StringBuilder sql, String condicao){
      if(condicao != null){
        sql.append(" WHERE ");
        sql.append(condicao);
      }
    }
    
    public void addCondicaoWhere(Tabela tabela, String campoComparacao, String operador, String parametro){
      this.condicaoWhere = tabela.getAlias() + "." + campoComparacao + " " + operador + " " + parametro;
    }
}
