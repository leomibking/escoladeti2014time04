package br.unicesumar.escoladeti.sql.dinamico;

import java.util.ArrayList;
import java.util.List;

public class Tabela {

    private final String nome;
    private final String alias;
    private List<String> colunasComAlias = new ArrayList<String>();
    private List<String> groupFunctions = new ArrayList<String>();

    public Tabela(String nome, String alias) {
        this.alias = alias;
        this.nome = nome;

    }

    public String getNome() {
        return nome;
    }

    public String getAlias() {
        return alias;
    }

    public void addColunasAoSelect(String coluna) {
        colunasComAlias.add(alias + "." + coluna);
        
    }

    public List<String> getColunasComAlias() {
        return colunasComAlias;
    }

    public void addGroupFunctions(String groupFunction) {
        groupFunctions.add(groupFunction);
    }

    public List<String> getGroupFunctions() {
        return groupFunctions;
    }
}
