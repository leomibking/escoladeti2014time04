package br.unicesumar.escoladeti.commons;

import br.unicesumar.escoladeti.entity.Aprendiz;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import static org.springframework.data.jpa.domain.Specifications.where;

public class AprendizEspecificacao {

    public static Specification<Aprendiz> like(final Filter f) {
        return new Specification<Aprendiz>() {

            @Override
            public Predicate toPredicate(Root<Aprendiz> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.like(root.<String>get(f.getChave()), "%"+f.getValor()+"%");
            }
        };
    }

    public static Specification<Aprendiz> valor(final Filter f) {
        return new Specification<Aprendiz>() {
            
            /*
                {'key':'Menor que','value':'<'},
                {'key':'Menor ou Igual a','value':'<='},
                {'key':'Maior ou Igual a','value':'>='},
                {'key':'Maior que','value':'>'},
                {'key':'Igual a','value':'='},
                {'key':'Diferente de','value':'<>'}
            
            */

            @Override
            public Predicate toPredicate(Root<Aprendiz> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Long l = new Long(f.getValor());
                switch (f.getCondicao()) {
                    case "<":
                        return cb.lessThan(root.<Long>get(f.getChave()), l);
                    case "<=":
                        return cb.lessThanOrEqualTo(root.<Long>get(f.getChave()), l);
                    case ">=":
                        return cb.greaterThanOrEqualTo(root.<Long>get(f.getChave()), l);
                    case ">":
                        return cb.greaterThan(root.<Long>get(f.getChave()), l);
                    case "=":
                        return cb.equal(root.<Long>get(f.getChave()), l);
                    case "<>":
                        return cb.notEqual(root.<Long>get(f.getChave()), l);
                    default:
                        return cb.equal(root.<Long>get(f.getChave()), l);
                }
            }
        };
    }
    
    public static Specification<Aprendiz> string(final Filter f) {
        return new Specification<Aprendiz>() {
            
            /*
                {'key':'Igual a','value':'='},
                {'key':'Diferente de','value':'<>'},
                {'key':'Contem','value':'C'},
                {'key':'Não Contem','value':'NC'}
            */

            @Override
            public Predicate toPredicate(Root<Aprendiz> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                switch (f.getCondicao()) {
                    case "=":
                        return cb.equal(root.<String>get(f.getChave()), f.getValor());
                    case "<>":
                        return cb.notEqual(root.<String>get(f.getChave()), f.getValor());
                    case "C":
                        return cb.like(root.<String>get(f.getChave()), "%"+f.getValor()+"%");
                    case "NC":
                        return cb.notLike(root.<String>get(f.getChave()), "%"+f.getValor()+"%");
                    default:
                        return cb.equal(root.<String>get(f.getChave()), f.getValor());
                }
                
                //return cb.valor(root.<String>get(f.getChave()), "%" + f.getValor() + "%");
            }
        };
    }

    public static Specification<Aprendiz> start() {
        return new Specification<Aprendiz>() {

            @Override
            public Predicate toPredicate(Root<Aprendiz> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                return cb.isNull(root.get("processo"));
            }
        };
    }

    public static Specification<Aprendiz> data(final Filter f) {
        return new Specification<Aprendiz>() {
            
            /*
                {'key':'Menor ou Igual a','value':'<='},
                {'key':'Maior ou Igual a','value':'>='}
            */

            @Override
            public Predicate toPredicate(Root<Aprendiz> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
                Long valor = new Long(f.getValor());

                Calendar c = Calendar.getInstance();
                c.setTime(new Date());
                Long ano = c.get(Calendar.YEAR) - valor;
                c.set(Calendar.YEAR, ano.intValue());
                
                Date d = c.getTime();
                /*
                aqui a logica eh invertida pois
                ano atual: 2014.    
                ae a pessoa tem 10 anos, ela nasceu em 2004.
                
                se eu filtro >= 5 e <= 10
                
                ele tem q trazer pessoas que nasceram entre
                2004 e 2010
                
                entao o filtro fica:
                >= 2004 (10) e <= 2010 (5)
                
                */
                switch (f.getCondicao()) {
                    case "<=":
                        return cb.greaterThanOrEqualTo(root.<Date>get(f.getChave()), d);
                    case ">=":
                        return cb.lessThanOrEqualTo(root.<Date>get(f.getChave()), d);
                    default:
                        return cb.equal(root.<Date>get(f.getChave()), d);
                }
                
            }
        };
    }

    public static Specifications<Aprendiz> getWhere(List<Filter> f) {
        Specifications<Aprendiz> s = where(start());

        if (f != null) {
            for (Filter filtro : f) {
                if (filtro.isValido()) {
                    switch (filtro.getTipo()) {
                        case "DATA":
                            s = s.and(data(filtro));
                            break;
                        case "STRING":
                            s = s.and(string(filtro));
                            break;
                        case "VALOR":
                            s = s.and(valor(filtro));
                            break;
                        default:
                            s = s.and(like(filtro));
                            break;
                    }
                }
            }
        }
        return s;
    }

}
