package br.unicesumar.escoladeti.commons;

import java.io.Serializable;

public class ServerResponse implements Serializable {

    private String message;

    private boolean success;

    public ServerResponse(String message, boolean success) {
        this.message = message;
        this.success = success;
    }

    public ServerResponse(String message) {
        this.message = message;
        this.success = true;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
