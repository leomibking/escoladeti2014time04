package br.unicesumar.escoladeti.commons;

import static br.unicesumar.escoladeti.util.nvl.NvlUtil.nvlToOne;
import static org.springframework.data.domain.Sort.Direction.ASC;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

public class DataPage<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int MAX_ROWS = 10;
    public static final int MAX_PAGES_TO_SHOW = 5;

    private final List<T> itemList;
    private final int currentPage;
    private final int totalPage;
    private final int[] indexList;

    public static PageRequest pageRequestForAsc(Integer pageIndex, String property) {
        return pageRequestFor(pageIndex, new Sort(new Sort.Order(ASC, property)));
    }

    public static PageRequest pageRequestFor(Integer pageIndex, Sort sort) {
        return new PageRequest(nvlToOne(pageIndex) - 1, MAX_ROWS, sort);
    }

    public DataPage(Page<T> page) {
        this.itemList = page.getContent();
        this.currentPage = page.getNumber() + 1;
        this.totalPage = page.getTotalPages();
        this.indexList = generateIndexList();
    }

    public DataPage(List<T> list, int currentPage, int totalPage, int[] indexList) {
        this.itemList = list;
        this.currentPage = currentPage;
        this.totalPage = totalPage;
        this.indexList = indexList;
    }

    public DataPage(List<T> list, DataPage<T> page) {
        this.itemList = list;
        this.currentPage = page.getCurrentPage();
        this.totalPage = page.getTotalPage();
        this.indexList = page.getIndexList();
    }

    private int[] generateIndexList() {
        //ter sempre 1 pagina, mesmo com a lista vazia
        int total = Math.max(totalPage, 1);
        int maxLength = Math.min(total, MAX_PAGES_TO_SHOW);
        int distance = (int) Math.ceil(maxLength / 2);

        //calcula o excesso fim
        int excess = currentPage + distance;
        if (excess > total) {
            distance += excess - total;
        }
        //calcula o começo
        int toStart = currentPage - distance;
        toStart = Math.max(toStart, 1);

        int[] toReturn = new int[maxLength];
        for (int i = 0; i < maxLength; i++) {
            toReturn[i] = toStart++;
        }

        return toReturn;
    }

    public List<T> getItemList() {
        return itemList;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getTotalPage() {
        return totalPage;
    }

    public int[] getIndexList() {
        return indexList;
    }
}
