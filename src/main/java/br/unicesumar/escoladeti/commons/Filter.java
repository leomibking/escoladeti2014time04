package br.unicesumar.escoladeti.commons;

public class Filter {

    private String chave;
    private String condicao;
    private String valor;
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    public String getCondicao() {
        return condicao;
    }

    public void setCondicao(String condicao) {
        this.condicao = condicao;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Filter(String chave, String condicao, String valor, String tipo) {
        this.chave = chave;
        this.condicao = condicao;
        this.valor = valor;
        this.tipo = tipo;
    }

    public Filter() {
    }

    @Override
    public String toString() {
        return "Filters{" + "chave=" + chave + ", condicao=" + condicao + ", valor=" + valor + '}';
    }

    public boolean isValido() {
        return (this.getChave()    != "" && this.getChave()    != null)
            && (this.getCondicao() != "" && this.getCondicao() != null)
            && (this.getValor()    != "" && this.getValor()    != null);
    }

}
