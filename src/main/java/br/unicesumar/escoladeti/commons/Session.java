package br.unicesumar.escoladeti.commons;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.persistence.Entity;
import javax.validation.Validation;
import javax.validation.Validator;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

/**
 * Facade para o Hibernate. O objetivo desta classe é facilitar o uso do
 * hibernate e deixar o código mais simples e mais fácil de ler.
 * 
 * É necessario chamar begin antes de qualquer operação (save,delete,List,etc),
 * iniciando assim uma transação, e commit ou rollback para encerrar a
 * transação.
 * 
 * Exemplo de uso:
 * 
 * Session.Begin(); try { TspdNFCe nfce = Session.GetById(TspdNFCe.class,
 * "51130700819201004455551240000000011000000019");
 * nfce.setSituacao("AUTORIZADA"); Session.SaveOrUpdate(nfce); Session.Commit();
 * } catch(Exception e) { Session.Rollback(); }
 */
public abstract class Session {
	private static final String CLASSNAME = Session.class.getName();
	private static SessionFactory sessionFactory;
	private static ServiceRegistry serviceRegistry;

	static {
		configuraHiberate("src/main/resources/hibernate.cfg.xml");
	}

	private static void configuraHiberate(final String fileHibernate) {
		try {
			File xml = new File(fileHibernate);
			Configuration configuration = new Configuration().configure(xml);
			setMappedClassess(configuration);
			serviceRegistry = new ServiceRegistryBuilder().applySettings(
					new Configuration().configure(xml).getProperties())
					.buildServiceRegistry();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}
	}

	private static void setMappedClassess(Configuration configuration) {
		ArrayList<Class> classes = new ArrayList<Class>();

		  // the following will detect all classes that are annotated as @Entity
		  ClassPathScanningCandidateComponentProvider scanner =
		    new ClassPathScanningCandidateComponentProvider(false);
		  scanner.addIncludeFilter(new AnnotationTypeFilter(Entity.class));

		  // only register classes within "com.fooPackage" package
		  for (BeanDefinition bd : scanner.findCandidateComponents("br.unicesumar.escoladeti.entity")) {
		    String name = bd.getBeanClassName();
		    try {
		      classes.add(Class.forName(name));
		    } catch (Exception E) {
		      // TODO: handle exception - couldn't load class in question
		    }
		  } // for

		  // register detected classes with AnnotationSessionFactoryBean
		  for (Class class1 : classes) {
			configuration.addAnnotatedClass(class1);
		}
	}

	public static void begin() {
		try {
			sessionFactory.getCurrentSession().beginTransaction();
		} catch (HibernateException e) {
			throw e;
		}
	}

	public static void commit() {
		sessionFactory.getCurrentSession().getTransaction().commit();
	}

	public static void rollback() {
		sessionFactory.getCurrentSession().getTransaction().rollback();
	}

	@SuppressWarnings("rawtypes")
	public static Object getById(final Class entityClass, final String id) {
		return sessionFactory.getCurrentSession().get(entityClass, id);
	}

	public static Object get(final String query) {
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		return q.uniqueResult();
	}

	@SuppressWarnings("rawtypes")
	public static List listar(final Class entityClass) {
		Query query = sessionFactory.getCurrentSession().createQuery(
				"FROM " + entityClass.getName());
		return query.list();
	}

	@SuppressWarnings("rawtypes")
	public static List list(final String query) {
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		return q.list();
	}

	@SuppressWarnings("rawtypes")
	public static List list(final String query, final int maxResults) {
		Query q = sessionFactory.getCurrentSession().createQuery(query);
		q.setMaxResults(maxResults);
		return q.list();
	}

	public static Query query(final String query) {
		return sessionFactory.getCurrentSession().createQuery(query);
	}
}
