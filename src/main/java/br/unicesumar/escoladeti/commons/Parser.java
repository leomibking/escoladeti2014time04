package br.unicesumar.escoladeti.commons;

import br.unicesumar.escoladeti.dtos.BaseDTO;
import br.unicesumar.escoladeti.entity.BaseEntity;

public interface Parser<TEntity extends BaseEntity> {

    public <T extends BaseDTO> DataPage<T> parse(DataPage<TEntity> data);

}
