package br.unicesumar.escoladeti.commons;

import br.unicesumar.escoladeti.dtos.AprendizListaDTO;
import br.unicesumar.escoladeti.dtos.AprendizResumidoDTO;
import br.unicesumar.escoladeti.dtos.CandidatoProcessoDTO;
import br.unicesumar.escoladeti.dtos.ContratoIndexDTO;
import br.unicesumar.escoladeti.dtos.EmpresaListarDTO;
import br.unicesumar.escoladeti.dtos.EscolaListarDTO;
import br.unicesumar.escoladeti.dtos.ProcessoSeletivoIndexDTO;
import br.unicesumar.escoladeti.dtos.ProfessorListaDTO;
import br.unicesumar.escoladeti.dtos.TurmaDTO;
import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.Contrato;
import br.unicesumar.escoladeti.entity.Empresa;
import br.unicesumar.escoladeti.entity.Escola;
import br.unicesumar.escoladeti.entity.PessoaFisica;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.Vaga;
import java.util.ArrayList;
import java.util.List;

public class ParserFactory {

    public static Parser getParserGenericDTO() {
        Parser parserGeneric = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                return data;
            }
        };
        return parserGeneric;
    }

    public static Parser getCandidatoDTO() {
        Parser p = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<CandidatoProcessoDTO> ret = new ArrayList<>();
                for (Object o : data.getItemList()) {
                    Aprendiz a = (Aprendiz) o;
                    ret.add(new CandidatoProcessoDTO(a));
                }
                return new DataPage<>(ret, data);
            }
        };
        return p;
    }

    public static Parser getAprendizListaDTO() {

        Parser parserAprendiz = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<AprendizListaDTO> aprendizesSerializados = new ArrayList<>();

                for (Object o : data.getItemList()) {
                    PessoaFisica aprendiz = (PessoaFisica) o;

                    AprendizListaDTO basicAprendiz = new AprendizListaDTO();
                    basicAprendiz.setId(aprendiz.getId());
                    basicAprendiz.setDataCadastro(aprendiz.getDataCadastro());
                    basicAprendiz.setNome(aprendiz.getNome());
                    basicAprendiz.setDataNascimento(aprendiz
                            .getDataNascimento());
                    aprendizesSerializados.add(basicAprendiz);
                }

                return new DataPage<>(aprendizesSerializados, data);
            }
        };
        return parserAprendiz;
    }

    public static Parser getProfessorListaDTO() {

        Parser parserProfessor = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<ProfessorListaDTO> professoresSerializados = new ArrayList<>();

                for (Object o : data.getItemList()) {
                    PessoaFisica professor = (PessoaFisica) o;

                    ProfessorListaDTO basicProfessor = new ProfessorListaDTO();
                    basicProfessor.setId(professor.getId());
                    basicProfessor.setDataCadastro(professor.getDataCadastro());
                    basicProfessor.setNome(professor.getNome());
                    basicProfessor.setDataNascimento(professor
                            .getDataNascimento());
                    professoresSerializados.add(basicProfessor);
                }

                return new DataPage<>(professoresSerializados, data);
            }
        };
        return parserProfessor;
    }

    public static Parser getEmpresaListaDTO() {

        Parser parser = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<EmpresaListarDTO> result = new ArrayList<>();

                for (Object o : data.getItemList()) {
                    Empresa empresa = (Empresa) o;
                    EmpresaListarDTO e = new EmpresaListarDTO(empresa);
                    result.add(e);
                }
                return new DataPage<>(result, data);
            }
        };
        return parser;
    }

    public static Parser getEscolaListaDTO() {

        Parser parser = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<EscolaListarDTO> result = new ArrayList<>();

                for (Object o : data.getItemList()) {
                    Escola escola = (Escola) o;
                    EscolaListarDTO e = new EscolaListarDTO(escola);
                    result.add(e);

                }
                return new DataPage<>(result, data);
            }
        };
        return parser;
    }

    public static Parser getTurmaParser() {
        Parser parser = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<TurmaDTO> result = new ArrayList<>();

                for (Object o : data.getItemList()) {
                    Turma t = (Turma) o;
                    TurmaDTO td = new TurmaDTO(t);
                    result.add(td);
                }
                return new DataPage<>(result, data);
            }
        };
        return parser;
    }

    public static Parser getProcessoParser() {
        Parser parser = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<ProcessoSeletivoIndexDTO> result = new ArrayList<>();
                for (Object o : data.getItemList()) {
                    result.add(new ProcessoSeletivoIndexDTO((ProcessoSeletivo) o));
                }
                return new DataPage<>(result, data);
            }
        };
        return parser;
    }

    public static Parser getContratoParser() {
        Parser parser = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<ContratoIndexDTO> result = new ArrayList<>();
                for (Object o : data.getItemList()) {
                    result.add(new ContratoIndexDTO((Contrato) o));
                }
                return new DataPage<>(result, data);
            }

        };
        return parser;
    }

    public static Parser getAprendizResumidoDTO() {
        Parser parser = new Parser() {

            @Override
            public DataPage parse(DataPage data) {
                List<AprendizResumidoDTO> ret = new ArrayList<>();
                for (Object o : data.getItemList()) {
                    ret.add(new AprendizResumidoDTO((Aprendiz) o));
                }
                return new DataPage(ret, data);
            }
        };
        return parser;
    }
}
