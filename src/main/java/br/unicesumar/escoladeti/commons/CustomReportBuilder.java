package br.unicesumar.escoladeti.commons;

import static net.sf.dynamicreports.report.builder.DynamicReports.cmp;
import static net.sf.dynamicreports.report.builder.DynamicReports.col;
import static net.sf.dynamicreports.report.builder.DynamicReports.report;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import static net.sf.dynamicreports.report.builder.DynamicReports.type;

import java.awt.Color;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalAlignment;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

import org.json.JSONArray;
import org.json.JSONObject;

public class CustomReportBuilder {

	public String build(String title, JSONObject aJson, String formato) {
		checkParams(aJson);
		StyleBuilder boldStyle = createBoldStyle();
		StyleBuilder boldCenteredStyle = createBoldCenteredStyle(boldStyle);
		StyleBuilder columnTitleStyle = createColumnTitlstyle(boldCenteredStyle);
		try {
			JasperReportBuilder report = report();
			setColumnTitleStyle(report, columnTitleStyle);
			setReportColumns(report, aJson);
			setReportTitle(report, boldCenteredStyle, title);
			setReportPageFooter(report, boldCenteredStyle);
			setDataSource(report, aJson);
			return exportToPdf(report,formato);
		} catch (Exception e) {
			throw new RuntimeException("Erro ao gerar relatório: "
					+ e.getMessage());
		}
	}

	private void checkParams(JSONObject aJson) {
		JSONArray colunas = aJson.getJSONArray("columns");
		Integer[] lenghtValues = new Integer[colunas.length()];
		for (int i = 0; i < colunas.length(); i++) {
			JSONObject coluna = colunas.getJSONObject(i);
			lenghtValues[i] = coluna.getJSONArray("values").length();
		}
		int oldValue = lenghtValues[0];
		for (int i = 1; i < lenghtValues.length; i++) {
			if (!lenghtValues[i].equals(oldValue)) {
				throw new RuntimeException(
						"Os valores das colunas devem ter a mesma quantidade");
			}
			oldValue = lenghtValues[i];
		}

	}

	private String exportToPdf(JasperReportBuilder report, String formato)
			throws FileNotFoundException, DRException {
		String fileName = String.valueOf(System.currentTimeMillis());
		if (!formato.toLowerCase().equals("excel")){
			fileName = String.format("/resources/reports/%s.pdf",
					fileName);
			report.toPdf(new FileOutputStream("src/main/webapp"+fileName));
		}else{
			fileName = String.format("/resources/reports/%s.xlsx",
					fileName);
			report.toXlsx(new FileOutputStream("src/main/webapp"+fileName));
		}
		return fileName;
	}

	private void setDataSource(JasperReportBuilder report, JSONObject aJson) {
		report.setDataSource(createDataSource(aJson));
	}

	private void setReportPageFooter(JasperReportBuilder report,
			StyleBuilder boldCenteredStyle) {
		// mostra o número das paginas no rodapé
		report.pageFooter(cmp.pageXofY().setStyle(boldCenteredStyle));
	}

	private void setReportTitle(JasperReportBuilder report,
			StyleBuilder boldCenteredStyle, String title) {
		report.title(cmp.text(title).setStyle(boldCenteredStyle));
	}

	private void setReportColumns(JasperReportBuilder report, JSONObject aJson) {
		JSONArray colunas = aJson.getJSONArray("columns");
		for (int i = 0; i < colunas.length(); i++) {
			JSONObject coluna = colunas.getJSONObject(i);
			report.addColumn(col.column(coluna.getString("name"),
					coluna.getString("name"), type.stringType()));
		}
	}

	private void setColumnTitleStyle(JasperReportBuilder report,
			StyleBuilder columnTitleStyle) {
		report.setColumnTitleStyle(columnTitleStyle).highlightDetailEvenRows();
	}

	private StyleBuilder createColumnTitlstyle(StyleBuilder boldCenteredStyle) {
		StyleBuilder columnTitleStyle = stl.style(boldCenteredStyle)
				.setBorder(stl.pen1Point())
				.setBackgroundColor(Color.LIGHT_GRAY);
		return columnTitleStyle;
	}

	private StyleBuilder createBoldCenteredStyle(StyleBuilder boldStyle) {
		StyleBuilder boldCenteredStyle = stl.style(boldStyle)
				.setHorizontalAlignment(HorizontalAlignment.CENTER);
		return boldCenteredStyle;
	}

	private StyleBuilder createBoldStyle() {
		StyleBuilder boldStyle = stl.style().bold();
		return boldStyle;
	}

	private JRDataSource createDataSource(JSONObject aJson) {
		JSONArray colunas = aJson.getJSONArray("columns");
		String[] colunasDataSource = new String[colunas.length()];
		List<String[]> dataSourceValues = new ArrayList<String[]>();
		for (int i = 0; i < colunas.length(); i++) {
			JSONObject coluna = colunas.getJSONObject(i);
			colunasDataSource[i] = coluna.getString("name");
			JSONArray valores = coluna.getJSONArray("values");
			for (int j = 0; j < valores.length(); j++) {
				String[] registro;
				if (i==0){
					registro = new String[colunasDataSource.length];
					dataSourceValues.add(registro);
				}else{
					registro = dataSourceValues.get(j);
				}
				registro[i] = valores.getString(j); 
			}
		}
	    
	    DRDataSource dataSource = new DRDataSource(colunasDataSource);
	    for (String[] strings : dataSourceValues) {
	    	dataSource.add(strings);
		}

		return dataSource;
	}

}
