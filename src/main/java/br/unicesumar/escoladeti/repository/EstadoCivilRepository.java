package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.EstadoCivil;
import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadoCivilRepository extends JpaRepository<EstadoCivil, Long>{
    
}
