package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Atividade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtividadeRepository extends JpaRepository<Atividade, Long> {

    public Page<Atividade> findAllByTipoAtividadeContainingIgnoreCase(String filtro, Pageable pr);

}
