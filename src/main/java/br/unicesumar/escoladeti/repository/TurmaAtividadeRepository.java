package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAtividade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurmaAtividadeRepository extends JpaRepository<TurmaAtividade, Long> {
    
    public Page<TurmaAtividade> findAllByTurmaOrderByDataAtividadeDesc(Turma t, Pageable pg);
    

}
