package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Nacionalidade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NacionalidadeRepository extends JpaRepository<Nacionalidade, Long> {

}
