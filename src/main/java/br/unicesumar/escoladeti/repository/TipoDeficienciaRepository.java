package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.TipoDeficiencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoDeficienciaRepository extends JpaRepository<TipoDeficiencia, Long> {

}
