package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.CNH;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CNHRepository extends JpaRepository<CNH, Long>{
    
}
