package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Empresa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmpresaRepository extends JpaRepository<Empresa, Long> {

    public Page<Empresa> findAllByRazaoSocialContainingIgnoreCase(String filtro, Pageable pr);

}
