package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessoSeletivoRepository extends JpaRepository<ProcessoSeletivo, Long> {
    public Page<ProcessoSeletivo> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);
}
