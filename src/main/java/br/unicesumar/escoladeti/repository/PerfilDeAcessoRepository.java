package br.unicesumar.escoladeti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import br.unicesumar.escoladeti.entity.PerfilDeAcesso;

@Repository
public interface PerfilDeAcessoRepository extends JpaRepository<PerfilDeAcesso, Long> {

	PerfilDeAcesso findByNome(String string);

}
