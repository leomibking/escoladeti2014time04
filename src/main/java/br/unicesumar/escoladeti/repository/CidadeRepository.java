package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Cidade;
import br.unicesumar.escoladeti.entity.UF;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CidadeRepository extends JpaRepository<Cidade, Long> {

    public List<Cidade> findAllByUfOrderByNomeAsc(UF uf);

    public Page<Cidade> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);
}
