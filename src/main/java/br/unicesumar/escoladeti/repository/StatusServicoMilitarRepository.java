package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.StatusServicoMilitar;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusServicoMilitarRepository extends JpaRepository<StatusServicoMilitar, Long> {

}
