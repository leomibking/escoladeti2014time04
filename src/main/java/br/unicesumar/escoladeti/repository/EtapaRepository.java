package br.unicesumar.escoladeti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.unicesumar.escoladeti.entity.Etapa;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


@Repository
public interface EtapaRepository extends JpaRepository<Etapa, Long> {
    
    public List<Etapa> findAllByDescricaoContainingIgnoreCaseOrderByDescricaoAsc(String descricao);
    
    public Page<Etapa> findAllByDescricaoContainingIgnoreCase(String filtro, Pageable pr);
    
}
