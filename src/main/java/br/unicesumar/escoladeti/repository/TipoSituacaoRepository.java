package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.TipoSituacao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoSituacaoRepository extends JpaRepository<TipoSituacao, Long> {

    public Page<TipoSituacao> findAllByDescricaoContainingIgnoreCase(String filtro, Pageable pr);

}
