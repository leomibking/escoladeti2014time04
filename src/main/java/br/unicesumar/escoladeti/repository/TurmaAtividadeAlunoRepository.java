package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.TurmaAtividadeAluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TurmaAtividadeAlunoRepository extends JpaRepository<TurmaAtividadeAluno, Long>{
    
}
