package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.HabilidadeManual;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HabilidadeManualRepository extends JpaRepository<HabilidadeManual, Long> {
    
}
