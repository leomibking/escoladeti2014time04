package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoAprendiz;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessoSeletivoAprendizRepository extends JpaRepository<ProcessoSeletivoAprendiz, Long>{
    
    public Page<ProcessoSeletivoAprendiz> findAllByProcessoOrderByCandidatoNomeAsc(ProcessoSeletivo p, Pageable pg);
    public List<ProcessoSeletivoAprendiz> findAllByProcessoOrderByCandidatoNomeAsc(ProcessoSeletivo p);
    
    public Page<ProcessoSeletivoAprendiz> findAllByProcessoAndAprovadoTrueOrderByCandidatoNomeAsc(ProcessoSeletivo p, Pageable pg);
    public List<ProcessoSeletivoAprendiz> findAllByProcessoAndAprovadoTrueOrderByCandidatoNomeAsc(ProcessoSeletivo p);
            
    
}
