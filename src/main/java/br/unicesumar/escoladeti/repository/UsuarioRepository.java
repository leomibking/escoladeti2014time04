package br.unicesumar.escoladeti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.unicesumar.escoladeti.entity.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByLoginAndSenha(String login, String senha);

    Usuario findByLogin(String login);

    public Page<Usuario> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);

    public Page<Usuario> findAllByLoginContainingIgnoreCase(String filtro, Pageable pr);
}
