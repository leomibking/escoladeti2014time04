package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Documento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DocumentoRepository extends JpaRepository<Documento, Long>{
    
}
