package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Escolaridade;
import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EscolaridadeRepository extends JpaRepository<Escolaridade, Long> {

    public Page<Escolaridade> findAllByEscolaridadeContainingIgnoreCase(String filtro, Pageable pr);
    
}
