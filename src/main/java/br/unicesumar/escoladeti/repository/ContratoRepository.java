package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Contrato;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratoRepository extends JpaRepository<Contrato, Long> {

    public Page<Contrato> findAllByAprendizNomeContainingIgnoreCase(String filtro, Pageable pr);

}
