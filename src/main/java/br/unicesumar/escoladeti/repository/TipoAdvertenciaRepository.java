package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.TipoAdvertencia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoAdvertenciaRepository extends JpaRepository<TipoAdvertencia, Long> {

    public Page<TipoAdvertencia> findAllByDescricaoContainingIgnoreCase(String filtro, Pageable pr);

}
