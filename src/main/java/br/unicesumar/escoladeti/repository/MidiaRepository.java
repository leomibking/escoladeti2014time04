package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Midia;
import java.io.Serializable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MidiaRepository extends JpaRepository<Midia, Long>{

    public Page<Midia> findAllByMidiaContainingIgnoreCase(String filtro, Pageable pr);
    
}
