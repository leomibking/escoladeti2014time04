package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.TipoTelefone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoTelefoneRepository extends JpaRepository<TipoTelefone, Long> {

}
