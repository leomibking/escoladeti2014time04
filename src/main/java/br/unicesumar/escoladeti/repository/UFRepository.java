package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Pais;
import br.unicesumar.escoladeti.entity.UF;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UFRepository extends JpaRepository<UF, Long> {

    List<UF> findAllByPaisOrderByNomeAsc(Pais pais);

    public Page<UF> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);
}
