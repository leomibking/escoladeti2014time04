package br.unicesumar.escoladeti.repository;


import br.unicesumar.escoladeti.entity.AreaEndereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AreaEnderecoRepository extends JpaRepository<AreaEndereco, Long>{ 
    
}
