package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaChamada;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurmaChamadaRepository extends JpaRepository<TurmaChamada, Long> {

    public Page<TurmaChamada> findAllByTurmaOrderByDataChamadaDesc(Turma t,Pageable pg);
}
