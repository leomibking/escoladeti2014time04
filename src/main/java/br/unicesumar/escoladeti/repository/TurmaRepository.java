package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Turma;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TurmaRepository extends JpaRepository<Turma, Long> {
    
    @Query(value="SELECT P.NOME, TA.NOTA_AVALIACAO, T.NM_TURMA, TC.BO_PRESENTE\n" +
            " FROM TURMAATIVIDADE  AS TA\n" +
            "     ,TURMACHAMADA AS TC\n" +
            "     ,TURMA AS T\n" +
            "     ,PESSOA AS P\n" +
            "WHERE TA.ID_ALUNO = P.ID\n" +
            "  AND TA.ID_TURMA = T.ID\n" +
            "  AND TC.ID_TURMA = T.ID\n" +
            "  AND TC.ID_ALUNO = P.ID",nativeQuery = true)
    public List<Object[]> teste();

    //@Query("SELECT T FROM Turma T WHERE T.processos IS NULL")
    //public List<Turma> getTurmasSemProcessoSeletivo();

    public Page<Turma> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);

}
