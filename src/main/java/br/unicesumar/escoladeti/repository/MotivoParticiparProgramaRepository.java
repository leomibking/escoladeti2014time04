package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.MotivoParticiparPrograma;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MotivoParticiparProgramaRepository extends JpaRepository<MotivoParticiparPrograma, Long> {

    public Page<MotivoParticiparPrograma> findAllByDescricaoContainingIgnoreCase(String filtro, Pageable pr);

}
