package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapaAprendiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessoSeletivoEtapaAprendizRepository extends JpaRepository<ProcessoSeletivoEtapaAprendiz, Long>{
    
}
