package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Sexo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SexoRepository extends JpaRepository<Sexo, Long> {

}
