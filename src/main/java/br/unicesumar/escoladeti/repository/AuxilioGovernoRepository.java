package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.AuxilioGoverno;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuxilioGovernoRepository extends JpaRepository<AuxilioGoverno, Long> {

    public Page<AuxilioGoverno> findAllByDescricaoContainingIgnoreCase(String filtro, Pageable pr);

}
