package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.ProcessoSeletivoEtapa;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProcessoSeletivoEtapaRepository extends JpaRepository<ProcessoSeletivoEtapa, Long>{
    
    public Page<ProcessoSeletivoEtapa> findAllByProcessoOrderByEtapaDescricaoAsc(ProcessoSeletivo ps, Pageable pg);
    public List<ProcessoSeletivoEtapa> findAllByProcessoOrderByEtapaDescricaoAsc(ProcessoSeletivo ps);
            
}
