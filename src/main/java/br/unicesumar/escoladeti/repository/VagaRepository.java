package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.unicesumar.escoladeti.entity.Vaga;
import java.util.List;

@Repository
public interface VagaRepository extends JpaRepository<Vaga, Long> {

    public List<Vaga> findAllByEmpresaAndQuantidadeGreaterThanOrderByIdAsc(Empresa e,int qt);
}
