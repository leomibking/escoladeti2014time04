package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Aprendiz;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface AprendizRepository extends JpaRepository<Aprendiz, Long>,JpaSpecificationExecutor  {

    public List<Aprendiz> findAllByNomeContainingIgnoreCaseOrderByNomeAsc(String nome);

    public Page<Aprendiz> findAllByProcessoIsNullOrderByNomeAsc(Pageable p);

    public Page<Aprendiz> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);
}
