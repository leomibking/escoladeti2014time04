package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Aprendiz;
import br.unicesumar.escoladeti.entity.ProcessoSeletivo;
import br.unicesumar.escoladeti.entity.Turma;
import br.unicesumar.escoladeti.entity.TurmaAluno;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TurmaAlunoRepository extends JpaRepository<TurmaAluno, Long> {
    
    public Page<TurmaAluno> findAllByTurmaOrderByAlunoNomeAsc(Turma t, Pageable pg);

    public List<TurmaAluno> findAllByTurmaAndProcessoSeletivoOrderByAlunoNomeAsc(Turma t, ProcessoSeletivo p);

    public List<TurmaAluno> findAllByAlunoOrderByAlunoNomeAsc(Aprendiz a);

}
