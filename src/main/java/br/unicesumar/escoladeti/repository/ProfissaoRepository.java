package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Profissao;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfissaoRepository extends JpaRepository<Profissao, Long> {

    public Page<Profissao> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);

}
