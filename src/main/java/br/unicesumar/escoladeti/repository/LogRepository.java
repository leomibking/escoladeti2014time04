package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Log;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogRepository extends JpaRepository<Log, Long> {

    public List<Log> findAllByAprendizIdOrderByIdAsc(Long idAprendiz);

    public Page<Log> findAllByAprendizNomeContainingIgnoreCase(String filtro, Pageable pr);
}
