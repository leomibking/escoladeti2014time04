package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Professor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, Long> {

    public Page<Professor> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);

}
