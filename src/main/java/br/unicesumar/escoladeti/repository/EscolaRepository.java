package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Escola;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EscolaRepository extends JpaRepository<Escola, Long> {

    public Page<Escola> findAllByRazaoSocialContainingIgnoreCase(String filtro, Pageable pr);

    public List<Escola> findAllByRazaoSocialContainingIgnoreCase(String nome);

}
