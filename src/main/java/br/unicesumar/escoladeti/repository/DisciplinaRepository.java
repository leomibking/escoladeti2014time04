package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.Disciplina;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DisciplinaRepository extends JpaRepository<Disciplina, Long> {
    
    public List<Disciplina> findAllByNomeContainingOrderByNomeAsc(String nome);
    
    
    //@Query("SELECT d FROM Disciplina d WHERE UPPER(d.nome) LIKE UPPER(% :nome %)")
    //public List<Disciplina> findByNome(@Param("nome") String nome);

    public Page<Disciplina> findAllByNomeContainingIgnoreCase(String filtro, Pageable pr);

}
