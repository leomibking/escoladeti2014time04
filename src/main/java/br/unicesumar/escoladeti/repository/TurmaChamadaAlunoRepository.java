package br.unicesumar.escoladeti.repository;

import br.unicesumar.escoladeti.entity.TurmaChamadaAluno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TurmaChamadaAlunoRepository extends JpaRepository<TurmaChamadaAluno, Long>{
    
}
