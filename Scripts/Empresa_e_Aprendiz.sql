﻿---------INICIO EMPRESA-------------------
INSERT INTO pessoa VALUES(1,'07/07/2014','DB1');
INSERT INTO telefone VALUES(1, '44', '3033-6300', 'COMERCIAL');
insert into pessoa_telefone values (1,1);
INSERT INTO endereco VALUES(1, 'URBANA', 'ZONA 07', '87020-025','','Av. Duque de Caxias', '882', 6190);
INSERT INTO pessoa_endereco VALUES(1,1);
INSERT INTO email VALUES(1, 'comercial@db1.com.br');
insert into pessoa_email values (1,1);
INSERT INTO empresa VALUES('04204018000166', 'DB1 Informática LTDA', 1);

INSERT INTO pessoa VALUES(2,'08/07/2014','Accion');
INSERT INTO telefone VALUES(2, '44', '3225-8686', 'COMERCIAL');
insert into pessoa_telefone values (2,2);
INSERT INTO endereco VALUES(2, 'URBANA', 'ZONA 07', '87020-110','','Rua Móoca', '221', 6190);
INSERT INTO pessoa_endereco VALUES(2,2);
INSERT INTO email VALUES(2, 'accion@accion.com.br');
insert into pessoa_email values (2,2);
INSERT INTO empresa VALUES('03644084000194', 'Accion Ltda - ME', 2);

INSERT INTO pessoa VALUES(3,'08/07/2014','B5S Tecnologia');
INSERT INTO telefone VALUES(3, '44', '3221-3349', 'COMERCIAL');
insert into pessoa_telefone values (3,3);
INSERT INTO endereco VALUES(3, 'URBANA', 'CENTRO', '87020-025','Sala 610','Av. Duque de Caxias', '882', 6190);
INSERT INTO pessoa_endereco VALUES(3,3);
INSERT INTO email VALUES(3, 'b5s@b5s.com.br');
insert into pessoa_email values (3,3);
INSERT INTO empresa VALUES('08469490000181', 'B5S Consultoria e Desenvolvimento de Software Ltda', 3);

INSERT INTO pessoa VALUES(4,'08/07/2014','PC Help Sistemas');
INSERT INTO telefone VALUES(4, '44', '3028-2922', 'COMERCIAL');
insert into pessoa_telefone values (4,4);
INSERT INTO endereco VALUES(4, 'URBANA', 'CENTRO', '87020-025','Sala 608','Av. Duque de Caxias', '882', 6190);
INSERT INTO pessoa_endereco VALUES(4,4);
INSERT INTO email VALUES(4, 'rh@pchelpsistemas.com.br');
insert into pessoa_email values (4,4);
INSERT INTO empresa VALUES('00526351000130', 'Pc Help Informatica LTDA - ME', 4);

INSERT INTO pessoa VALUES(5,'08/07/2014','Benner Solution');
INSERT INTO telefone VALUES(5, '44', '2101-0800', 'COMERCIAL');
insert into pessoa_telefone values (5,5);
INSERT INTO endereco VALUES(5, 'URBANA', '', '87080-590','','Av. Alziro Zarur', '73', 6190);
INSERT INTO pessoa_endereco VALUES(5,5);
INSERT INTO email VALUES(5, 'rh@benner.com.br');
insert into pessoa_email values (5,5);
INSERT INTO empresa VALUES('02325352001120', 'Benner Sistemas S/A', 5);

INSERT INTO pessoa VALUES(6,'08/07/2014','Cwork');
INSERT INTO telefone VALUES(6, '44', '3031-5351', 'COMERCIAL');
insert into pessoa_telefone values (6,6);
INSERT INTO endereco VALUES(6, 'URBANA', 'ZONA 01', '87013-040','Salas 1411-1413','Av. São Paulo', '172', 6190);
INSERT INTO pessoa_endereco VALUES(6,6);
INSERT INTO email VALUES(6, 'suporte@cwork.com.br');
insert into pessoa_email values (6,6);
INSERT INTO empresa VALUES('01425412404410', 'Cwork Sistemas Ltda - ME', 6);

INSERT INTO pessoa VALUES(7,'08/07/2014','ID Brasil Sistemas');
INSERT INTO telefone VALUES(7, '44', '3029-4327', 'COMERCIAL');
insert into pessoa_telefone values (7,7);
INSERT INTO endereco VALUES(7, 'URBANA', 'CENTRO', '87020-025','','Av. Duque de Caxias', '882', 6190);
INSERT INTO pessoa_endereco VALUES(7,7);
INSERT INTO email VALUES(7, 'contato@idbrasil.com.br');
insert into pessoa_email values (7,7);
INSERT INTO empresa VALUES('00321517414511', 'Id Brasil Sistemas Ltda - ME', 7);

INSERT INTO pessoa VALUES(8,'08/07/2014','Getcard');
INSERT INTO telefone VALUES(8, '44', '3023-0062', 'COMERCIAL');
insert into pessoa_telefone values (8,8);
INSERT INTO endereco VALUES(8, 'URBANA', 'CENTRO', '82010-010','','R. Inocente', '310', 6190);
INSERT INTO pessoa_endereco VALUES(8,8);
INSERT INTO email VALUES(8, 'contato@getcard.com.br');
insert into pessoa_email values (8,8);
INSERT INTO empresa VALUES('05133521132530', 'Getcard Provedora de Transacoes Eletronicas Ltda - ME', 8);

INSERT INTO pessoa VALUES(9,'08/07/2014','Produtec');
INSERT INTO telefone VALUES(9, '44', '3033-3000', 'COMERCIAL');
insert into pessoa_telefone values (9,9);
INSERT INTO endereco VALUES(9, 'URBANA', 'ZONA 03', '87050-130','','Av. Paissandú', '718', 6190);
INSERT INTO pessoa_endereco VALUES(9,9);
INSERT INTO email VALUES(9, 'contato@produtec.com.br');
insert into pessoa_email values (9,9);
INSERT INTO empresa VALUES('02114577813631', 'Produtec Informatica Ltda - Epp', 9);

INSERT INTO pessoa VALUES(10,'08/07/2014','SG Sistemas');
INSERT INTO telefone VALUES(10, '44', '3026-2666', 'COMERCIAL');
insert into pessoa_telefone values (10,10);
INSERT INTO endereco VALUES(10, 'URBANA', 'ZONA 07', '87030-201','','R. São João', '1759', 6190);
INSERT INTO pessoa_endereco VALUES(10,10);
INSERT INTO email VALUES(10, 'contato@sgsis.com.br');
insert into pessoa_email values (10,10);
INSERT INTO empresa VALUES('80345311462300', 'Sg-Sistemas de Automacao Ltda', 10);

INSERT INTO pessoa VALUES(11,'08/07/2014','Sergio Yamada');
INSERT INTO telefone VALUES(11, '44', '3304-1110', 'COMERCIAL');
insert into pessoa_telefone values (11,11);
INSERT INTO endereco VALUES(11, 'URBANA', 'CENTRO', '87020-035','','Av. Racannello', '5550', 6190);
INSERT INTO pessoa_endereco VALUES(11,11);
INSERT INTO email VALUES(11, 'contato@symada.com.br');
insert into pessoa_email values (11,11);
INSERT INTO empresa VALUES('00751237112383', 'Yamada Educacao Tecnologica Ltda - ME', 11);

INSERT INTO pessoa VALUES(12,'08/07/2014','Tecnospeed');
INSERT INTO telefone VALUES(12, '44', '3028-3749', 'COMERCIAL');
insert into pessoa_telefone values (12,12);
INSERT INTO endereco VALUES(12, 'URBANA', 'CENTRO', '87020-025','','Av. Duque de Caxias', '882', 6190);
INSERT INTO pessoa_endereco VALUES(12,12);
INSERT INTO email VALUES(12, 'comercial@tecnospeed.com.br');
insert into pessoa_email values (12,12);
INSERT INTO empresa VALUES('08187114561570', 'Tecnospeed Tecnologia de Informacao Ltda', 12);
---------FIM EMPRESA-------------------

---------INICIO APRENDIZ-------------------
INSERT INTO pessoa VALUES(13,'07/02/2014','Eduardo Trintin');
INSERT INTO telefone VALUES(13, '44', '3028-3842', 'RESIDENCIAL');
insert into pessoa_telefone values (13,13);
INSERT INTO endereco VALUES(13, 'URBANA', 'Jardim Monte Carlo', '87080-300','','R. Das Dalias', '394', 6190);
INSERT INTO pessoa_endereco VALUES(13,13);
INSERT INTO email VALUES(13, 'duh.trintin@gmail.com');
insert into pessoa_email values (13,13);
INSERT INTO aprendiz VALUES('07890821956','18/04/1995',null,'98543848', 'SSP-PR', 'MASCULINO',13);

INSERT INTO pessoa VALUES(14,'11/03/2014','Guilherne Yamakawa');
INSERT INTO telefone VALUES(14, '44', '3033-2345', 'RESIDENCIAL');
insert into pessoa_telefone values (14,14);
INSERT INTO endereco VALUES(14, 'URBANA', 'ZONA 07', '87040-050','','R. Oliveira Silva', '311', 6190);
INSERT INTO pessoa_endereco VALUES(14,14);
INSERT INTO email VALUES(14, 'gguiyo@gmail.com');
insert into pessoa_email values (14,14);
INSERT INTO aprendiz VALUES('08834125678','20/05/1996',null,'73422318', 'SSP-PR', 'MASCULINO',14);

INSERT INTO pessoa VALUES(15,'12/03/2014','Leonardo Galdioli');
INSERT INTO telefone VALUES(15, '44', '3035-2345', 'RESIDENCIAL');
insert into pessoa_telefone values (15,15);
INSERT INTO endereco VALUES(15, 'URBANA', 'ZONA 07', '85030-100','','R. Minas Gerais', '233', 6190);
INSERT INTO pessoa_endereco VALUES(15,15);
INSERT INTO email VALUES(15, 'leogaldioli@hotmail.com');
insert into pessoa_email values (15,15);
INSERT INTO aprendiz VALUES('07721323421','20/10/1998',null,'93451237', 'SSP-PR', 'MASCULINO',15);

INSERT INTO pessoa VALUES(16,'15/03/2014','Leonardo Da Silva');
INSERT INTO telefone VALUES(16, '44', '9108-1108', 'CELULAR');
insert into pessoa_telefone values (16,16);
INSERT INTO endereco VALUES(16, 'URBANA', 'ZONA 05', '88020-200','','R. Pe. Raimundo', '456', 6190);
INSERT INTO pessoa_endereco VALUES(16,16);
INSERT INTO email VALUES(16, 'leomibking@gmail.com');
insert into pessoa_email values (16,16);
INSERT INTO aprendiz VALUES('07898721357','21/10/1996',null,'94511218', 'SSP-PR', 'MASCULINO',16);

INSERT INTO pessoa VALUES(17,'17/03/2014','Priscila Carvalho');
INSERT INTO telefone VALUES(17, '44', '9856-9459', 'CELULAR');
insert into pessoa_telefone values (17,17);
INSERT INTO endereco VALUES(17, 'URBANA', 'ZONA 05', '89050-150','','R. Silveira Neves', '565', 6190);
INSERT INTO pessoa_endereco VALUES(17,17);
INSERT INTO email VALUES(17, 'priscila_silvacarvalho@hotmail.com');
insert into pessoa_email values (17,17);
INSERT INTO aprendiz VALUES('07720934245','18/08/1999',null,'97212767', 'SSP-PR', 'FEMININO',17);

INSERT INTO pessoa VALUES(18,'21/03/2014','Marcelo Kanzaki');
INSERT INTO telefone VALUES(18, '44', '9963-9071', 'CELULAR');
insert into pessoa_telefone values (18,18);
INSERT INTO endereco VALUES(18, 'URBANA', 'ZONA 05', '89311-230','','R. Pe. Le Goff', '34', 6190);
INSERT INTO pessoa_endereco VALUES(18,18);
INSERT INTO email VALUES(18, 'marcelojnk@hotmail.com');
insert into pessoa_email values (18,18);
INSERT INTO aprendiz VALUES('03423467523','27/04/1996',null,'62132118', 'SSP-PR', 'MASCULINO',18);

INSERT INTO pessoa VALUES(19,'27/03/2014','Fernando Menolli');
INSERT INTO telefone VALUES(19, '44', '9930-7866', 'CELULAR');
insert into pessoa_telefone values (19,19);
INSERT INTO endereco VALUES(19, 'URBANA', 'ZONA 05', '86100-200','','R. Gerson', '111', 6190);
INSERT INTO pessoa_endereco VALUES(19,19);
INSERT INTO email VALUES(19, 'femenolli@hotmail.com');
insert into pessoa_email values (19,19);
INSERT INTO aprendiz VALUES('08921145330','23/10/1998',null,'82214549', 'SSP-PR', 'MASCULINO',19);

INSERT INTO pessoa VALUES(20,'30/03/2014','Felipe Bergamaschi');
INSERT INTO telefone VALUES(20, '44', '3034-7071', 'RESIDENCIAL');
insert into pessoa_telefone values (20,20);
INSERT INTO endereco VALUES(20, 'URBANA', 'ZONA 05', '85030-340','','R. Dolores', '2342', 6190);
INSERT INTO pessoa_endereco VALUES(20,20);
INSERT INTO email VALUES(20, 'febergamaschi@hotmail.com');
insert into pessoa_email values (20,20);
INSERT INTO aprendiz VALUES('09889031245','08/08/1997',null,'95467891', 'SSP-PR', 'MASCULINO',20);

INSERT INTO pessoa VALUES(21,'01/04/2014','Alexandre Hubner');
INSERT INTO telefone VALUES(21, '44', '3045-4312', 'RESIDENCIAL');
insert into pessoa_telefone values (21,21);
INSERT INTO endereco VALUES(21, 'URBANA', 'ZONA 05', '89050-150','','R. David Faustino', '133', 6190);
INSERT INTO pessoa_endereco VALUES(21,21);
INSERT INTO email VALUES(21, 'alexandre.hubner@hotmail.com');
insert into pessoa_email values (21,21);
INSERT INTO aprendiz VALUES('08023165478','17/01/1998',null,'75431319', 'SSP-PR', 'MASCULINO',21);

INSERT INTO pessoa VALUES(22,'05/04/2014','Julio Gabriel');
INSERT INTO telefone VALUES(22, '43', '8842-8361', 'CELULAR');
insert into pessoa_telefone values (22,22);
INSERT INTO endereco VALUES(22, 'URBANA', 'ZONA 05', '87050-690','','R. Cedro', '36', 6190);
INSERT INTO pessoa_endereco VALUES(22,22);
INSERT INTO email VALUES(22, 'juliogabriel@hotmail.com');
insert into pessoa_email values (22,22);
INSERT INTO aprendiz VALUES('07921856455','30/08/1998',null,'94561215', 'SSP-PR', 'MASCULINO',22);


INSERT INTO pessoa VALUES(23,'07/04/2014','Thiago Toshihiko');
INSERT INTO telefone VALUES(23, '43', '8811-1261', 'CELULAR');
insert into pessoa_telefone values (23,23);
INSERT INTO endereco VALUES(23, 'URBANA', 'ZONA 01', '88090-100','','R. Marialva', '211', 6183);
INSERT INTO pessoa_endereco VALUES(23,23);
INSERT INTO email VALUES(23, 'toshioizume@hotmail.com');
insert into pessoa_email values (23,23);
INSERT INTO aprendiz VALUES('04511121376','30/08/1999',null,'94561215', 'SSP-PR', 'MASCULINO',23);

INSERT INTO pessoa VALUES(24,'10/04/2014','Cristiano Felipe');
INSERT INTO telefone VALUES(24, '44', '3390-1232', 'RESIDENCIAL');
insert into pessoa_telefone values (24,24);
INSERT INTO endereco VALUES(24, 'URBANA', 'ZONA 05', '88010-100','','R. São Jorge', '321', 6190);
INSERT INTO pessoa_endereco VALUES(24,24);
INSERT INTO email VALUES(24, 'cfelipe@hotmail.com');
insert into pessoa_email values (24,24);
INSERT INTO aprendiz VALUES('06745421855','30/04/1999',null,'94536758', 'SSP-PR', 'MASCULINO',24);

INSERT INTO pessoa VALUES(25,'15/05/2014','Arthur Zavadski');
INSERT INTO telefone VALUES(25, '44', '3300-3421', 'RESIDENCIAL');
insert into pessoa_telefone values (25,25);
INSERT INTO endereco VALUES(25, 'URBANA', 'ZONA 05', '85011-150','','Av. Guedner', '1231', 6190);
INSERT INTO pessoa_endereco VALUES(25,25);
INSERT INTO email VALUES(25, 'azavadski@hotmail.com');
insert into pessoa_email values (25,25);
INSERT INTO aprendiz VALUES('07721121856','21/06/1997',null,'73421217', 'SSP-PR', 'MASCULINO',25);
---------FIM APRENDIZ-------------------