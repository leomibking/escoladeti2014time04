---------INICIO PROFISSAO-------------------
INSERT INTO Profissao (id, nome) values (1, 'Administrador');
INSERT INTO Profissao (id, nome) values (2, 'Advogado');
INSERT INTO Profissao (id, nome) values (3, 'Arquiteto');
INSERT INTO Profissao (id, nome) values (4, 'Agrônomo');
INSERT INTO Profissao (id, nome) values (5, 'Bibliotecário');
INSERT INTO Profissao (id, nome) values (6, 'Biomédico');
INSERT INTO Profissao (id, nome) values (7, 'Biólogo');
INSERT INTO Profissao (id, nome) values (8, 'Bombeiro Civil');
INSERT INTO Profissao (id, nome) values (9, 'Corretor de Imóveis');
INSERT INTO Profissao (id, nome) values (10, 'Corretor de Seguros');
INSERT INTO Profissao (id, nome) values (11, 'Engenheiro');
INSERT INTO Profissao (id, nome) values (12, 'Economista');
INSERT INTO Profissao (id, nome) values (13, 'Farmacêutico');
INSERT INTO Profissao (id, nome) values (14, 'Jornalista');
INSERT INTO Profissao (id, nome) values (15, 'Médico(a)');
INSERT INTO Profissao (id, nome) values (16, 'Massagista');
INSERT INTO Profissao (id, nome) values (17, 'Veterinário(a)');
INSERT INTO Profissao (id, nome) values (18, 'Músico');
INSERT INTO Profissao (id, nome) values (19, 'Nutricionista');
INSERT INTO Profissao (id, nome) values (20, 'Dentista');
INSERT INTO Profissao (id, nome) values (21, 'Químico');
INSERT INTO Profissao (id, nome) values (22, 'Publicitário');
INSERT INTO Profissao (id, nome) values (23, 'Fisioterapeuta');
INSERT INTO Profissao (id, nome) values (24, 'Sociólogo');
INSERT INTO Profissao (id, nome) values (25, 'Desenvolvedor de software');
INSERT INTO Profissao (id, nome) values (26, 'Desenvolvedor de Web');
INSERT INTO Profissao (id, nome) values (27, 'Analista de Sistemas');
INSERT INTO Profissao (id, nome) values (28, 'Programador');
INSERT INTO Profissao (id, nome) values (29, 'Gerente de TI');
INSERT INTO Profissao (id, nome) values (30, 'Administrador de Banco de Dados');

update Profissao set nome = upper(nome)

---------FIM PROFISSAO-------------------


---------INICIO Auxilo do Governo-------------------
INSERT INTO auxiliogoverno (id, descricao) values (1, 'Bolsa Familia');
INSERT INTO auxiliogoverno (id, descricao) values (2, 'Vale Gás');
INSERT INTO auxiliogoverno (id, descricao) values (3, 'Auxílio Acidente');
INSERT INTO auxiliogoverno (id, descricao) values (4, 'Auxílio Reclusão');
INSERT INTO auxiliogoverno (id, descricao) values (5, 'Bolsa Moradia');
INSERT INTO auxiliogoverno (id, descricao) values (6, 'Bolsa Transporte');
INSERT INTO auxiliogoverno (id, descricao) values (7, 'Bolsa Alimentação');
INSERT INTO auxiliogoverno (id, descricao) values (8, 'Bolsa Escola');
INSERT INTO auxiliogoverno (id, descricao) values (9, 'Bolsa Cidadão');


---------FIM Auxilo do Governo-------------------