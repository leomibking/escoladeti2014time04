﻿drop table if exists email cascade;
drop table if exists endereco cascade;
drop table if exists itemacesso cascade;
drop table if exists pais cascade;
drop table if exists perfilacesso cascade;
drop table if exists perfilacesso_itemacesso cascade;
drop table if exists pessoa cascade;
drop table if exists pessoafisica cascade;
drop table if exists pessoajuridica cascade;
drop table if exists processoseletivo cascade;
drop table if exists telefone cascade;
drop table if exists uf cascade;
drop table if exists databasechangeloglock cascade;
drop table if exists cidade cascade;
drop table if exists contato cascade;
drop table if exists databasechangelog cascade;
drop table if exists pessoa_endereco cascade;
drop table if exists contato_telefone cascade;
drop table if exists pessoa_email cascade;
drop table if exists pessoa_telefone cascade;

drop table if exists aprendiz cascade;
drop table if exists auxiliogoverno cascade;
drop table if exists distrito cascade;
drop table if exists empresa cascade;
drop table if exists empresa_profissao cascade;
drop table if exists empresa_vaga cascade;
drop table if exists escola cascade;
drop table if exists escola_telefone cascade;
drop table if exists profissao cascade;
drop table if exists vaga cascade;


drop table if exists alternativa cascade;
drop table if exists disciplina cascade;
drop table if exists questao cascade;
drop table if exists questaodisciplina cascade;
drop table if exists questaoobjetiva cascade;
drop table if exists usuario cascade;


drop table if exists alunoturma cascade;
drop table if exists itemdeacesso cascade;
drop table if exists perfildeacesso cascade;
drop table if exists perfildeacesso_itemdeacesso cascade;
drop table if exists turma cascade;
drop table if exists turma_alunoturma cascade;
drop table if exists usuario_itemdeacesso cascade;
drop table if exists usuario_perfildeacesso cascade;

drop table if exists listadechamada cascade;
drop table if exists turmaaluno cascade;
drop table if exists turmaavaliacao cascade;
drop table if exists turmachamada cascade;
drop table if exists turmalistadechamada cascade;

drop table if exists atividade cascade;
drop table if exists pessoafisica_telefone cascade;
drop table if exists professor cascade;



drop sequence if exists hibernate_sequence cascade;